#include <omniORB4/CORBA.h>
#include <cstdio>
#include "test.h"
#include "test_i.h"

using namespace Calculator;

int main(int argc, char** argv)
{
	try {
	    // Initialise the ORB.
	    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
	    CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
	    PortableServer::POA_var poa = PortableServer::POA::_narrow(obj);

		//create the object
	    CalcPrx* myCalculator_Calc_i = new CalcPrx();
	
		// Activate the object
    	PortableServer::ObjectId_var myCalculator_Calc_iid = poa->activate_object(myCalculator_Calc_i);
	
		obj = myCalculator_Calc_i->_this();
		CORBA::String_var sior(orb->object_to_string(obj));
		printf("%s\n", (char*)sior);

		//activate the portable server
	    PortableServer::POAManager_var pman = poa->the_POAManager();
	    pman->activate();

	    orb->run();
		orb->destroy();
	  }
	  catch(CORBA::TRANSIENT&) {
		printf("error1\n");
	  }
	  catch(CORBA::SystemException& ex) {
		printf("error2: %s\n", ex._name());
	  }
	  catch(CORBA::Exception& ex) {
		printf("error3: %s\n", ex._name());
	  }
	  catch(omniORB::fatalException& fe) {
		printf("error4: %s\n", fe.errmsg());
	  }
	  return 0;
}
