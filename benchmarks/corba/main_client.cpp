#include <omniORB4/CORBA.h>
#include <cstdio>
#include <ctime>
#include <vector>
#include <test.h>

#define MAX_ITER 1000

using namespace std;
using namespace Calculator;

int main(int argc, char* argv[])
{
	//some auxiliary variables
	int result;
	
    try {
		//initialize the middleware
	    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
	    CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
		
		//create max_iter proxies
		std::vector<Calc_var> test_objs;

		for (int i=0; i<MAX_ITER; i++)
		{
		    CORBA::Object_var obj = orb->string_to_object(argv[1]);
		    Calc_var test = Calc::_narrow(obj);
			test_objs.push_back(test);
		}

		test_objs.clear();
		
		//work with an specific object
	    CORBA::Object_var obj2 = orb->string_to_object(argv[1]);
	    Calc_var calc = Calc::_narrow(obj2);
	    if( CORBA::is_nil(calc) ) {
	      return 1;
	    }
		
		//add i and i+1 from 0 to MAX_ITER
		for (int i=0; i<MAX_ITER; i++)
		{
			result = calc->add(i, i+1);
			printf("Result: %d + %d = %d\n", i, i+1, result);
		}
		
		//print a message at the server
		for (int i=0; i<MAX_ITER; i++)
		{
			calc->print("hello world ñññáéíóù!!!");
		}
		
		//shutdown CORBA
		orb->destroy();
    }
	catch(CORBA::TRANSIENT&) {
	  }
	catch(CORBA::SystemException& ex) {
	  }
	catch(CORBA::Exception& ex) {
	  }
	catch(omniORB::fatalException& fe) {
	  }

    return 0;
}


