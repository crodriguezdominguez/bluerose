#include <iostream>
#include <test.h>

class CalcPrx: public POA_Calculator::Calc {
private:
public:
  CalcPrx();
  virtual ~CalcPrx();

  ::CORBA::Long add(::CORBA::Long x, ::CORBA::Long y);
  void print(const char* str);

};
