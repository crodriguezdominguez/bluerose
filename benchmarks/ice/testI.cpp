
#include <testI.h>
#include <cstdio>

::Ice::Int
Calculator::CalcI::add(::Ice::Int x,
                       ::Ice::Int y,
                       const Ice::Current& current)
{
    return x+y;
}

void
Calculator::CalcI::print(const ::std::string& str,
                         const Ice::Current& current)
{
	printf("%s\n", str.c_str());
}
