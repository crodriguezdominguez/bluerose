#include <cstdio>
#include <ctime>
#include <Ice/Ice.h>
#include <test.h>

#define MAX_ITER 1000

using namespace Calculator;

int main(int argc, char* argv[])
{
	//some auxiliary variables
	int result;

    Ice::CommunicatorPtr ic;
    try {
		//initialize the middleware
        ic = Ice::initialize(argc, argv);
		
		//create max_iter proxies
		std::vector<CalcPrx> test_objs;

		for (int i=0; i<MAX_ITER; i++)
		{
			Ice::ObjectPrx base = ic->stringToProxy("Calc:default -p 10000");
			CalcPrx test = CalcPrx::checkedCast(base);
			test_objs.push_back(test);
		}

		test_objs.clear();
		
		//work with an specific object
		Ice::ObjectPrx base = ic->stringToProxy("Calc:default -p 10000");
		CalcPrx printer = CalcPrx::checkedCast(base);
		
        if (!printer)
            throw "Invalid proxy";
		
		//add i and i+1 from 0 to MAX_ITER
		for (int i=0; i<MAX_ITER; i++)
		{
			result = printer->add(i, i+1);
			printf("Result: %d + %d = %d\n", i, i+1, result);
		}
		
		//print a message at the server
		for (int i=0; i<MAX_ITER; i++)
		{
			printer->print("hello world ñññáéíóù!!!");
		}
    } catch (const Ice::Exception& ex) {
    } catch (const char* msg) {
    }

	//shutdown ICE
    if (ic)
        ic->destroy();

    return 0;
}


