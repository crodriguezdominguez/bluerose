#ifndef __testI_h__
#define __testI_h__

#include <test.h>

namespace Calculator
{

class CalcI : virtual public Calc
{
public:

    virtual ::Ice::Int add(::Ice::Int,
                           ::Ice::Int,
                           const Ice::Current&);

    virtual void print(const ::std::string&,
                       const Ice::Current&);
};

}

#endif
