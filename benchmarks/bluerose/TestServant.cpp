#include "TestServant.h"

namespace Test {

int TestServant::add(int x, int y)
{
	return x+y;
}

void TestServant::print(const std::wstring& str)
{
	printf("%ls\n", str.c_str());
}

};
