/**
* YOU SHOULD NOT MODIFY THIS FILE
*/

#ifndef _BLUEROSE_TEST_CALC_PROXY_H_
#define _BLUEROSE_TEST_CALC_PROXY_H_

#include "BlueRose.h"

namespace Test {

class TestProxy : public BlueRose::ObjectProxy
{
public:
	TestProxy(BlueRose::ICommunicationDevice* _iface = NULL,
				bool waitOnline = true,
				const BlueRose::DictionaryDataType& connParameters = BlueRose::DictionaryDataType())
	: BlueRose::ObjectProxy()
	{
		identity.name = "TestServant";
		identity.category = "Test";
		
		resolveInitialization(_iface, waitOnline, connParameters);
	}
	
	TestProxy(const std::string& _servantID,
		BlueRose::ICommunicationDevice* _iface = NULL,
		bool waitOnline = true,
		const BlueRose::DictionaryDataType& connParameters = BlueRose::DictionaryDataType())
	: BlueRose::ObjectProxy(_servantID, _iface, waitOnline, connParameters)
	{
		identity.name = "TestServant";
		identity.category = "Test";
	}
	
	~TestProxy()
	{
		
	}
	
	std::string getTypeID() const
	{
		return "Test::TestServant";
	}
	
	int add(int x, int y)
	{
		int reqId;
		BlueRose::ByteStreamWriter writer;
		BlueRose::ByteStreamReader reader;
		currentMode = BR_TWOWAY_MODE;

		std::vector<BlueRose::byte> result_bytes;
		std::vector<BlueRose::byte> enc;

		writer.open(enc);
		writer.writeInteger(x);
		writer.writeInteger(y);
		writer.close();

		reqId = sendRequest(servantID, "add", enc);
		receiveReply(reqId, result_bytes);

		reader.open(result_bytes);
		int result = reader.readInteger();
		reader.close();

		return result;
	}
	
	void print(const std::wstring& str)
	{
		int reqId;
		BlueRose::ByteStreamWriter writer;
		
		currentMode = BR_ONEWAY_MODE;

		std::vector<BlueRose::byte> result_bytes;
		std::vector<BlueRose::byte> enc;

		writer.open(enc);
		writer.writeUTF8String(str);
		writer.close();

		reqId = sendRequest(servantID, "print", enc);
		receiveReply(reqId, result_bytes);
	}
};

};

#endif

