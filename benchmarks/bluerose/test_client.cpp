#include <cstdio>
#include <ctime>
#include <unistd.h>
#include <locale.h>
#include "BlueRose.h"
#include "TestProxy.h"

#define MAX_ITER 100000

using namespace Test;

int main(int argc, char** argv)
{
	//some auxiliary variables
	int result;
		
	//initialize the middleware
	BlueRose::TcpCompatibleDevice cli;
	BlueRose::DevicePool::addDevice(&cli);
	BlueRose::DevicePool::init();
	
	//create max_iter proxies
	std::vector<TestProxy> test_objs;
	
	for (int i=0; i<MAX_ITER; i++)
	{
		TestProxy test("localhost:10000");
		test_objs.push_back(test);
	}
	
	test_objs.clear();
	
	//work with an specific object
	TestProxy* dcalc = new TestProxy("localhost:10000");
	
	if (!dcalc)
        throw "Invalid proxy";
	
	//add i and i+1 from 0 to MAX_ITER
	for (int i=0; i<MAX_ITER; i++)
	{
		result = dcalc->add(i, i+1);
		printf("Result: %d + %d = %d\n", i, i+1, result);
	}
	
	//print a message at the server
	for (int i=0; i<MAX_ITER; i++)
	{
		dcalc->print(L"hello world ñññáéíóù!!!");
	}
	
	//free object
	delete dcalc;

	sleep(10);
	
	//shutdown bluerose
	BlueRose::Initializer::destroy();
	
	return 0;
}

