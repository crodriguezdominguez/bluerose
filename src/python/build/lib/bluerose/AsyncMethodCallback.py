# encoding: utf-8
"""
AsyncMethodCallback.py

Created by Carlos Rodriguez Dominguez on 2010-04-25.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

class AsyncMethodCallback(object):
	"""Interface for the reception of asynchronous replies."""
	
	def callback(self, message):
		"""Perform an action whenever an specific asynchronous reply (message) is received."""
		pass