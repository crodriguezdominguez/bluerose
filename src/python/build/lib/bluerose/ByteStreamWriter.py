# encoding: utf-8
"""
ByteStreamWriter.py

Created by Carlos Rodriguez Dominguez on 2010-04-28.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from struct import *
import codecs

class ByteStreamWriter(object):
	def __init__(self):
		self.buffer = []
		
	def toList(self):
		return self.buffer
		
	def reset(self):
		self.buffer = []
	
	def writeByte(self, byte):
		self.buffer.append(byte)
		
	def writeShort(self, num):
		aux = pack("<h", num)
		for i in range(len(aux)):
			self.buffer.append(ord(aux[i]))
	
	def writeInteger(self, num):
		aux = pack("<i", num)
		for i in range(len(aux)):
			self.buffer.append(ord(aux[i]))
	
	def writeSize(self, size):
		if size < 255:
			self.writeByte(size)
		else:
			self.writeByte(255)
			self.writeInteger(size)
	
	def writeBoolean(self, boolean):
		if boolean == True:
			self.writeByte(1)
		else:
			self.writeByte(0)

	def writeLong(self, num):
		aux = pack("<l", num)
		for i in range(len(aux)):
			self.buffer.append(ord(aux[i]))	

	def writeFloat(self, num):
		aux = pack("<f", num)
		for i in range(len(aux)):
			self.buffer.append(ord(aux[i]))	

	def writeDouble(self, num):
		aux = pack("<d", num)
		for i in range(len(aux)):
			self.buffer.append(ord(aux[i]))		
	
	def writeString(self, string):
		len_str = len(string)
		
		self.writeSize(len_str)
		for i in range(len_str):
			self.writeByte(ord(string[i]))
			
	def writeUTF8String(self, string):
		utf8string = string.encode("utf-8")
		len_str = len(utf8string)

		self.writeSize(len_str)
		for i in range(len_str):
			self.writeByte(ord(utf8string[i]))
			
	def writeRawBytes(self, bytes):
		self.buffer.extend(bytes)
	
	def writeByteSeq(self, bytes):
		self.writeSize(len(bytes))
		self.writeRawBytes(bytes)
		
	def writeBooleanSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeBoolean(seq[i])
			
	def writeShortSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeShort(seq[i])
			
	def writeIntegerSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeInteger(seq[i])
			
	def writeLongSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeLong(seq[i])
					
	def writeFloatSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeFloat(seq[i])

	def writeDoubleSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeDouble(seq[i])
			
	def writeStringSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeString(seq[i])
			
	def writeUTF8StringSeq(self, seq):
		len_seq = len(seq)
		self.writeSize(len_seq)
		for i in range(len_seq):
			self.writeUTF8String(seq[i])

	def writeDictionary(self, seq):
		for key in seq:
			self.writeString(key)
			self.writeRawBytes(seq[key])
			
	def writeObject(self, obj):
		obj.marshall(self)
