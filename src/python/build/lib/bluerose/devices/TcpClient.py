# encoding: utf-8
"""
TcpClient.py

Created by Carlos Rodriguez Dominguez on 2010-05-11.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

import threading
import socket
from bluerose.ByteStreamReader import *
from array import *

class TcpClient(object):
	def __init__(self, host, port, sock=None):
		self.outputMutex = threading.Lock()
		self.inputMutex = threading.Lock()
		
		if sock == None:
			self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
			self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
			self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
			self.socket.connect((host, port))
			self.read()
		else:
			self.socket = sock

	def __del__(self):
		try:
			self.socket.close()
		except Exception:
			pass

	def isOpenned(self):
		try:
			self.socket.getpeername()
			return True
		except Exception:
			return False

	def getID(self):
		return self.socket.getsockname()

	def write(self, data):
		self.outputMutex.acquire()
		try:
			self.socket.send(array("B", data))
		except Exception:
			self.outputMutex.release()
			return False
		
		self.outputMutex.release()
		return True

	def read(self):
		size = -1
		buffer_size = 14;

		self.inputMutex.acquire()
		try:
			n = 0
			while n != buffer_size:
				buffer_init = self.socket.recv(buffer_size)
				s = len(buffer_init)
				n = n+s

				if s == 0:
					self.inputMutex.release()
					return None
					
		except Exception as e:
			print(str(e))
			self.inputMutex.release()
			return None

		msg = array("B", buffer_init)
		msg = msg.tolist()
		
		reader = ByteStreamReader(msg[10:])
		size = reader.readInteger()

		buffer_size = size-buffer_size
		
		if buffer_size == 0:
			self.inputMutex.release()
			return msg
		else:
			try:
				k = 0
				while k != buffer_size:
					buf = self.socket.recv(buffer_size)
					r = len(buf)
					k = k+r

					if r == 0:
						self.inputMutex.release()
						return None
			except Exception:
				self.inputMutex.release()
				return None

			msg_aux = array("B", buf)
			msg.extend(msg_aux.tolist())

			self.inputMutex.release()
			
			return msg
		