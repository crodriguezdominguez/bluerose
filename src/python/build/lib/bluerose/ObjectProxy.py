# encoding: utf-8
"""
ObjectProxy.py

Created by Carlos Rodriguez Dominguez on 2010-05-10.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from ByteStreamWriter import *
from ReplyMessageStack import *
from DevicePool import *
from messages.MessageHeader import *
from messages.RequestMessage import *
from messages.ReplyMessage import *
from threads.OpenConnectionThread import *
from Identity import *
import threading
import sys

class ObjectProxy(object):
	"""
	Proxy for transparently connecting to a servant.
	"""
	
	MAX_REQUEST_ID = 10000
	asyncThread = None
	numInstances = 0
	requestId = 0
	writer = ByteStreamWriter()
	staticMutex = threading.Lock()
	
	def __init__(self, servantID=None, device=None, wait=True, parameters=dict()):
		self.mutex = threading.Lock()
		self.currentMode = MessageHeader.TWOWAY_MODE
		
		if servantID == None:
			self.device = device
			self.openThread = None
			self.servantID = None
			self.identity = Identity()
		
			ObjectProxy.staticMutex.acquire()
			ObjectProxy.numInstances = ObjectProxy.numInstances+1
			ObjectProxy.staticMutex.release()
			
		else:
			self.initializeAttributes(servantID, device, wait, parameters)

	def __del__(self):
		ObjectProxy.staticMutex.acquire()
		ObjectProxy.numInstances = ObjectProxy.numInstances-1
		if ObjectProxy.numInstances <= 0:
			ObjectProxy.asyncThread = None
			ObjectProxy.numInstances = 0
		ObjectProxy.staticMutex.release()

		self.device = None

	def initializeAttributes(self, servID, dev, wait, pars):
		self.identity = Identity()
		self.servantID = servID
		self.device = dev
		self.openThread = None

		ObjectProxy.staticMutex.acquire()
		ObjectProxy.numInstances = ObjectProxy.numInstances+1
		ObjectProxy.staticMutex.release()

		if self.device != None:
			iface_aux = self.device 
		else:
			iface_aux = DevicePool.getCurrentDevice()
		
		if not iface_aux.isConnectionOpenned(self.servantID):
			self.openThread = OpenConnectionThread(iface_aux, self.servantID)
			self.openThread.start()

			if wait:
				self.openThread.join()
				self.openThread = None
			else:
				iface_aux.openConnection(self.servantID)
		else:
			iface_aux.openConnection(self.servantID)
		
		self.currentMode = MessageHeader.TWOWAY_MODE

	def sendRequest(self, servantID, operation, requestMessage):
		"""Send a request to a servant."""
		
		msg = RequestMessage()
		hd = msg.header

		ObjectProxy.staticMutex.acquire()
		ObjectProxy.requestId = (ObjectProxy.requestId+1) % ObjectProxy.MAX_REQUEST_ID
		req = ObjectProxy.requestId;
		ObjectProxy.staticMutex.release()

		hd.requestId = req
		hd.operation = operation

		self.mutex.acquire()
		hd.mode = self.currentMode
		hd.identity = self.identity
		self.mutex.release()

		msg.addToEncapsulation(requestMessage)

		msg_bytes = msg.getBytes()

		if self.device == None:
			res = DevicePool.getCurrentDevice().write(servantID, msg_bytes)

		else:
			res = self.device.write(servantID, msg_bytes)

		if not res:
			self.mutex.acquire()
			if self.openThread == None:
				if self.device != None:
					self.openThread = OpenConnectionThread(self.device, servantID)	
				else:
					self.openThread = OpenConnectionThread(DevicePool.getCurrentDevice(), servantID)
				self.openThread.start()
			self.mutex.release()
		else:
			self.mutex.acquire()
			if self.openThread != None:
				self.openThread = None
			self.mutex.release()

		return req

	def receiveReply(self, reqID):
		"""Receives a reply. Is a blockant method."""
		
		self.mutex.acquire()
		currMode = self.currentMode
		self.mutex.release()

		res = None

		if currMode == MessageHeader.TWOWAY_MODE:
			recv_bytes = ReplyMessageStack.consume(reqID)
			msg = ReplyMessage(recv_bytes)
			res = msg.body.byteCollection

		return res

	def receiveCallback(self, reqID, asyncmethod):
		"""
		Receive a message with the callback thread and treats the received
		message with the specified AsyncMethodCallback.
		"""
		
		ObjectProxy.staticMutex.acquire()
		
		if ObjectProxy.asyncThread == None:
			ObjectProxy.asyncThread = AsyncCallback()
			ObjectProxy.asyncThread.start()
			
		ObjectProxy.asyncThread.pushRequestId(reqID, asyncmethod)	
		ObjectProxy.staticMutex.release()

	def resolveInitialization(self, device, wait, parameters):
		if DiscoveryProxy.defaultProxy == None:
			raise Exception("ERROR: DiscoveryProxy not initialized")

		_servantID = DiscoveryProxy.defaultProxy.resolveName(self.getTypeID())

		if _servantID == "":
			raise Exception("Servant for the "+self.getTypeID()+" proxy has not been found")

		self.servantID = _servantID
		self.device = device
		self.openThread = None

		if device != None: 
			iface_aux = self.device
		else:
			iface_aux = DevicePool.getCurrentDevice()
			
		if not iface_aux.isConnectionOpenned(self.servantID):
			if wait:
				self.openThread = OpenConnectionThread(iface_aux, self.servantID, sys.maxint, parameters)
				self.openThread.start()

				if wait:
					self.openThread.join()
					self.openThread = None
			else:
				iface_aux.openConnection(self.servantID)
		else:
			iface_aux.openConnection(self.servantID)

	def waitOnlineMode(self):
		"""Wait for a Servant to be online."""
		
		self.mutex.acquire()
		wait = self.openThread != None
		self.mutex.release()

		if not wait:
			return
		else:
			self.mutex.acquire()
			self.openThread.join()
			self.openThread = None
			self.mutex.release()

	def getTypeID(self):
		"""Get the type ID of the proxy."""
		pass

class AsyncCallback(threading.Thread):
	"""Callback method for an asynchronous request."""
	
	def __init__(self):
		self.requestsId_callbacks = dict() #stack of requestIds and asynchronous method callbacks
		self.lock = threading.Condition()
		threading.Thread.__init__(self)

	def pushRequestId(self, reqID, method):
		"""Adds a new request id to the async method callback stack."""
		
		self.lock.acquire()
		self.requestsId_callbacks[reqID] = method
		self.lock.notifyAll()
		self.lock.release()

	def run(self):
		while True:
			self.lock.acquire()
			while len(self.requestsId_callbacks) <= 0:
				self.lock.wait()

			size = len(self.requestsId_callbacks)
			self.lock.release()

			if size > 0:
				self.lock.acquire()
				
				for key in self.requestsId_callbacks:
					reqID = key
					amc = self.requestsId_callbacks[key]
					break
					
				self.lock.release()

				recv_bytes = ReplyMessageStack.consume(reqID)

				msg = ReplyMessage(recv_bytes)
				recv_bytes2 = msg.body.byteCollection;
				if len(recv_bytes2) > 0:
					amc.callback(recv_bytes2)

				self.lock.acquire()
				del self.requestsId_callbacks[key]
				self.lock.release()

class DiscoveryProxy(ObjectProxy):
	
	defaultProxy = None
	
	def __init__(self, servantID=None, device=None, wait=True, parameters=dict()):
		ObjectProxy.__init__(self, servantID, device, wait, parameters)

	@classmethod
	def initialize(cls, servantID, device=None, wait=True, parameters=dict()):
		DiscoveryProxy.defaultProxy = DiscoveryProxy(servantID, device, wait, parameters)
		DiscoveryProxy.defaultProxy.identity.idName = "Discovery"
		DiscoveryProxy.defaultProxy.identity.category = "BlueRoseService"

	@classmethod
	def destroy(cls):
		DiscoveryProxy.defaultProxy = None

	def addRegistration(self, name, address):
		"""Register a servant name and an address."""

		self.currentMode = MessageHeader.TWOWAY_MODE

		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeString(name)
		ObjectProxy.writer.writeString(address)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()

		reqID = self.sendRequest(self.servantID, "addRegistration", data)
		self.receiveReply(reqID)

	def removeRegistration(self, name):
		"""Unregister the address that is associated with a name."""

		self.currentMode = MessageHeader.TWOWAY_MODE

		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeString(name)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()

		reqID = self.sendRequest(self.servantID, "removeRegistration", data)
		self.receiveReply(reqID)

	def resolveName(self, name, wait=False):
		self.currentMode = MessageHeader.TWOWAY_MODE
		result = ""

		while result == "":
			ObjectProxy.staticMutex.acquire()
			ObjectProxy.writer.writeString(name)
			data = ObjectProxy.writer.toList()
			ObjectProxy.writer.reset()
			ObjectProxy.staticMutex.release()

			reqID = self.sendRequest(self.servantID, "resolve", data)
			result_bytes = self.receiveReply(reqID)

			reader = ByteStreamReader(result_bytes)
			result = reader.readString()

			if result == "" and wait:
				time.sleep(1)
			if not wait:
				break

		return result

	def getTypeID(self):
		return "BlueRoseService::Discovery"