# encoding: utf-8
"""
Comparison.py

Created by Carlos Rodriguez Dominguez on 2010-04-25.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

class Comparison(object):
	EQUAL=0
	NOT_EQUAL=1
	LESS=2
	LESS_EQUAL=3
	GREATER=4
	GREATER_EQUAL=5
