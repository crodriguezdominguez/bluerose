# encoding: utf-8
"""
Identity.py

Created by Carlos Rodriguez Dominguez on 2010-04-24.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

class Identity(object):
	
	__hash__ = object.__hash__
	
	def __init__(self):
		self.idName = ""
		self.category = ""
		
	def __str__(self):
		return self.category+":"+self.idName
		
	def __eq__(self, other):
		if type(self) != type(other):
			return False
		
		if self.idName == other.idName and self.category == other.category:
			return True
		else:
			return False
			
	def __lt__(self, other):
		if type(self) != type(other):
			return False
		
		if self.idName < other.idName:
			return True
		elif self.idName == other.idName and self.category < other.category:
			return True
		else:
			return False
			
	def __gt__(self, other):
		if type(self) != type(other):
			return False
		
		if self.idName > other.idName:
			return True
		elif self.idName == other.idName and self.category > other.category:
			return True
		else:
			return False
			
	def __ne__(self, other):
		return not (self == other)
