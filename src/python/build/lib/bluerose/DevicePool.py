# encoding: utf-8
"""
DevicePool.py

Created by Carlos Rodriguez Dominguez on 2010-04-28.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from ReplyMessageStack import *
from DiscoveryProxy import *
import threading
import sys

class DevicePool(object):
	@classmethod
	def initialize(cls):
		DevicePool.deviceCollection = dict()
		DevicePool.objects = dict()
		DevicePool.lock = threading.Lock()
		ReplyMessageStack.initialize()
		DevicePool.device = DevicePool.getNextDevice()
		
	@classmethod
	def destroy(cls):
		ReplyMessageStack.destroy()
		
		for key in DevicePool.objects:
			value = DevicePool.objects[key]
			DiscoveryProxy.defaultProxy.removeRegistration(str(value.identity))
		
		DevicePool.objects.clear()
	
	@classmethod		
	def addDevice(cls, dev):
		DevicePool.lock.acquire()
		DevicePool.deviceCollection[dev.getDeviceName()] = dev
		length = len(DevicePool.deviceCollection)
		DevicePool.lock.release()
		
		if length == 1:
			device = DevicePool.getNextDevice()
			DevicePool.lock.acquire()
			DevicePool.device = device
			DevicePool.lock.release()
			
		
	@classmethod
	def registerObjectServant(cls, obj):
		DevicePool.lock.acquire()
		
		DevicePool.objects[str(obj.identity)] = obj
		
		DevicePool.lock.release() 
		
	@classmethod
	def unregisterObjectServant(cls, obj):
		DevicePool.lock.acquire()

		del DevicePool.objects[str(obj.identity)]
		
		if DiscoveryProxy.defaultProxy != None:
			DiscoveryProxy.defaultProxy.removeRegistration(str(obj.identity))

		DevicePool.lock.release() 

	@classmethod
	def getNextDevice(cls):
		DevicePool.lock.acquire()
		
		minPriority = sys.maxint
		result = None
		
		for key in DevicePool.deviceCollection:
			value = DevicePool.deviceCollection[key]
			if value.getPriority() < minPriority:
				minPriority = value.getPriority()
				result = value
				
		DevicePool.lock.release()
		
		return result
	
	@classmethod
	def dispatchMessage(cls, message, userID):
		reply = ReplyMessage()
		orig = RequestMessage(message)
		
		header = reply.header
		header2 = orig.header
		
		requestId = header2.requestId

		header.requestId = requestId
		
		DevicePool.lock.acquire()

		serv = DevicePool.objects.get(str(header2.identity))

		if serv != None:
			(result, status) = serv.runMethod(header2.operation, userID, orig.body.byteCollection)
		else:
			status = MessageHeader.OBJECT_NOT_EXIST_STATUS

		header.replyStatus = status

		if status == MessageHeader.SUCCESS_STATUS:
			reply.addToEncapsulation(result)

		reply_bytes = reply.getBytes()

		if header2.mode == MessageHeader.TWOWAY_MODE:
			DevicePool.device.write(userID, reply_bytes)
			
		DevicePool.lock.release()
		
	@classmethod
	def getCurrentDevice(cls):
		DevicePool.lock.acquire()
		dev = DevicePool.device
		DevicePool.lock.release()
		
		return dev
		
	@classmethod
	def getDeviceForName(cls, name):
		DevicePool.lock.acquire()
		name = DevicePool.deviceCollection[name]
		DevicePool.lock.release()

		return name