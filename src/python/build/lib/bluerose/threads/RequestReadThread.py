#!/usr/bin/env python
# encoding: utf-8
"""
RequestReadThread.py

Created by Carlos Rodriguez Dominguez on 2010-05-10.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

import threading
from bluerose.messages.ValidateConnectionMessage import *
from bluerose.messages.MessageHeader import *
from bluerose.DevicePool import *

class RequestReadThread(threading.Thread):
	def __init__(self, device, userID):
		self.device = device
		self.userID = userID
		threading.Thread.__init__(self)
		
	def run(self):
		#send validate connection msg
		msg = ValidateConnectionMessage()
		bytes = msg.getBytes()
		self.device.write(self.userID, bytes)
		
		bytes = []

		while True: 
			bytes = self.device.read(self.userID) #read messages
			
			if bytes == None:
				break
			
			if bytes[8] == MessageHeader.CLOSE_CONNECTION_MSG:
				break

			if bytes[8] == MessageHeader.REQUEST_MSG:
				#process message
				DevicePool.dispatchMessage(bytes, self.userID)

			bytes = []

		print("Connection closed: "+self.userID)
		self.device.closeConnection(self.userID)
