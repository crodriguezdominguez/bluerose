# encoding: utf-8
"""
ReplyReadThread.py

Created by Carlos Rodriguez Dominguez on 2010-05-10.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

import threading
from bluerose.ByteStreamReader import *
from bluerose.ReplyMessageStack import *
from bluerose.messages.MessageHeader import *
from bluerose.events.Event import *
from bluerose.events.EventHandler import *

class ReplyReadThread(threading.Thread):
	"""
	Auxiliary thread for reading replies from a transmission 
	channel. It's used for receiving servant replies.
	"""
	
	def __init__(self, device, userID):
		self.device = device #Transmission interface for the thread
		self.userID = userID #User identificator of the sender
		threading.Thread.__init__(self)

	def run(self):
		bytes = []
		
		while True: #read messages
			bytes = self.device.read(self.userID)
			
			if bytes == None:
				break
			
			if bytes[8] == MessageHeader.REPLY_MSG:
				reader = ByteStreamReader(bytes)

				#add to reply stack
				reader.skip(14)
				requestId = reader.readInteger()
				ReplyMessageStack.produce(requestId, bytes)

			elif bytes[8] == MessageHeader.EVENT_MSG:
				reader = ByteStreamReader(bytes)

				reader.skip(14)
				bb = reader.readToEnd()

				evt = Event()
				reader2 = ByteStreamReader(bb)
				evt.unmarshall(reader2)
				
				EventHandler.onConsume(evt)

			elif bytes[8] == MessageHeader.CLOSE_CONNECTION_MSG:
				break
			
			bytes = None

		self.device.closeConnection(self.userID)
