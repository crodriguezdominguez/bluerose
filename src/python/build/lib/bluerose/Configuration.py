# encoding: utf-8
"""
Configuration.py

Created by Carlos Rodriguez Dominguez on 2010-04-28.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from xml.dom.minidom import *

class Configuration(object):
	def __init__(self, xmlfile):
		self.doc = xml.dom.minidom.parse(xmlfile)
	
	def getDevices(self):
		res = []
		
		root_element = self.doc.documentElement
		nodes = root_element.childNodes
		
		for cur_node in nodes:
			if cur_node.nodeType == Node.ELEMENT_NODE:
				if cur_node.nodeName == "devices":
					nodes2 = cur_node.childNodes
					
					for cur_node2 in nodes2:
						if cur_node2.nodeType == Node.ELEMENT_NODE:
							c = cur_node2.attributes["name"]
							res.append(str(c.nodeValue))
					
					break
		
		return res
		
	def getModuleForDevice(self, device):
		root_element = self.doc.documentElement
		nodes = root_element.childNodes

		for cur_node in nodes:
			if cur_node.nodeType == Node.ELEMENT_NODE:
				if cur_node.nodeName == "devices":
					nodes2 = cur_node.childNodes
					for cur_node2 in nodes2:
						if cur_node2.nodeType == Node.ELEMENT_NODE:
							name = cur_node2.attributes["name"].nodeValue
							if name == device:
								return cur_node2.attributes["module"].nodeValue

		return None
	
	def getCategories(self, device):
		res = []
		
		root_element = self.doc.documentElement
		nodes = root_element.childNodes

		for cur_node in nodes:
			if cur_node.nodeType == Node.ELEMENT_NODE:
				if cur_node.nodeName == "services":
					nodes2 = cur_node.childNodes

					for cur_node2 in nodes2:
						if cur_node2.nodeType == Node.ELEMENT_NODE:
							name = cur_node2.attributes["name"].nodeValue
							res.append(str(name))

					break

		return res
	
	def getServicesForCategory(self, category):
		root_element = self.doc.documentElement
		nodes = root_element.childNodes

		for cur_node in nodes:
			if cur_node.nodeType == Node.ELEMENT_NODE:
				if cur_node.nodeName == "services":
					nodes2 = cur_node.childNodes

					for cur_node2 in nodes2:
						if cur_node2.nodeType == Node.ELEMENT_NODE:
							name1 = cur_node2.attributes["name"].nodeValue

							if name1 != None:
								if name1 == category:
									nodes3 = cur_node2.childNodes

									res = []

									for cur_node3 in nodes3:
										if cur_node3.nodeType == Node.ELEMENT_NODE:
											name2 = cur_node3.attributes["name"].nodeValue
											res.append(str(name2))

									return res
		return None
		
	def getDevicesForService(self, category, service):
		root_element = self.doc.documentElement
		nodes = root_element.childNodes

		for cur_node1 in nodes:
			if cur_node1.nodeType == Node.ELEMENT_NODE:
				if cur_node1.nodeName == "services":
					nodes2 = cur_node1.childNodes

					for cur_node2 in nodes2:
						if cur_node2.nodeType == Node.ELEMENT_NODE:
							name1 = cur_node2.attributes["name"].nodeValue

							if name1 == category:
								nodes3 = cur_node2.childNodes

								for cur_node3 in nodes3:
									if cur_node3.nodeType == Node.ELEMENT_NODE:
										name2 = cur_node3.attributes["name"].nodeValue

										if name2 == service:
											res = []

											nodes4 = cur_node3.childNodes
											for cur_node4 in nodes4:
												if cur_node4.nodeType == Node.ELEMENT_NODE:
													name3 = cur_node4.attributes["name"].nodeValue
													res.append(str(name3))

											return res

		return None
		
	def getPort(self, category, service, device):
		root_element = self.doc.documentElement
		nodes = root_element.childNodes

		for cur_node1 in nodes:
			if cur_node1.nodeType == Node.ELEMENT_NODE:
				if cur_node1.nodeName == "services":
					nodes2 = cur_node1.childNodes

					for cur_node2 in nodes2:
						if cur_node2.nodeType == Node.ELEMENT_NODE:
							name1 = cur_node2.attributes["name"].nodeValue

							if name1 == category:
								nodes3 = cur_node2.childNodes

								for cur_node3 in nodes3:
									if cur_node3.nodeType == Node.ELEMENT_NODE:
										name2 = cur_node3.attributes["name"].nodeValue

										if name2 == service:
											nodes4 = cur_node3.childNodes
											for cur_node4 in nodes4:
												if cur_node4.nodeType == Node.ELEMENT_NODE:
													name3 = cur_node4.attributes["name"].nodeValue

													if name3 == device:
														result = cur_node4.attributes["port"].nodeValue
														return str(result)

		return None
		
	def getAddress(self, category, service, device):
		root_element = self.doc.documentElement
		nodes = root_element.childNodes

		for cur_node1 in nodes:
			if cur_node1.nodeType == Node.ELEMENT_NODE:
				if cur_node1.nodeName == "services":
					nodes2 = cur_node1.childNodes

					for cur_node2 in nodes2:
						if cur_node2.nodeType == Node.ELEMENT_NODE:
							name1 = cur_node2.attributes["name"].nodeValue

							if name1 == category:
								nodes3 = cur_node2.childNodes

								for cur_node3 in nodes3:
									if cur_node3.nodeType == Node.ELEMENT_NODE:
										name2 = cur_node3.attributes["name"].nodeValue

										if name2 == service:
											nodes4 = cur_node3.childNodes
											for cur_node4 in nodes4:
												if cur_node4.nodeType == Node.ELEMENT_NODE:
													name3 = cur_node4.attributes["name"].nodeValue

													if name3 == device:
														result = cur_node4.attributes["address"].nodeValue
														return str(result)

		return None
	
	def getUserID(self, category, service, device):
		address = self.getAddress(category, service, device)
		port = self.getPort(category, service, device)
		if address != None and port != None:
			return address+":"+port
		else:
			return None
