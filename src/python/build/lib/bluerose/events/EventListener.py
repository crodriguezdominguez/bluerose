# encoding: utf-8
"""
EventListener.py

Created by Carlos Rodriguez Dominguez on 2010-05-11.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from Event import *
from Predicate import *

class EventListener(object):
	
	__hash__ = object.__hash__
	
	def __init__(self, topic=Event.DEFAULT_TOPIC, predicate=Predicate()):
		self.topic = topic
		self.predicate = predicate

	def acceptsTopic(self, topic):
		"""Checks whether this listener accepts a topic or not."""
		
		return (self.topic == topic)

	def acceptsEvent(self, event):
		"""
		Checks whether this listener accepts an event or not. It is
		a more complex check than the check method that only accepts
		a topic. In this method, it is assured that the event is
		related with the topic established in the constructor. It uses
		the predicate to check if it is accepted or not.
		"""
		
		if self.predicate.isEmpty():
			return True

		for key in event.nodes:
			node = event.nodes[key]
			if not self.predicate.isConsistent(key, node.value):
				return False

		return True

	def performAction(self, event):
		"""Action that is run whenever an appropriate event is received."""
		pass

	def __eq__(self, otherListener):
		return type(self) == type(otherListener)
