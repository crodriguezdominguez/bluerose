# encoding: utf-8
"""
Event.py

Created by Carlos Rodriguez Dominguez on 2010-05-01.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.ByteStreamWriter import *
from bluerose.ByteStreamReader import *
from bluerose.Marshallable import *
from Value import *

class Event(Marshallable):
	
	DEFAULT_TOPIC = 0
	
	__hash__ = Marshallable.__hash__
	
	def __init__(self, topic=DEFAULT_TOPIC):
		self.topic = topic
		self.nodes = dict()
		
	def __lt__(self, otherEvent):
		if type(self) != type(otherEvent):
			return False
		return self.nodes < otherEvent.nodes
		
	def __eq__(self, otherEvent):
		if type(self) != type(otherEvent):
			return False
		return self.nodes == otherEvent.nodes
		
	def __ne__(self, otherEvent):
		return not (self == otherEvent)
		
	def __gt__(self, otherEvent):
		if type(self) != type(otherEvent):
			return False
		return self.nodes > otherEvent.nodes

	def __getitem__(self, member):
		return self.nodes.get(member)
		
	def __setitem__(self, member, node):
		if type(node) == EventNode:
			self.nodes[member] = node
		elif type(node) == Value:
			self.setMemberValue(member, node)
		else:
			raise Exception("Node is not of event node type")

	def __len__(self):
		return len(self.nodes)
		
	def __str__(self):
	 	res = "Topic="+str(self.topic)+", {"
		counter = 0
		end = len(self.nodes)
		for key in self.nodes:
			value = self.nodes[key]
			res += key+":"+str(value)
			counter+=1
			if counter != end:
				res +=", "
			
		res += "}"
		return res
		
	def copy(self, otherEvent):
		otherEvent.topic = self.topic
		otherEvent.nodes = self.nodes
		return otherEvent
		
	def getMember(self, member):
		return self.nodes.get(member)
		
	def getMemberValue(self, member):
		node = self.nodes.get(member)
		if node != None:
			return node.value
		else:
			return None
			
	def setMember(self, member, node):
		if type(node) == EventNode:
			self.nodes[member] = node
		else:
			raise Exception("Node is not of event node type")
		
	def setMemberValue(self, member, value):
		node = EventNode(member, value)
		self.nodes[member] = node
		
	def existsMember(self, member):
		if self.nodes.get(member) != None:
			return True
		else:
			return False
			
	def marshall(self, writer):
		#topic
		writer.writeInteger(self.topic)

		#num. event nodes
		writer.writeSize(len(self.nodes))

		#event nodes
		for key in self.nodes:
			node = self.nodes[key]
			writer.writeObject(node)
		
	def unmarshall(self, reader):
		self.topic = reader.readInteger()
		self.nodes = dict()

		#num. event nodes
		tam = reader.readSize()

		if tam == 0:
			return

		#event nodes
		for i in range(tam):
			aux = EventNode()
			aux.unmarshall(reader)
			self.setMember(aux.identifier, aux)

class EventNode(Event):
	def __init__(self, identifier="", value=Value()):
		Event.__init__(self)
		self.identifier = identifier
		self.value = value
		
	def __str__(self):
		return str(self.value)

	def marshall(self, writer):
		#identifier
		writer.writeString(self.identifier)

		#value
		self.value.marshall(writer)

		#topic
		writer.writeInteger(self.topic)

		#num. event nodes
		writer.writeSize(len(self.nodes))

		#event nodes
		for key in self.nodes:
			node = self.nodes[key]
			writer.writeObject(node)

	def unmarshall(self, reader):
		#identifier
		self.identifier = reader.readString()

		#value
		self.value = Value()
		self.value.unmarshall(reader)

		#topic
		self.topic = reader.readInteger()

		#num. event nodes
		size = reader.readSize()

		if size == 0:
			return

		#event nodes
		for i in range(size):
			aux = EventNode()
			aux.unmarshall(reader)

			self.setMember(aux.identifier, aux)
