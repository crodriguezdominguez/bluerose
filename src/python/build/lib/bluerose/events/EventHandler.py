# encoding: utf-8
"""
EventHandler.py

Created by Carlos Rodriguez Dominguez on 2010-05-11.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from Event import *
from bluerose.ByteStreamWriter import *
from bluerose.ByteStreamReader import *
from bluerose.ObjectProxy import *
from bluerose.messages.MessageHeader import *

class EventHandler(object):
	
	proxy = None
	
	@classmethod
	def initialize(cls, servantID=None, device=None, wait=True, parameters=None):
		EventHandler.proxy = PubSubProxy(servantID, device, wait, parameters)
		EventHandler.listeners = dict()
	
	@classmethod
	def destroy(cls):
		if EventHandler.proxy != None:
			EventHandler.proxy.unsubscribe()

	@classmethod
	def onConsume(cls, event):
		"""Action to be performed whenever an event is consumed."""
		
		topic = event.topic
		array = EventHandler.listeners.get(topic)
		if array != None:
			for listener in array:
				if listener.acceptsTopic(topic):
					if listener.acceptsEvent(event):
						listener.performAction(event)

	@classmethod
	def addEventListener(cls, listener):
		""""Add an event listener to the listeners."""
		
		topic = listener.topic
		array = EventHandler.listeners.get(topic)
		if array == None:
			array = []

		array.append(listener)
		EventHandler.listeners[topic] = array
		
		if listener.predicate == None:
			EventHandler.proxy.subscribe(topic)
		else:
			EventHandler.proxy.subscribe(topic, listener.predicate)

		#send an event
		evt = Event()
		EventHandler.proxy.publish(evt)

	@classmethod
	def removeEventListeners(cls, topic):
		"""Removes all the event listener that are related with a topic."""
		
		array = EventHandler.listeners.get(topic)
		if array != None:
			EventHandler.proxy.unsubscribe(topic)
			del EventHandler.listeners[topic]

	@classmethod
	def removeEventListener(cls, listener):
		"""Remove an event listener from the listeners."""
	
		topic = listener.topic
		array = EventHandler.listeners.get(topic)
		if array != None:
			for _list in array:
				if _list == listener:
					array.remove(_list)

					EventHandler.listeners[topic] = array

					if len(array) == 0:
						EventHandler.removeEventListeners(topic)

					return

	@classmethod
	def publish(cls, event, comeback=True):
		"""Publishes an event to all its subscribers."""
				
		EventHandler.proxy.publish(event, comeback)


class PubSubProxy(ObjectProxy):
	def __init__(self, servantID=None, device=None, waitOnline=True, parameters=None):
		ObjectProxy.__init__(self, servantID, device, waitOnline, parameters)
		
		self.identity.idName = "PubSub"
		self.identity.category = "BlueRoseService"

		if servantID == None:
			self.resolveInitialization(device, waitOnline, parameters)

	def subscribe(self, topic, predicate=None):
		"""Add a new subscription to a topic with a set of constraints."""
		
		self.currentMode = MessageHeader.ONEWAY_MODE
				
		if predicate==None:
			ObjectProxy.staticMutex.acquire()
			ObjectProxy.writer.writeInteger(topic)
			data = ObjectProxy.writer.toList()
			ObjectProxy.writer.reset()
			ObjectProxy.staticMutex.release()
			
			reqID = self.sendRequest(self.servantID, "subscribe0", data)
		else:
			ObjectProxy.staticMutex.acquire()
			ObjectProxy.writer.writeInteger(topic)
			ObjectProxy.writer.writeObject(predicate)
			data = ObjectProxy.writer.toList()
			ObjectProxy.writer.reset()
			ObjectProxy.staticMutex.release()
			
			reqID = self.sendRequest(self.servantID, "subscribe1", data)
		
		self.receiveReply(reqID)
			
	def unsubscribe(self, topic=None):
		"""Remove subscriptions to a topic. If the topic is None, then remove all subscriptions."""
		
		self.currentMode = MessageHeader.ONEWAY_MODE
		
		if topic == None:
			reqID = self.sendRequest(self.servantID, "unsubscribe0", [])
		else:
			ObjectProxy.staticMutex.acquire()
			ObjectProxy.writer.writeInteger(topic)
			data = ObjectProxy.writer.toList()
			ObjectProxy.writer.reset()
			ObjectProxy.staticMutex.release()
			
			reqID = self.sendRequest(self.servantID, "unsubscribe1", data)
			
		self.receiveReply(reqID)

	def isSubscribed(self, userID, templateEvent):
		"""
		Check if any of the user subscriptions allow him/her to
		receive the event that is specified by the template
		"""

		self.currentMode = MessageHeader.TWOWAY_MODE

		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeString(userID)
		ObjectProxy.writer.writeObject(templateEvent)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()

		reqID = self.sendRequest(self.servantID, "isSubscribed", data)
		result_bytes = self.receiveReply(reqID)

		reader = ByteStreamReader(result_bytes)
		return reader.readBoolean()

	def getSubscribers(self, templateEvent=None):
		"""Return all the subscribers that may receive the specified event."""

		self.currentMode = MessageHeader.TWOWAY_MODE;

		if templateEvent==None:
			reqID = self.sendRequest(self.servantID, "getSubscribers0", [])
		else:
			ObjectProxy.staticMutex.acquire()
			ObjectProxy.writer.writeObject(templateEvent)
			data = ObjectProxy.writer.toList()
			ObjectProxy.writer.reset()
			ObjectProxy.staticMutex.release()
			
			reqID = self.sendRequest(self.servantID, "getSubscribers1", data)
		
		result_bytes = self.receiveReply(reqID);

		reader = ByteStreamReader(result_bytes)
		return reader.readStringSeq()

	def publish(self, event, comeback=True):
		"""Publish an event to all its subscribers."""
		
		self.currentMode = MessageHeader.TWOWAY_MODE;

		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeObject(event)
		ObjectProxy.writer.writeBoolean(comeback)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()

		reqID = self.sendRequest(self.servantID, "publish", data)
		self.receiveReply(reqID)

	def getTypeID(self):
		return "BlueRoseService::PubSub"

