# encoding: utf-8
"""
Predicate.py

Created by Carlos Rodriguez Dominguez on 2010-05-04.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.Marshallable import *
from bluerose.Comparison import *
from Value import *

class Predicate(Marshallable):
	"""Models a set of constraints over the values of a set of properties."""
	
	def __init__(self):
		self.constraints = dict()
		
	def __len__(self):
		"""Return the number of constraints."""
		
		return len(constraints)

	def addConstraint(self, property, comparison, value):
		"""Add a new constraint to the set of constraints, but only if the set remains consistent."""
		
		if not self.isConsistent(property, comp, value):
			return False
			
		else:
			array = self.constraints[property]
			pair = [comparison, value]
			
			if array == None:
				array = []
				array.append(pair)
				self.constraints[property] = array
			else:
				array.append(pair)
				self.constraints[property] = array

			return True

	def removeConstraints(self, property):
		"""Remove a constraint from the set."""
		
		del self.constraints[property]

	def isConsistent(self, property, comparison, value):
		"""
		Checks if the set of constraints is consistent after adding "property comp value".
		For example: "x == 5", "y <= 3", etc.
		"""
		
		if len(self.constraints) == 0:
			return True
		else:
			v = self.constraints.get(property)
			if v == None:
				return True

			for pair in v:
				#check inconsistencies
				first = pair[0]
				second = pair[1]

				if second == value: #value == pair.second
					if first == Comparison.EQUAL:
						if (comparison == Comparison.LESS) or \
						   (comparison == Comparison.GREATER) or \
						   (comparison == Comparison.NOT_EQUAL):
							return False

					elif first == Comparison.NOT_EQUAL:
						if comparison == Comparison.EQUAL:
							return False

					elif first == Comparison.LESS:
						if (comparison == Comparison.EQUAL) or \
						   (comparison == Comparison.GREATER) or \
						   (comparison == Comparison.GREATER_EQUAL): 
							return False

					elif first == Comparison.LESS_EQUAL:
						if comparison == Comparison.GREATER:
							return False

					elif first == Comparison.GREATER:
						if (comparison == Comparison.EQUAL) or \
						   (comparison == Comparison.LESS) or \
						   (comparison == Comparison.LESS_EQUAL):
							return False

					elif first == Comparison.GREATER_EQUAL:
						if comparison == Comparison.LESS:
							return False
							
				else: #value != pair.second
					if first == Comparison.EQUAL:
						return False

					elif first == Comparison.NOT_EQUAL:
						return True

					elif (first == Comparison.LESS) or (first == Comparison.LESS_EQUAL):
						if value > second:
							if (comparison == Comparison.EQUAL) or \
							   (comparison == Comparison.GREATER) or \
							   (comparison == Comparison.GREATER_EQUAL):
								return False

					elif (first == Comparison.GREATER) or (first == Comparison.GREATER_EQUAL):
						if value < second:
							if (comparison == Comparison.EQUAL) or \
							   (comparison == Comparison.LESS) or \
							   (comparison == Comparison.LESS_EQUAL):
								return False

			return True

	def isConsistent(self, property, value):
		"""Check if the set of constraints is consistent after adding "property == value"."""
		
		return self.isConsistent(property, Comparison.EQUAL, value)

	def isEmpty(self):
		"""Check if the set of constraints is empty."""
		
		return (len(self.constraints) == 0)

	def marshall(self, writer):
		writer.writeSize(len(self.constraints))

		for key in self.constraints:
			writer.writeString(key)
			array = self.constraints[key]
			writer.writeSize(len(array))

			for pair in array:
				number = pair[0]
				value = pair[1]

				writer.writeSize(number)
				writer.writeSize(value.type)
				writer.writeByteSeq(value.rawValue)

	def unmarshall(self, reader):
		self.constraints = dict()

		size = reader.readSize()

		for i in range(size):
			property = reader.readString()

			size_v = reader.readSize()
			for j in range(size_v):
				comp = reader.readSize()

				value = Value(reader.readSize())

				#read byte array
				data = reader.readByteSeq()
					
				value.rawValue = data
				self.addConstraint(property, comp, value)
