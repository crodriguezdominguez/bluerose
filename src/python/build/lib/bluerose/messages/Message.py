# encoding: utf-8
"""
Message.py

Created by Carlos Rodriguez Dominguez on 2010-04-29.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.ByteStreamWriter import *

class Message(object):
	"""Models an empty message."""
	
	__hash__ = object.__hash__
	
	def __init__(self):
		self.body = None
		self.header = None
	
	def __eq__(self, otherMessage):
		if type(a) != type(otherMessage):
			return False
			
		if self.getBytes() == otherMessage.getBytes():
			return True
		else:
			return False
	
	def __lt__(self, otherMessage):
		if type(a) != type(otherMessage):
			return False
			
		if self.getBytes() < otherMessage.getBytes():
			return True
		else:
			return False
			
	def __gt__(self, otherMessage):
		if type(a) != type(otherMessage):
			return False
			
		if self.getBytes() > otherMessage.getBytes():
			return True
		else:
			return False
			
	def __ne__(self, otherMessage):
		return not (self == otherMessage)
	
	def getBytes(self):
		"""Return the full message (header+body, if body exists) as a byte sequence."""
		
		writer = ByteStreamWriter()

		if self.body != None:
			hd = self.header.getBytes()
			self.header.messageSize = len(hd) + 6 + len(self.body.byteCollection)

			hd = self.header.getBytes()

			writer.writeRawBytes(hd)
			writer.writeInteger(self.body.size)
			writer.writeByte(self.body.major)
			writer.writeByte(self.body.minor)
			writer.writeRawBytes(self.body.byteCollection)
		else:
			writer.writeRawBytes(self.header.getBytes())

		return writer.toList()

	def addToEncapsulation(self, bytes):
		"""Add bytes to the body."""
		
		if self.body != None:
			self.body.size += len(bytes)
			self.body.byteCollection.extend(bytes)
	
