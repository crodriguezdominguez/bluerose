# encoding: utf-8
"""
ReplyMessageHeader.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from MessageHeader import *
from bluerose.ByteStreamWriter import *

class ReplyMessageHeader(MessageHeader):
	"""
	Header for a reply message. 
	Definition taken from ICE (zeroc.com) for compatibility reasons.
	"""
	
	def __init__(self):
		MessageHeader.__init__(self)
		self.messageType = MessageHeader.REPLY_MSG
		self.requestId = 0
		self.messageSize = 19
		self.replyStatus = MessageHeader.SUCCESS_STATUS

	def getBytes(self):
		bytes = MessageHeader.getBytes(self)
		
		writer = ByteStreamWriter()

		writer.writeRawBytes(bytes)

		writer.writeInteger(self.requestId)
		writer.writeByte(self.replyStatus)

		return writer.toList()
