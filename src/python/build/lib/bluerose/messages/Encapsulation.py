# encoding: utf-8
"""
Encapsulation.py

Created by Carlos Rodriguez Dominguez on 2010-04-24.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

class Encapsulation(object):
	def __init__(self):
		self.size = 6
		self.major = 0x01
		self.minor = 0x00
		self.byteCollection = []
