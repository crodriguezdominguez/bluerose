# encoding: utf-8
"""
MessageHeader.py

Created by Carlos Rodriguez Dominguez on 2010-04-29.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.ByteStreamWriter import *

class MessageHeader(object):
	"""
	Common header for any message. Definition taken from ICE (zeroc.com) for compatibility reasons.
	"""
	
	#Messages types
	REQUEST_MSG = 0x00
	BATCH_REQUEST_MSG = 0x01
	REPLY_MSG = 0x02
	VALIDATE_CONNECTION_MSG = 0x03
	CLOSE_CONNECTION_MSG = 0x04
	EVENT_MSG = 0x05   #extension for natively support events

	#Reply status
	SUCCESS_STATUS = 0x00;
	USER_EXCEPTION_STATUS = 0x01;
	OBJECT_NOT_EXIST_STATUS = 0x02
	FACET_NOT_EXIST_STATUS = 0x03
	OPERATION_NOT_EXIST_STATUS = 0x04
	UNKNOWN_LOCAL_EXCEPTION_STATUS = 0x05
	UNKNOWN_USER_EXCEPTION_STATUS = 0x06
	UNKNOWN_EXCEPTION_STATUS = 0x07

	#Operation Mode
	TWOWAY_MODE = 0x00
	ONEWAY_MODE = 0x01
	BATCH_ONEWAY_MODE = 0x02
	DATAGRAM_MODE = 0x03
	BATCH_DATAGRAM_MODE = 0x04

	#Other definitions
	PROTOCOL_MINOR = 0x00
	PROTOCOL_MAJOR = 0x01
	ENCODING_MINOR = 0x00
	ENCODING_MAJOR = 0x01
	
	def __init__(self):
		self.magic = [0x49, 0x63, 0x65, 0x50]
		self.protocolMajor = MessageHeader.PROTOCOL_MAJOR
		self.protocolMinor = MessageHeader.PROTOCOL_MINOR
		self.encodingMajor = MessageHeader.ENCODING_MAJOR
		self.encodingMinor = MessageHeader.ENCODING_MINOR
		self.messageType = MessageHeader.VALIDATE_CONNECTION_MSG
		self.compressionStatus = 0x00
		self.messageSize = 14
		
	def getBytes(self):
		"""Return the bytes of the message header."""
		writer = ByteStreamWriter()
		
		writer.writeRawBytes(self.magic)
		writer.writeByte(self.protocolMajor)
		writer.writeByte(self.protocolMinor)
		writer.writeByte(self.encodingMajor)
		writer.writeByte(self.encodingMinor)
		writer.writeByte(self.messageType)
		writer.writeByte(self.compressionStatus)
		writer.writeInteger(self.messageSize)
		
		return writer.toList()
		
