# encoding: utf-8
"""
CloseConnectionMessage.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from Message import *
from CloseConnectionMessageHeader import *

class CloseConnectionMessage(Message):
	"""A close connection message."""
	
	def __init__(self):
		Message.__init__(self)
		self.header = CloseConnectionMessageHeader()