# encoding: utf-8
"""
ObjectServant.py

Created by Carlos Rodriguez Dominguez on 2010-05-11.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

import threading
from Identity import *
from ByteStreamWriter import *

class ObjectServant(object):
	
	mutex = threading.Lock()
	writer = ByteStreamWriter()
	
	__hash__ = object.__hash__
	
	def __init__(self, identity=Identity()):
		self.identity = identity
		
	def __eq__(self, otherServant):
		if type(self) == type(otherServant):
			if self.identity == otherServant.identity:
				return True
			else:
				return False
		else:
			return False

	def getIdentifier(self):
		return self.identity.category+"::"+self.identity.idName

	def runMethod(self, methodName, userID, arguments):
		pass
