from setuptools import setup, find_packages

setup(name='bluerose', 
	author='Carlos Rodriguez Dominguez. GEDES Research Group', 
	version='0.1',
	author_email='carlosrodriguez@ugr.es', 
	url='http://www.ugr.es/~carlosrodriguez',
	packages=find_packages(),
	install_requires=['netifaces']
)


