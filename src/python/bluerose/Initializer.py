# encoding: utf-8
"""
Initializer.py

Created by Carlos Rodriguez Dominguez on 2010-05-11.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from Configuration import *
from DevicePool import *
from events.EventHandler import *

class Initializer(object):
	
	configuration = None
	
	@classmethod
	def initialize(cls, xmlfile):
		Initializer.configuration = Configuration(xmlfile)

	@classmethod
	def initializeNonWaitingClient(cls, device):
		DevicePool.initialize()
		DevicePool.addDevice(device)

		if DiscoveryProxy.defaultProxy == None:
			discoveryAddress = Initializer.configuration.getUserID("BlueRoseService", "Discovery", device.getDeviceName())

			if discoveryAddress != None:
				DiscoveryProxy.initialize(discoveryAddress, device, False)
			else:
				raise Exception("ERROR: Discovery servant not configured in config file")

		EventHandler.initialize(None, device, False)

	@classmethod
	def initializeClient(cls, device, withDiscoveryServant=True, withEventHandler=True):
		DevicePool.initialize()
		DevicePool.addDevice(device)

		if withDiscoveryServant:
			if DiscoveryProxy.defaultProxy == None:
				discoveryAddress = Initializer.configuration.getUserID("BlueRoseService", "Discovery", device.getDeviceName())

				if discoveryAddress != None:
					DiscoveryProxy.initialize(discoveryAddress, device)
				else:
					raise Exception("ERROR: Discovery servant not configured in config file")

		if withEventHandler:
			if withDiscoveryServant:
				EventHandler.initialize(None, device)

			else: #extract from config file
				pub_sub = Initializer.configuration.getUserID("BlueRoseService", "PubSub", device.getDeviceName())

				if pub_sub != None:
					EventHandler.initialize(pub_sub, device)
				else:
					raise Exception("ERROR: PubSub servant not configured in config file")

	@classmethod
	def initializeServant(cls, servant, device, registerToDiscover=True):
		DevicePool.initialize()
		DevicePool.addDevice(device)

		identity = servant.identity
		address = Initializer.configuration.getAddress(identity.category, identity.idName, device.getDeviceName())
		port = Initializer.configuration.getPort(identity.category, identity.idName, device.getDeviceName())
		if port == None:
			raise Exception("ERROR: Port not specified in config file")

		#set as servant
		device.setServantIdentifier(address)
		device.setServant(port)

		if registerToDiscover:
			if DiscoveryProxy.defaultProxy == None:
				discoveryAddress = Initializer.configuration.getUserID("BlueRoseService", "Discovery", device.getDeviceName())

				if discoveryAddress != None:
					DiscoveryProxy.initialize(discoveryAddress, device)
				else:
					raise Exception("ERROR: Discovery servant not configured in config file")

			DiscoveryProxy.defaultProxy.addRegistration(servant.getIdentifier(), device.getServantIdentifier())

		DevicePool.registerObjectServant(servant)

	@classmethod
	def destroy(cls):
		DevicePool.destroy()
		DiscoveryProxy.destroy()
		EventHandler.destroy()
		