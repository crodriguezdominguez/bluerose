# encoding: utf-8
"""
ICommunicationDevice.py

Created by Carlos Rodriguez Dominguez on 2010-04-29.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

class ICommunicationDevice(object):
	"""Common interface for declaring a communication device."""
	
	def openConnection(self, userID, pars=None):
		"""Open a new connection between local user and a remote one with pars parameters."""
		pass
		
	def isConnectionOpenned(self, userID):
		"""Check whether the connection to an specific user is openned or not."""
		pass

	def closeConnection(self, userID):
		"""Close a connection between local user and a remote one."""
		pass

	def closeAllConnections(self):
		"""Close all connections in this transmission channel."""
		pass

	def write(self, userID, message):
		"""Send a message to a remote user."""
		pass

	def read(self, userID):
		"""Receive a message from a sender."""
		pass

	def setPriority(self, priority):
		"""Set the priority of the transmission device in the device pool."""
		pass

	def getPriority(self):
		"""Get the priority of the transmission device in the device pool."""
		pass

	def getNeighbours(self):
		"""Get the identifier of the neighbour users."""
		pass

	def broadcast(self, message):
		"""Broadcast a message to all neighbour users."""
		pass

	def isBlockantConnection(self):
		"""Check if writing or reading is blockant in this device."""
		pass

	def isAvailable(self):
		"""Check if the transmission of data is possible."""
		pass
		
	def isUserAvailable(self, userID):
		"""Check if the transmission of data to an specific user is possible."""
		pass

	def isConnectionOriented(self):
		"""Check if the communication device is connection oriented."""
		pass

	def setServantIdentifier(self, servantID):
		"""Set the identifier to be used for the servant (i.e. an IP in TCP)."""
		pass

	def setServant(self, multiplexingId):
		"""Set the device as a servant."""
		pass

	def waitForConnections(self):
		"""Wait for incoming connections."""
		pass

	def getDeviceName(self):
		"""Return the name of communication device."""
		pass

	def getServantIdentifier(self):
		"""Return the identifier of the servant."""
		pass
