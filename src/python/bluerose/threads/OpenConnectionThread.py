# encoding: utf-8
"""
OpenConnectionThread.py

Created by Carlos Rodriguez Dominguez on 2010-05-10.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

import sys
import time
import threading

class OpenConnectionThread(threading.Thread):
	def __init__(self, device, userID, maxRetries=sys.maxint, parameters=None):
		self.device = device
		self.userID = userID
		self.parameters = parameters
		self.maxRetries = maxRetries
		threading.Thread.__init__(self)

	def run(self):
		if self.maxRetries == sys.maxint:
			while True:
				try:
					self.device.openConnection(self.userID, self.parameters)
					break
				except Exception:
					pass
				
				time.sleep(1)
		else:
			for i in range(self.maxRetries):
				try:
					self.device.openConnection(self.userID, self.parameters);
					break
				except Exception:
					pass

				time.sleep(1)

				if i >= self.maxRetries:
					break
