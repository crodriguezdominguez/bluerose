#!/usr/bin/env python
# encoding: utf-8
"""
Value.py

Created by Carlos Rodriguez Dominguez on 2010-05-01.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.ByteStreamWriter import *
from bluerose.ByteStreamReader import *
from bluerose.Comparison import *
from bluerose.Marshallable import *

class Value(Marshallable):
	BYTE_TYPE=0
	SHORT_TYPE=1
	INTEGER_TYPE=2
	LONG_TYPE=3
	BOOLEAN_TYPE=4
	FLOAT_TYPE=5
	DOUBLE_TYPE=6
	STRING_TYPE=7
	SEQ_BYTE_TYPE=8
	SEQ_SHORT_TYPE=9
	SEQ_INTEGER_TYPE=10
	SEQ_LONG_TYPE=11
	SEQ_BOOLEAN_TYPE=12
	SEQ_FLOAT_TYPE=13
	SEQ_DOUBLE_TYPE=14
	SEQ_STRING_TYPE=15
	OTHER_TYPE=16
	NULL_TYPE=17
	
	__hash__ = Marshallable.__hash__
	
	def __init__(self, valueType=NULL_TYPE):
		self.type = valueType
		self.rawValue = []
		
	def setByte(self, value):
		self.type = Value.BYTE_TYPE
		writer = ByteStreamWriter()
		writer.writeByte(value)
		self.rawValue = writer.toList()

	def setInteger(self, value):
		self.type = Value.INTEGER_TYPE
		writer = ByteStreamWriter()
		writer.writeInteger(value)
		self.rawValue = writer.toList()
		
	def setShort(self, value):
		self.type = Value.SHORT_TYPE
		writer = ByteStreamWriter()
		writer.writeShort(value)
		self.rawValue = writer.toList()
		
	def setLong(self, value):
		self.type = Value.LONG_TYPE
		writer = ByteStreamWriter()
		writer.writeLong(value)
		self.rawValue = writer.toList()
		
	def setBoolean(self, value):
		self.type = Value.BOOLEAN_TYPE
		writer = ByteStreamWriter()
		writer.writeBoolean(value)
		self.rawValue = writer.toList()
		
	def setFloat(self, value):
		self.type = Value.FLOAT_TYPE
		writer = ByteStreamWriter()
		writer.writeFloat(value)
		self.rawValue = writer.toList()
		
	def setDouble(self, value):
		self.type = Value.DOUBLE_TYPE
		writer = ByteStreamWriter()
		writer.writeDouble(value)
		self.rawValue = writer.toList()
		
	def setString(self, value):
		self.type = Value.STRING_TYPE
		writer = ByteStreamWriter()
		writer.writeUTF8String(value)
		self.rawValue = writer.toList()

	def setByteSeq(self, value):
		self.type = Value.SEQ_BYTE_TYPE
		writer = ByteStreamWriter()
		writer.writeByteSeq(value)
		self.rawValue = writer.toList()

	def setIntegerSeq(self, value):
		self.type = Value.SEQ_INTEGER_TYPE
		writer = ByteStreamWriter()
		writer.writeIntegerSeq(value)
		self.rawValue = writer.toList()
		
	def setShortSeq(self, value):
		self.type = Value.SEQ_SHORT_TYPE
		writer = ByteStreamWriter()
		writer.writeShortSeq(value)
		self.rawValue = writer.toList()

	def setLongSeq(self, value):
		self.type = Value.SEQ_LONG_TYPE
		writer = ByteStreamWriter()
		writer.writeLongSeq(value)
		self.rawValue = writer.toList()
		
	def setBooleanSeq(self, value):
		self.type = Value.SEQ_BOOLEAN_TYPE
		writer = ByteStreamWriter()
		writer.writeBooleanSeq(value)
		self.rawValue = writer.toList()
	
	def setFloatSeq(self, value):
		self.type = Value.SEQ_FLOAT_TYPE
		writer = ByteStreamWriter()
		writer.writeFloatSeq(value)
		self.rawValue = writer.toList()

	def setDoubleSeq(self, value):
		self.type = Value.SEQ_DOUBLE_TYPE
		writer = ByteStreamWriter()
		writer.writeDoubleSeq(value)
		self.rawValue = writer.toList()

	def setStringSeq(self, value):
		self.type = Value.SEQ_STRING_TYPE
		writer = ByteStreamWriter()
		writer.writeUTF8StringSeq(value)
		self.rawValue = writer.toList()
		
	def getByte(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.read()

	def getInteger(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readInteger()

	def getShort(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readShort()

	def getLong(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readLong()

	def getBoolean(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readBoolean()

	def getFloat(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readFloat()

	def getDouble(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readDouble()

	def getString(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readUTF8String()

	def getByteSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readByteSeq()

	def getIntegerSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readIntegerSeq()

	def getShortSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readShortSeq()

	def getLongSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readLongSeq()

	def getBooleanSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readBooleanSeq()

	def getFloatSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readFloatSeq()

	def getDoubleSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readDoubleSeq()

	def getStringSeq(self):
		reader = ByteStreamReader(self.rawValue)
		return reader.readUTF8StringSeq()

	def __lt__(self, otherValue):
		if type(self) != type(otherValue):
			return False
		
		if otherValue.type != self.type:
			return False

		if self.type == Value.BYTE_TYPE: 
			return self.getByte() < otherValue.getByte()
			
		elif self.type == Value.SHORT_TYPE: 
			return self.getShort() < otherValue.getShort()
			
		elif self.type == Value.INTEGER_TYPE:
			return self.getInteger() < otherValue.getInteger()
			
		elif self.type == Value.LONG_TYPE:
			return self.getLong() < otherValue.getLong()
			
		elif self.type == Value.FLOAT_TYPE:
			return self.getFloat() < otherValue.getFloat()
			
		elif self.type == Value.DOUBLE_TYPE:
			return self.getDouble() < otherValue.getDouble()
			
		elif self.type == Value.STRING_TYPE:
			return self.getString() < otherValue.getString()
			
		elif self.type == Value.SEQ_BYTE_TYPE:
			return self.getByteSeq() < otherValue.getByteSeq()
			
		elif self.type == Value.SEQ_SHORT_TYPE:
			return self.getShortSeq() < otherValue.getShortSeq()
			
		elif self.type == Value.SEQ_INTEGER_TYPE:
			return self.getIntegerSeq() < otherValue.getIntegerSeq()
			
		elif self.type == Value.SEQ_LONG_TYPE:
			return self.getLongSeq() < otherValue.getLongSeq()
			
		elif self.type == Value.SEQ_FLOAT_TYPE:
			return self.getFloatSeq() < otherValue.getFloatSeq()
			
		elif self.type == Value.SEQ_DOUBLE_TYPE:
			return self.getDoubleSeq() < otherValue.getDoubleSeq()
			
		elif self.type == Value.SEQ_STRING_TYPE:
			return self.getStringSeq() < otherValue.getStringSeq()
			
		else:
			return False

	def __gt__(self, otherValue):
		if type(self) != type(otherValue):
			return False
		
		if otherValue.type != self.type:
			return False

		if self.type == Value.BYTE_TYPE: 
			return self.getByte() > otherValue.getByte()
			
		elif self.type == Value.SHORT_TYPE: 
			return self.getShort() > otherValue.getShort()
			
		elif self.type == Value.INTEGER_TYPE:
			return self.getInteger() > otherValue.getInteger()
			
		elif self.type == Value.LONG_TYPE:
			return self.getLong() > otherValue.getLong()
			
		elif self.type == Value.FLOAT_TYPE:
			return self.getFloat() > otherValue.getFloat()
			
		elif self.type == Value.DOUBLE_TYPE:
			return self.getDouble() > otherValue.getDouble()
			
		elif self.type == Value.STRING_TYPE:
			return self.getString() > otherValue.getString()
			
		elif self.type == Value.SEQ_BYTE_TYPE:
			return self.getByteSeq() > otherValue.getByteSeq()
			
		elif self.type == Value.SEQ_SHORT_TYPE:
			return self.getShortSeq() > otherValue.getShortSeq()
			
		elif self.type == Value.SEQ_INTEGER_TYPE:
			return self.getIntegerSeq() > otherValue.getIntegerSeq()
			
		elif self.type == Value.SEQ_LONG_TYPE:
			return self.getLongSeq() > otherValue.getLongSeq()
			
		elif self.type == Value.SEQ_FLOAT_TYPE:
			return self.getFloatSeq() > otherValue.getFloatSeq()
			
		elif self.type == Value.SEQ_DOUBLE_TYPE:
			return self.getDoubleSeq() > otherValue.getDoubleSeq()
			
		elif self.type == Value.SEQ_STRING_TYPE:
			return self.getStringSeq() > otherValue.getStringSeq()
			
		else:
			return False

	def __eq__(self, otherValue):
		if type(self) != type(otherValue):
			return False
		
		if otherValue.type != self.type:
			return False

		if self.rawValue == otherValue.rawValue:
			return True
		else:
			return False

	def __ne__(self, otherValue):
		return not (self==otherValue)

	def __le__(self, otherValue):
		return not (self > otherValue)

	def __ge__(self, otherValue):
		return not (self < otherValue)
		
	def __str__(self):
		if self.type == Value.BYTE_TYPE: 
			return str(self.getByte())
			
		elif self.type == Value.SHORT_TYPE: 
			return str(self.getShort())
			
		elif self.type == Value.INTEGER_TYPE:
			return str(self.getInteger())
			
		elif self.type == Value.LONG_TYPE:
			return str(self.getLong())
			
		elif self.type == Value.FLOAT_TYPE:
			return str(self.getFloat())
			
		elif self.type == Value.DOUBLE_TYPE:
			return str(self.getDouble())
			
		elif self.type == Value.STRING_TYPE:
			return self.getString()
			
		elif self.type == Value.SEQ_BYTE_TYPE:
			return str(self.getByteSeq())
			
		elif self.type == Value.SEQ_SHORT_TYPE:
			return str(self.getShortSeq())
			
		elif self.type == Value.SEQ_INTEGER_TYPE:
			return str(self.getIntegerSeq())
			
		elif self.type == Value.SEQ_LONG_TYPE:
			return str(self.getLongSeq())
			
		elif self.type == Value.SEQ_FLOAT_TYPE:
			return str(self.getFloatSeq())
			
		elif self.type == Value.SEQ_DOUBLE_TYPE:
			return str(self.getDoubleSeq())
			
		elif self.type == Value.SEQ_STRING_TYPE:
			return str(self.getStringSeq())
			
		else:
			return ""

	def marshall(self, writer):
		writer.writeSize(self.type)
		writer.writeByteSeq(self.rawValue)

	def unmarshall(self, reader):
		self.type = reader.readSize()
		self.rawValue = reader.readByteSeq()

