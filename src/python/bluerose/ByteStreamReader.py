# encoding: utf-8
"""
ByteStreamReader.py

Created by Carlos Rodriguez Dominguez on 2010-04-25.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from array import *
from struct import *
from ByteStreamWriter import *
import codecs

class ByteStreamReader(object):
	def __init__(self, buffer):
		if type(buffer) == array:
			self.buffer = buffer
		elif type(buffer) == list:
			self.buffer = array("B", buffer)
		elif type(buffer) == ByteStreamWriter:
			self.buffer = array("B", buffer.toList())
		else:
			raise Exception("bytestreamreader", "Buffer type is not a list or an array")
			
		self.index = 0
		
	def read(self):
		res = self.buffer[self.index]
		self.index += 1
		return res
		
	def readn(self, n):
		res = []
		for i in range(n):
			res.append(self.read())
		return array("B", res)
		
	def skip(self, n):
		self.index += n
	
	def readToEnd(self):
		res = self.buffer[self.index:]
		self.index = len(self.buffer)
		return res
	
	def readBoolean(self):
		res = self.read()
		if res == 0:
			return False
		else:
			return True
	
	def readShort(self):
		buf = self.readn(2)
		res, = unpack('<h', buf)
		return res
	
	def readInteger(self):
		buf = self.readn(4)
		res, = unpack('<i', buf)
		return res
		
	def readLong(self):
		buf = self.readn(8)
		res, = unpack('<l', buf)
		return res
		
	def readFloat(self):
		buf = self.readn(4)
		res, = unpack('<f', buf)
		return res
		
	def readDouble(self):
		buf = self.readn(8)
		res, = unpack('<d', buf)
		return res		

	def readSize(self):
		size = self.read()
		if size == 255:
			return self.readInteger()
		elif size == -1:
			return 0
		else:
			return size;
			
	def readUTF8String(self):
		size = self.readSize()
		res = ""
		for i in range(size):
			res += chr(self.read())
		
		res = res.decode("utf-8")
		return res
			
	def readString(self):
		size = self.readSize()
		res = ""
		for i in range(size):
			res += chr(self.read())
		return res

	def readByteSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.read())
		return res

	def readBooleanSeq(self):
		size = self.readsize()
		res = []
		for i in range(size):
			res.append(self.readBoolean())
		return res
			
	def readShortSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.readShort())
		return res
		
	def readIntegerSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.readInteger())
		return res

	def readLongSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.readLong())
		return res
					
	def readFloatSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.readDouble())
		return res
		
	def readDoubleSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.readDouble())
		return res
		
	def readUTF8StringSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.readUTF8String())
		return res
			
	def readStringSeq(self):
		size = self.readSize()
		res = []
		for i in range(size):
			res.append(self.readString())
		return res
		
	def readDictionary(self):
		res = dict()
		size = self.readSize()
		
		for i in range(size):
			key = self.readString()
			value = self.readByteSeq()
			res[key] = value
		
		return res
			