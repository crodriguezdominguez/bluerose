# encoding: utf-8
"""
TcpCompatibleDevice.py

Created by Carlos Rodriguez Dominguez on 2010-05-12.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from TcpClient import *
from bluerose.ByteStreamReader import *
from bluerose.ICommunicationDevice import *
from bluerose.messages.MessageHeader import *
from bluerose.messages.RequestMessage import *
from bluerose.threads.ReplyReadThread import *
from bluerose.threads.RequestReadThread import *
import threading
import socket
import netifaces

class TcpCompatibleDevice(ICommunicationDevice, threading.Thread):
	"""
	Module for transferring messages through the system
	default TCP transmission interface.
	"""
		
	def __init__(self):
		self.clients = dict()
		self.clientThreads = dict()
		self.servantAddress = ""
		self.isServant = False
		self.servantPort = -1
		self.servantSocket = -1
		self.priority = 0
		
		threading.Thread.__init__(self)
		
	def __str__(self):
		return self.getDeviceName()

	def broadcast(self, message):
		neighbours = self.getNeighbours()

		for neighbour in neighbours:
			self.write(neighbour, message)

	def closeAllConnections(self):
		del self.clients
		del self.clientThreads
		self.clients = dict()
		self.clientThreads = dict()

	def closeConnection(self, userID):
		if self.isConnectionOpenned(userID):
			try:
				del self.clients[userID]
				del self.clientThreads[userID]
			except KeyError:
				pass

	def getDeviceName(self):
		return "TcpCompatibleDevice"

	def getNeighbours(self):
		res = []
		for key in self.clients:
			res.append(key)
		return res

	def getServantIdentifier(self):
		return self.servantAddress

	def isAvailable(self):
		for key in self.clients:
			if self.clients[key].isOpenned():
				return True
		return False

	def isBlockantConnection(self):
		return True

	def isConnectionOpenned(self, userID):
		cli = self.clients.get(userID)
		if cli == None:
			return False
		else:
			return cli.isOpenned()

	def isConnectionOriented(self):
		return True

	def isUserAvailable(self, userID):
		return self.isConnectionOpenned(userID)

	def openConnection(self, userID, parameters=None):
		if self.clients.get(userID) != None:
			return

		strs = userID.split(":")
		host = strs[0]
		port = int(strs[1])

		self.clients[userID] = TcpClient(host, port)

	def read(self, senderID):
		cli = self.clients[senderID]
		return cli.read()

	def setServantIdentifier(self, servantID):
		self.servantAddress = servantID

	def setServant(self, multiplexingId):
		self.isServant = True
		self.servantPort = int(multiplexingId)

		if self.servantAddress == None and self.servantAddress == "" and self.servantAddress == "localhost" and self.servantAddress == "127.0.0.1":
			#Get localhost IP
			interfaces = netifaces.interfaces()
			fin = False
			for dev in interfaces:
				if not "lo" in dev and len(dev) <= 3:
					addresses = netifaces.ifaddresses(dev).get(netifaces.AF_INET)
			
					if addresses != None:
						for addr in addresses:
							aux = addr.get('addr')
							if aux != None:
								self.servantAddress = str(aux)+":"+str(self.servantPort)

								if not self.servantAddress.startswith("127"):
									if not self.servantAddress.startswith("0.0.0.0") and not ":" in aux:
										fin = True
										break
							
				if fin:
					break

			if not fin:
				print("Warning: Only local connections will be allowed. Please, check your network configuration")
		else:
			results = self.servantAddress.split(":")
			if len(results) < 2:
				self.servantAddress = self.servantAddress+":"+str(self.servantPort)

		self.start()

	def waitForConnections(self):
		self.join()

	def write(self, readerID, data):
		cli = self.clients.get(readerID)
		if cli == None:
			return False

		if data[8] == MessageHeader.REQUEST_MSG:
			msg = RequestMessage(data)

			if msg.header.mode == MessageHeader.TWOWAY_MODE:
				if not readerID in self.clientThreads:
					rth = ReplyReadThread(self, readerID)
					self.clientThreads[readerID] = rth
					rth.start()

		return cli.write(data)
		
	def setPriority(self, priority):
		self.priority = priority

	def getPriority(self):
		return self.priority

	def run(self):
		self.servantSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
		self.servantSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.servantSocket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
		self.servantSocket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

		self.servantSocket.bind(('', self.servantPort))
		self.servantSocket.listen(socket.SOMAXCONN)

		while True:
			newsockfd, info = self.servantSocket.accept()
			
			if newsockfd != None:
				userID = str(info[0])+":"+str(info[1])
				print("Connection opened: "+userID)

				self.clients[userID] = TcpClient("", "", newsockfd)

				th = RequestReadThread(self, userID)
				th.start()
			
