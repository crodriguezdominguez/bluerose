# encoding: utf-8
"""
RequestMessage.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from Message import *
from Encapsulation import *
from RequestMessageHeader import *
from bluerose.ByteStreamReader import *

class RequestMessage(Message):
	"""A request message."""
	
	def __init__(self, bytes=None):
		"""Initialize the request message with optional bytes as content"""
		
		Message.__init__(self)
		self.header = RequestMessageHeader()
		self.body = Encapsulation()
		
		if bytes != None:
			reader = ByteStreamReader(bytes)
			reader.skip(14)

			self.header.requestId = reader.readInteger()

			self.header.identity.idName = reader.readString()
			self.header.identity.category = reader.readString()

			self.header.facet = reader.readStringSeq()

			self.header.operation = reader.readString()

			self.header.mode = reader.read()

			size = reader.readSize()
			for i in range(size):
				key = reader.readString()
				value = reader.readString()
				self.header.context[key] = value

			self.body.size = reader.readInteger()

			self.body.major = reader.read()
			self.body.minor = reader.read()

			self.body.byteCollection = reader.readToEnd()

