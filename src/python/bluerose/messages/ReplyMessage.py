# encoding: utf-8
"""
ReplyMessage.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from Message import *
from Encapsulation import *
from ReplyMessageHeader import *
from bluerose.ByteStreamReader import *

class ReplyMessage(Message):
	"""A reply message."""
	
	def __init__(self, bytes=None):
		"""Initialize the reply message with optional bytes as content"""
		
		Message.__init__(self)
		self.header = ReplyMessageHeader()
		self.body = Encapsulation()
		
		if bytes != None:
			reader = ByteStreamReader(bytes)
			reader.skip(14)

			self.header.requestId = reader.readInteger()
			self.header.replyStatus = reader.read()

			self.body.size = reader.readInteger()

			self.body.major = reader.read()
			self.body.minor = reader.read()

			self.body.byteCollection = reader.readToEnd()

