# encoding: utf-8
"""
CloseConnectionMessageHeader.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from MessageHeader import *

class CloseConnectionMessageHeader(MessageHeader):
	"""
	Header for a connection closing message. 
	Definition taken from ICE (zeroc.com) for compatibility reasons.
	"""
	
	def __init__(self):
		MessageHeader.__init__(self)
		self.messageType = MessageHeader.CLOSE_CONNECTION_MSG
