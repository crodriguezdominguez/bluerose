# encoding: utf-8
"""
RequestMessageHeader.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.Identity import *
from MessageHeader import *
from bluerose.ByteStreamWriter import *

class RequestMessageHeader(MessageHeader):
	"""
	Header for a request message. 
	Definition taken from ICE (zeroc.com) for compatibility reasons.
	"""
	
	def __init__(self):
		MessageHeader.__init__(self)
		self.messageType = MessageHeader.REQUEST_MSG
		self.requestId = 0
		self.identity = Identity()
		self.facet = []
		self.context = dict()
		self.mode = MessageHeader.TWOWAY_MODE
		self.operation = ""

	def getBytes(self):
		bytes = MessageHeader.getBytes(self)
		
		writer = ByteStreamWriter()

		writer.writeRawBytes(bytes)

		#numRquests
		writer.writeInteger(self.requestId)

		#insertar Identity
		writer.writeString(self.identity.idName)
		writer.writeString(self.identity.category)

		#insertar facet
		writer.writeStringSeq(self.facet)

		#insertar operation
		writer.writeString(self.operation)

		#insertar mode
		writer.writeByte(self.mode)

		#insertar context
		writer.writeSize(len(self.context))

		for key in self.context:
			value = self.context[key]

			writer.writeString(key)
			writer.writeString(value)

		return writer.toList()
