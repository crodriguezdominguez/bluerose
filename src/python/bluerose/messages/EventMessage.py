#!/usr/bin/env python
# encoding: utf-8
"""
EventMessage.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from Message import *
from bluerose.ByteStreamWriter import *
from EventMessageHeader import *

class EventMessage(Message):
	"""An event message."""
	
	def __init__(self, bytes=None):
		Message.__init__(self)
		self.header = EventMessageHeader()
		
		if bytes == None:
			self.event = []
		else:
			self.event = bytes
	
	def getBytes(self):
		writer = ByteStreamWriter()

		header_bytes = self.header.getBytes()

		self.header.messageSize = len(header_bytes)+len(self.event)

		header_bytes = self.header.getBytes()

		writer.writeRawBytes(header_bytes)
		writer.writeRawBytes(self.event)

		return writer.toList()
