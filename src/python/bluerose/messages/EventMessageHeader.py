# encoding: utf-8
"""
EventMessageHeader.py

Created by Carlos Rodriguez Dominguez on 2010-04-30.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from MessageHeader import *

class EventMessageHeader(MessageHeader):
	"""
	Header for an event message. 
	Extension over ICE Protocol (zeroc.com).
	"""
	
	def __init__(self):
		MessageHeader.__init__(self)
		self.messageType = MessageHeader.EVENT_MSG
		