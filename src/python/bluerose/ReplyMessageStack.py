# encoding: utf-8
"""
ReplyMessageStack.py

Created by Carlos Rodriguez Dominguez on 2010-04-28.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""
from threading import Condition

class ReplyMessageStack(object):
	@classmethod
	def initialize(cls):
		ReplyMessageStack.stack = dict()
		ReplyMessageStack.waitingreply = False
		ReplyMessageStack.lock = Condition()
	
	@classmethod
	def destroy(cls):
		ReplyMessageStack.waitingreply = False
	
	@classmethod
	def produce(cls, requestid, bytes):
		ReplyMessageStack.lock.acquire()
		
		ReplyMessageStack.stack[requestid] = bytes
		
		ReplyMessageStack.lock.notifyAll()
		ReplyMessageStack.lock.release()
		
	@classmethod
	def consume(cls, requestid):
		ReplyMessageStack.lock.acquire()
		
		while not requestid in ReplyMessageStack.stack.keys():
			ReplyMessageStack.lock.wait()
			
		value = ReplyMessageStack.stack[requestid]
		
		ReplyMessageStack.lock.notifyAll()
		
		ReplyMessageStack.lock.release()
		return value
		
	@classmethod
	def isWaitingReply(cls):
		ReplyMessageStack.lock.acquire()
		key = ReplyMessageStack.waitingreply
		ReplyMessageStack.lock.release()
		
		return key