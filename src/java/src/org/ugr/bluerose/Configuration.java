package org.ugr.bluerose;

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class Configuration 
{
	private Document doc;
	private DocumentBuilder builder;

	public Configuration(java.io.File f) throws Exception
	{
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		doc = builder.parse(f);
	}
	
	public Configuration(java.io.FileDescriptor desc) throws Exception
	{
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		doc = builder.parse(new java.io.FileInputStream(desc));
	}
	
	public Configuration(java.io.InputStream is) throws Exception
	{
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		doc = builder.parse(is);
	}
	
	public Document getDocument()
	{
		return doc;
	}
	
	public void save(java.io.OutputStream os) throws Exception
	{
		/*
		CAN'T USE THIS DUE TO ANDROID INCOMPATIBILITY
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		//initialize StreamResult with File object to save to file
		StreamResult result = new StreamResult(file);
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);*/
	     
		//we have to bind the new file with a FileOutputStream
        java.io.PrintWriter writer = null;
        try{
        	writer = new java.io.PrintWriter(os);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return;
        }
        
		writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.println("<configuration>");
		
		//write devices...
		writer.println("\t<devices>");
		java.util.Vector<String> devices = this.getDevices();
		for (String device : devices)
		{
			String result = "\t\t<device name=\"";
			result += device;
			result += "\" module=\"";
			result += this.getModuleForDevice(device);
			result += "\" />";
			
			writer.println(result);
		}
		
		writer.println("\t</devices>");
		
		//write services...
		writer.println("\t<services>");
		
		java.util.Vector<String> categories = this.getCategories();
		for (String category : categories)
		{
			writer.println("\t\t<category name=\""+category+"\">");
			
			//get services for category
			java.util.Vector<String> services = this.getServicesForCategory(category);
			for (String service : services)
			{
				writer.println("\t\t\t<service name=\""+service+"\">");
				
				java.util.Vector<String> devs = this.getDevicesForService(category, service);
				for (String device : devs)
				{
					String result = "\t\t\t\t<device name=\""+device+"\" ";
					result += "address=\""+this.getAddress(category, service, device)+"\" ";
					result += "port=\""+this.getPort(category, service, device)+"\" />";
					
					writer.println(result);
				}
				
				writer.println("\t\t\t</service>");
			}
			
			writer.println("\t\t</category>");
		}
		
		writer.println("\t</services>");
		
		//end writing...
		writer.println("</configuration>");
		writer.flush();
        writer.close();
	}
	
	public boolean existsDevice(String device)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for (int i = 0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("devices"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String c = ((Element)cur_node2).getAttribute("name");
							if (c != null)
							{
								if (c.equals(device)) return true;
							}
						}
					}
					
					break;
				}
			}
		}
		
		return false;
	}
	
	public void setDevice(String device, String module)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for (int i = 0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("devices"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String c = ((Element)cur_node2).getAttribute("name");
							if (c != null)
							{
								if (c.equals(device))
								{
									//the device exists...
									((Element)cur_node2).setAttribute("module", module);
									return;
								}
							}
						}
					}
					
					//the device does not exist
					Element n = doc.createElement("device");
					n.setAttribute("module", module);
					n.setAttribute("name", device);
					cur_node.appendChild(n);
					
					return;
				}
			}
		}
	}
	
	public java.util.Vector<String> getDevices()
	{
		java.util.Vector<String> res = new java.util.Vector<String>();
		
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for (int i = 0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("devices"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String c = ((Element)cur_node2).getAttribute("name");
							if (c != null)
							{
								res.add(c);
							}
						}
					}
					
					break;
				}
			}
		}
		
		return res;
	}
	
	public String getModuleForDevice(String device)
	{	
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("devices"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name = ((Element)cur_node2).getAttribute("name");
							if (name != null)
							{
								if (name.equals(device))
								{
									name = ((Element)cur_node2).getAttribute("module");
									
									if (name != null)
									{
										return name;
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public void setCategory(String category)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name = ((Element)cur_node2).getAttribute("name");
							
							if (name != null)
							{
								//exists...
								if (name.equals(category))
								{
									((Element)cur_node2).setAttribute("name", category);
									return;
								}
							}
						}
					}
					
					//does not exist...
					Element n = doc.createElement("category");
					n.setAttribute("name", category);
					cur_node.appendChild(n);
					
					return;
				}
			}
		}
	}
	
	public java.util.Vector<String> getCategories()
	{
		java.util.Vector<String> res = new java.util.Vector<String>();
		
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name = ((Element)cur_node2).getAttribute("name");
							
							if (name != null)
							{
								res.add(name);
							}
						}
					}
					
					break;
				}
			}
		}
		
		return res;
	}
	
	public void setAddressForService(String category, String service, String device, String address)
	{
		String port = this.getPort(category, service, device);
		if (port != null)
			this.setDeviceForService(category, service, device, address, port);
		else this.setDeviceForService(category, service, device, address, "");
	}
	
	public void setPortForService(String category, String service, String device, String port)
	{
		String address = this.getAddress(category, service, device);
		if (address != null)
			this.setDeviceForService(category, service, device, address, port);
		else
			this.setDeviceForService(category, service, device, "", port);
	}
	
	public void setDeviceForService(String category, String service, String device, String address, String port)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name1 = ((Element)cur_node2).getAttribute("name");
							
							if (name1 != null)
							{
								if (name1.equals(category))
								{
									NodeList nodes3 = cur_node2.getChildNodes();
									
									for(int k=0; k<nodes3.getLength(); k++)
									{
										Node cur_node3 = nodes3.item(k);
										if (cur_node3.getNodeType() == Node.ELEMENT_NODE)
										{
											String name2 = ((Element)cur_node3).getAttribute("name");
											
											if (name2 != null)
											{
												//exists...
												if (name2.equals(service))
												{
													//check if device exists...
													NodeList nodes4 = cur_node3.getChildNodes();
													for (int w=0; w<nodes4.getLength(); w++)
													{
														Node cur_node4 = nodes4.item(w);
														if (cur_node4.getNodeType() == Node.ELEMENT_NODE)
														{
															String name3 = ((Element)cur_node4).getAttribute("name");
															if (name3.equals(device)) //exists...
															{
																Element n = (Element)cur_node4;
																n.setAttribute("address", address);
																n.setAttribute("port", port);
																return;
															}
														}
													}
													
													//did not exist...
													Element n = doc.createElement("device");
													n.setAttribute("name", device);
													n.setAttribute("address", address);
													n.setAttribute("port", port);
													cur_node3.appendChild(n);
													return;
												}
											}
										}
									}
									
									//does not exist
									
									//add service
									Element n = doc.createElement("service");
									n.setAttribute("name", service);
									
									//add device
									Element n2 = doc.createElement("device");
									n2.setAttribute("name", device);
									n2.setAttribute("address", address);
									n2.setAttribute("port", port);
									n.appendChild(n2);
									
									cur_node2.appendChild(n);
									
									return;
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void setServiceForCategory(String category, String service)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name1 = ((Element)cur_node2).getAttribute("name");
							
							if (name1 != null)
							{
								if (name1.equals(category))
								{
									NodeList nodes3 = cur_node2.getChildNodes();
									
									for(int k=0; k<nodes3.getLength(); k++)
									{
										Node cur_node3 = nodes3.item(k);
										if (cur_node3.getNodeType() == Node.ELEMENT_NODE)
										{
											String name2 = ((Element)cur_node3).getAttribute("name");
											
											if (name2 != null)
											{
												if (name2.equals(service)) //exists...
													return;
											}
										}
									}
									
									//does not exist
									Element n = doc.createElement("service");
									n.setAttribute("name", category);
									cur_node2.appendChild(n);
									return;
								}
							}
						}
					}
				}
			}
		}
	}
	
	public java.util.Vector<String> getServicesForCategory(String category)
	{	
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node = nodes.item(i);
			if (cur_node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name1 = ((Element)cur_node2).getAttribute("name");
							
							if (name1 != null)
							{
								if (name1.equals(category))
								{
									NodeList nodes3 = cur_node2.getChildNodes();
									
									java.util.Vector<String> res = new java.util.Vector<String>();
									
									for(int k=0; k<nodes3.getLength(); k++)
									{
										Node cur_node3 = nodes3.item(k);
										if (cur_node3.getNodeType() == Node.ELEMENT_NODE)
										{
											String name2 = ((Element)cur_node3).getAttribute("name");
											
											if (name2 != null)
											{
												res.add(name2);
											}
										}
									}
									
									return res;
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public java.util.Vector<String> getDevicesForService(String category, String service)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node1 = nodes.item(i);
			if (cur_node1.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node1.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node1.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name1 = ((Element)cur_node2).getAttribute("name");
							
							if (name1 != null)
							{
								if (name1.equals(category))
								{
									NodeList nodes3 = cur_node2.getChildNodes();
									
									for(int m=0; m<nodes3.getLength(); m++)
									{
										Node cur_node3 = nodes3.item(m);
										if (cur_node3.getNodeType() == Node.ELEMENT_NODE)
										{
											String name2 = ((Element)cur_node3).getAttribute("name");
											
											if (name2 != null)
											{
												if (name2.equals(service))
												{
													java.util.Vector<String> res = new java.util.Vector<String>();
													
													NodeList nodes4 = cur_node3.getChildNodes();
													for(int h=0; h<nodes4.getLength(); h++)
													{
														Node cur_node4 = nodes4.item(h);
														if (cur_node4.getNodeType() == Node.ELEMENT_NODE)
														{
															String name3 = ((Element)cur_node4).getAttribute("name");
															
															if (name3 != null)
															{
																res.add(name3);
															}
														}
													}
													
													return res;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public String getPort(String category, String service, String device)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node1 = nodes.item(i);
			if (cur_node1.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node1.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node1.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name1 = ((Element)cur_node2).getAttribute("name");
							
							if (name1 != null)
							{
								if (name1.equals(category))
								{
									NodeList nodes3 = cur_node2.getChildNodes();
									
									for(int k=0; k<nodes3.getLength(); k++)
									{
										Node cur_node3 = nodes3.item(k);
										if (cur_node3.getNodeType() == Node.ELEMENT_NODE)
										{
											String name2 = ((Element)cur_node3).getAttribute("name");
											
											if (name2 != null)
											{
												if (name2.equals(service))
												{
													NodeList nodes4 = cur_node3.getChildNodes();
													for(int m=0; m<nodes4.getLength(); m++)
													{
														Node cur_node4 = nodes4.item(m);
														if (cur_node4.getNodeType() == Node.ELEMENT_NODE)
														{
															String name3 = ((Element)cur_node4).getAttribute("name");
															
															if (name3 != null)
															{
																if (name3.equals(device))
																{
																	String result = ((Element)cur_node4).getAttribute("port");
																	
																	if (result != null)
																	{
																		return result;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public String getAddress(String category, String service, String device)
	{
		Element root_element = doc.getDocumentElement();
		NodeList nodes = root_element.getChildNodes();
		
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node cur_node1 = nodes.item(i);
			if (cur_node1.getNodeType() == Node.ELEMENT_NODE)
			{
				if (cur_node1.getNodeName().equals("services"))
				{
					//extract devices
					NodeList nodes2 = cur_node1.getChildNodes();
					
					for(int j=0; j<nodes2.getLength(); j++)
					{
						Node cur_node2 = nodes2.item(j);
						if (cur_node2.getNodeType() == Node.ELEMENT_NODE)
						{
							String name1 = ((Element)cur_node2).getAttribute("name");
							
							if (name1 != null)
							{
								if (name1.equals(category))
								{
									NodeList nodes3 = cur_node2.getChildNodes();
									
									for(int k=0; k<nodes3.getLength(); k++)
									{
										Node cur_node3 = nodes3.item(k);
										if (cur_node3.getNodeType() == Node.ELEMENT_NODE)
										{
											String name2 = ((Element)cur_node3).getAttribute("name");
											
											if (name2 != null)
											{
												if (name2.equals(service))
												{
													NodeList nodes4 = cur_node3.getChildNodes();
													for(int m=0; m<nodes4.getLength(); m++)
													{
														Node cur_node4 = nodes4.item(m);
														if (cur_node4.getNodeType() == Node.ELEMENT_NODE)
														{
															String name3 = ((Element)cur_node4).getAttribute("name");
															
															if (name3 != null)
															{
																if (name3.equals(device))
																{
																	String result = ((Element)cur_node4).getAttribute("address");
																	
																	if (result != null)
																	{
																		return result;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public String getUserID(String category, String service, String device)
	{
		String address = this.getAddress(category, service, device);
		String port = this.getPort(category, service, device);
		
		if (address != null && port != null)
		{
			return ( address+":"+port );
		}
		
		return null;
	}
}


