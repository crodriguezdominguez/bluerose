package org.ugr.bluerose;

/**
 * Identity for a request message
 */
public class Identity implements Comparable<Identity> {
	public String id_name;
	public String category;

	@Override
	public boolean equals(Object object)
	{
		try{
			Identity id = (Identity)object;
			return isEqualToIdentity(id);
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	public boolean isEqualToIdentity(Identity identity)
	{
		if ( identity.id_name.equals(id_name) && 
			 identity.category.equals(category) )
		{
			return true;
		}
			
		else return false;
	}

	public int compareTo(Identity o)
	{
		if (this.isEqualToIdentity(o))
		{
			return 0;
		}
		
		int comp = this.id_name.compareTo(o.id_name);
		
		if (comp < 0)
		{
			return -1;
		}
		if (comp > 0)
		{
			return 1;
		}
		
		return this.category.compareTo(o.category);
	}
	
	@Override
	public String toString()
	{
		return category+"::"+id_name;
	}
}
