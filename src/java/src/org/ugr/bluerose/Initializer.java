package org.ugr.bluerose;

import org.ugr.bluerose.events.EventHandler;

public class Initializer {
	public static Configuration configuration = null;

	public static void initialize(java.io.File file) throws Exception
	{
		configuration = new Configuration(file);
	}
	
	public static void initialize(java.io.FileDescriptor descriptor) throws Exception
	{
		configuration = new Configuration(descriptor);
	}
	
	public static void initialize(java.io.InputStream is) throws Exception
	{
		configuration = new Configuration(is);
	}
	
	public static void initializeNonWaitingClient(ICommunicationDevice device) throws Exception
	{
		DevicePool.initialize();
		DevicePool.addDevice(device);
		
		if (DiscoveryProxy.defaultProxy == null)
		{
			String discoveryAddress = configuration.getUserID("BlueRoseService", "Discovery", device.getDeviceName());
			
			if (discoveryAddress != null)
			{
				DiscoveryProxy.initialize(discoveryAddress, device, false);
			}
			else
			{
				throw new Exception("ERROR: Discovery servant not configured in config file");
			}
		}
		
		EventHandler.initialize(device, false);
	}

	public static void initializeClient(ICommunicationDevice device) throws Exception
	{
		initializeClient(device, true, true);
	}

	public static void initializeClient(ICommunicationDevice device, boolean discoveryServant) throws Exception
	{
		initializeClient(device, discoveryServant, true);
	}

	public static void initializeClient(ICommunicationDevice device, boolean discoveryServant, boolean eventHandler) throws Exception
	{
		DevicePool.initialize();
		DevicePool.addDevice(device);
		
		if (discoveryServant)
		{
			if (DiscoveryProxy.defaultProxy == null)
			{
				String discoveryAddress = configuration.getUserID("BlueRoseService", "Discovery", device.getDeviceName());
				
				if (discoveryAddress != null)
				{
					DiscoveryProxy.initialize(discoveryAddress, device);
				}
				else
				{
					throw new Exception("ERROR: Discovery servant not configured in config file");
				}
			}
		}
		
		if (eventHandler)
		{
			if (discoveryServant)
			{
				EventHandler.initialize(device);
			}
			
			else //extract from config file
			{
				String pub_sub = configuration.getUserID("BlueRoseService", "PubSub", device.getDeviceName());
				
				if (pub_sub != null)
				{
					EventHandler.initialize(pub_sub, device);
				}
				else
				{
					throw new Exception("ERROR: PubSub servant not configured in config file");
				}
			}
		}
	}

	public static void initializeServant(ObjectServant servant, ICommunicationDevice device) throws Exception
	{
		Initializer.initializeServant(servant, device, true);
	}

	public static void initializeServant(ObjectServant servant, ICommunicationDevice device, boolean registerToDiscover) throws Exception
	{
		DevicePool.initialize();
		DevicePool.addDevice(device);
		
		Identity identity = servant.identity;
		String address = configuration.getAddress(identity.category, identity.id_name, device.getDeviceName());
		String port = configuration.getPort(identity.category, identity.id_name, device.getDeviceName());
		if (port == null)
		{
			throw new Exception("ERROR: Port not specified in config file");
		}
		
		//set as servant
		device.setServantIdentifier(address);
		device.setServant(port);
		
		if (registerToDiscover)
		{
			if (DiscoveryProxy.defaultProxy == null)
			{
				String discoveryAddress = configuration.getUserID("BlueRoseService", "Discovery", device.getDeviceName());
				
				if (discoveryAddress != null)
				{
					DiscoveryProxy.initialize(discoveryAddress, device);
				}
				else
				{
					throw new Exception("ERROR: Discovery servant not configured in config file");
				}
			}
			
			DiscoveryProxy.defaultProxy.addRegistration(servant.getIdentifier(), device.getServantIdentifier());
		}
		
		DevicePool.registerObjectServant(servant);
	}

	public static void destroy()
	{
		DevicePool.destroy();
		DiscoveryProxy.destroy();
		EventHandler.destroy();
		//[BREventHandler destroyEventHandler];
	}
}
