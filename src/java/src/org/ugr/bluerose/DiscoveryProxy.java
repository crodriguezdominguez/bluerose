package org.ugr.bluerose;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Vector;

import org.ugr.bluerose.messages.MessageHeader;

public class DiscoveryProxy extends ObjectProxy 
{
	public static DiscoveryProxy defaultProxy;
	
	protected DiscoveryProxy() {
		super();
	}

	protected DiscoveryProxy(String servID, ICommunicationDevice dev,
			boolean wait, Dictionary<String, Vector<Byte>> pars) throws Exception {
		super(servID, dev, wait, pars);
	}

	protected DiscoveryProxy(String servID, ICommunicationDevice dev, boolean wait) throws Exception {
		super(servID, dev, wait);
	}

	protected DiscoveryProxy(String servID, ICommunicationDevice dev) throws Exception {
		super(servID, dev);
	}

	protected DiscoveryProxy(String servID) throws Exception {
		super(servID);
	}
	
	public static void initialize(String servID) throws Exception
	{
		defaultProxy = new DiscoveryProxy(servID);
		defaultProxy.identity.id_name = "Discovery";
		defaultProxy.identity.category = "BlueRoseService";
	}

	public static void initialize(String servID, ICommunicationDevice device) throws Exception
	{
		defaultProxy = new DiscoveryProxy(servID, device);
		defaultProxy.identity.id_name = "Discovery";
		defaultProxy.identity.category = "BlueRoseService";
	}
	
	public static void initialize(String servID, ICommunicationDevice device, boolean wait) throws Exception
	{
		defaultProxy = new DiscoveryProxy(servID, device, wait);
		defaultProxy.identity.id_name = "Discovery";
		defaultProxy.identity.category = "BlueRoseService";
	}
	
	public static void destroy()
	{
		defaultProxy = null;
	}

	/**
	 * Registers a servant name and an address
	 *
	 * @param name Name of the servant to register
	 * @param address Address of the servant
	 */
	public void addRegistration(String name, String address)
	{
		int reqID;
		
		currentMode = MessageHeader.TWOWAY_MODE;
		
		synchronized(mutex) {
			writer.writeString(name);
			writer.writeString(address);
		
			reqID = this.sendRequest(servantID, "addRegistration", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}

	/**
	 * Unregisters the address that is associated with a name
	 *
	 * @param name Name of the servant to unregister
	 */
	public void removeRegistration(String name)
	{
		int reqID;
		
		currentMode = MessageHeader.TWOWAY_MODE;
		
		synchronized(mutex) {
			writer.writeString(name);
		
			reqID = this.sendRequest(servantID, "removeRegistration", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}

	public String resolveName(String name)
	{
		return resolveName(name, false);
	}

	public String resolveName(String name, boolean wait)
	{
		int reqID;
		ByteStreamReader reader = null;
		
		currentMode = MessageHeader.TWOWAY_MODE;
		String result = null;
		java.util.Vector<Byte> result_bytes = null;
		
		do {
			synchronized(mutex) {
				writer.writeString(name);
			
				reqID = this.sendRequest(servantID, "resolve", writer.toVector());
				result_bytes = this.receiveReply(reqID);
				writer.reset();
			}
			
			reader = new ByteStreamReader(result_bytes);
			result = reader.readString();
			
			try {
				reader.close();
			} catch (IOException e) {
			}
					
			if (result.equals("") && wait)
			{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		} while(result.equals("") && wait);
			
		return result;
	}
	
	@Override
	public String getTypeID() {
		return "BlueRoseService::Discovery";
	}

}
