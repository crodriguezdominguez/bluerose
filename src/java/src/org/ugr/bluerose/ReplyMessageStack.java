package org.ugr.bluerose;

/**
 * Stack where all received messages are stored
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
public class ReplyMessageStack 
{	
	protected static java.util.Dictionary<Integer, java.util.Vector<Byte>> stack; /**< Stack as pairs of requestIds-Messages */
	protected static boolean waitingReply; /**< True if someone is waiting a reply */
	
	/**
	 * Initializes the stack
	 */
	public static synchronized void initialize()
	{
		stack = new java.util.Hashtable<Integer, java.util.Vector<Byte>>();
		waitingReply = false;
	}

	/**
	 * Destroys the stack
	 */
	public static synchronized void destroy()
	{
		waitingReply = false;
	}

	/**
	 * Adds a new message to the stack
	 *
	 * @param requestId Id of the request
	 * @param bytes Message
	 */
	@SuppressWarnings("unchecked")
	public static void produce(int requestId, java.util.Vector<Byte> bytes)
	{
		Integer key = new Integer(requestId);
		
		synchronized(ReplyMessageStack.class) {
			stack.put(key, (java.util.Vector<Byte>)bytes.clone());
			ReplyMessageStack.class.notifyAll();
		}
	}

	/**
	 * Removes a message from the stack. If the message does not exist,
	 * it blocks undefinitely.
	 *
	 * @param requestId Id of the request
	 * @return Message removed from the stack
	 */
	public static java.util.Vector<Byte> consume(int requestId)
	{
		Integer key = new Integer(requestId);
		java.util.Vector<Byte> value = null;
		
		synchronized (ReplyMessageStack.class) {
			
			while( (value = stack.get(key)) == null )
			{
				try {
					ReplyMessageStack.class.wait();
				} catch (InterruptedException e) {
				}
			}
			
			value = stack.remove(key);
			
			ReplyMessageStack.class.notifyAll();
			
			return value;
		}
	}

	/**
	 * Returns whether somebody is waiting a reply or not
	 *
	 * @return True if someone is waiting. False otherwise else.
	 */
	public static synchronized boolean isWaitingReply()
	{
		return waitingReply;
	}
}
