package org.ugr.bluerose;

import org.ugr.bluerose.messages.MessageHeader;
import org.ugr.bluerose.messages.ReplyMessage;
import org.ugr.bluerose.messages.RequestMessage;
import org.ugr.bluerose.messages.RequestMessageHeader;
import org.ugr.bluerose.threads.OpenConnectionThread;


/**
 * Proxy for transparently connecting to a servant
 *
 * @author Carlos Rodriguez Dominguez
 * @date 11-11-2009
 */
public abstract class ObjectProxy 
{
	protected static final int MAX_REQUEST_ID = 1000000;
	
	protected static AsyncCallback asyncThread = null; /**< Callback thread for asynchronous methods */
	protected static int numInstances = 0; /**< Number of instances of the object */
	protected static int requestId = 0; /**< Request Id */
	
	protected static ByteStreamWriter writer = new ByteStreamWriter();
	protected static Object mutex = new Object();
	
	public ICommunicationDevice device; /**< Interface for transferring information */
	public org.ugr.bluerose.threads.OpenConnectionThread openThread; /**< Thread for openning connections in case the servant is disconnected */
	public String servantID; /**< Id of the servant */
	public Identity identity; /**< Identity of the object */
	public byte currentMode; /**< Current operational mode. @see MessageHeader.h */
	
	public ObjectProxy()
	{
		device = null;
		openThread = null;
		identity = new Identity();
		
		synchronized(ObjectProxy.class) {
			numInstances++;
		}
		
		//interface->openConnection(_servantID, connParameters);
		currentMode = MessageHeader.TWOWAY_MODE;
	}
	
	/**
	 * Constructor. Uses the current interface of the DevicePool.
	 * If that interface changes, it also changes for the object.
	 *
	 * @param servID Id of the servant to connect to
	 */
	public ObjectProxy(String servID) throws Exception
	{
		initalizeAttributes(servID, null, true, null);
	}
	
	/**
	 * Constructor. Uses the current interface of the DevicePool.
	 * If that interface changes, it also changes for the object.
	 *
	 * @param servID Id of the servant to connect to
	 * @param dev Interface to connect to. It it's NULL, it will connect to the current interface in the
	 *               interface pool.
	 */
	public ObjectProxy(String servID, ICommunicationDevice dev) throws Exception
	{
		initalizeAttributes(servID, dev, true, null);
	}
	
	/**
	 * Constructor. Uses the current interface of the DevicePool.
	 * If that interface changes, it also changes for the object.
	 *
	 * @param servID Id of the servant to connect to
	 * @param dev Interface to connect to. It it's NULL, it will connect to the current interface in the
	 *               interface pool.
	 * @param wait If it's true, it will wait for the servant to be alive
	 */
	public ObjectProxy(String servID, ICommunicationDevice dev, boolean wait) throws Exception
	{
		initalizeAttributes(servID, dev, wait, null);
	}

	/**
	 * Constructor. Uses the current interface of the DevicePool.
	 * If that interface changes, it also changes for the object.
	 *
	 * @param servID Id of the servant to connect to
	 * @param dev Interface to connect to. It it's NULL, it will connect to the current interface in the
	 *               interface pool.
	 * @param wait If it's true, it will wait for the servant to be alive
	 * @param pars Connection parameters
	 */
	public ObjectProxy(String servID, ICommunicationDevice dev, boolean wait, java.util.Dictionary<String, java.util.Vector<Byte>> pars) throws Exception
	{
		initalizeAttributes(servID, dev, wait, pars);
	}
	
	@Override
	public void finalize() throws Throwable
	{
		openThread.interrupt();
		
		synchronized(ObjectProxy.class) {
			numInstances--;
			if (numInstances <= 0)
			{
				asyncThread.interrupt();
				asyncThread = null;
				numInstances = 0;
			}
		}
	
		device = null;
		
		super.finalize();
	}

	/**
	 * Auxiliar method for constructors
	 */
	protected void initalizeAttributes(String servID, ICommunicationDevice dev, boolean wait, java.util.Dictionary<String, java.util.Vector<Byte>> pars)
		throws Exception
	{
		identity = new Identity();
		servantID = servID;
		device = dev;
		openThread = null;
		
		synchronized(ObjectProxy.class) {
			numInstances++;
		}
		
		ICommunicationDevice iface_aux = (device != null) ? device : DevicePool.getCurrentDevice();
		if (!iface_aux.isConnectionOpenned(servantID))
		{
			openThread = new org.ugr.bluerose.threads.OpenConnectionThread(iface_aux, servantID, Integer.MAX_VALUE, null);
			openThread.start();
			
			if (wait)
			{ 
				try {
					openThread.join();
				} catch (InterruptedException e) {
				}
				openThread = null;
			}
			else iface_aux.openConnection(servantID);
		}
		else
		{
			iface_aux.openConnection(servantID);
		}
		
		//interface->openConnection(_servantID, connParameters);
		currentMode = MessageHeader.TWOWAY_MODE;
	}

	/**
	 * Sends a request to a servant
	 * 
	 * @param servantID ID of the servant to send the request to
	 * @param operation Operation of the request
	 * @param request Message of the request
	 * @return Request Identifier for receiving the reply message
	 */
	public int sendRequest(String servID, String oper, java.util.Vector<Byte> request)
	{
		boolean res;
		
		RequestMessage msg = new RequestMessage();
		RequestMessageHeader hd = (RequestMessageHeader)msg.header;
		
		int req;
		synchronized(ObjectProxy.class) {
			requestId = ((requestId+1) % MAX_REQUEST_ID);
			req = requestId;
		}
		
		hd.requestId = req;
		hd.operation = oper;
		
		synchronized(this) {
			hd.mode = currentMode;
			hd.identity = identity;
		}
		
		msg.addToEncapsulation(request);
		
		java.util.Vector<Byte> msg_bytes = msg.getBytes();
		
		if (device == null)
			res = DevicePool.getCurrentDevice().write(servantID, msg_bytes);
		
		else res = device.write(servantID, msg_bytes);
		
		if (!res)
		{
			synchronized (this) {
			/**
			 * TODO: Crear una cache para tratar de proveer algunos mensajes
			 */
				if (openThread == null)
				{
					if (device != null)
						openThread = new OpenConnectionThread(device, servantID, Integer.MAX_VALUE, null);	
					else openThread = new OpenConnectionThread(DevicePool.getCurrentDevice(), servantID, Integer.MAX_VALUE, null);
					
					openThread.start();
				}
			}
			
			//throw "Service Not Available";
		}
		else
		{
			synchronized(this) {
				if (openThread != null)
				{
					openThread.interrupt();
					openThread = null;
				}
			}
		}
		
		return req;
	}

	/**
	 * Receives a reply. Is a blockant method.
	 *
	 * @param reqID Request Identifier of the request message
	 * @result Received Message
	 */
	public java.util.Vector<Byte> receiveReply(int reqID)
	{
		byte currMode;
		synchronized(this) {
			currMode = currentMode;
		}
		
		java.util.Vector<Byte> res = null;
		
		if (currMode == MessageHeader.TWOWAY_MODE)
		{
			java.util.Vector<Byte> recv_bytes = ReplyMessageStack.consume(reqID);
			ReplyMessage msg = new ReplyMessage(recv_bytes);
			res = msg.body.byteCollection;
		}
		
		return res;
	}

	/**
	 * Receives a message with the callback thread and treats the received
	 * message with the specified AsyncMethodCallback
	 *
	 * @param reqID Request Identifier of the request message
	 * @param method Async method callback for treating the received message
	 */
	public void receiveCallback(int reqID, AsyncMethodCallback method)
	{
		synchronized(ObjectProxy.class) {
			if (asyncThread == null)
			{
				asyncThread = new AsyncCallback();
				asyncThread.start();
			}
			
			asyncThread.pushRequestId(reqID, method);
		}
	}

	public void resolveInitialization(ICommunicationDevice dev, boolean wait, java.util.Dictionary<String, java.util.Vector<Byte>> pars)
		throws Exception
	{
		if (DiscoveryProxy.defaultProxy == null)
		{
			throw new Exception("ERROR: DiscoveryProxy not initialized");
		}
		
		String _servantID = DiscoveryProxy.defaultProxy.resolveName(this.getTypeID());
		
		if (_servantID.equals(""))
		{
			throw new Exception("Servant for the "+this.getTypeID()+" proxy has not been found");
		}
		
		servantID = _servantID;
		device = dev;
		openThread = null;
		
		ICommunicationDevice iface_aux = (dev != null) ? device : DevicePool.getCurrentDevice();
		if (!iface_aux.isConnectionOpenned(servantID))
		{
			if (wait)
			{
				openThread = new OpenConnectionThread(iface_aux, servantID, Integer.MAX_VALUE, pars);
				openThread.start();
				
				if (wait)
				{
					openThread.join();
					openThread = null;
				}
			}
			else
			{
				iface_aux.openConnection(servantID);
			}
		}
		else
		{
			iface_aux.openConnection(servantID);
		}
	}

	/**
	 * Waits for a Servant to be online
	 */
	public void waitOnlineMode()
	{
		boolean wait;
		synchronized(this) {
			wait = (openThread != null);
		}
		
		if (!wait) return;
		else
		{
			synchronized(this) {
				try {
					openThread.join();
				} catch (InterruptedException e) {
				}
				openThread = null;
			}
		}
	}

	/**
	 * Gets the type ID of the proxy
	 *
	 * @return String that represents the type ID
	 */
	public abstract String getTypeID();
}


/**
 * Callback method for an asynchronous request
 *
 * @author Carlos Rodriguez Dominguez
 * @date 15-11-2009
 */
class AsyncCallback extends Thread 
{
	protected java.util.Dictionary<Integer, AsyncMethodCallback> requestsId_callbacks; /**< stack of requestIds and asynchronous method callbacks */
	
	public AsyncCallback()
	{
		requestsId_callbacks = new java.util.Hashtable<Integer, AsyncMethodCallback>();
	}
	
	/**
	 * Adds a new request id to the async method callback stack
	 *
	 * @param reqId Request identifier for the reply
	 * @param amc Callback method that is called when the reply is received
	 */
	public synchronized void pushRequestId(int reqID, AsyncMethodCallback method)
	{
		this.requestsId_callbacks.put(new Integer(reqID), method);
		this.notifyAll();
	}
	
	@Override
	public void run()
	{
		while(true)
		{
			int size;
			synchronized(this) {
				while(this.requestsId_callbacks.size() <= 0)
				{
					try {
						this.wait();
					} catch (InterruptedException e) {
					}
				}
				
				size = this.requestsId_callbacks.size();
			}
			
			if (size > 0)
			{
				java.util.Enumeration<Integer> e;
				int reqID;
				Integer key;
				AsyncMethodCallback amc;
				synchronized(this) {
					e = this.requestsId_callbacks.keys();
					key = e.nextElement().intValue();
					reqID = key.intValue();
					amc = this.requestsId_callbacks.get(key);
				}
				
				java.util.Vector<Byte> recv_bytes = ReplyMessageStack.consume(reqID);
				
				ReplyMessage msg = new ReplyMessage(recv_bytes);
				java.util.Vector<Byte> recv_bytes2 = msg.body.byteCollection;
				if (recv_bytes2.size() > 0)
				{
					amc.callback(recv_bytes2);
				}
				
				synchronized(this) {
					this.requestsId_callbacks.remove(key);
				}
			}
		}
	}
}

