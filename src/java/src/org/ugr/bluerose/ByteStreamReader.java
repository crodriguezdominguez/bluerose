package org.ugr.bluerose;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteStreamReader extends java.io.ByteArrayInputStream {
	/**
	* Constructor
	*/
	public ByteStreamReader(byte[] buffer)
	{
		super(buffer);
	}
	
	/**
	 * Constructor
	 */
	public ByteStreamReader(java.util.Vector<Byte> buffer)
	{
		super(Util.convertByteVectorToArray(buffer));
	}
	
	/**
	 * Reads until the end of the stream is reached and
	 * returns the bytes.
	 * @return Bytes that were read
	 */
	public java.util.Vector<Byte> readToEnd()
	{
		java.util.Vector<Byte> bytes = new java.util.Vector<Byte>();
		
		while(this.available() > 0)
		{
			bytes.add(new Byte(this.readByte()));
		}
		
		return bytes;
	}

	/**
	* Reads a size from the stream
	*
	* @return Size extracted from the stream
	*/
	public int readSize()
	{
		int size = this.read();
		
		if (size == 255)
		{
			return this.readInteger();
		}
		
		else if (size == -1) return 0;
		
		else return size;
	}

	/**
	* Reads a byte from the stream
	*
	* @return Byte extracted from the stream
	*/
	public byte readByte()
	{
		return (byte)this.read();
	}
	
	/**
	* Reads a boolean from the stream
	*
	* @return Boolean extracted from the stream
	*/
	public boolean readBoolean()
	{
		int b = this.read();
		return (b==1)?true:false;
	}
	
	/**
	* Reads a short from the stream
	*
	* @return Short extracted from the stream
	*/
	public short readShort()
	{
		byte[] buffer = new byte[2];
		try {
			this.read(buffer);
		} catch (IOException e) {
		}
		
		ByteBuffer buf = ByteBuffer.wrap(buffer);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		return buf.getShort();
	}
	
	/**
	* Reads an integer from the stream
	*
	* @return Integer extracted from the stream
	*/
	public int readInteger()
	{
		byte[] buffer = new byte[4];
		try {
			this.read(buffer);
		} catch (IOException e) {
		}
		
		ByteBuffer buf = ByteBuffer.wrap(buffer);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		return buf.getInt();
	}
	
	/**
	* Reads a long from the stream
	*
	* @return Long extracted from the stream
	*/
	public long readLong()
	{
		byte[] buffer = new byte[8];
		try {
			this.read(buffer);
		} catch (IOException e) {
		}
		
		ByteBuffer buf = ByteBuffer.wrap(buffer);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		return buf.getLong();
	}
	
	/**
	* Reads a float from the stream
	*
	* @return Float extracted from the stream
	*/
	public float readFloat()
	{
		byte[] buffer = new byte[4];
		try {
			this.read(buffer);
		} catch (IOException e) {
		}
		
		ByteBuffer buf = ByteBuffer.wrap(buffer);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		return buf.getFloat();
	}
	
	/**
	* Reads a double from the stream
	*
	* @return Double extracted from the stream
	*/
	public double readDouble()
	{
		byte[] buffer = new byte[8];
		try {
			this.read(buffer);
		} catch (IOException e) {
		}
		
		ByteBuffer buf = ByteBuffer.wrap(buffer);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		return buf.getDouble();
	}
	
	/**
	* Reads an UTF16 string from the stream
	*
	* @param res String extracted from the stream
	*/
	public String readUTF8String()
	{
		int size = this.readSize();
		byte[] buffer = new byte[size];
		try {
			this.read(buffer);
			return new String(buffer, "UTF8");
		} 
		catch (IOException e) {
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	/**
	* Reads an string from the stream
	*
	* @param res String extracted from the stream
	*/
	public String readString()
	{
		int size = this.readSize();
		byte[] buffer = new byte[size];
		try {
			this.read(buffer);
		} catch (IOException e) {
			return "";
		}
		
		String str = "";
		for (int i=0; i<buffer.length; i++)
		{
			str = str+(char)buffer[i];
		}
		
		return str;
	}

	/**
	* Reads a byte sequence from the stream
	*
	* @return Byte sequence extracted from the stream
	*/
	public java.util.Vector<Byte> readByteSeq()
	{
		int size = this.readSize();
		java.util.Vector<Byte> res = new java.util.Vector<Byte>();
		
		for (int i=0; i<size; i++)
		{
			res.add(new Byte(this.readByte()));
		}
		
		return res;
	}
	
	/**
	* Reads a boolean sequence from the stream
	*
	* @return Boolean sequence extracted from the stream
	*/
	public java.util.Vector<Boolean> readBooleanSeq()
	{
		int size = this.readSize();
		java.util.Vector<Boolean> res = new java.util.Vector<Boolean>();
		
		for (int i=0; i<size; i++)
		{
			res.add(new Boolean(this.readBoolean()));
		}
		
		return res;
	}
	
	/**
	* Reads a short sequence from the stream
	*
	* @return Short sequence extracted from the stream
	*/
	public java.util.Vector<Short> readShortSeq()
	{
		int size = this.readSize();
		java.util.Vector<Short> res = new java.util.Vector<Short>();
		
		for (int i=0; i<size; i++)
		{
			res.add(new Short(this.readShort()));
		}
		
		return res;
	}
	
	/**
	* Reads an integer sequence from the stream
	*
	* @return Integer sequence extracted from the stream
	*/
	public java.util.Vector<Integer> readIntegerSeq()
	{
		int size = this.readSize();
		java.util.Vector<Integer> res = new java.util.Vector<Integer>();
		
		for (int i=0; i<size; i++)
		{
			res.add(new Integer(this.readInteger()));
		}
		
		return res;
	}
	
	/**
	* Reads a long sequence from the stream
	*
	* @return Long sequence extracted from the stream
	*/
	public java.util.Vector<Long> readLongSeq()
	{
		int size = this.readSize();
		java.util.Vector<Long> res = new java.util.Vector<Long>();
		
		for (int i=0; i<size; i++)
		{
			res.add(new Long(this.readLong()));
		}
		
		return res;
	}
	
	/**
	* Reads a float sequence from the stream
	*
	* @return Float sequence extracted from the stream
	*/
	public java.util.Vector<Float> readFloatSeq()
	{
		int size = this.readSize();
		java.util.Vector<Float> res = new java.util.Vector<Float>();
		
		for (int i=0; i<size; i++)
		{
			res.add(new Float(this.readFloat()));
		}
		
		return res;
	}
	
	/**
	* Reads a double sequence from the stream
	*
	* @return Double sequence extracted from the stream
	*/
	public java.util.Vector<Double> readDoubleSeq()
	{
		int size = this.readSize();
		java.util.Vector<Double> res = new java.util.Vector<Double>();
		
		for (int i=0; i<size; i++)
		{
			res.add(new Double(this.readDouble()));
		}
		
		return res;
	}
	
	/**
	* Reads a string sequence from the stream
	*
	* @return String sequence extracted from the stream
	*/
	public java.util.Vector<String> readStringSeq()
	{
		int size = this.readSize();
		java.util.Vector<String> res = new java.util.Vector<String>();
		
		for (int i=0; i<size; i++)
		{
			res.add(this.readString());
		}
		
		return res;
	}
	
	/**
	* Reads a string sequence from the stream
	*
	* @return String sequence extracted from the stream
	*/
	public java.util.Vector<String> readUTF8StringSeq()
	{
		int size = this.readSize();
		java.util.Vector<String> res = new java.util.Vector<String>();
		
		for (int i=0; i<size; i++)
		{
			res.add(this.readUTF8String());
		}
		
		return res;
	}

	/**
	* Reads a dictionary from the stream
	*
	* @return Dictionary extracted from the stream
	*/
	public java.util.Dictionary<String, java.util.Vector<Byte>> readDictionary()
	{
		java.util.Dictionary<String, java.util.Vector<Byte>> res = 
			new java.util.Hashtable<String, java.util.Vector<Byte>>();
		
		int size = this.readSize();
		
		for (int i=0; i<size; i++)
		{
			String key = this.readString();
			java.util.Vector<Byte> value = this.readByteSeq();
			
			res.put(key, value);
		}
		
		return res;
	}
}
