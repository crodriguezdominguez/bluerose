package org.ugr.bluerose;

public class Util {
	public static byte[] convertByteVectorToArray(java.util.Vector<Byte> buffer)
	{
		Object[] aux = buffer.toArray();
		byte[] aux2 = new byte[aux.length];
		
		for (int i=0; i<aux2.length; i++)
		{
			aux2[i] = ((Byte)aux[i]).byteValue();
		}
		
		return aux2;
	}
	
	public static <T extends Comparable<? super T>> int compare(java.util.Vector<T> v1, java.util.Vector<T> v2)
	{
		if (v1.equals(v2)) return 0;
		else
		{
			int size = 0;
			if (v1.size() < v2.size())
				size = v1.size();
			
			else size = v2.size();
			
			for (int i=0; i<size; i++)
			{
				int comp = v1.get(i).compareTo(v2.get(i));
				if (comp != 0) return comp;
			}
			
			//everything is equal...
			if (v1.size() < v2.size())
				return -1;
			else if (v1.size() > v2.size())
				return 1;
			
			return 0;
		}
		
		
	}
}
