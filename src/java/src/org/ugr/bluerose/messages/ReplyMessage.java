package org.ugr.bluerose.messages;

import java.io.IOException;

import org.ugr.bluerose.ByteStreamReader;

/**
 * Reply message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
public class ReplyMessage extends Message {
	/**
	 * Default constructor
	 */
	public ReplyMessage()
	{
		header = new ReplyMessageHeader();
		body = new Encapsulation();
	}
	
	/**
	 * Constructor that initializes the message with the appropriate bytes
	 * @param bytes Bytes that represent the message body
	 */
	public ReplyMessage(java.util.Vector<Byte> bytes)
	{
		header = new ReplyMessageHeader();
		body = new Encapsulation();
		
		ReplyMessageHeader h3 = (ReplyMessageHeader)header;
		
		ByteStreamReader reader = new ByteStreamReader(bytes);
		
		reader.skip(14);
		
		h3.requestId = reader.readInteger();
		h3.replyStatus = reader.readByte();
		
		body.size = reader.readInteger();
		
		body.major = reader.readByte();
		body.minor = reader.readByte();
		
		body.byteCollection = reader.readToEnd();
		
		try {
			reader.close();
		} catch (IOException e) {
		}
	}
}
