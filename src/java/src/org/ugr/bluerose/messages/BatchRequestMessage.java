package org.ugr.bluerose.messages;

import java.io.IOException;

import org.ugr.bluerose.ByteStreamReader;

/**
 * Batch request message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
public class BatchRequestMessage extends Message 
{
	/**
	 * Default constructor
	 */
	public BatchRequestMessage()
	{
		header = new BatchRequestMessageHeader();
		body = new Encapsulation();
	}
	
	/**
	 * Constructor that initializes the message with the appropriate bytes
	 * @param bytes Bytes that represent the message body
	 */
	public BatchRequestMessage(java.util.Vector<Byte> bytes)
	{
		java.util.Dictionary<String, String> context =
			new java.util.Hashtable<String, String>();
		
		int size, i;
		
		header = new BatchRequestMessageHeader();
		body = new Encapsulation();
		
		BatchRequestMessageHeader h2 = (BatchRequestMessageHeader)header;
		
		ByteStreamReader reader = new ByteStreamReader(bytes);
		
		reader.skip(14);
		
		h2.numRequests = reader.readInteger();
		
		h2.identity.id_name = reader.readString();
		h2.identity.category = reader.readString();
		
		h2.facet = reader.readStringSeq();
		
		h2.operation = reader.readString();
		
		h2.mode = reader.readByte();
		
		size = reader.readSize();
		for (i=0; i<size; i++)
		{
			String key = reader.readString();
			String value = reader.readString();
			
			context.put(key, value);
		}
		
		h2.context = context;
		
		body.size = reader.readInteger();
		
		body.major = reader.readByte();
		body.minor = reader.readByte();
		
		body.byteCollection = reader.readToEnd();
		
		try {
			reader.close();
		} catch (IOException e) {
		}
	}
}

