package org.ugr.bluerose.messages;


/**
 * Validate connection message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
public class ValidateConnectionMessage extends Message 
{	
	/**
	 * Default constructor
	 */
	public ValidateConnectionMessage()
	{
		header =  new ValidateConnectionMessageHeader();
	}
}
