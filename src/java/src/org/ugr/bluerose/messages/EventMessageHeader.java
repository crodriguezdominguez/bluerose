package org.ugr.bluerose.messages;

/**
* Header for an event message. 
* Extension over ICE Protocol (zeroc.com).
*
* @author Carlos Rodriguez Dominguez
* @date 07-10-2009
*/
public class EventMessageHeader extends MessageHeader
{
	public EventMessageHeader()
	{
		super();
		messageType = MessageHeader.EVENT_MSG;
	}
}
