package org.ugr.bluerose.messages;

/**
 * Header for a connection closing message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
public class CloseConnectionMessageHeader extends MessageHeader
{
	public CloseConnectionMessageHeader()
	{
		super();
		messageType = MessageHeader.CLOSE_CONNECTION_MSG;
	}
}
