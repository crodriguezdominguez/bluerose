package org.ugr.bluerose.messages;

/**
 * Header for a connection validating message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
public class ValidateConnectionMessageHeader extends MessageHeader
{
	public ValidateConnectionMessageHeader()
	{
		super();
		messageType = MessageHeader.VALIDATE_CONNECTION_MSG;
	}
}
