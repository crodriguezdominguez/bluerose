package org.ugr.bluerose.messages;

/**
* Header for a replying message. 
* Definition taken from ICE (zeroc.com) for compatibility reasons.
*
* @author Carlos Rodriguez Dominguez
* @date 07-10-2009
*/
public class ReplyMessageHeader extends MessageHeader
{
	public int requestId; /**< Identifier of the request */
	public byte replyStatus; /**< Status of the reply */
	
	public ReplyMessageHeader()
	{
		super();
		
		messageType = MessageHeader.REPLY_MSG;
		requestId = 0;
		replyStatus = MessageHeader.SUCCESS_STATUS;
		messageSize = 19;
	}
	
	@Override
	public java.util.Vector<Byte> getBytes()
	{
		java.util.Vector<Byte> bytes = super.getBytes();
		java.util.Vector<Byte> result = null;
		
		synchronized(mutex) {
			writer.writeRawBytes(bytes);
		
			writer.writeInteger(requestId);
			writer.writeByte(replyStatus);
		
			result = writer.toVector();
			writer.reset();
		}
		
		return result;
	}
}

