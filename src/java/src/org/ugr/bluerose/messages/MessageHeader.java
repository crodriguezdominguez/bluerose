package org.ugr.bluerose.messages;

import org.ugr.bluerose.ByteStreamWriter;

/**
 * Common header for any message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
public class MessageHeader
{
	/*********** Size Definitions ***********/
	public static final int SIZE_OF_INT = 4;
	public static final int SIZE_OF_LONG = 8;
	public static final int SIZE_OF_FLOAT = 4;
	public static final int SIZE_OF_DOUBLE = 8;
	public static final int SIZE_OF_BOOLEAN = 1;
	/****************************************/

	/************* Types Definition *********/
	public static final int INTEGER = 0x00;
	public static final int LONG = 0x01;
	public static final int FLOAT = 0x02;
	public static final int DOUBLE = 0x03;
	public static final int STRING = 0x04;
	public static final int BOOLEAN = 0x05;
	public static final int LIST_INT = 0x07;
	public static final int LIST_LONG = 0x08;
	public static final int LIST_FLOAT = 0x09;
	public static final int LIST_DOUBLE = 0x0A;
	public static final int LIST_STRING = 0x0B;
	public static final int LIST_BOOLEAN = 0x0C;
	public static final int DICTIONARY = 0x0D;
	/*****************************************/


	/***********  Message Types **********/
	public static final int REQUEST_MSG = 0x00;
	public static final int BATCH_REQUEST_MSG = 0x01;
	public static final int REPLY_MSG = 0x02;
	public static final int VALIDATE_CONNECTION_MSG = 0x03;
	public static final int CLOSE_CONNECTION_MSG = 0x04;
	public static final int EVENT_MSG = 0x05;   //extension for natively support events
	/*************************************/


	/********** Reply Status ************/
	public static final int SUCCESS_STATUS = 0x00;
	public static final int USER_EXCEPTION_STATUS = 0x01;
	public static final int OBJECT_NOT_EXIST_STATUS = 0x02;
	public static final int FACET_NOT_EXIST_STATUS = 0x03;
	public static final int OPERATION_NOT_EXIST_STATUS = 0x04;
	public static final int UNKNOWN_LOCAL_EXCEPTION_STATUS = 0x05;
	public static final int UNKNOWN_USER_EXCEPTION_STATUS = 0x06;
	public static final int UNKNOWN_EXCEPTION_STATUS = 0x07;
	/*************************************/


	/********** Operation Mode *********/
	public static final int TWOWAY_MODE = 0x00;
	public static final int ONEWAY_MODE = 0x01;
	public static final int BATCH_ONEWAY_MODE = 0x02;
	public static final int DATAGRAM_MODE = 0x03;
	public static final int BATCH_DATAGRAM_MODE = 0x04;
	/**********************************/


	/************ Other Definitions *********/
	public static final int TRUE = 0x01;
	public static final int FALSE = 0x00;

	public static final int PROTOCOL_MINOR = 0x00;
	public static final int PROTOCOL_MAJOR = 0x01;

	public static final int ENCODING_MINOR = 0x00;
	public static final int ENCODING_MAJOR = 0x01;
	/*****************************************/
	
	public byte [] magic = new byte[4]; /**< Magic bytes: "I", "c", "e", "P" */
	public byte protocolMajor; /**< Major version of the protocol: 0x01 */
	public byte protocolMinor; /**< Minor version of the protocol: 0x00 */
	public byte encodingMajor; /**< Major version of the encoding: 0x01 */
	public byte encodingMinor; /**< Minor version of the encoding: 0x00 */
	public byte messageType; /**< Message type */
	public byte compressionStatus; /**< Compression of the message. Currently only 0x00 */
	public int messageSize; /**< Size of the message, including the header */
	
	protected static ByteStreamWriter writer = new ByteStreamWriter();
	protected static Object mutex = new Object();
	
	public MessageHeader()
	{
		magic[0] = 0x49;  //IceP
		magic[1] = 0x63;
		magic[2] = 0x65;
		magic[3] = 0x50;
		
		protocolMajor = PROTOCOL_MAJOR;
		protocolMinor = PROTOCOL_MINOR;
		encodingMajor = ENCODING_MAJOR;
		encodingMinor = ENCODING_MINOR;
		messageType = VALIDATE_CONNECTION_MSG;
		compressionStatus = 0x00;
		
		messageSize = 14;
	}
	
	public java.util.Vector<Byte> getBytes()
	{
		java.util.Vector<Byte> result = null;
		
		synchronized(mutex) {
			writer.writeByte(magic[0]);
			writer.writeByte(magic[1]);
			writer.writeByte(magic[2]);
			writer.writeByte(magic[3]);
			
			writer.writeByte(protocolMajor);
			writer.writeByte(protocolMinor);
			writer.writeByte(encodingMajor);
			writer.writeByte(encodingMinor);
			
			writer.writeByte(messageType);
			writer.writeByte(compressionStatus);
			
			writer.writeInteger(messageSize);
			
			result = writer.toVector();
			writer.reset();
		}
		
		return result;
	}
}




