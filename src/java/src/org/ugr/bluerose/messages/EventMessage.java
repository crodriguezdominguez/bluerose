package org.ugr.bluerose.messages;

/**
 * Event message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
public class EventMessage extends Message {
	public java.util.Vector<Byte> event;
	
	/**
	 * Default constructor
	 */
	public EventMessage()
	{
		header = new EventMessageHeader();
		event = new java.util.Vector<Byte>();
	}
	
	/**
	 * Constructor that initializes the message with the appropriate bytes
	 * @param bytes Bytes that represent the message body
	 */
	public EventMessage(java.util.Vector<Byte> bytes)
	{
		header = new EventMessageHeader();
		event = bytes;
	}
	
	@Override
	public java.util.Vector<Byte> getBytes()
	{
		java.util.Vector<Byte> result = null;
		
		synchronized(mutex) {
			java.util.Vector<Byte> header_bytes = header.getBytes();
			
			header.messageSize = header_bytes.size()+event.size();
	
			header_bytes = header.getBytes();
			
			writer.writeRawBytes(header_bytes);
			writer.writeRawBytes(event);
			
			result = writer.toVector();
			writer.reset();
		}
		
		return result;
	}
}
