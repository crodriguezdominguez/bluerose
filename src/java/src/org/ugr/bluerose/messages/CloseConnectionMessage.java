package org.ugr.bluerose.messages;


/**
 * Close connection message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
public class CloseConnectionMessage extends Message 
{
	/**
	 * Default constructor
	 */
	public CloseConnectionMessage()
	{
		header =  new CloseConnectionMessageHeader();
	}
}
