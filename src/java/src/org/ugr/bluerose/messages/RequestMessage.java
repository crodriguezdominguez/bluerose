package org.ugr.bluerose.messages;

import java.io.IOException;

import org.ugr.bluerose.ByteStreamReader;

/**
 * Request message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
public class RequestMessage extends Message 
{	
	/**
	 * Default constructor
	 */
	public RequestMessage()
	{
		header = new RequestMessageHeader();
		body = new Encapsulation();
	}
	
	/**
	 * Constructor that initializes the message with the appropriate bytes
	 * @param bytes Bytes that represent the message body
	 */
	public RequestMessage(java.util.Vector<Byte> bytes)
	{
		java.util.Dictionary<String, String> context =
			new java.util.Hashtable<String, String>();
		
		int size, i;
		
		header = new RequestMessageHeader();
		body = new Encapsulation();
		
		RequestMessageHeader h1 = (RequestMessageHeader)header;
		
		ByteStreamReader reader = new ByteStreamReader(bytes);
		
		reader.skip(14);
		
		h1.requestId = reader.readInteger();
		
		h1.identity.id_name = reader.readString();
		h1.identity.category = reader.readString();
		
		h1.facet = reader.readStringSeq();
		
		h1.operation = reader.readString();
		h1.mode = reader.readByte();
		
		size = reader.readSize();
		for (i=0; i<size; i++)
		{
			String key = reader.readString();
			String value = reader.readString();
			
			context.put(key, value);
		}
		
		h1.context = context;
		
		body.size = reader.readInteger();
		
		body.major = reader.readByte();
		body.minor = reader.readByte();
		
		body.byteCollection = reader.readToEnd();
		
		try {
			reader.close();
		} catch (IOException e) {
		}
	}	
}
