package org.ugr.bluerose;

public interface Marshallable {
	public void marshall(ByteStreamWriter writer);
	public void unmarshall(ByteStreamReader reader);
}
