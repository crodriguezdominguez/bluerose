package org.ugr.bluerose;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteStreamWriter extends java.io.ByteArrayOutputStream {
	
	/**
	* Constructor
	*/
	public ByteStreamWriter()
	{
		super();
	}
	
	public java.util.Vector<Byte> toVector()
	{
		java.util.Vector<Byte> buffer = new java.util.Vector<Byte>();
		
		byte[] aux = this.toByteArray();
		for (int i=0; i<aux.length; i++)
		{
			buffer.add(new Byte(aux[i]));
		}
		
		return buffer;
	}
	
	/**
	* Adds an integer to the stream
	*
	* @param b Integer to add
	*/
	public void writeInteger(int n)
	{
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putInt(n);
		
		try {
			this.write(buffer.array());
		} catch (IOException e) {
		}
	}
	
	/**
	* Writes a size to the stream. A size is coded as
	* a single byte if is lower than 255. If it's greater,
	* then it uses 5 bytes: The first one is always 255 and
	* the following are the integer representation of the size.
	*
	* @param size Size to write to the stream
	*/
	public void writeSize(int size)
	{
		if (size < 255)
		{
			this.writeByte((byte)size);
		}
		else
		{
			this.writeByte((byte)255);
			this.writeInteger(size);
		}
	}
	
	/**
	* Adds a byte to the stream
	*
	* @param b Byte to add
	*/
	public void writeByte(byte b)
	{
		this.write(b);
	}
	
	/**
	* Adds a boolean to the stream
	*
	* @param b Boolean to add
	*/
	public void writeBoolean(boolean b)
	{
		this.writeByte((byte)( (b)?1:0 ));
	}
	
	/**
	* Adds a short to the stream
	*
	* @param b Short to add
	*/
	public void writeShort(short b)
	{
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putShort(b);
		
		try {
			this.write(buffer.array());
		} catch (IOException e) {
		}
	}
	
	/**
	* Adds a long to the stream
	*
	* @param b Long to add
	*/
	public void writeLong(long b)
	{
		ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putLong(b);
		
		try {
			this.write(buffer.array());
		} catch (IOException e) {
		}
	}
	
	/**
	* Adds a float to the stream
	*
	* @param b Float to add
	*/
	public void writeFloat(float b)
	{
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putFloat(b);
		
		try {
			this.write(buffer.array());
		} catch (IOException e) {
		}
	}
	
	/**
	* Adds a double to the stream
	*
	* @param b Double to add
	*/
	public void writeDouble(double b)
	{
		ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putDouble(b);
		
		try {
			this.write(buffer.array());
		} catch (IOException e) {
		}
	}
	
	/**
	* Adds an UTF16 string to the stream
	*
	* @param b String to add
	*/
	public void writeUTF8String(String b)
	{
		try {
			byte[] bytes = b.getBytes("UTF8");
			this.writeSize(bytes.length);
			this.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	* Adds an string to the stream
	*
	* @param b String to add
	*/
	public void writeString(String b)
	{
		this.writeSize(b.length());
		try {
			this.write(b.getBytes());
		} catch (IOException e) {
		}
	}
	
	/**
	* Adds raw bytes to the stream
	*
	* @param b Bytes to add
	*/
	public void writeRawBytes(java.util.Vector<Byte> b)
	{	
		for (int i=0; i<b.size(); i++)
		{
			this.write(b.get(i).byteValue());
		}
	}

	/**
	* Adds a sequence of bytes to the stream
	*
	* @param b Bytes to add
	*/
	public void writeByteSeq(java.util.Vector<Byte> b)
	{
		this.writeSize(b.size());
		this.writeRawBytes(b);
	}
	
	/**
	* Adds a sequence of booleans to the stream
	*
	* @param b Booleans to add
	*/
	public void writeBooleanSeq(java.util.Vector<Boolean> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeBoolean(b.get(i).booleanValue());
		}
	}
	
	/**
	* Adds a sequence of shorts to the stream
	*
	* @param b Shorts to add
	*/
	public void writeShortSeq(java.util.Vector<Short> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeShort(b.get(i).shortValue());
		}
	}
	
	/**
	* Adds a sequence of integers to the stream
	*
	* @param b Integers to add
	*/
	public void writeIntegerSeq(java.util.Vector<Integer> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeInteger(b.get(i).intValue());
		}
	}
	
	/**
	* Adds a sequence of longs to the stream
	*
	* @param b Longs to add
	*/
	public void writeLongSeq(java.util.Vector<Long> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeLong(b.get(i).longValue());
		}
	}
	
	/**
	* Adds a sequence of floats to the stream
	*
	* @param b Floats to add
	*/
	public void writeFloatSeq(java.util.Vector<Float> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeFloat(b.get(i).floatValue());
		}
	}
	
	/**
	* Adds a sequence of doubles to the stream
	*
	* @param b Doubles to add
	*/
	public void writeDoubleSeq(java.util.Vector<Double> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeDouble(b.get(i).doubleValue());
		}
	}
	
	/**
	* Adds a sequence of strings to the stream
	*
	* @param b Strings to add
	*/
	public void writeStringSeq(java.util.Vector<String> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeString(b.get(i));
		}
	}
	
	/**
	* Adds a sequence of utf8 strings to the stream
	*
	* @param b Strings to add
	*/
	public void writeUTF8StringSeq(java.util.Vector<String> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		for (int i=0; i<size; i++)
		{
			this.writeUTF8String(b.get(i));
		}
	}
	
	/**
	* Adds a dictionary to the stream
	*
	* @param b Bytes to add
	*/
	public void writeDictionary(java.util.Dictionary<String, java.util.Vector<Byte>> b)
	{
		int size = b.size();
		this.writeSize(size);
		
		java.util.Enumeration<String> e = b.keys();
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			java.util.Vector<Byte> value = b.get(key);
			
			this.writeString(key);
			this.writeRawBytes(value);
		}
	}
	
	/**
	 * Adds a marshallable object to the stream
	 * @param object Object to add
	 */
	public void writeObject(Marshallable object)
	{
		object.marshall(this);
	}
}
