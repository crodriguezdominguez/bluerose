package org.ugr.bluerose.devices;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class TcpClient {
	public java.net.Socket socket;
	protected java.io.InputStream input;
	protected java.io.OutputStream output;
	
	public TcpClient(String host, int port) throws Exception
	{
		socket = new java.net.Socket();
		socket.setKeepAlive(true);
		socket.setTcpNoDelay(true);
		socket.connect(new java.net.InetSocketAddress(host, port));
		input = socket.getInputStream();
		output = socket.getOutputStream();
		
		//read validate connection msg
		this.read();
	}
	
	public TcpClient(java.net.Socket sock) throws Exception
	{
		socket = sock;
		
		input = socket.getInputStream();
		output = socket.getOutputStream();
	}
	
	@Override
	public void finalize() throws Throwable
	{
		try {
			input.close();
			output.close();
			socket.close();
		}catch(Exception ex)
		{
			
		}
		
		super.finalize();
	}
	
	public boolean isOpenned()
	{
		return socket.isConnected();
	}
	
	public String getID()
	{
		return socket.getInetAddress().toString();
	}
	
	public boolean write(java.util.Vector<Byte> data)
	{	
		synchronized(output) {
			try {
				output.write(org.ugr.bluerose.Util.convertByteVectorToArray(data));
			} catch (IOException e) {
				return false;
			}
			
			return true;
		}
	}
	
	public synchronized java.util.Vector<Byte> read()
	{
		if (input == null) return null;
		
		int size = -1;
		int buffer_size = 14;
		
		byte buffer_init[] = new byte[buffer_size];
		try {
			int n=0;
			while (n != buffer_size)
			{
				int s;
				s = input.read(buffer_init, 0, buffer_size);
				n = n+s;
				
				if (s == -1)
				{
					return null;
				}
			}
		} catch (IOException e1) {
			return null;
		}
		
		java.util.Vector<Byte> msg = new java.util.Vector<Byte>();
		
		for (int i=0; i<buffer_size; i++)
		{
			msg.add(new Byte(buffer_init[i]));
		}
		
		size = 0;
		java.nio.ByteBuffer buf = ByteBuffer.allocate(5);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.put(buffer_init[10]);
		buf.put(buffer_init[11]);
		buf.put(buffer_init[12]);
		buf.put(buffer_init[13]);
		buf.rewind();
		
		size = buf.getInt();
		
		buf.clear();
		
		buffer_size = size-buffer_size;
		
		if (buffer_size == 0)
		{
			return msg;
		}
		else
		{
			byte buffer[] = new byte[buffer_size];
			
			try {
				int k = 0;
				while(k != buffer_size)
				{
					int r;
					r = input.read(buffer, 0, buffer_size);
					k = k+r;
					
					if (r == -1)
					{
						return null;
					}
				}
			} catch (IOException e) {
				return null;
			}

			for (int i=0; i<buffer_size; i++)
			{
				msg.add(new Byte(buffer[i]));
			}
		
			return msg;
		}
	}
}
