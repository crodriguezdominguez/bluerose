package org.ugr.bluerose.threads;

import org.ugr.bluerose.ICommunicationDevice;

public class OpenConnectionThread extends Thread 
{	
	protected ICommunicationDevice device; /**< Transmission interface for the thread */
	protected String userID; /**< User identificator of the sender */
	protected java.util.Dictionary<String, java.util.Vector<Byte>> parameters;
	protected int maxRetries;
	
	public OpenConnectionThread(ICommunicationDevice dev, String identifier, int retries, java.util.Dictionary<String, java.util.Vector<Byte>> pars)
	{
		device = dev;
		userID = identifier;
		parameters = pars;
		maxRetries = retries;
	}
	
	@Override
	public void run()
	{
		if (maxRetries == Integer.MAX_VALUE)
		{
			while(true)
			{
				try{
					device.openConnection(userID, parameters);
					return;
				} catch(Exception ex) {
				}
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					return;
				}
			}
		}
		else
		{
			int i;
			for (i=0; i<maxRetries; i++)
			{
				try{
					device.openConnection(userID, parameters);
					return;
				} catch(Exception ex) {
				}
					
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					return;
				}
			}
			
			if (i>=maxRetries)
			{
				return;
			}
		}
	}
}
