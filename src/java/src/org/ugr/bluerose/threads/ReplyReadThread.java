package org.ugr.bluerose.threads;

import org.ugr.bluerose.ByteStreamReader;
import org.ugr.bluerose.ICommunicationDevice;
import org.ugr.bluerose.ReplyMessageStack;
import org.ugr.bluerose.events.Event;
import org.ugr.bluerose.events.EventHandler;
import org.ugr.bluerose.messages.MessageHeader;

/**
* Auxiliary thread for reading replies from a transmission 
* channel. It's used for receiving servant replies
*
* @author Carlos Rodriguez Dominguez
* @date 7-10-2009
*/
public class ReplyReadThread extends Thread
{
	protected ICommunicationDevice device; /**< Transmission interface for the thread */
	protected String userID; /**< User identificator of the sender */


	/**
	* Constructor
	*
	* @param iface Transmission interface
	*/
	public ReplyReadThread(ICommunicationDevice iface, String _userID)
	{
		device = iface;
		userID = _userID;
	}

	/**
	* Overload of the main method of the thread
	*/
	@Override
	public void run()
	{	
		java.util.Vector<Byte> bytes;
		
		while ( (bytes = device.read(userID)) != null ) //read messages
		{
			if (bytes.get(8) == MessageHeader.REPLY_MSG)
			{
				ByteStreamReader reader = new ByteStreamReader(bytes);
				
				//add to reply stack
				reader.skip(14);
				int requestId = reader.readInteger();
				
				ReplyMessageStack.produce(requestId, bytes);
				
				bytes.clear();
			}
			
			else if (bytes.get(8) == MessageHeader.EVENT_MSG)
			{	
				ByteStreamReader reader = new ByteStreamReader(bytes);
				
				reader.skip(14);
				java.util.Vector<Byte> bb = reader.readToEnd();

				Event evt = new Event();
				evt.unmarshall(new ByteStreamReader(bb));
				
				EventHandler.onConsume(evt);
				
				bytes.clear();
			}
			
			else if (bytes.get(8) == MessageHeader.CLOSE_CONNECTION_MSG)
			{
				bytes.clear();
				break;
			}
		}
		
		device.closeConnection(userID);
	}
}
