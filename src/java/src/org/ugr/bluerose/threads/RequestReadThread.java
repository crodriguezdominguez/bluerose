package org.ugr.bluerose.threads;

import org.ugr.bluerose.DevicePool;
import org.ugr.bluerose.ICommunicationDevice;
import org.ugr.bluerose.messages.MessageHeader;
import org.ugr.bluerose.messages.ValidateConnectionMessage;

public class RequestReadThread extends Thread {
	protected ICommunicationDevice device; /**< Transmission interface for the thread */
	protected String userID; /**< User identificator of the sender */

	/**
	* Constructor
	*
	* @param iface Transmission interface
	*/
	public RequestReadThread(ICommunicationDevice iface, String _userID)
	{
		device = iface;
		userID = _userID;
	}

	/**
	* Overload of the main method of the thread
	*/
	@Override
	public void run()
	{
		//send validate connection msg
		ValidateConnectionMessage msg = new ValidateConnectionMessage();
		java.util.Vector<Byte> bytes = msg.getBytes();
		device.write(userID, bytes);
		bytes.clear();

		while ( (bytes=device.read(userID)) != null ) //read messages
		{
			if (bytes.get(8) == MessageHeader.CLOSE_CONNECTION_MSG)
			{
				break;
			}
			
			if (bytes.get(8) == MessageHeader.REQUEST_MSG)
			{
				//process message
				DevicePool.dispatchMessage(bytes, userID);
			}
			
			bytes.clear();
		}
		
		System.out.println("Connection closed: "+userID);
		
		device.closeConnection(userID);
	}
}
