package org.ugr.bluerose.events;

import org.ugr.bluerose.ByteStreamReader;
import org.ugr.bluerose.ByteStreamWriter;
import org.ugr.bluerose.Marshallable;

public class Event implements Marshallable
{	
	public static final int DEFAULT_TOPIC = 0;
	
	public int topic; /**< Event topic */
	public java.util.Dictionary<String, EventNode> nodes; /**< Map of identifiers and associated nodes */
	
	public Event()
	{
		topic = DEFAULT_TOPIC;
		nodes = new java.util.Hashtable<String, EventNode>();
	}
	
	public Event(int top)
	{
		topic = top;
		nodes = new java.util.Hashtable<String, EventNode>();
	}
	
	public Event(Event evt)
	{
		topic = evt.topic;
		nodes = evt.nodes;
	}
	
	public boolean existsMember(String member)
	{
		return (nodes.get(member) != null);
	}
	
	public int size()
	{
		return nodes.size();
	}
	
	public EventNode getMember(String member)
	{
		return nodes.get(member);
	}
	
	public Value getMemberValue(String member)
	{
		EventNode node = nodes.get(member);
		if (node != null)
			return node.value;
		else return null;
	}
	
	public void setMember(String member, EventNode node)
	{
		nodes.put(member, node);
	}
	
	public void setMember(String member, Value value)
	{
		EventNode node = new EventNode(member, value);
		nodes.put(member, node);
	}

	public void marshall(ByteStreamWriter writer) 
	{
		//topic
		writer.writeInteger(this.topic);
		
		//num. event nodes
		writer.writeSize(nodes.size());
		
		//event nodes
		java.util.Enumeration<String> e = this.nodes.keys();
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			
			EventNode node = this.nodes.get(key);
			writer.writeObject(node);
		}
	}

	public void unmarshall(ByteStreamReader reader) {
		int top = reader.readInteger();
		
		this.topic = top;
		this.nodes = new java.util.Hashtable<String, EventNode>();
		
		//num. event nodes
		int tam = reader.readSize();
		
		if (tam == 0)
		{
			return;
		}
		
		//event nodes
		for (int i=0; i<tam; i++)
		{
			EventNode aux = new EventNode();
			aux.unmarshall(reader);
			
			this.setMember(aux.identifier, aux);
		}
	}
	
	@Override
	public String toString()
	{
		String res = "Topic="+topic+", {";
		
		int counter = 0;
		int end = nodes.size();
		java.util.Enumeration<String> e = nodes.keys();
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			EventNode value = nodes.get(key);
			res += key+":"+value.toString();
			counter++;
			if (counter != end) res +=", ";
		}
			
		res += "}";
		return res;
	}
}
