package org.ugr.bluerose.events;

import java.util.Enumeration;


public abstract class EventListener {
	public int topic;
	public Predicate predicate;
	
	/**
	 * Default constructor
	 */
	public EventListener()
	{
		topic = Event.DEFAULT_TOPIC;
		predicate = new Predicate();
	}
	
	/**
	 * Default constructor
	 *
	 * @param top Topic to listen
	 */
	public EventListener(int top)
	{
		topic = top;
		predicate = new Predicate();
	}
	
	/**
	 * Default constructor
	 *
	 * @param top Topic to listen
	 * @param pred Predicate to accomplish
	 */
	public EventListener(int top, Predicate pred)
	{
		topic = top;
		predicate = pred;
	}
	
	/**
	 * Checks whether this listener accepts a topic or not
	 *
	 * @param top Topic to check
	 * @return YES if the topic is accepted. NO otherwise else
	 */
	public boolean acceptsTopic(int top)
	{
		return (topic == top);
	}
	
	/**
	 * Checks whether this listener accepts an event or not. It is
	 * a more complex check than the check method that only accepts
	 * a topic. In this method, it is assured that the event is
	 * related with the topic established in the constructor. It uses
	 * the predicate to check if it is accepted or not.
	 *
	 * @param event Event to check
	 * @return YES if the event is accepted. NO otherwise else
	 */
	public boolean acceptsEvent(Event event)
	{
		if (predicate.isEmpty()) return true;
		
		java.util.Dictionary<String, EventNode> dict = event.nodes;
		Enumeration<String> e = dict.keys();
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			EventNode node = dict.get(key);
			if (!predicate.isConsistent(key, node.value))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Action that is run whenever an appropriate event is received.
	 *
	 * @param evt Event that is received
	 */
	public abstract void performAction(Event event);
	
	/**
	 * Checks if two listeners are equal (i.e., have the same name)
	 *
	 * @return YES if both are equal. NO otherwise else.
	 */
	@Override
	public boolean equals(Object obj)
	{
		return this.getClass().equals(obj.getClass());
	}
}
