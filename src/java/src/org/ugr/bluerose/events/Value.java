package org.ugr.bluerose.events;

import org.ugr.bluerose.ByteStreamReader;
import org.ugr.bluerose.ByteStreamWriter;
import org.ugr.bluerose.Comparison;
import org.ugr.bluerose.Marshallable;
import org.ugr.bluerose.Util;

public class Value implements Comparable<Value>, Marshallable
{
	public static final int BYTE_TYPE=0, 
							SHORT_TYPE=1, 
							INTEGER_TYPE=2, 
							LONG_TYPE=3,
							BOOLEAN_TYPE=4,
							FLOAT_TYPE=5, 
							DOUBLE_TYPE=6,
							STRING_TYPE=7,
							SEQ_BYTE_TYPE=8, 
							SEQ_SHORT_TYPE=9,
							SEQ_INTEGER_TYPE=10, 
							SEQ_LONG_TYPE=11,
							SEQ_BOOLEAN_TYPE=12,
							SEQ_FLOAT_TYPE=13,
							SEQ_DOUBLE_TYPE=14,
							SEQ_STRING_TYPE=15,
							OTHER_TYPE=16, 
							NULL_TYPE=17;
	
	public int type; /**< Type of the value */
	public java.util.Vector<Byte> rawValue; /**< Value */
	
	protected ByteStreamWriter writer = new ByteStreamWriter();
	protected Object mutex = new Object();
	
	public Value()
	{
		type = NULL_TYPE;
		rawValue = new java.util.Vector<Byte>();
	}
	
	public Value(int valueType)
	{
		type = valueType;
		rawValue = new java.util.Vector<Byte>();
	}

	public void setByte(byte value)
	{
		type = BYTE_TYPE;
		
		synchronized(mutex) {
			writer.writeByte(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setInteger(int value)
	{
		type = INTEGER_TYPE;
		
		synchronized(mutex) {
			writer.writeInteger(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setShort(short value)
	{
		type = SHORT_TYPE;
		
		synchronized(mutex) {
			writer.writeShort(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setLong(long value)
	{
		type = LONG_TYPE;
		
		synchronized(mutex) {
			writer.writeLong(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setBoolean(boolean value)
	{
		type = BOOLEAN_TYPE;
		
		synchronized(mutex) {
			writer.writeBoolean(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}

	public void setFloat(float value)
	{
		type = FLOAT_TYPE;
		
		synchronized(mutex) {
			writer.writeFloat(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setDouble(double value)
	{
		type = DOUBLE_TYPE;
		
		synchronized(mutex) {
			writer.writeDouble(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}

	public void setString(String value)
	{
		type = STRING_TYPE;
		
		synchronized(mutex) {
			writer.writeUTF8String(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}

	public void setByteSeq(java.util.Vector<Byte> value)
	{
		type = SEQ_BYTE_TYPE;
		
		synchronized(mutex) {
			writer.writeByteSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setIntegerSeq(java.util.Vector<Integer> value)
	{
		type = SEQ_INTEGER_TYPE;
		
		synchronized(mutex) {
			writer.writeIntegerSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setShortSeq(java.util.Vector<Short> value)
	{
		type = SEQ_SHORT_TYPE;
		
		synchronized(mutex) {
			writer.writeShortSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setLongSeq(java.util.Vector<Long> value)
	{
		type = SEQ_LONG_TYPE;
		
		synchronized(mutex) {
			writer.writeLongSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setBooleanSeq(java.util.Vector<Boolean> value)
	{
		type = SEQ_BOOLEAN_TYPE;
		
		synchronized(mutex) {
			writer.writeBooleanSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setFloatSeq(java.util.Vector<Float> value)
	{
		type = SEQ_FLOAT_TYPE;
		
		synchronized(mutex) {
			writer.writeFloatSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setDoubleSeq(java.util.Vector<Double> value)
	{
		type = SEQ_DOUBLE_TYPE;
		
		synchronized(mutex) {
			writer.writeDoubleSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public void setStringSeq(java.util.Vector<String> value)
	{
		type = SEQ_STRING_TYPE;
		
		synchronized(mutex) {
			writer.writeUTF8StringSeq(value);
			rawValue = writer.toVector();
			writer.reset();
		}
	}
	
	public byte getByte()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readByte();
	}

	public int getInteger()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readInteger();
	}
	
	public short getShort()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readShort();
	}

	public long getLong()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readLong();
	}
	
	public boolean getBoolean()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readBoolean();
	}
	
	public float getFloat()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readFloat();
	}

	public double getDouble()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readDouble();
	}
	
	public String getString()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readUTF8String();
	}
	
	public java.util.Vector<Byte> getByteSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readByteSeq();
	}

	public java.util.Vector<Integer> getIntegerSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readIntegerSeq();
	}
	
	public java.util.Vector<Short> getShortSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readShortSeq();
	}
	
	public java.util.Vector<Long> getLongSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readLongSeq();
	}
	
	public java.util.Vector<Boolean> getBooleanSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readBooleanSeq();
	}
	
	public java.util.Vector<Float> getFloatSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readFloatSeq();
	}

	public java.util.Vector<Double> getDoubleSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readDoubleSeq();
	}
	
	public java.util.Vector<String> getStringSeq()
	{
		ByteStreamReader reader = new ByteStreamReader(rawValue);
		return reader.readUTF8StringSeq();
	}
	
	public boolean compare(Comparison comp, Value value)
	{
		if (type != value.type) return false;
		
		if (comp == Comparison.EQUAL)
		{
			if (rawValue.equals(value.rawValue)) return true;
			else return false;
		}
		
		if (comp == Comparison.NOT_EQUAL)
		{
			if (!rawValue.equals(value.rawValue)) return true;
			else return false;
		}
		
		if (comp == Comparison.LESS) return this.isLessThan(value);
		else if (comp == Comparison.LESS_EQUAL) return this.isLessEqualThan(value);
		else if (comp == Comparison.GREATER) return this.isGreaterThan(value);
		else if (comp == Comparison.GREATER_EQUAL) return this.isGreaterEqualThan(value);
		else return false;
	}

	public int compareTo(Value value) 
	{	
		if (this.equals(value))
		{
			return 0;
		}
		else if (this.isLessThan(value))
		{
			return -1;
		}
		else return 1;
	}
	
	protected boolean isLessThan(Value value)
	{
		if (value.type != type) return false;
		
		if (type == BYTE_TYPE) return (this.getByte() < value.getByte());
		else if (type == SHORT_TYPE) return (this.getShort() < value.getShort());
		else if (type == INTEGER_TYPE) return (this.getInteger() < value.getInteger());
		else if (type == LONG_TYPE) return (this.getLong() < value.getLong());
		else if (type == FLOAT_TYPE) return (this.getFloat() < value.getFloat());
		else if (type == DOUBLE_TYPE) return (this.getDouble() < value.getDouble());
		else if (type == STRING_TYPE) return (this.getString().compareTo(value.getString()) < 0);
		else if (type == SEQ_BYTE_TYPE) return (Util.compare(this.getByteSeq(), value.getByteSeq()) < 0);
		else if (type == SEQ_SHORT_TYPE) return (Util.compare(this.getShortSeq(), value.getShortSeq()) < 0);
		else if (type == SEQ_INTEGER_TYPE) return (Util.compare(this.getIntegerSeq(), value.getIntegerSeq()) < 0);
		else if (type == SEQ_LONG_TYPE) return (Util.compare(this.getLongSeq(), value.getLongSeq()) < 0);
		else if (type == SEQ_FLOAT_TYPE) return (Util.compare(this.getFloatSeq(), value.getFloatSeq()) < 0);
		else if (type == SEQ_DOUBLE_TYPE) return (Util.compare(this.getDoubleSeq(), value.getDoubleSeq()) < 0);
		else if (type == SEQ_STRING_TYPE) return (Util.compare(this.getStringSeq(), value.getStringSeq()) < 0);
		else return false;
	}
	
	protected boolean isGreaterThan(Value value)
	{
		if (type != value.type) return false;
		
		if (type == BYTE_TYPE) return (this.getByte() > value.getByte());
		else if (type == SHORT_TYPE) return (this.getShort() > value.getShort());
		else if (type == INTEGER_TYPE) return (this.getInteger() > value.getInteger());
		else if (type == LONG_TYPE) return (this.getLong() > value.getLong());
		else if (type == FLOAT_TYPE) return (this.getFloat() > value.getFloat());
		else if (type == DOUBLE_TYPE) return (this.getDouble() > value.getDouble());
		else if (type == STRING_TYPE) return (this.getString().compareTo(value.getString()) > 0);
		else if (type == SEQ_BYTE_TYPE) return (Util.compare(this.getByteSeq(), value.getByteSeq()) > 0);
		else if (type == SEQ_SHORT_TYPE) return (Util.compare(this.getShortSeq(), value.getShortSeq()) > 0);
		else if (type == SEQ_INTEGER_TYPE) return (Util.compare(this.getIntegerSeq(), value.getIntegerSeq()) > 0);
		else if (type == SEQ_LONG_TYPE) return (Util.compare(this.getLongSeq(), value.getLongSeq()) > 0);
		else if (type == SEQ_FLOAT_TYPE) return (Util.compare(this.getFloatSeq(), value.getFloatSeq()) > 0);
		else if (type == SEQ_DOUBLE_TYPE) return (Util.compare(this.getDoubleSeq(), value.getDoubleSeq()) > 0);
		else if (type == SEQ_STRING_TYPE) return (Util.compare(this.getStringSeq(), value.getStringSeq()) > 0);
		else return false;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		Value value = (Value)obj;
		
		if (value.type != this.type) return false;
		
		if (type == BYTE_TYPE) return (this.getByte() == value.getByte());
		else if (type == SHORT_TYPE) return (this.getShort() == value.getShort());
		else if (type == INTEGER_TYPE) return (this.getInteger() == value.getInteger());
		else if (type == LONG_TYPE) return (this.getLong() == value.getLong());
		else if (type == FLOAT_TYPE) return (this.getFloat() == value.getFloat());
		else if (type == DOUBLE_TYPE) return (this.getDouble() == value.getDouble());
		else if (type == STRING_TYPE) return (this.getString().equals(value.getString()));
		else if (type == SEQ_BYTE_TYPE) return (this.getByteSeq().equals(value.getByteSeq()));
		else if (type == SEQ_SHORT_TYPE) return (this.getShortSeq().equals(value.getShortSeq()));
		else if (type == SEQ_INTEGER_TYPE) return (this.getIntegerSeq().equals(value.getIntegerSeq()));
		else if (type == SEQ_LONG_TYPE) return (this.getLongSeq().equals(value.getLongSeq()));
		else if (type == SEQ_FLOAT_TYPE) return (this.getFloatSeq().equals(value.getFloatSeq()));
		else if (type == SEQ_DOUBLE_TYPE) return (this.getDoubleSeq().equals(value.getDoubleSeq()));
		else if (type == SEQ_STRING_TYPE) return (this.getStringSeq().equals(value.getStringSeq()));
		else return false;
	}
	
	protected boolean isLessEqualThan(Value value)
	{
		return !this.isGreaterThan(value);
	}
	
	protected boolean isGreaterEqualThan(Value value)
	{
		return !this.isLessThan(value);
	}

	public void marshall(ByteStreamWriter writer) 
	{
		writer.writeSize(type);
		writer.writeByteSeq(rawValue);
	}

	public void unmarshall(ByteStreamReader reader) 
	{
		type = reader.readSize();
		rawValue = reader.readByteSeq();
	}
	
	@Override
	public String toString()
	{
		if (type == BYTE_TYPE) return ""+this.getByte();
		else if (type == SHORT_TYPE) return ""+this.getShort();
		else if (type == INTEGER_TYPE) return ""+this.getInteger();
		else if (type == LONG_TYPE) return ""+this.getLong();
		else if (type == FLOAT_TYPE) return ""+this.getFloat();
		else if (type == DOUBLE_TYPE) return ""+this.getDouble();
		else if (type == STRING_TYPE) return this.getString();
		else if (type == SEQ_BYTE_TYPE) return this.getByteSeq().toString();
		else if (type == SEQ_SHORT_TYPE) return this.getShortSeq().toString();
		else if (type == SEQ_INTEGER_TYPE) return this.getIntegerSeq().toString();
		else if (type == SEQ_LONG_TYPE) return this.getLongSeq().toString();
		else if (type == SEQ_FLOAT_TYPE) return this.getFloatSeq().toString();
		else if (type == SEQ_DOUBLE_TYPE) return this.getDoubleSeq().toString();
		else if (type == SEQ_STRING_TYPE) return this.getStringSeq().toString();
		else return rawValue.toString();
	}
}
