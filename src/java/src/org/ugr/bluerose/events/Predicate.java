package org.ugr.bluerose.events;

import org.ugr.bluerose.ByteStreamReader;
import org.ugr.bluerose.ByteStreamWriter;
import org.ugr.bluerose.Comparison;
import org.ugr.bluerose.Marshallable;
import org.ugr.bluerose.Pair;

/**
 * Class that models a set of constraints over the values of a set of properties
 *
 * @author Carlos Rodriguez Dominguez
 * @date 09-02-2010
 */
public class Predicate implements Marshallable {
	public java.util.Dictionary<String, java.util.Vector<Pair<Comparison, Value>>> constraints;

	public Predicate()
	{
		constraints = new java.util.Hashtable<String, java.util.Vector<Pair<Comparison, Value>>>();
	}
	
	/**
	 * Adds a new constraint to the set of constraints, but only if the set remains
	 * consistent.
	 *
	 * @param property Property associated with the constraint
	 * @param comp Comparison operator between the property and the value
	 * @param value Value associated with the property
	 * @return True if the constraint was added to the set (i.e., the set remains
	 *         consistent after adding the constraint). False otherwise else.
	 */
	public boolean addConstraint(String property, Comparison comp, Value value)
	{
		if (!this.isConsistent(property, comp, value)) return false;
		else
		{
			java.util.Vector<Pair<Comparison, Value>> array = constraints.get(property);
			Pair<Comparison, Value> pair = new Pair<Comparison, Value>();
			pair.first = comp;
			pair.second = value;
			
			if (array == null)
			{
				array = new java.util.Vector<Pair<Comparison, Value>>();

				array.add(pair);
				constraints.put(property, array);
			}
			else
			{
				array.add(pair);
				
				constraints.put(property, array);
			}
			
			return true;
		}
	}
	
	/**
	 * Removes a constraint from the set
	 *
	 * @param property Property associated with the constraint to remove
	 */
	public void removeConstraints(String property)
	{
		constraints.remove(property);
	}
	
	/**
	 * Checks if the set of constraints is consistent after adding "property comp value".
	 * For example: "x == 5", "y <= 3", etc.
	 *
	 * @param property Property to check
	 * @param value Value of the property
	 */
	public boolean isConsistent(String property, Comparison comp, Value value)
	{
		if (constraints.size() == 0) return true;
		else
		{
			java.util.Vector<Pair<Comparison, Value>> v = constraints.get(property);
			if (v == null) return true;
			
			for (Pair<Comparison, Value> pair : v)
			{
				//check inconsistencies
				Comparison first = pair.first;
				Value second = pair.second;
				
				if (second.compare(Comparison.EQUAL, value)) //value == pair.second
				{
					if (first == Comparison.EQUAL)
					{
						if ( (comp == Comparison.LESS) ||
							(comp == Comparison.GREATER) ||
							(comp == Comparison.NOT_EQUAL) ) 
							return false;
					}
					
					else if (first == Comparison.NOT_EQUAL)
					{
						if (comp == Comparison.EQUAL) return false;
					}
					
					else if (first == Comparison.LESS)
					{
						if ( (comp == Comparison.EQUAL) ||
							(comp == Comparison.GREATER) ||
							(comp == Comparison.GREATER_EQUAL) ) 
							return false;
					}
					
					else if (first == Comparison.LESS_EQUAL)
					{
						if (comp == Comparison.GREATER) return false;
					}
					
					else if (first == Comparison.GREATER)
					{
						if ( (comp == Comparison.EQUAL) ||
							(comp == Comparison.LESS) ||
							(comp == Comparison.LESS_EQUAL) ) 
							return false;
					}
					
					else if (first == Comparison.GREATER_EQUAL)
					{
						if (comp == Comparison.LESS) return false;
					}
				}
				else //value != pair.second
				{
					if (first == Comparison.EQUAL) return false;
					
					else if (first == Comparison.NOT_EQUAL) return true;
					
					else if ( (first == Comparison.LESS) ||
							 (first == Comparison.LESS_EQUAL) )
					{
						if (value.compare(Comparison.GREATER, second))
						{
							if ( (comp == Comparison.EQUAL) ||
								(comp == Comparison.GREATER) ||
								(comp == Comparison.GREATER_EQUAL) )
								return false;
						}
					}
					
					else if ( (first == Comparison.GREATER) ||
							 (first == Comparison.GREATER_EQUAL) )
					{
						if (value.compare(Comparison.LESS, second))
						{
							if ( (comp == Comparison.EQUAL) ||
								(comp == Comparison.LESS) ||
								(comp == Comparison.LESS_EQUAL) )
								return false;
						}
					}
				}
			}
			
			return true;
		}

	}
	
	/**
	 * Checks if the set of constraints is consistent after adding "property == value".
	 *
	 * @param property Property to check
	 * @param value Value of the property
	 */
	public boolean isConsistent(String property, Value value)
	{
		return isConsistent(property, Comparison.EQUAL, value);
	}

	/**
	 * Checks if the set of constraints is empty
	 */
	public boolean isEmpty()
	{
		return (constraints.size() == 0);
	}
	
	/**
	 * Returns the number of constraints
	 *
	 * @return Number of constraints
	 */
	public int size()
	{
		return constraints.size();
	}

	public void marshall(ByteStreamWriter writer) 
	{
		writer.writeSize(this.size());
		
		java.util.Enumeration<String> e = constraints.keys();
		
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			writer.writeString(key);
			java.util.Vector<Pair<Comparison, Value>> array = constraints.get(key);
			writer.writeSize(array.size());
			
			for (Pair<Comparison, Value> pair : array)
			{
				int number = pair.first.ordinal();
				Value value = pair.second;
				
				writer.writeSize(number);
				
				writer.writeSize(value.type);
				writer.writeByteSeq(value.rawValue);
			}
		}
	}

	public void unmarshall(ByteStreamReader reader) 
	{
		constraints = new java.util.Hashtable<String, java.util.Vector<Pair<Comparison, Value>>>(); 
		
		int size = reader.readSize();
		
		for (int i = 0; i < size; i++)
		{
			String property = reader.readString();
			
			int size_v = reader.readSize();
			for (int j=0; j<size_v; j++)
			{
				int comp_int = reader.readSize();
				Comparison comp = null;
				
				switch(comp_int)
				{
					case 0:
						comp = Comparison.EQUAL;
					case 1:
						comp = Comparison.NOT_EQUAL;
					case 2:
						comp = Comparison.LESS;
					case 3:
						comp = Comparison.LESS_EQUAL;
					case 4:
						comp = Comparison.GREATER;
					case 5:
						comp = Comparison.GREATER_EQUAL;
				}
				
				Value value = new Value(reader.readSize());
				
				//read byte array
				int n = reader.readSize();
				
				java.util.Vector<Byte> data = new java.util.Vector<Byte>();
				for (int k=0; k<n; k++)
				{
					data.add(new Byte(reader.readByte()));
				}
				value.rawValue = data;
				
				this.addConstraint(property, comp, value);
			}
		}
	}
}
