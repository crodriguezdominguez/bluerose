package org.ugr.bluerose.events;

import org.ugr.bluerose.ByteStreamReader;
import org.ugr.bluerose.ByteStreamWriter;

public class EventNode extends Event {
	public String identifier;
	public Value value;
	
	public EventNode()
	{
		identifier = null;
		value = null;
	}
	
	public EventNode(String ident, Value val)
	{
		identifier = ident;
		value = val;
	}

	public void marshall(ByteStreamWriter writer) 
	{	
		//identifier
		writer.writeString(this.identifier);
		
		//value
		this.value.marshall(writer);
		
		//topic
		writer.writeInteger(this.topic);
		
		//num. event nodes
		writer.writeSize(nodes.size());
		
		//event nodes
		java.util.Enumeration<String> e = this.nodes.keys();
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			
			EventNode node = this.nodes.get(key);
			writer.writeObject(node);
		}
	}
	
	public void unmarshall(ByteStreamReader reader) 
	{	
		//identifier
		this.identifier = reader.readString();
		
		//value
		this.value = new Value();
		value.unmarshall(reader);
		
		//topic
		this.topic = reader.readInteger();
		
		//num. event nodes
		int size = reader.readSize();
		
		if (size == 0)
		{
			return;
		}
		
		//event nodes
		for (int i=0; i<size; i++)
		{
			EventNode aux = new EventNode();
			aux.unmarshall(reader);
			
			this.setMember(aux.identifier, aux);
		}
	}
	
	@Override
	public String toString()
	{
		return value.toString();
	}
}
