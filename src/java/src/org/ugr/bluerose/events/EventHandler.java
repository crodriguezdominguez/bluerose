package org.ugr.bluerose.events;

import org.ugr.bluerose.ByteStreamReader;
import org.ugr.bluerose.ObjectProxy;
import org.ugr.bluerose.ICommunicationDevice;
import org.ugr.bluerose.messages.MessageHeader;

class PubSubProxy extends ObjectProxy {
	
	public PubSubProxy() throws Exception
	{
		super();
		
		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
		
		this.resolveInitialization(null, true, null);
	}
	
	public PubSubProxy(ICommunicationDevice device) throws Exception
	{
		super();

		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
		this.resolveInitialization(device, true, null);
	}
	
	public PubSubProxy(ICommunicationDevice device, boolean waitOnline) throws Exception
	{
		super();

		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
		this.resolveInitialization(device, waitOnline, null);
	}
	
	public PubSubProxy(ICommunicationDevice device, boolean waitOnline, java.util.Dictionary<String, java.util.Vector<Byte>> pars) throws Exception
	{
		super();

		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
		this.resolveInitialization(device, waitOnline, pars);
	}

	public PubSubProxy(String servantID) throws Exception
	{
		super(servantID);

		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
	}
	
	public PubSubProxy(String servantID, ICommunicationDevice device) throws Exception
	{
		super(servantID, device);

		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
	}
	
	public PubSubProxy(String servantID, ICommunicationDevice device, boolean waitOnline) throws Exception
	{
		super(servantID, device, waitOnline);

		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
	}

	public PubSubProxy(String servantID, ICommunicationDevice device, boolean waitOnline, java.util.Dictionary<String, java.util.Vector<Byte>> pars)
		throws Exception
	{
		super(servantID, device, waitOnline, pars);

		identity.id_name = "PubSub";
		identity.category = "BlueRoseService";
	}

	
	/**
	 * Adds a new subscription to a topic
	 *
	 * @param _topic Topic to subscribe to
	 */
	public void subscribe(int topic)
	{
		int reqID;
		currentMode = MessageHeader.ONEWAY_MODE;
		
		synchronized(mutex) {
			writer.writeInteger(topic);
		
			reqID = this.sendRequest(servantID, "subscribe0", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}

	/**
	 * Adds a new subscription to a topic, but with a set of constraints
	 *
	 * @param _topic Topic to subscribe to
	 * @param _pred Set of constraints over the properties of the events
	 */
	public void subscribe(int topic, Predicate predicate)
	{
		int reqID;
		currentMode = MessageHeader.ONEWAY_MODE;
		
		synchronized(mutex) {
			writer.writeInteger(topic);
			writer.writeObject(predicate);
		
			reqID = this.sendRequest(servantID, "subscribe1", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}

	/**
	 * Removes all the subscriptions
	 */
	public void unsubscribe()
	{
		int reqID;
		currentMode = MessageHeader.ONEWAY_MODE;
		
		synchronized(mutex){
			reqID = this.sendRequest(servantID, "unsubscribe0", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}

	/**
	 * Remove a subscription to a topic
	 *
	 * @param _topic Topic associated with the subscription to remove
	 */
	public void unsubscribe(int topic)
	{
		int reqID;
		currentMode = MessageHeader.ONEWAY_MODE;
		
		synchronized(mutex) {
			writer.writeInteger(topic);
		
			reqID = this.sendRequest(servantID, "unsubscribe1", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}

	/**
	 * Checks if any of the user subscriptions allow him/her to receive the event
	 * that is specified by the template
	 *
	 * @param userID User whose subscription is going to be checked
	 * @param templateEvent Event template
	 * @return YES if the user may receive the event specified by the template.
	 *         NO otherwise else.
	 */
	public boolean isSubscribed(String userID, Event templateEvent)
	{
		int reqID;
		
		currentMode = MessageHeader.TWOWAY_MODE;
		java.util.Vector<Byte> result_bytes = null;
		
		synchronized(mutex) {
			writer.writeString(userID);
			writer.writeObject(templateEvent);
		
			reqID = this.sendRequest(servantID, "isSubscribed", writer.toVector());
			result_bytes = this.receiveReply(reqID);
			
			writer.reset();
		}
		
		ByteStreamReader reader = new ByteStreamReader(result_bytes);
		boolean result = reader.readBoolean();
		
		return result;
	}

	/**
	 * Returns all the subscribers
	 *
	 * @return Vector of subscribers
	 */
	public java.util.Vector<String> getSubscribers()
	{
		int reqID;
		
		currentMode = MessageHeader.TWOWAY_MODE;
		java.util.Vector<Byte> result_bytes = null;
		
		synchronized(mutex) {
			reqID = this.sendRequest(servantID, "getSubscribers0", writer.toVector());
			result_bytes = this.receiveReply(reqID);
			
			writer.reset();
		}
		
		ByteStreamReader reader = new ByteStreamReader(result_bytes);
		java.util.Vector<String> result = reader.readStringSeq();
		
		return result;
	}

	/**
	 * Returns all the subscribers that may receive the specified event
	 *
	 * @param templateEvent Event
	 * @return Vector of subscribers
	 */
	public java.util.Vector<String> getSubscribers(Event templateEvent)
	{
		int reqID;
		
		currentMode = MessageHeader.TWOWAY_MODE;
		java.util.Vector<Byte> result_bytes = null;
		
		synchronized(mutex) {
			writer.writeObject(templateEvent);
			
			reqID = this.sendRequest(servantID, "getSubscribers1", writer.toVector());
			result_bytes = this.receiveReply(reqID);
			
			writer.reset();
		}
		
		ByteStreamReader reader = new ByteStreamReader(result_bytes);
		java.util.Vector<String> result = reader.readStringSeq();
		
		return result;
	}

	/**
	 * Publishes an event to all its subscribers (including the publisher, if it's subscribed
	 * to the event)
	 *
	 * @param event Event to be published
	 *
	 */
	public void publish(Event event)
	{
		this.publish(event, true);
	}

	/**
	 * Publishes an event to all its subscribers
	 *
	 * @param event Event to be published
	 * @param comeBack True if the event has to be published to the publisher because
	 *                 it is subscribed. False if the event will not be published to
	 *                 the published even if it is subscribed to it.
	 */
	public void publish(Event event, boolean comeback)
	{
		int reqID;
		currentMode = MessageHeader.TWOWAY_MODE;
		
		synchronized(mutex) {
			writer.writeObject(event);
			writer.writeBoolean(comeback);
			
			reqID = this.sendRequest(servantID, "publish", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}

	@Override
	public String getTypeID() {
		return "BlueRoseService::PubSub";
	}
}


public class EventHandler {
	
	public static PubSubProxy proxy;
	protected static java.util.Dictionary<Integer, java.util.Vector<EventListener>> listeners;

	public static void initialize(ICommunicationDevice device) throws Exception
	{
		listeners = new java.util.Hashtable<Integer, java.util.Vector<EventListener>>();
		proxy = new PubSubProxy(device);
	}

	public static void initialize(ICommunicationDevice device, boolean wait) throws Exception
	{
		listeners = new java.util.Hashtable<Integer, java.util.Vector<EventListener>>();
		proxy = new PubSubProxy(device, wait);
	}
	
	public static void initialize(ICommunicationDevice device, boolean wait, java.util.Dictionary<String, java.util.Vector<Byte>> pars) throws Exception
	{
		listeners = new java.util.Hashtable<Integer, java.util.Vector<EventListener>>();
		proxy = new PubSubProxy(device, wait, pars);
	}

	public static void initialize(String servID) throws Exception
	{
		listeners = new java.util.Hashtable<Integer, java.util.Vector<EventListener>>();
		proxy = new PubSubProxy(servID);
	}
	
	public static void initialize(String servID, ICommunicationDevice device) throws Exception
	{
		listeners = new java.util.Hashtable<Integer, java.util.Vector<EventListener>>();
		proxy = new PubSubProxy(servID, device);
	}
	
	public static void initialize(String servID, ICommunicationDevice device, boolean wait) throws Exception
	{
		listeners = new java.util.Hashtable<Integer, java.util.Vector<EventListener>>();
		proxy = new PubSubProxy(servID, device, wait);
	}
	
	public static void initialize(String servID, ICommunicationDevice device, boolean wait, java.util.Dictionary<String, java.util.Vector<Byte>> pars)
		throws Exception
	{
		listeners = new java.util.Hashtable<Integer, java.util.Vector<EventListener>>();
		proxy = new PubSubProxy(servID, device, wait, pars);
	}

	/**
	 * Destructor
	 */
	public static void destroy()
	{
		proxy.unsubscribe();
	}

	/**
	 * Action to be performed whenever an event is consumed
	 *
	 * @param event Received event
	 */
	public static void onConsume(Event event)
	{
		int topic = event.topic;
		java.util.Vector<EventListener> array = listeners.get(new Integer(topic));
		if (array != null)
		{
			for (EventListener listener : array)
			{
				if (listener.acceptsTopic(topic))
				{
					if (listener.acceptsEvent(event))
					{
						listener.performAction(event);
					}
				}
			}
		}
	}

	/**
	 * Adds an event listener to the listeners. The event handler
	 * manages the memory of the listener that is passed as an argument.
	 *
	 * @param listener Event listener to add
	 */
	public static void addEventListener(EventListener listener)
	{
		int topic = listener.topic;
		
		java.util.Vector<EventListener> array = listeners.get(new Integer(topic));
		if (array == null)
		{
			java.util.Vector<EventListener> v = new java.util.Vector<EventListener>();
			v.add(listener);
			
			listeners.put(new Integer(topic), v);
		}
		else
		{
			array.add(listener);
			listeners.put(new Integer(topic), array);
		}
		
		if (listener.predicate == null)
			proxy.subscribe(topic);
		
		else proxy.subscribe(topic, listener.predicate);
		
		//send an event
		Event evt = new Event();
		proxy.publish(evt);
	}

	/**
	 * Removes all the event listener that are related with a topic
	 *
	 * @param topic Topic to remove
	 */
	public static void removeEventListeners(int topic)
	{
		Integer top = new Integer(topic);
		java.util.Vector<EventListener> array = listeners.get(top);
		if (array != null)
		{
			proxy.unsubscribe(topic);
			listeners.remove(top);
		}
	}

	/**
	 * Removes an event listener from the listeners
	 *
	 * @param listener Listener to remove
	 */
	public static void removeEventListener(EventListener listener)
	{
		int topic = listener.topic;
		
		Integer top = new Integer(topic);
		java.util.Vector<EventListener> array = listeners.get(top);
		if (array != null)
		{
			for (EventListener list : array)
			{
				if (list.equals(listener))
				{
					array.remove(list);
					
					listeners.put(top, array);
					
					if (array.size() == 0)
					{
						removeEventListeners(topic);
					}
					
					return;
				}
			}
		}
	}

	/**
	 * Publishes an event to all its subscribers (including the publisher, if it's subscribed
	 * to the event)
	 *
	 * @param event Event to be published
	 */
	public static void publish(Event event)
	{
		proxy.publish(event);
	}

	/**
	 * Publishes an event to all its subscribers
	 *
	 * @param event Event to be published
	 * @param comeBack True if the event has to be published to the publisher because
	 *                 it is subscribed. False if the event will not be published to
	 *                 the published even if it is subscribed to it.
	 */
	public static void publish(Event event, boolean comeback)
	{
		proxy.publish(event, comeback);
	}
}

