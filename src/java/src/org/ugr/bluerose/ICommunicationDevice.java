package org.ugr.bluerose;

/**
 * Generic representation of transmission channel
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
public interface ICommunicationDevice {

	/**
	 * Open a new connection between local user and a remote one
	 *
	 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
	 * @param parameters A dictionary containing some parameters for the connection (i.e.: QoS)
	 * @throws Exception 
	 */
	public void openConnection(String userID, java.util.Dictionary<String, java.util.Vector<Byte>> pars) throws Exception;

	/**
	 * Open a new connection between local user and a remote one
	 *
	 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
	 */
	public void openConnection(String userID) throws Exception;


	/**
	 * Checks whether the connection to an specific user is openned or not
	 *
	 * @param userID User to check whether the connection is openned with or not.
	 * @return True if the connection is openned. False otherwise else.
	 */
	public boolean isConnectionOpenned(String userID);

	/**
	 * Close a connection between local user and a remote one
	 *
	 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
	 */
	public void closeConnection(String userID);

	/**
	 * Close all connections in this transmission channel
	 */
	public void closeAllConnections();

	/**
	 * Send a message to a remote user
	 *
	 * @param userID Receiver identification
	 * @param message Message to send
	 */
	public boolean write(String userID, java.util.Vector<Byte> message);

	/**
	 * Receive a message from a sender
	 *
	 * @param userID Identification of the sender
	 * @return Received message or nil if the userID was not connected
	 */
	public java.util.Vector<Byte> read(String userID);

	/**
	 * Set priority of the transmission channel in the interface pool
	 *
	 * @param priority Priority of the channel
	 */
	public void setPriority(int priority);

	/**
	 * Get priority of the transmission channel in the interface pool
	 *
	 * @return Priority
	 */
	public int getPriority();

	/**
	 * Get identificators of neighbour users
	 *
	 * @return Vector containing user identificators
	 */
	public java.util.Vector<String> getNeighbours();

	/**
	 * Broadcast a message to all neighbour users
	 *
	 * @param message Message to send
	 */
	public void broadcast(java.util.Vector<Byte> message);

	/**
	 * Checks if writing or reading is blockant in this channel
	 *
	 * @return True if it's blockant. False otherwise else.
	 */
	public boolean isBlockantConnection();

	/**
	 * Checks if the transmission of data is possible
	 *
	 * @return True if is available. False otherwise else.
	 */
	public boolean isAvailable();

	/**
	 * Checks if the transmission of data to an specific user is possible
	 *
	 * @param userID Identifier of the user
	 * @return True if is available. False otherwise else.
	 */
	public boolean isUserAvailable(String userID);

	/**
	 * Checks if the transmission interface is connection oriented
	 */
	public boolean isConnectionOriented();

	/**
	* Sets the current identifier of the servant (i.e. the ip address in case of TCP)
	*
	* @param servantID Identifier of the servant (i.e. the ip address in case of TCP)
	*/
	public void setServantIdentifier(String servantID);
	
	/**
	 * Sets the current machine as a servant
	 *
	 * @param multiplexingId Id for multiplexing connections (i.e. a port in case of TCP)
	 */
	public void setServant(String multiplexingId);

	/**
	 * Waits for connections. Only for servants.
	 */
	public void waitForConnections();

	/**
	 * Returns the name of the interface
	 *
	 * @return Name of the interface
	 */
	public String getDeviceName();

	/**
	 * If it's a servant, it returns the identifier
	 *
	 * @return Identifier of the current servant or "" if it's not a servant
	 */
	public String getServantIdentifier();	
}

