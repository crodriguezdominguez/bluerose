package org.ugr.bluerose;

public abstract class ObjectServant 
{
	public Identity identity;
	
	protected static Object mutex = new Object();
	protected static ByteStreamWriter writer = new ByteStreamWriter();
	
	public ObjectServant()
	{
		identity = new Identity();
	}
	
	public ObjectServant(Identity ident)
	{
		identity = ident;
	}
	
	public ObjectServant(String id_name, String category)
	{
		identity = new Identity();
		
		identity.id_name = id_name;
		identity.category = category;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if (object.getClass().equals(this.getClass()))
		{
			Identity aux = (Identity)object;
			if (aux.isEqualToIdentity(identity))
			{
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public String getIdentifier()
	{
		return identity.category+"::"+identity.id_name;
	}
	
	public abstract MethodResult runMethod(String method_name, String userID, java.util.Vector<Byte> args);
	/*
	 TODO 
		MethodResult result = new MethodResult();
		java.lang.reflect.Method[] methods = this.getClass().getMethods();
		
		java.lang.reflect.Method method = null;
		for (int i=0; i<methods.length; i++)
		{
			if (methods[i].toString().equals(method_name))
			{
				method = methods[i];
				break;
			}
		}
		
		if (method != null)
		{
			Class<?>[] parameters = method.getParameterTypes();
			method.
			
			for (int i=0; i<parameters.length; i++)
			{	
				if (parameters[i].equals(int.class))
				{
					
				}
			}
			
			method.in
		}
		else
		{
			result.status = MessageHeader.OPERATION_NOT_EXIST_STATUS;
			return result;
		}
	}*/
}
