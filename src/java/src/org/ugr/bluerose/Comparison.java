package org.ugr.bluerose;

/**
 * Types of comparisons between values
 */
public enum Comparison {
	EQUAL,
	NOT_EQUAL,
	LESS,
	LESS_EQUAL,
	GREATER,
	GREATER_EQUAL
}