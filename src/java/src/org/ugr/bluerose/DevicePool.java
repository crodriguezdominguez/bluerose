package org.ugr.bluerose;

import org.ugr.bluerose.messages.MessageHeader;
import org.ugr.bluerose.messages.ReplyMessage;
import org.ugr.bluerose.messages.ReplyMessageHeader;
import org.ugr.bluerose.messages.RequestMessage;
import org.ugr.bluerose.messages.RequestMessageHeader;

/**
 * Pool of interfaces. It also includes servant objects, as they may be
 * considered as an abstraction over an specific interface.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
public class DevicePool 
{	
	protected static java.util.Dictionary<String, ICommunicationDevice> deviceCollection; /**< Allowed transmission devices */
	protected static ICommunicationDevice device; /**< Current transmission channel */
	protected static java.util.Dictionary<String, ObjectServant> objects; /**< Registered objects servants */
	
	/**
	 * Starts the pool, selecting the current interface
	 */
	public static synchronized void initialize()
	{
		deviceCollection = new java.util.Hashtable<String, ICommunicationDevice>();
		objects = new java.util.Hashtable<String, ObjectServant>();
		ReplyMessageStack.initialize();
		device = getNextDevice();
	}

	/**
	 * Shutdown the interface pool, releasing all resources.
	 */
	public static synchronized void destroy()
	{
		ReplyMessageStack.destroy();
		
		java.util.Enumeration<String> e = objects.keys();
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			ObjectServant value = objects.get(key);
			
			if (DiscoveryProxy.defaultProxy != null)
			{
				DiscoveryProxy.defaultProxy.removeRegistration(value.identity.toString());
			}
		}
	}

	/**
	 * Adds a new interface to the collection of interfaces
	 *
	 * @param dev Interface to add
	 */
	public static synchronized void addDevice(ICommunicationDevice dev)
	{
		deviceCollection.put(dev.getDeviceName(), dev);
		
		if (deviceCollection.size() == 1)
		{
			device = getNextDevice();
		}
	}

	/**
	 * Registers a new object into the pool
	 *
	 * @param object Object to register
	 */
	public static synchronized void registerObjectServant(ObjectServant object)
	{
		objects.put(object.identity.toString(), object);
	}

	/**
	 * Unregisters an object from the pool
	 *
	 * @param obj Object to unregister
	 */
	public static synchronized void unregisterObjectServant(ObjectServant object)
	{
		objects.remove(object.identity.toString());
		
		//unregister from the discovery proxy
		if (DiscoveryProxy.defaultProxy != null)
		{
			DiscoveryProxy.defaultProxy.removeRegistration(object.identity.toString());
		}
		
		DevicePool.unregisterObjectServant(object);
	}

	/**
	 * Selects a new interface for transmitting data based upon the priority of the channels
	 *
	 * @return New transmission interface
	 */
	public static synchronized ICommunicationDevice getNextDevice()
	{
		//find the device with the higher priority (the lowest number)
		java.util.Enumeration<String> e = deviceCollection.keys();
		ICommunicationDevice result = null;
		int minPriority;
		if (e.hasMoreElements())
		{
			String key = e.nextElement();
			result = deviceCollection.get(key);
			minPriority = result.getPriority();
		}
		else return result;
		
		while(e.hasMoreElements())
		{
			String key = e.nextElement();
			ICommunicationDevice value = deviceCollection.get(key);
			if (value.getPriority() < minPriority)
			{
				minPriority = value.getPriority();
				result = value;
			}
		}
		
		return result;
	}

	/**
	 * Message Dispatcher for asynchronous calls and/or server requests
	 *
	 * @param message Message to dispatch
	 * @param userID Sender user id
	 */
	public static synchronized void dispatchMessage(java.util.Vector<Byte> message, String userID)
	{
		ReplyMessage reply = new ReplyMessage();
		RequestMessage orig = new RequestMessage(message);

		MethodResult replyStatus = new MethodResult();
		
		ReplyMessageHeader header = (ReplyMessageHeader)reply.header;
		RequestMessageHeader header2 = (RequestMessageHeader)orig.header;
		
		int requestId = header2.requestId;
		
		header.requestId = requestId;
		
		//identify destination object
		ObjectServant serv = objects.get(header2.identity.toString());
		
		if (serv != null)
		{
			replyStatus = serv.runMethod(header2.operation, userID, orig.body.byteCollection);
		}
		else
		{
			replyStatus.status = MessageHeader.OBJECT_NOT_EXIST_STATUS;
		}
		
		header.replyStatus = replyStatus.status;
		
		if (replyStatus.status == MessageHeader.SUCCESS_STATUS)
			reply.addToEncapsulation(replyStatus.result);
		
		//write reply msg
		java.util.Vector<Byte> reply_bytes = reply.getBytes();
		
		if (header2.mode == MessageHeader.TWOWAY_MODE)
		{
			device.write(userID, reply_bytes);
		}
	}

	/**
	 * Retrieves the current interface
	 */
	public static synchronized ICommunicationDevice getCurrentDevice()
	{
		return device;
	}

	/**
	 * Returns the transmission interface for the provided name
	 *
	 * @param name Name of the transmission interface
	 *
	 * @return Transmission interface
	 */
	public static synchronized ICommunicationDevice getDeviceForName(String name)
	{
		return deviceCollection.get(name);
	}
}

