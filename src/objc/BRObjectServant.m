#import "BRObjectServant.h"
#import "BRMessageHeader.h"

@implementation BRObjectServant

@synthesize identity;

-(id) init
{
	if (self = [super init])
	{
		identity = [[BRIdentity alloc] init];
	}
	
	return self;
}

-(id) initWithIdentity:(BRIdentity*)ident
{
	if (self = [super init])
	{
		identity = [ident retain];
	}
	
	return self;
}

-(id) initWithName:(NSString*)id_name andCategory:(NSString*)category
{
	if (self = [super init])
	{
		identity = [[BRIdentity alloc] init];
		identity.id_name = id_name;
		identity.category = category;
	}
	
	return self;
}

-(void) dealloc
{
	[identity release];
	
	[super dealloc];
}

-(NSString*) identifier
{
	return [NSString stringWithFormat:@"%@::%@", identity.category, identity.id_name];
}

-(BRMethodResult*) runMethod:(NSString*)method forUserID:(NSString*)userID arguments:(NSData*)args
{
	BRMethodResult* result = [[BRMethodResult alloc] init];
	result.status = BR_OPERATION_NOT_EXIST_STATUS;
	
	return [result autorelease];
}

@end
