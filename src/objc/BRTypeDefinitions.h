typedef unsigned char BRByte; /**< Definition of a byte */

/**
 * Result for the runMethod method of the servant of an object
 *
 * @author Carlos Rodriguez Dominguez
 * @date 03-03-2010
 */
@interface BRMethodResult : NSObject {
	BRByte status;
	NSData* result;
}

@property (readwrite, assign) BRByte status;
@property (readwrite, retain) NSData* result;

@end

/**
 * Types of comparisons between values
 */
typedef enum _BRComparison {
	BR_EQUAL, BR_NOT_EQUAL, BR_LESS, BR_LESS_EQUAL, BR_GREATER, BR_GREATER_EQUAL
} BRComparison;


/**
 * Identity for a request message
 */
@interface BRIdentity : NSObject {
	NSString* id_name;
	NSString* category;
}

@property (readwrite, retain) NSString* id_name;
@property (readwrite, retain) NSString* category;

-(BOOL) isEqualToIdentity:(BRIdentity*)identity;
-(NSString*) stringValue;

@end

/**
 * Pair of values
 */
@interface BRPair : NSObject {
	id first;
	id second;
}

@property (readwrite, retain) id first;
@property (readwrite, retain) id second;

@end

