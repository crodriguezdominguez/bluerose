#import <Foundation/Foundation.h>
#import "BRICommunicationDevice.h"

/**
 * Thread for openning a new connection whenever connection
 * to a service is lost.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 15-12-2009
 */
@interface BROpenConnectionThread : NSThread {
	id<BRICommunicationDevice> device; /**< Transmission interface for the thread */
	NSString* userID; /**< User identificator of the sender */
	NSMutableDictionary* parameters;
	NSInteger maxRetries;
}

/**
 * Constructor with parameters
 *
 * @param dev Interface for openning the connection
 * @param idenetifier Id of the user to open the connection with
 */
-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier;

/**
 * Constructor with parameters
 *
 * @param dev Interface for openning the connection
 * @param identifier Id of the user to open the connection with
 * @param retries Max number of retries before ending the thread. if mRetries==INT_MAX, it will retry forever.
 */
-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier maxRetries:(NSInteger)retries;

/**
 * Constructor with parameters
 *
 * @param dev Interface for openning the connection
 * @param identifier Id of the user to open the connection with
 * @param retries Max number of retries before ending the thread. if mRetries==INT_MAX, it will retry forever.
 * @param pars Parameters of the connection 
 */
-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev 
						   userID:(NSString*)identifier 
					   maxRetries:(NSInteger)retries 
					   parameters:(NSMutableDictionary*)pars;

@end
