#import <Foundation/Foundation.h>
#import "BRTypeDefinitions.h"


@interface BRObjectServant : NSObject {
	BRIdentity* identity;
}

@property (readonly) BRIdentity* identity;

-(id) initWithIdentity:(BRIdentity*)ident;
-(id) initWithName:(NSString*)id_name andCategory:(NSString*)category;
-(NSString*) identifier;
-(BRMethodResult*) runMethod:(NSString*)method forUserID:(NSString*)userID arguments:(NSData*)args;

@end
