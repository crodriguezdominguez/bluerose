#import <pthread.h>
#import "BRMessage.h"
#import "BRByteStreamWriter.h"
#import "BRByteStreamReader.h"


@implementation BRMessage

@synthesize header;
@synthesize body;

static BRByteStreamWriter* writer = nil;
static BRByteStreamReader* reader = nil;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t mutex_reader = PTHREAD_MUTEX_INITIALIZER;

-(id) init
{
	if (self = [super init])
	{
		header = nil;
		body = nil;
		
		if (writer == nil)
			writer = [[BRByteStreamWriter alloc] init];
		
		if (reader == nil)
			reader = [[BRByteStreamReader alloc] init];
	}
	
	return self;
}

-(void) dealloc
{
	[header release];
	[body release];
	
	[super dealloc];
}

-(NSData*) bytes
{
	NSMutableData* res = [[NSMutableData alloc] init];
	
	pthread_mutex_lock(&mutex);
	
	[writer open:res];
	
	if (body != nil)
	{
		NSData* hd = [header bytes];
		header.messageSize = ([hd length] + 6 + [body.byteCollection length]);
		
		NSData* hd2 = [header bytes];
		
		[writer writeRawBytes:hd2];
		[writer writeInteger:body.size];
		[writer writeByte:body.major];
		[writer writeByte:body.minor];
		[writer writeRawBytes:body.byteCollection];
	}
	else
	{
		NSData* hd = [header bytes];
		[writer writeRawBytes:hd];
	}
	
	[writer close];
	pthread_mutex_unlock(&mutex);
	
	return [res autorelease];
}

-(void) addToEncapsulation:(NSData*)bytes
{
	if (body != nil)
	{
		body.size += [bytes length];
		[body.byteCollection appendData:bytes];
	}
}

@end

@implementation BRBatchRequestMessage

-(id) init
{
	if (self = [super init])
	{
		header = [[BRBatchRequestMessageHeader alloc] init];
		body = [[BREncapsulation alloc] init];
	}
	
	return self;
}

-(id) initWithBytes:(NSData*)bytes
{
	if (self = [super init])
	{
		NSMutableDictionary* context = [[NSMutableDictionary alloc] init];
		NSUInteger size, i;
		
		header = [[BRBatchRequestMessageHeader alloc] init];
		body = [[BREncapsulation alloc] init];
		
		BRBatchRequestMessageHeader* h2 = (BRBatchRequestMessageHeader*)header;
		
		pthread_mutex_lock(&mutex_reader);
		[reader open:bytes];
		
		[reader skip:14];
		
		h2.numRequests = [reader readInteger];
		
		h2.identity.id_name = [reader readString];
		h2.identity.category = [reader readString];
		
		h2.facet = [NSMutableArray arrayWithArray:[reader readStringSeq]];
		
		h2.operation = [reader readString];
		
		h2.mode = [reader readByte];
		
		size = [reader readSize];
		for (i=0; i<size; i++)
		{
			NSString* key = [reader readString];
			NSString* value = [reader readString];
			
			[context setValue:value forKey:key];
		}
		
		h2.context = context;
		
		body.size = [reader readInteger];
		
		body.major = [reader readByte];
		body.minor = [reader readByte];
		
		body.byteCollection = [NSMutableData dataWithData:[reader readToEnd]];
		
		[reader close];
		
		pthread_mutex_unlock(&mutex_reader);
		
		[context release];
	}
	
	return self;
}

@end

@implementation BRRequestMessage

-(id) init
{
	if (self = [super init])
	{
		header = [[BRRequestMessageHeader alloc] init];
		body = [[BREncapsulation alloc] init];
	}
	
	return self;
}

-(id) initWithBytes:(NSData*)bytes
{
	if (self = [super init])
	{
		NSMutableDictionary* context = [[NSMutableDictionary alloc] init];
		NSUInteger size, i;
		
		header = [[BRRequestMessageHeader alloc] init];
		body = [[BREncapsulation alloc] init];
		
		BRRequestMessageHeader* h1 = (BRRequestMessageHeader*)header;
		
		pthread_mutex_lock(&mutex_reader);
		[reader open:bytes];
		
		[reader skip:14];
		
		h1.requestId = [reader readInteger];
		
		h1.identity.id_name = [reader readString];
		h1.identity.category = [reader readString];
		
		h1.facet = [NSMutableArray arrayWithArray:[reader readStringSeq]];
		
		h1.operation = [reader readString];
		h1.mode = [reader readByte];
		
		size = [reader readSize];
		for (i=0; i<size; i++)
		{
			NSString* key = [reader readString];
			NSString* value = [reader readString];
			
			[context setValue:value forKey:key];
		}
		
		h1.context = context;
		
		body.size = [reader readInteger];
		
		body.major = [reader readByte];
		body.minor = [reader readByte];
		
		body.byteCollection = [NSMutableData dataWithData:[reader readToEnd]];
		
		[reader close];
		
		pthread_mutex_unlock(&mutex_reader);
		[context release];
	}
	
	return self;
}

@end

@implementation BRReplyMessage

-(id) init
{
	if (self = [super init])
	{
		header = [[BRReplyMessageHeader alloc] init];
		body = [[BREncapsulation alloc] init];
	}
	
	return self;
}

-(id) initWithBytes:(NSData*)bytes
{
	if (self = [super init])
	{
		header = [[BRReplyMessageHeader alloc] init];
		body = [[BREncapsulation alloc] init];
		
		BRReplyMessageHeader* h3 = (BRReplyMessageHeader*)header;
		
		pthread_mutex_lock(&mutex_reader);
		[reader open:bytes];
		
		[reader skip:14];
		
		h3.requestId = [reader readInteger];
		h3.replyStatus = [reader readByte];
		
		body.size = [reader readInteger];
		
		body.major = [reader readByte];
		body.minor = [reader readByte];
		
		body.byteCollection = [NSMutableData dataWithData:[reader readToEnd]];
		
		[reader close];
		pthread_mutex_unlock(&mutex_reader);
	}
	
	return self;
}

@end

@implementation BRValidateConnectionMessage

-(id) init
{
	if (self = [super init])
	{
		header = [[BRValidateConnectionMessageHeader alloc] init];
	}
	
	return self;
}

@end

@implementation BRCloseConnectionMessage

-(id) init
{
	if (self = [super init])
	{
		header = [[BRCloseConnectionMessageHeader alloc] init];
	}
	
	return self;
}

@end

@implementation BREventMessage

@synthesize evt;

-(id) init
{
	if (self = [super init])
	{
		evt = [[NSMutableData alloc] init];
		header = [[BREventMessageHeader alloc] init];
	}
	
	return self;
}

-(id) initWithBytes:(NSData*)bytes
{
	if (self = [super init])
	{
		evt = [[NSMutableData alloc] initWithData:bytes];
		header = [[BREventMessageHeader alloc] init];
	}
	
	return self;
}

-(void) dealloc
{
	[evt release];
	
	[super dealloc];
}

-(NSData*) bytes
{
	NSMutableData* res = [[NSMutableData alloc] init];
	
	pthread_mutex_lock(&mutex);
	[writer open:res];
	
	NSData* header_bytes = [header bytes];
	header.messageSize = ([header_bytes length] + [evt length]);

	header_bytes = [header bytes];
	
	[writer writeRawBytes:header_bytes];
	[writer writeRawBytes:evt];
	
	[writer close];
	pthread_mutex_unlock(&mutex);
	
	return [res autorelease];
}

@end


