#import <pthread.h>
#import "BRValue.h"
#import "BRByteStreamReader.h"
#import "BRByteStreamWriter.h"

@implementation NSArray (BRComparing)

-(NSComparisonResult) compare:(NSArray*)otherArray
{
	NSUInteger size;
	
	if ([self isEqualToArray:otherArray])
	{
		return NSOrderedSame;
	}
	
	if ([self count] < [otherArray count])
	{
		size = [self count];
	}
	else
	{
		size = [otherArray count];
	}
	
	//equal sizes...
	for (NSUInteger i=0; i<size; i++)
	{
		id value1 = [self objectAtIndex:i];
		id value2 = [otherArray objectAtIndex:i];
		
		@try {
			int comp = [value1 compare:value2];
			if (comp == NSOrderedAscending)
			{
				return NSOrderedAscending;
			}
			
			else if (comp == NSOrderedDescending)
			{
				return NSOrderedDescending;
			}
		}
		@catch (NSException* e)
		{
			@throw [NSException exceptionWithName:@"Arrays not comparable" reason:@"Objects of the arrays are not comparable" userInfo:nil];
		}
	}
	
	//everything is equal...
	if ([self count] < [otherArray count])
	{
		return NSOrderedAscending;
	}
	else if ([self count] > [otherArray count])
	{
		return NSOrderedDescending;
	}
	
	return NSOrderedSame;
}

@end


@implementation BRValue

@synthesize type;
@synthesize rawValue;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t mutex_reader = PTHREAD_MUTEX_INITIALIZER;
static BRByteStreamWriter* writer = nil;
static BRByteStreamReader* reader = nil;

-(id) init
{
    self = [super init];
	if (self)
	{
		type = BR_NULL_TYPE;
		rawValue = [[NSMutableData alloc] init];
		
		if (writer == nil)
			writer = [[BRByteStreamWriter alloc] init];
		
		if (reader == nil)
			reader = [[BRByteStreamReader alloc] init];
	}
	
	return self;
}

-(id) initWithValueType:(BRValueType)value_type
{
    self = [super init];
	if (self)
	{
		type = value_type;
		rawValue = [[NSMutableData alloc] init];
		
		if (writer == nil)
			writer = [[BRByteStreamWriter alloc] init];
		
		if (reader == nil)
			reader = [[BRByteStreamReader alloc] init];
	}
	
	return self;
}

-(id)copyWithZone:(NSZone *)zone
{
    BRValue* copy = [[BRValue allocWithZone:zone] init];
    copy.type = self.type;
    id raw = [self.rawValue mutableCopy];
    copy.rawValue = raw;
    [raw release];
    
    return copy;
}

-(void)dealloc
{
    [rawValue release];
    
    [super dealloc];
}

-(BOOL) isLessThan:(BRValue*)value
{	
	if (type == BR_BYTE_TYPE) return ([self getByte] < [value getByte]);
	else if (type == BR_SHORT_TYPE) return ([self getShort] < [value getShort]);
	else if (type == BR_INTEGER_TYPE) return ([self getInteger] < [value getInteger]);
	else if (type == BR_LONG_TYPE) return ([self getLong] < [value getLong]);
	else if (type == BR_FLOAT_TYPE) return ([self getFloat] < [value getFloat]);
	else if (type == BR_DOUBLE_TYPE) return ([self getDouble] < [value getDouble]);
	else if (type == BR_STRING_TYPE) return ([[self getString] compare:[value getString]] == NSOrderedAscending);
	else if (type == BR_SEQ_BYTE_TYPE) return ([[self getByteSeq] compare:[value getByteSeq]] == NSOrderedAscending);
	else if (type == BR_SEQ_SHORT_TYPE) return ([[self getShortSeq] compare:[value getShortSeq]] == NSOrderedAscending);
	else if (type == BR_SEQ_INTEGER_TYPE) return ([[self getIntegerSeq] compare:[value getIntegerSeq]] == NSOrderedAscending);
	else if (type == BR_SEQ_LONG_TYPE) return ([[self getLongSeq] compare:[value getLongSeq]] == NSOrderedAscending);
	else if (type == BR_SEQ_FLOAT_TYPE) return ([[self getFloatSeq] compare:[value getFloatSeq]] == NSOrderedAscending);
	else if (type == BR_SEQ_DOUBLE_TYPE) return ([[self getDoubleSeq] compare:[value getDoubleSeq]] == NSOrderedAscending);
	else if (type == BR_SEQ_STRING_TYPE) return ([[self getStringSeq] compare:[value getStringSeq]] == NSOrderedAscending);
	else return NO;
}

-(BOOL) isGreaterThan:(BRValue*)value
{
	if (type == BR_BYTE_TYPE) return ([self getByte] > [value getByte]);
	else if (type == BR_SHORT_TYPE) return ([self getShort] > [value getShort]);
	else if (type == BR_INTEGER_TYPE) return ([self getInteger] > [value getInteger]);
	else if (type == BR_LONG_TYPE) return ([self getLong] > [value getLong]);
	else if (type == BR_FLOAT_TYPE) return ([self getFloat] > [value getFloat]);
	else if (type == BR_DOUBLE_TYPE) return ([self getDouble] > [value getDouble]);
	else if (type == BR_STRING_TYPE) return ([[self getString] compare:[value getString]] == NSOrderedDescending);
	else if (type == BR_SEQ_BYTE_TYPE) return ([[self getByteSeq] compare:[value getByteSeq]] == NSOrderedDescending);
	else if (type == BR_SEQ_SHORT_TYPE) return ([[self getShortSeq] compare:[value getShortSeq]] == NSOrderedDescending);
	else if (type == BR_SEQ_INTEGER_TYPE) return ([[self getIntegerSeq] compare:[value getIntegerSeq]] == NSOrderedDescending);
	else if (type == BR_SEQ_LONG_TYPE) return ([[self getLongSeq] compare:[value getLongSeq]] == NSOrderedDescending);
	else if (type == BR_SEQ_FLOAT_TYPE) return ([[self getFloatSeq] compare:[value getFloatSeq]] == NSOrderedDescending);
	else if (type == BR_SEQ_DOUBLE_TYPE) return ([[self getDoubleSeq] compare:[value getDoubleSeq]] == NSOrderedDescending);
	else if (type == BR_SEQ_STRING_TYPE) return ([[self getStringSeq] compare:[value getStringSeq]] == NSOrderedDescending);
	else return NO;
}

-(BOOL) isLessEqualThan:(BRValue*)value
{
	return ![self isGreaterThan:value];
}

-(BOOL) isGreaterEqualThan:(BRValue*)value
{
	return ![self isLessThan:value];
}

-(BOOL) isEqualTo:(BRValue*)value
{
	if (type == BR_BYTE_TYPE) return ([self getByte] == [value getByte]);
	else if (type == BR_SHORT_TYPE) return ([self getShort] == [value getShort]);
	else if (type == BR_INTEGER_TYPE) return ([self getInteger] == [value getInteger]);
	else if (type == BR_LONG_TYPE) return ([self getLong] == [value getLong]);
	else if (type == BR_FLOAT_TYPE) return ([self getFloat] == [value getFloat]);
	else if (type == BR_DOUBLE_TYPE) return ([self getDouble] == [value getDouble]);
	else if (type == BR_STRING_TYPE) return ([[self getString] compare:[value getString]] == NSOrderedSame);
	else if (type == BR_SEQ_BYTE_TYPE) return ([[self getByteSeq] compare:[value getByteSeq]] == NSOrderedSame);
	else if (type == BR_SEQ_SHORT_TYPE) return ([[self getShortSeq] compare:[value getShortSeq]] == NSOrderedSame);
	else if (type == BR_SEQ_INTEGER_TYPE) return ([[self getIntegerSeq] compare:[value getIntegerSeq]] == NSOrderedSame);
	else if (type == BR_SEQ_LONG_TYPE) return ([[self getLongSeq] compare:[value getLongSeq]] == NSOrderedSame);
	else if (type == BR_SEQ_FLOAT_TYPE) return ([[self getFloatSeq] compare:[value getFloatSeq]] == NSOrderedSame);
	else if (type == BR_SEQ_DOUBLE_TYPE) return ([[self getDoubleSeq] compare:[value getDoubleSeq]] == NSOrderedSame);
	else if (type == BR_SEQ_STRING_TYPE) return ([[self getStringSeq] compare:[value getStringSeq]] == NSOrderedSame);
	else return NO;
}

-(BOOL) compareTo:(BRValue*)value withComparison:(BRComparison)comparison
{
	if (type != value.type) return NO;
	
	if (comparison == BR_EQUAL)
	{
		if ([rawValue isEqualToData:value.rawValue]) return YES;
		else return false;
	}
	
	if (comparison == BR_NOT_EQUAL)
	{
		if (![rawValue isEqualToData:value.rawValue]) return YES;
		else return NO;
	}
	
	if (comparison == BR_LESS) return [self isLessThan:value];
	else if (comparison == BR_LESS_EQUAL) return [self isLessEqualThan:value];
	else if (comparison == BR_GREATER) return [self isGreaterThan:value];
	else if (comparison == BR_GREATER_EQUAL) return [self isGreaterEqualThan:value];
	else return NO;
}

-(void) setByte:(BRByte)value
{
	type = BR_BYTE_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeByte:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setInteger:(NSInteger)value
{
	type = BR_INTEGER_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeInteger:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setShort:(short)value
{
	type = BR_SHORT_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeShort:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setBoolean:(BOOL)value
{
	type = BR_BOOLEAN_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeBoolean:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setLong:(long)value
{
	type = BR_LONG_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeLong:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setFloat:(float)value
{
	type = BR_FLOAT_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeFloat:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setDouble:(double)value
{
	type = BR_DOUBLE_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeDouble:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setString:(NSString*)value
{
	type = BR_STRING_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeUTF8String:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setByteSeq:(NSArray*)value
{
	type = BR_SEQ_BYTE_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeByteSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setData:(NSData*)value
{
    type = BR_SEQ_BYTE_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeData:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setIntegerSeq:(NSArray*)value
{
	type = BR_SEQ_INTEGER_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeIntegerSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setShortSeq:(NSArray*)value
{
	type = BR_SEQ_SHORT_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeShortSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setLongSeq:(NSArray*)value
{
	type = BR_SEQ_LONG_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeLongSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setBooleanSeq:(NSArray*)value
{
	type = BR_SEQ_BOOLEAN_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeBooleanSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setFloatSeq:(NSArray*)value
{
	type = BR_SEQ_FLOAT_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeFloatSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setDoubleSeq:(NSArray*)value
{
	type = BR_SEQ_DOUBLE_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeDoubleSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(void) setStringSeq:(NSArray*)value
{
	type = BR_SEQ_STRING_TYPE;
	[rawValue setLength:0];
	
	pthread_mutex_lock(&mutex);
	[writer open:rawValue];
	[writer writeUTF8StringSeq:value];
	[writer close];
	pthread_mutex_unlock(&mutex);
}

-(BRByte) getByte
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	BRByte res = [reader readByte];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSInteger) getInteger
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSInteger res = [reader readInteger];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(short) getShort
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	short res = [reader readShort];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(long) getLong
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	long res = [reader readLong];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(BOOL) getBoolean
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	BOOL res = [reader readBoolean];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(float) getFloat
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	float res = [reader readFloat];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(double) getDouble
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	double res = [reader readDouble];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSString*) getString
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSString* res = [reader readUTF8String];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getByteSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readByteSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSData*) getData
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSData* res = [reader readData];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getIntegerSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readIntegerSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getShortSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readShortSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getLongSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readLongSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getBooleanSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readBooleanSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getFloatSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readFloatSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getDoubleSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readDoubleSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(NSArray*) getStringSeq
{
	pthread_mutex_lock(&mutex_reader);
	[reader open:rawValue];
	NSArray* res = [reader readUTF8StringSeq];
	[reader close];
	pthread_mutex_unlock(&mutex_reader);
	
	return res;
}

-(void) marshall:(BRByteStreamWriter*)writer
{
	[writer writeSize:(int)type];
	[writer writeSize:[rawValue length]];
	[writer writeRawBytes:rawValue];
}

-(void) unmarshall:(BRByteStreamReader*)reader
{
	type = (BRValueType)[reader readSize];
	NSArray* array = [reader readByteSeq];
		
    self.rawValue = nil;
	rawValue = [[NSMutableData alloc] init];
	for (NSNumber* number in array)
	{
		BRByte b = [number unsignedCharValue];
		[rawValue appendBytes:&b length:1];
	}
}

@end
