#import "BRUtil.h"
#import "BRTypeDefinitions.h"

/**
 * Reader for byte streams. Data types will
 * be decoded as specified by ICE (zeroc.com)
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRByteStreamReader : NSObject {
	NSData* stream; /**< Byte stream to read from */
	NSUInteger iterator; /**< Current position in the stream */
}

@property (readonly) NSData* stream;

/**
 * Allows reading from the specified byte stream
 *
 * @param str Byte stream to read from 
 */
-(void) open:(NSData*)str;

/**
 * Avoids reading from the current byte stream
 */
-(void) close;

/**
 * Skips a number of bytes from the stream
 *
 * @param num Number of bytes to skip
 */
-(void) skip:(NSInteger)num;

/**
 * Reads a size from the stream
 *
 * @return Size extracted from the stream
 */
-(NSUInteger) readSize;

/**
 * Reads from the current position to the end, but
 * without modifying the current position.
 *
 * @return Bytes extracted
 */
-(NSData*) readToEnd;

/**
 * Reads a byte from the stream
 *
 * @return Byte extracted from the stream
 */
-(BRByte) readByte;

/**
 * Reads a boolean from the stream
 *
 * @return Boolean extracted from the stream
 */
-(BOOL) readBoolean;

/**
 * Reads a short from the stream
 *
 * @return Short extracted from the stream
 */
-(short) readShort;

/**
 * Reads an integer from the stream
 *
 * @return Integer extracted from the stream
 */
-(NSInteger) readInteger;

/**
 * Reads a long from the stream
 *
 * @return Long extracted from the stream
 */
-(long) readLong;

/**
 * Reads a float from the stream
 *
 * @return Float extracted from the stream
 */
-(float) readFloat;

/**
 * Reads a double from the stream
 *
 * @return Double extracted from the stream
 */
-(double) readDouble;

/**
 * Reads an string from the stream
 *
 * @return String extracted from the stream
 */
-(NSString*) readString;

/**
 * Reads an utf8 string from the stream
 *
 * @return String extracted from the stream
 */
-(NSString*) readUTF8String;

/**
 * Reads a byte sequence from the stream
 *
 * @return Byte sequence extracted from the stream
 */
-(NSArray*) readByteSeq;

/**
 * Reads a boolean sequence from the stream
 *
 * @return Boolean sequence extracted from the stream
 */
-(NSArray*) readBooleanSeq;

/**
 * Reads a short sequence from the stream
 *
 * @return Short sequence extracted from the stream
 */
-(NSArray*) readShortSeq;

/**
 * Reads an integer sequence from the stream
 *
 * @return Integer sequence extracted from the stream
 */
-(NSArray*) readIntegerSeq;

/**
 * Reads a long sequence from the stream
 *
 * @return Long sequence extracted from the stream
 */
-(NSArray*) readLongSeq;

/**
 * Reads a float sequence from the stream
 *
 * @return Float sequence extracted from the stream
 */
-(NSArray*) readFloatSeq;

/**
 * Reads a double sequence from the stream
 *
 * @return Double sequence extracted from the stream
 */
-(NSArray*) readDoubleSeq;

/**
 * Reads a string sequence from the stream
 *
 * @return String sequence extracted from the stream
 */
-(NSArray*) readStringSeq;

/**
 * Reads an utf8 string sequence from the stream
 *
 * @return String sequence extracted from the stream
 */
-(NSArray*) readUTF8StringSeq;

/**
 * Reads a dictionary from the stream
 *
 * @return Dictionary extracted from the stream
 */
-(NSDictionary*) readDictionary;

/**
 * Reads a stream of data
 *
 * @return Stream of data extracted from the stream
 */
-(NSData*) readData;

@end
