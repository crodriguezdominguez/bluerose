#import "BRConfiguration.h"


@implementation BRConfiguration

-(id) initWithFile:(NSString*)xmlFile
{
	if (self = [super init])
	{
		LIBXML_TEST_VERSION
		
		doc = xmlParseFile([xmlFile UTF8String]);
		if (doc == NULL)
		{
			@throw [NSException exceptionWithName:@"Configuration file not found" 
								reason:@"The xml configuration file was not accesible" 
								userInfo:nil];
		}
	}
	
	return self;
}

-(void) dealloc
{
	xmlFreeDoc(doc);
	xmlCleanupParser();
	
	[super dealloc];
}

-(NSMutableArray*) devices
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;
	
	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"devices") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						if (c != NULL)
						{
							NSString* name = [NSString stringWithUTF8String:c];
							[res addObject:name];
							xmlFree(c);
						}
					}
					
					cur_node2 = cur_node2->next;
				}
				
				break;
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return [res autorelease];
}

-(NSString*) moduleForDevice:(NSString*)device
{
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;
	
	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"devices") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						if (c != NULL)
						{
							NSString* name = [NSString stringWithUTF8String:c];
							
							if ([name isEqualToString:device])
							{
								xmlFree(c);
								c = (char*)xmlGetProp(cur_node2, (xmlChar*)"module");
								
								if (c != NULL)
								{
									NSString* result = [NSString stringWithUTF8String:c];
									xmlFree(c);
									return result;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return nil;
}

-(NSMutableArray*) categories
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;
	
	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{
							NSString* name = [NSString stringWithUTF8String:c];
							[res addObject:name];
							xmlFree(c);
						}
					}
					
					cur_node2 = cur_node2->next;
				}
				
				break;
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return [res autorelease];
}

-(NSMutableArray*) servicesForCategory:(NSString*)category
{
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;
	
	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{
							NSString* name1 = [NSString stringWithUTF8String:c];
							
							if ([name1 isEqualToString:category])
							{
								xmlFree(c);
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
								
								NSMutableArray* res = [[NSMutableArray alloc] init];
								
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											NSString* name2 = [NSString stringWithUTF8String:c];
											[res addObject:name2];
											xmlFree(c);
										}
									}
									
									cur_node3 = cur_node3->next;
								}
								
								return [res autorelease];
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return nil;
}

-(NSMutableArray*) devicesForService:(NSString*)service andCategory:(NSString*)category
{
	xmlNodePtr root_element;
	xmlNodePtr cur_node1;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node1 = root_element->xmlChildrenNode;
	
	while(cur_node1 != NULL)
	{
		if (cur_node1->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node1->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node1->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{
							NSString* name1 = [NSString stringWithUTF8String:c];
							xmlFree(c);
							
							if ([name1 isEqualToString:category])
							{
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
								
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											NSString* name2 = [NSString stringWithUTF8String:c];
											xmlFree(c);
											
											if ([name2 isEqualToString:service])
											{
												NSMutableArray* res = [[NSMutableArray alloc] init];
												
												xmlNodePtr cur_node4 = cur_node3->xmlChildrenNode;
												while(cur_node4 != NULL)
												{
													if (cur_node4->type == XML_ELEMENT_NODE)
													{
														c = (char*)xmlGetProp(cur_node4, (xmlChar*)"name");
														
														if (c != NULL)
														{
															NSString* name3 = [NSString stringWithUTF8String:c];
															[res addObject:name3];
															xmlFree(c);
														}
													}
													
													cur_node4 = cur_node4->next;
												}
												
												return [res autorelease];
											}
										}
									}
									
									cur_node3 = cur_node3->next;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node1 = cur_node1->next;
	}
	
	return nil;
}

-(NSString*) portForService:(NSString*)service category:(NSString*)category andDevice:(NSString*)device
{
	xmlNodePtr root_element;
	xmlNodePtr cur_node1;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node1 = root_element->xmlChildrenNode;
	
	while(cur_node1 != NULL)
	{
		if (cur_node1->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node1->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node1->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{	
							NSString* name1 = [NSString stringWithUTF8String:c];
							xmlFree(c);
							
							if ([name1 isEqualToString:category])
							{
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
								
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											NSString* name2 = [NSString stringWithUTF8String:c];
											xmlFree(c);
											
											if ([name2 isEqualToString:service])
											{
												xmlNodePtr cur_node4 = cur_node3->xmlChildrenNode;
												while(cur_node4 != NULL)
												{
													if (cur_node4->type == XML_ELEMENT_NODE)
													{
														c = (char*)xmlGetProp(cur_node4, (xmlChar*)"name");
														
														if (c != NULL)
														{
															NSString* name3 = [NSString stringWithUTF8String:c];
															xmlFree(c);
															if ([name3 isEqualToString:device])
															{
																c = (char*)xmlGetProp(cur_node4, (xmlChar*)"port");
																
																if (c != NULL)
																{
																	NSString* result = [NSString stringWithUTF8String:c];
																	xmlFree(c);
																	return result;
																}
															}
														}
													}
													
													cur_node4 = cur_node4->next;
												}
											}
										}
									}
									
									cur_node3 = cur_node3->next;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node1 = cur_node1->next;
	}
	
	return nil;
}

-(NSString*) addressForService:(NSString*)service category:(NSString*)category andDevice:(NSString*)device
{
	xmlNodePtr root_element;
	xmlNodePtr cur_node1;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node1 = root_element->xmlChildrenNode;
	
	while(cur_node1 != NULL)
	{
		if (cur_node1->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node1->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node1->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{	
							NSString* name1 = [NSString stringWithUTF8String:c];
							xmlFree(c);
							
							if ([name1 isEqualToString:category])
							{
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
								
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											NSString* name2 = [NSString stringWithUTF8String:c];
											xmlFree(c);
											
											if ([name2 isEqualToString:service])
											{
												xmlNodePtr cur_node4 = cur_node3->xmlChildrenNode;
												while(cur_node4 != NULL)
												{
													if (cur_node4->type == XML_ELEMENT_NODE)
													{
														c = (char*)xmlGetProp(cur_node4, (xmlChar*)"name");
														
														if (c != NULL)
														{
															NSString* name3 = [NSString stringWithUTF8String:c];
															xmlFree(c);
															if ([name3 isEqualToString:device])
															{
																c = (char*)xmlGetProp(cur_node4, (xmlChar*)"address");
																
																if (c != NULL)
																{
																	NSString* result = [NSString stringWithUTF8String:c];
																	xmlFree(c);
																	return result;
																}
															}
														}
													}
													
													cur_node4 = cur_node4->next;
												}
											}
										}
									}
									
									cur_node3 = cur_node3->next;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node1 = cur_node1->next;
	}
	
	return nil;
}

-(NSString*) userIDForService:(NSString*)service category:(NSString*)category andDevice:(NSString*)device
{
	NSString* address = [self addressForService:service category:category andDevice:device];
	NSString* port = [self portForService:service category:category andDevice:device];
	
	if (address != nil && port != nil)
	{
		return ( [NSString stringWithFormat:@"%@:%@",address,port] );
	}
	
	return nil;
}

@end
