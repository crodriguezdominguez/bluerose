#import <Foundation/Foundation.h>
#import <pthread.h>

/**
 * Stack where all received messages are stored
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BRReplyMessageStack : NSObject {
	
}

/**
 * Initializes the stack
 */
+(void) initializeStack;

/**
 * Destroys the stack
 */
+(void) destroyStack;

/**
 * Adds a new message to the stack
 *
 * @param requestId Id of the request
 * @param msg Message
 */
+(void) produceForRequestId:(NSInteger)requestId bytes:(NSData*)bytes;

/**
 * Removes a message from the stack. If the message does not exist,
 * it blocks undefinitely.
 *
 * @param requestId Id of the request
 * @param res Message removed from the stack
 */
+(NSData*) consumeRequestId:(NSInteger)requestId;

/**
 * Returns whether somebody is waiting a reply or not
 *
 * @return True if someone is waiting. False otherwise else.
 */
+(BOOL) isWaitingReply;

@end
