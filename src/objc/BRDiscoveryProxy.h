#import "BRICommunicationDevice.h"
#import "BRObjectProxy.h"


@interface BRDiscoveryProxy : BRObjectProxy {

}

+(void) initializeDiscoveryProxy:(NSString*)servID;
+(void) initializeDiscoveryProxy:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device;
+(void) initializeDiscoveryProxy:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait;
+(void) initializeDiscoveryProxy:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars;

+(void) destroyDiscoveryProxy;

+(BRDiscoveryProxy*) defaultProxy;

/**
 * Overload of the function that returns the type identifier.
 * (@see BRObjectProxy.h)
 *
 * @return Type identifier
 */
-(NSString*) typeID;

/**
 * Registers a servant name and an address
 *
 * @param name Name of the servant to register
 * @param address Address of the servant
 */
-(void) addRegistration:(NSString*)name address:(NSString*)address;

/**
 * Unregisters the address that is associated with a name
 *
 * @param name Name of the servant to unregister
 */
-(void) removeRegistration:(NSString*)name;

-(NSString*) resolveName:(NSString*)name;
-(NSString*) resolveName:(NSString*)name waitOnline:(BOOL)wait;

@end
