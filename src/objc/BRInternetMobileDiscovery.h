#import <Foundation/Foundation.h>
#import "BRIMobileDiscovery.h"

typedef enum {
	BRInternetMobileDiscoveryServicePrinter,
	BRInternetMobileDiscoveryServiceBluerose,
	BRInternetMobileDiscoveryServiceDAAP,
	BRInternetMobileDiscoveryServiceDACP,
	BRInternetMobileDiscoveryServiceFTP,
	BRInternetMobileDiscoveryServiceHomeSharing,
	BRInternetMobileDiscoveryServiceHTTP,
	BRInternetMobileDiscoveryServiceHTTPS,
	BRInternetMobileDiscoveryServiceIPP,
	BRInternetMobileDiscoveryServiceJini,
	BRInternetMobileDiscoveryServiceLonworks,
	BRInternetMobileDiscoveryServiceNFS,
	BRInternetMobileDiscoveryServiceRFID,
	BRInternetMobileDiscoveryServiceRTSP,
	BRInternetMobileDiscoveryServiceScanner,
	BRInternetMobileDiscoveryServiceSFTP,
	BRInternetMobileDiscoveryServiceSSH,
	BRInternetMobileDiscoveryServiceTelnet,
	BRInternetMobileDiscoveryServiceTftp,
	BRInternetMobileDiscoveryServiceUPnP
} BRInternetMobileDiscoveryServiceType;

/**
 * An implementation of the mobile discoverer that works on a Wifi. The methods of the
 * listeners will be called with "element" parameters as instances of NSNetService class
 * and "serviceBrowser" parameters as instances of NSNetServiceBrowser class.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 16-06-2010
 */
@interface BRInternetMobileDiscovery : NSObject<BRIMobileDiscovery, NSNetServiceDelegate, NSNetServiceBrowserDelegate> {
	NSMutableDictionary* refs;
	NSNetServiceBrowser* browser;
}

@property (readonly) NSMutableArray* listeners;

-(NSString*) resolveService:(NSString*)name serviceType:(NSString*)serviceType;
-(NSString*) regType:(BRInternetMobileDiscoveryServiceType)serviceType;

@end

