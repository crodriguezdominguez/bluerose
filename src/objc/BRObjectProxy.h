#import <Foundation/Foundation.h>
#import <pthread.h>
#import "BRICommunicationDevice.h"
#import "BROpenConnectionThread.h"
#import "BRTypeDefinitions.h"
#import "BRByteStreamWriter.h"
#import "BRByteStreamReader.h"


#define MAX_REQUEST_ID 1000000


/**
 * Thread for waiting an asynchronous reply
 *
 * @author Carlos Rodriguez Dominguez
 * @date 15-11-2009
 */
@protocol BRAsyncMethodCallback<NSObject>

/**
 * Callback method that is called when an specific asynchronous
 * reply is received
 *
 * @param result Received reply message
 */
-(void) callback:(NSData*)message;

@end

/**
 * Proxy for transparently connecting to a servant
 *
 * @author Carlos Rodriguez Dominguez
 * @date 11-11-2009
 */
@interface BRObjectProxy : NSObject {
	id<BRICommunicationDevice> interface; /**< Interface for transferring information */
	BROpenConnectionThread* openThread; /**< Thread for openning connections in case the servant is disconnected */
	NSString* servantID; /**< Id of the servant */
	BRIdentity* identity; /**< Identity of the object */
	BRByte currentMode; /**< Current operational mode. @see MessageHeader.h */
	pthread_mutex_t mutex; /**< Semaphore for concurrently accesing the proxy */
}

@property (readwrite, retain) BRIdentity* identity;

/**
 * Constructor. Uses the current interface of the DevicePool.
 * If that interface changes, it also changes for the object.
 *
 * @param servID Id of the servant to connect to
 */
-(id) initWithServant:(NSString*)servID;

/**
 * Constructor. Uses the current interface of the DevicePool.
 * If that interface changes, it also changes for the object.
 *
 * @param servID Id of the servant to connect to
 * @param device Interface to connect to. It it's NULL, it will connect to the current interface in the
 *               interface pool.
 */
-(id) initWithServant:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device;

/**
 * Constructor. Uses the current interface of the DevicePool.
 * If that interface changes, it also changes for the object.
 *
 * @param servID Id of the servant to connect to
 * @param device Interface to connect to. It it's NULL, it will connect to the current interface in the
 *               interface pool.
 * @param wait If it's true, it will wait for the servant to be alive
 */
-(id) initWithServant:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait;

/**
 * Constructor. Uses the current interface of the DevicePool.
 * If that interface changes, it also changes for the object.
 *
 * @param servID Id of the servant to connect to
 * @param device Interface to connect to. It it's NULL, it will connect to the current interface in the
 *               interface pool.
 * @param wait If it's true, it will wait for the servant to be alive
 * @param pars Connection parameters
 */
-(id) initWithServant:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars;

/**
 * Auxiliar method for constructors
 */
-(void) initalizeAttributes:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars;

/**
 * Sends a request to a servant
 * 
 * @param servantID ID of the servant to send the request to
 * @param operation Operation of the request
 * @param request Message of the request
 * @return Request Identifier for receiving the reply message
 */
-(NSInteger) sendRequestToServantID:(NSString*)servID operation:(NSString*)oper request:(NSData*)request;

/**
 * Broadcasts a request
 * 
 * @param operation Operation of the request
 * @param request Message of the request
 * @return Request Identifier for receiving the reply message
 */
-(NSInteger) broadcastRequestWithOperation:(NSString*)oper request:(NSData*)request;

/**
 * Broadcast message
 * 
 * @param message Message to be broadcasted
 * @return Request Identifier for receiving the reply message
 */
-(NSInteger) broadcastMessage:(NSData*)message;

/**
 * Receives a reply. Is a blockant method.
 *
 * @param reqID Request Identifier of the request message
 * @result Received Message
 */
-(NSData*) receiveReply:(NSInteger)reqID;

/**
 * Receives a message with the callback thread and treats the received
 * message with the specified AsyncMethodCallback
 *
 * @param reqID Request Identifier of the request message
 * @param method Async method callback for treating the received message
 */
-(void) receiveCallback:(NSInteger)reqID callback:(id<BRAsyncMethodCallback>)method;

-(void) resolveInitialization:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars;

/**
 * Waits for a Servant to be online
 */
-(void) waitOnlineMode;

/**
 * Gets the type ID of the proxy
 *
 * @return String that represents the type ID
 */
-(NSString*) typeID;

+(BRByteStreamWriter*) writer;
+(BRByteStreamReader*) reader;
+(pthread_mutex_t*) mutexWriter;
+(pthread_mutex_t*) mutexReader;

@end


/**
 * Callback method for an asynchronous request
 *
 * @author Carlos Rodriguez Dominguez
 * @date 15-11-2009
 */
@interface BRAsyncCallback : NSThread {
	NSMutableDictionary* requestsId_callbacks; /**< stack of requestIds and asynchronous method callbacks */
	pthread_mutex_t mutex; /**< Mutex */
	pthread_cond_t cond; /**< Conditional variable */
}

/**
 * Adds a new request id to the async method callback stack
 *
 * @param reqId Request identifier for the reply
 * @param amc Callback method that is called when the reply is received
 */
-(void) pushRequestId:(NSInteger)requestId callback:(id<BRAsyncMethodCallback>)method;

-(void) main;

@end

