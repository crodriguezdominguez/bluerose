#import <Foundation/Foundation.h>
#import <pthread.h>
#import "BRSocketCollection.h"
#import "BRICommunicationDevice.h"

/**
 * Module for transferring messages through the default network interface.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 16-10-2009
 */
@interface BRInternetDevice : NSThread<BRICommunicationDevice> {
	NSInteger priority; /**< Priority of the module inside the interface pool. @see DevicePool.h */
	NSInteger servantSocket; /**< Socket for accepting connections to a servant object */
	NSInteger servantPort; /**< Port for accepting connections to a servant object */
	BOOL isServant; /**< True if the interface is used for a servant */
	NSString* servantAddress; /**< If it's a servant, here it resides the address */
	pthread_mutex_t mutex; /**< Mutex */
	BRSocketCollection* sockets; /**< Sockets for each accepted connection */
	NSMutableDictionary* openConnRefCounter; /**< Number of connections to a servant */
	NSMutableDictionary* servantThreads; /**< Threads for servants */
	NSMutableDictionary* clientThreads; /**< Threads for proxy objects */
}

@end
