#import "BRObjectProxy.h"
#import "BRMessageHeader.h"
#import "BRDevicePool.h"
#import "BRMessage.h"
#import "BRReplyMessageStack.h"
#import "BRDiscoveryProxy.h"
#import "BRByteStreamWriter.h"
#import "BRByteStreamReader.h"


@implementation BRObjectProxy

@synthesize identity;

static BRAsyncCallback* asyncThread = nil; /**< Callback thread for asynchronous methods */
static NSInteger numInstances = 0; /**< Number of instances of the object */
static pthread_mutex_t shared_mutex = PTHREAD_MUTEX_INITIALIZER; /**< Mutex for static data */
static NSInteger requestId = 0; /**< Request Id */

static BRByteStreamWriter* writer = nil; /**< Shared writer */
static pthread_mutex_t mutex_writer = PTHREAD_MUTEX_INITIALIZER; /**< Shared mutex for the writer */
static BRByteStreamReader* reader = nil; /**< Shared reader */
static pthread_mutex_t mutex_reader = PTHREAD_MUTEX_INITIALIZER; /**< Shared mutex for the reader */

-(id) init
{
    self = [super init];
	if (self)
	{
		interface = nil;
		openThread = nil;
		identity = [[BRIdentity alloc] init];
		
		pthread_mutex_lock(&shared_mutex);
		numInstances++;
		pthread_mutex_unlock(&shared_mutex);
		
		//interface->openConnection(_servantID, connParameters);
		currentMode = BR_TWOWAY_MODE;
		pthread_mutex_init(&mutex, 0);
		
		if (writer == nil)
			writer = [[BRByteStreamWriter alloc] init];
	
		if (reader == nil)
			reader = [[BRByteStreamReader alloc] init];
	}
	
	return self;
}

-(id) initWithServant:(NSString*)servID
{
	self = [super init];
	if (self)
	{
		[self initalizeAttributes:servID device:nil waitOnline:YES parameters:nil];
	}
	
	return self;
}

-(id) initWithServant:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device
{
	self = [super init];
	if (self)
	{
		[self initalizeAttributes:servID device:device waitOnline:YES parameters:nil];
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	self = [super init];
	if (self)
	{
		[self initalizeAttributes:servID device:device waitOnline:wait parameters:nil];
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	self = [super init];
	if (self)
	{
		[self initalizeAttributes:servID device:device waitOnline:wait parameters:pars];
	}
	
	return self;
}

-(void) initalizeAttributes:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	if (writer == nil)
		writer = [[BRByteStreamWriter alloc] init];
	
	if (reader == nil)
		reader = [[BRByteStreamReader alloc] init];
	
	identity = [[BRIdentity alloc] init];
	servantID = [servID retain];
	interface = [device retain];
	openThread = nil;
	
	pthread_mutex_lock(&shared_mutex);
	numInstances++;
	pthread_mutex_unlock(&shared_mutex);
	
	id<BRICommunicationDevice> iface_aux = (device != nil) ? interface : [BRDevicePool currentDevice];
	if (![iface_aux isConnectionOpenned:servantID])
	{
		openThread = [[BROpenConnectionThread alloc] initWithCommunicationDevice:iface_aux userID:servantID maxRetries:NSIntegerMax];
		[openThread start];
		
		if (wait)
		{ 
			while(![openThread isFinished])
			{
				[NSThread sleepForTimeInterval:0.5];
			}
			
			[openThread release];
			openThread = nil;
		}
	}
	else
	{
		[iface_aux openConnectionForUserID:servantID];
	}
	
	//interface->openConnection(_servantID, connParameters);
	currentMode = BR_TWOWAY_MODE;
	pthread_mutex_init(&mutex, 0);
}

-(void) dealloc
{
	[servantID release];
	[identity release];
	[openThread release];
	
	pthread_mutex_lock(&shared_mutex);
	numInstances--;
	if (numInstances <= 0)
	{
		[asyncThread release];
		asyncThread = nil;
		numInstances = 0;
	}
	pthread_mutex_unlock(&shared_mutex);
	
	//if (interface == NULL) DevicePool::currentInterface()->closeConnection(servantID);
	//else interface->closeConnection(servantID);
	
	interface = nil;
	
	pthread_mutex_destroy(&mutex);
	
	[super dealloc];
}

-(NSInteger) sendRequestToServantID:(NSString*)servID operation:(NSString*)oper request:(NSData*)request
{
	BOOL res;
	
	BRRequestMessage* msg = [[BRRequestMessage alloc] init];
	BRRequestMessageHeader* hd = (BRRequestMessageHeader*)msg.header;
	
	pthread_mutex_lock(&shared_mutex);
	requestId = ((requestId+1) % MAX_REQUEST_ID);
	NSInteger req = requestId;
	pthread_mutex_unlock(&shared_mutex);
	
	hd.requestId = req;
	hd.operation = oper;
	
	pthread_mutex_lock(&mutex);
	hd.mode = currentMode;
	hd.identity = identity;
    
	pthread_mutex_unlock(&mutex);
	
	[msg addToEncapsulation:request];
	
	NSData* msg_bytes = [msg bytes];
	
	if (interface == nil)
		res = [[BRDevicePool currentDevice] writeToUserID:servantID message:msg_bytes];
	
	else
    {
        res = [interface writeToUserID:servantID message:msg_bytes];
    }
	
	if (!res)
	{
		pthread_mutex_lock(&mutex);
		/**
		 * TODO: Crear una cache para tratar de proveer algunos mensajes
		 */
		if (openThread == nil)
		{
			if (interface != nil)
				openThread = [[BROpenConnectionThread alloc] initWithCommunicationDevice:interface userID:servantID maxRetries:NSIntegerMax];	
			else openThread = [[BROpenConnectionThread alloc] initWithCommunicationDevice:[BRDevicePool currentDevice] userID:servantID maxRetries:NSIntegerMax];
			
			[openThread start];
		}
		pthread_mutex_unlock(&mutex);
		
		//throw "Service Not Available";
	}
	else
	{
		pthread_mutex_lock(&mutex);
		if (openThread != nil)
		{
			[openThread release];
			openThread = nil;
		}
		pthread_mutex_unlock(&mutex);
	}
	
	[msg release];
	
	return req;
}

-(NSInteger) broadcastRequestWithOperation:(NSString *)oper request:(NSData *)request
{	
	BRRequestMessage* msg = [[BRRequestMessage alloc] init];
	BRRequestMessageHeader* hd = (BRRequestMessageHeader*)msg.header;
	
	pthread_mutex_lock(&shared_mutex);
	requestId = ((requestId+1) % MAX_REQUEST_ID);
	NSInteger req = requestId;
	pthread_mutex_unlock(&shared_mutex);
	
	hd.requestId = req;
	hd.operation = oper;
	
	pthread_mutex_lock(&mutex);
	hd.mode = currentMode;
	hd.identity = identity;
	pthread_mutex_unlock(&mutex);
	
	[msg addToEncapsulation:request];
	
	NSData* msg_bytes = [msg bytes];
	
	if (interface == nil)
		[[BRDevicePool currentDevice] broadcast:msg_bytes];
	
	else
    {
        [interface broadcast:msg_bytes];
    }
    
    [msg release];
	
	return req;
}

-(NSInteger) broadcastMessage:(NSData*)message
{
    pthread_mutex_lock(&shared_mutex);
	requestId = ((requestId+1) % MAX_REQUEST_ID);
	NSInteger req = requestId;
	pthread_mutex_unlock(&shared_mutex);
    
    if (interface == nil)
		[[BRDevicePool currentDevice] broadcast:message];
	
	else
    {
        [interface broadcast:message];
    }
    
    return req;
}

-(NSData*) receiveReply:(NSInteger)reqID
{
	pthread_mutex_lock(&mutex);
	BRByte currMode = currentMode;
	pthread_mutex_unlock(&mutex);
	
	NSData* res = nil;
	
	if (currMode == BR_TWOWAY_MODE)
	{
		NSData* recv_bytes = [BRReplyMessageStack consumeRequestId:reqID];
		BRReplyMessage* msg = [[BRReplyMessage alloc] initWithBytes:recv_bytes];
		res = [msg.body.byteCollection retain];
		
		[msg release];
	}
	
	return [res autorelease];
}

-(void) receiveCallback:(NSInteger)reqID callback:(id<BRAsyncMethodCallback>)method
{
	pthread_mutex_lock(&shared_mutex);
	
	if (asyncThread == nil)
	{
		asyncThread = [[BRAsyncCallback alloc] init];
		[asyncThread start];
	}
	
	[asyncThread pushRequestId:reqID callback:method];
	
	pthread_mutex_unlock(&shared_mutex);
}

-(void) resolveInitialization:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	if ([BRDiscoveryProxy defaultProxy] == nil)
	{
		@throw [NSException exceptionWithName:@"ERROR: DiscoveryProxy not initialized" 
									   reason:@"DiscoveryProxy has to be initialized" 
									 userInfo:nil];
	}
	
	NSString* _servantID = [[BRDiscoveryProxy defaultProxy] resolveName:[self typeID]];
	
	if ([_servantID isEqualToString:@""])
	{
		NSString* excep_msg = [NSString stringWithFormat:@"Servant for the %@ proxy has not been found", [self typeID]];
		@throw [NSException exceptionWithName:excep_msg reason:excep_msg userInfo:nil];
	}
	
	servantID = [_servantID retain];
	interface = [device retain];
	openThread = nil;
	
	id<BRICommunicationDevice> iface_aux = (device != nil) ? interface : [BRDevicePool currentDevice];
	if (![iface_aux isConnectionOpenned:servantID])
	{
		openThread = [[BROpenConnectionThread alloc] initWithCommunicationDevice:iface_aux userID:servantID maxRetries:INT_MAX parameters:pars];
		[openThread start];
		
		if (wait)
		{ 
			while(![openThread isFinished])
			{
				[NSThread sleepForTimeInterval:0.5];
			}
			
			[openThread release];
			openThread = nil;
		}
	}
	else
	{
		[iface_aux openConnectionForUserID:servantID];
	}
}

-(void) waitOnlineMode
{
	pthread_mutex_lock(&mutex);
	BOOL wait = (openThread != nil);
	pthread_mutex_unlock(&mutex);
	
	if (!wait) return;
	else
	{
		pthread_mutex_lock(&mutex);
		
		while(![openThread isFinished])
		{
			[NSThread sleepForTimeInterval:0.5];
		}
		
		[openThread release];
		openThread = nil;
		
		pthread_mutex_unlock(&mutex);
	}
}

-(NSString*) typeID
{
	return nil;
}

+(BRByteStreamWriter*) writer
{
	return writer;
}

+(BRByteStreamReader*) reader
{
	return reader;
}

+(pthread_mutex_t*) mutexWriter
{
	return &mutex_writer;
}

+(pthread_mutex_t*) mutexReader
{
	return &mutex_reader;
}

@end


@implementation BRAsyncCallback

-(id) init
{
	self = [super init];
	if (self)
	{
		requestsId_callbacks = [[NSMutableDictionary alloc] init];
		pthread_mutex_init(&mutex, 0);
		pthread_cond_init(&cond, 0);
	}
	
	return self;
}

-(void) dealloc
{
	[requestsId_callbacks release];
	
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
	
	[super dealloc];
}

-(void) pushRequestId:(NSInteger)reqID callback:(id<BRAsyncMethodCallback>)method
{
	pthread_mutex_lock(&mutex);
	[requestsId_callbacks setObject:method forKey:[NSNumber numberWithInt:reqID]];
	pthread_mutex_unlock(&mutex);
	
	pthread_cond_broadcast(&cond);
}

-(void) main
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	BOOL run = YES;
	while(run)
	{
		pthread_mutex_lock(&mutex);
		
		while([requestsId_callbacks count] <= 0)
		{
			pthread_cond_wait(&cond, &mutex);
		}
		
		NSUInteger size = [requestsId_callbacks count];
		pthread_mutex_unlock(&mutex);
		
		if (size > 0)
		{
			pthread_mutex_lock(&mutex);
			
			NSArray* v = [requestsId_callbacks allKeys];
			NSNumber* key = [v objectAtIndex:0];
			NSInteger reqID = [key intValue];
			
			id<BRAsyncMethodCallback> amc = [requestsId_callbacks objectForKey:key];
			pthread_mutex_unlock(&mutex);
			
			NSData* recv_bytes = [BRReplyMessageStack consumeRequestId:reqID];
			
			BRReplyMessage* msg = [[BRReplyMessage alloc] initWithBytes:recv_bytes];
			NSData* recv_bytes2 = msg.body.byteCollection;
			if ([recv_bytes2 length] > 0)
			{
				[amc callback:recv_bytes2];
			}
			
			pthread_mutex_lock(&mutex);
			[requestsId_callbacks removeObjectForKey:key];
			pthread_mutex_unlock(&mutex);
			
			[msg release];
		}
	}
	
	[pool drain];
}

@end


