#import <Foundation/Foundation.h>
#import "BRICommunicationDevice.h"
#import "BRObjectServant.h"

/**
 * Pool of interfaces. It also includes servant objects, as they may be
 * considered as an abstraction over an specific interface.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRDevicePool : NSObject {

}

/**
 * Starts the pool, selecting the current interface
 */
+(void) initializeDevicePool;

/**
 * Shutdown the interface pool, releasing all resources.
 */
+(void) destroyDevicePool;

/**
 * Adds a new interface to the collection of interfaces
 *
 * @param _interface Interface to add
 */
+(void) addDevice:(id<BRICommunicationDevice>)device;

/**
 * Registers a new object into the pool
 *
 * @param obj Object to register
 */
+(void) registerObjectServant:(BRObjectServant*)object;

/**
 * Unregisters an object from the pool
 *
 * @param obj Object to unregister
 */
+(void) unregisterObjectServant:(BRObjectServant*)object;

/**
 * Retrieves a registered object servant based on its identity
 *
 * @param identity Identity of the servant
 * @return Registered object servant or nil if it's not found
 */
+(BRObjectServant*) objectServantForIdentity:(BRIdentity*)identity;

/**
 * Selects a new interface for transmitting data based upon the priority of the channels
 *
 * @return New transmission interface
 */
+(id<BRICommunicationDevice>) nextDevice;

/**
 * Message Dispatcher for asynchronous calls and/or server requests
 *
 * @param userId Sender user id
 * @param msg Message to dispatch
 */
+(void) dispatchMessage:(NSData*)message fromUserID:(NSString*)userID;

/**
 * Retrieves the current interface
 */
+(id<BRICommunicationDevice>) currentDevice;

/**
 * Returns the transmission interface for the provided name
 *
 * @param name Name of the transmission interface
 *
 * @return Transmission interface
 */
+(id<BRICommunicationDevice>) deviceForName:(NSString*)name;

@end
