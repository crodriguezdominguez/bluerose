#import "BREvent.h"


@implementation BREvent

@synthesize nodes;
@synthesize topic;

-(id) init
{
    self = [super init];
	if (self)
	{
		topic = BR_DEFAULT_TOPIC;
		nodes = [[NSMutableDictionary alloc] init];
	}
	
	return self;
}

-(id) initWithEvent:(BREvent*)event
{
    self = [super init];
	if (self)
	{
		topic = event.topic;
        
        if (event.nodes)
            nodes = [[NSMutableDictionary alloc] initWithDictionary:event.nodes copyItems:YES];
        else self.nodes = nil;
	}
	
	return self;
}

-(id) initWithTopic:(NSInteger)top
{
    self = [super init];
	if (self)
	{
		topic = top;
		nodes = [[NSMutableDictionary alloc] init];
	}
	
	return self;
}

-(void) dealloc
{
	[nodes release];
	
	[super dealloc];
}

-(BOOL) existsMember:(NSString*)member
{
	return ([nodes valueForKey:member] != nil);
}

-(NSUInteger) length
{
	return [nodes count];
}

-(BREventNode*) member:(NSString*)member
{
    BREventNode* node = [nodes valueForKey:member];
    if (node)
        return [[node copy] autorelease];
    else return nil;
}

-(void) setMember:(NSString*)member node:(BREventNode*)node
{
	[nodes setValue:node forKey:member];
}

-(void) setMember:(NSString*)member value:(BRValue*)value
{
	BREventNode* node = [[BREventNode alloc] initWithIdentifier:member value:value];
	[nodes setValue:node forKey:member];
	
	[node release];
}

-(void) marshall:(BRByteStreamWriter*)writer
{
	//topic
	[writer writeInteger:topic];
	
	//num. event nodes
	[writer writeSize:[nodes count]];
	
	//event nodes
	for (NSString* key in nodes)
	{
		BREventNode* node = [nodes valueForKey:key];
		[node marshall:writer];
	}
}

-(void) unmarshall:(BRByteStreamReader*)reader
{
	topic = [reader readInteger];
	
	//num. event nodes
	NSUInteger tam = [reader readSize];
	
    self.nodes = nil;
    nodes = [[NSMutableDictionary alloc] init];
	if (tam == 0)
	{
		return;
	}
	
	//event nodes
	for (NSUInteger i=0; i<tam; i++)
	{
		BREventNode* aux = [[BREventNode alloc] init];
		
		[aux unmarshall:reader];
		[nodes setValue:aux forKey:aux.identifier];
		
		[aux release];
	}
}

@end

@implementation BREventNode

@synthesize identifier;
@synthesize value;

-(id) init
{
    self = [super init];
	if (self)
	{
		self.identifier = nil;
		self.value = nil;
	}
	
	return self;
}

-(id) initWithIdentifier:(NSString*)ident value:(BRValue*)val
{
    self = [super init];
	if (self)
	{
		self.identifier = ident;
		self.value = val;
	}
	
	return self;
}

-(void) dealloc
{
	self.identifier = nil;
	self.value = nil;
	
	[super dealloc];
}

-(id)copyWithZone:(NSZone *)zone
{
    BREventNode* copy = [[BREventNode allocWithZone:zone] initWithEvent:self];
    copy.identifier = self.identifier;
    copy.value = self.value;
    
    return copy;
}

-(void) marshall:(BRByteStreamWriter*)writer
{
	//identifier
	[writer writeString:identifier];
	
	//value
	[value marshall:writer];
	
	//topic
	[writer writeInteger:topic];
	
	//num. event nodes
	[writer writeSize:[nodes count]];
	
	//event nodes
	for (NSString* key in nodes)
	{
		BREventNode* aux = [nodes valueForKey:key];
		[aux marshall:writer];
	}
}

-(void) unmarshall:(BRByteStreamReader*)reader
{
	//identifier
	self.identifier = [reader readString];
	
	//value
    self.value = [[[BRValue alloc] init] autorelease];
	[value unmarshall:reader];
	
	//topic
	topic = [reader readInteger];
	
	//num. event nodes
	NSUInteger size = [reader readSize];
	
    self.nodes = nil;
    nodes = [[NSMutableDictionary alloc] init];
	if (size == 0)
	{
		return;
	}
	
	//event nodes
	for (NSUInteger i=0; i<size; i++)
	{
		BREventNode* aux = [[BREventNode alloc] init];
		[aux unmarshall:reader];
		[self setMember:aux.identifier node:aux];
		[aux release];
	}
}

@end
