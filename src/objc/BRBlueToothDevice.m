#import "BRBlueToothDevice.h"
#import "BRTypeDefinitions.h"
#import "BRByteStreamReader.h"
#import "BREvent.h"
#import "BRReplyMessageStack.h"
#import "BREventHandler.h"
#import "BRDevicePool.h"

@implementation BRBlueToothDevice

@synthesize session, lastDataReceived, dataReceptionUserID;

#pragma mark - Construction and destruction

-(id) init
{
    self = [super init];
	if (self)
	{
        session = nil;
		priority = 1;
		dataModes = [[NSMutableDictionary alloc] init];
		listeners = [[NSMutableArray alloc] init];
		
        // Set up starting/stopping session on application hiding/terminating
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willTerminate:)
                                                     name:UIApplicationWillTerminateNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willTerminate:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willResume:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
		
		pthread_mutex_init(&mutex, 0);
		
		isServant = NO;
	}
	
	return self;
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
	[listeners release];
	[dataModes release];
	
	[self destroySession];
	
	pthread_mutex_destroy(&mutex);
	
	[super dealloc];
}

#pragma mark - GKSession delegate

-(void) receiveData:(NSData *)data fromPeer:(NSString *)peer inSession: (GKSession *)session context:(void *)context
{
	NSInteger requestId;
	BRByteStreamReader* reader = [[BRByteStreamReader alloc] init];
	
    const BRByte* buffer = [data bytes];
    BRByte type = (BRByte)buffer[8];
    
    if (type == BR_REPLY_MSG)
    {
        //add to reply stack
        [reader open:data];
        
        [reader skip:14];
        requestId = [reader readInteger];
        
        [BRReplyMessageStack produceForRequestId:requestId bytes:data];
        
        [reader close];
    }
    
    else if (type == BR_EVENT_MSG)
    {
        BREvent* event = [[BREvent alloc] init];
        
        [reader open:data];
        [reader skip:14];
        
        [event unmarshall:reader];
        
        [BREventHandler onConsume:event];
        
        [reader close];
        
        [event release];
    }
    
    else if (type == BR_REQUEST_MSG)
    {
        //process message
        [BRDevicePool dispatchMessage:data fromUserID:peer];
    }
    
    else if (type == BR_CLOSE_CONNECTION_MSG)
    {
        [self.session disconnectPeerFromAllPeers:peer];
    }
    
    [reader release];
}

-(void) session:(GKSession*)s connectionWithPeerFailed:(NSString*)peerID withError:(NSError*)error
{
	pthread_mutex_lock(&mutex);
    NSArray* listenersCopy = [listeners copy];
    pthread_mutex_unlock(&mutex);
    
	for (id<BRBlueToothEventListener> listener in listenersCopy)
	{
        if ([listener respondsToSelector:@selector(connectionToUserIDFailed:withError:device:)])
            [listener connectionToUserIDFailed:peerID withError:error device:self];
	}
    
    [listenersCopy release];
}

-(void) session:(GKSession *)s didFailWithError:(NSError*)error
{
	pthread_mutex_lock(&mutex);
    NSArray* listenersCopy = [listeners copy];
    pthread_mutex_unlock(&mutex);
    
	for (id<BRBlueToothEventListener> listener in listenersCopy)
	{
        if ([listener respondsToSelector:@selector(clientFailed:withError:)])
		[listener clientFailed:self withError:error];
	}
	
    [listenersCopy release];
}

-(void) session:(GKSession*)s didReceiveConnectionRequestFromPeer:(NSString*)peerID
{
    [self.session acceptConnectionFromPeer:peerID error:NULL];
    
	pthread_mutex_lock(&mutex);
    NSArray* listenersCopy = [listeners copy];
    pthread_mutex_unlock(&mutex);
    
	for (id<BRBlueToothEventListener> listener in listenersCopy)
	{
        if ([listener respondsToSelector:@selector(connectionRequestFromUserID:device:)])
            [listener connectionRequestFromUserID:peerID device:self];
	}
    
    [listenersCopy release];
}

-(void) session:(GKSession*)s peer:(NSString*)peerID didChangeState:(GKPeerConnectionState)state
{
    NSLog(@"%@ --> %d", peerID, (int)state);
    
	pthread_mutex_lock(&mutex);
    NSArray* listenersCopy = [listeners copy];
    pthread_mutex_unlock(&mutex);
    
	for (id<BRBlueToothEventListener> listener in listenersCopy)
	{
        if ([listener respondsToSelector:@selector(userID:stateChanged:device:)])
            [listener userID:peerID stateChanged:state device:self];
	}
    
    [listenersCopy release];
    
    switch (state)
    {
        case GKPeerStateDisconnected: //if connection is lost, try to reconnect...
        case GKPeerStateAvailable:
            [self openConnectionForUserID:peerID];
            break;
        case GKPeerStateUnavailable:
            [self closeConnectionForUserID:peerID];
            break;
        default:
            break;
    }
}

#pragma mark - Auxiliary Methods

-(void) willTerminate:(NSNotification*)notification
{
    [self destroySession];
}

-(void) willResume:(NSNotification*)notification
{
    [self setupSession];
}

-(void) setupSession
{
    if (!session)
    {
        session = [[GKSession alloc] initWithSessionID:@"brbluetooth" displayName:nil sessionMode:GKSessionModePeer];
        [session setDelegate:self];
        [session setDataReceiveHandler:self withContext:NULL];
        session.available = YES;
    }
}

-(void) destroySession
{
    [session disconnectFromAllPeers];
    session.delegate = nil;
    [session setDataReceiveHandler:nil withContext:NULL];
	[session release];
    session = nil;
}

#pragma mark - BRICommunicationDevice implementation

-(void) addEventListener:(id<BRBlueToothEventListener>)listener
{
	pthread_mutex_lock(&mutex);
	[listeners addObject:listener];
	pthread_mutex_unlock(&mutex);
}

-(void) removeEventListener:(id<BRBlueToothEventListener>)listener
{
	pthread_mutex_lock(&mutex);
	[listeners removeObject:listener];
	pthread_mutex_unlock(&mutex);
}

-(void) openConnectionForUserID:(NSString*)userID andParameters:(NSMutableDictionary*)parameters
{
	float timeout = 5.0;
	GKSendDataMode mode = GKSendDataReliable;
	
	if ([parameters objectForKey:@"timeout"] != nil)
	{
		timeout = [[parameters objectForKey:@"timeout"] floatValue];
	}
	
	if ([parameters objectForKey:@"datamode"] != nil)
	{
		mode = [[parameters objectForKey:@"datamode"] intValue];
	}
	
	[dataModes setValue:[NSNumber numberWithInt:mode] forKey:userID];
    
    //if it's connecting, do not try again...
    if (![[session peersWithConnectionState:GKPeerStateConnecting] containsObject:userID] &&
        ![[session peersWithConnectionState:GKPeerStateConnected] containsObject:userID])
    {
        [session connectToPeer:userID withTimeout:timeout];
    }
    
}

-(void) openConnectionForUserID:(NSString*)userID
{
	[dataModes setValue:[NSNumber numberWithInt:GKSendDataReliable] forKey:userID];
	if (![[session peersWithConnectionState:GKPeerStateConnecting] containsObject:userID] &&
        ![[session peersWithConnectionState:GKPeerStateConnected] containsObject:userID])
    {
        [session connectToPeer:userID withTimeout:5.0];
    }
}

-(BOOL) isConnectionOpenned:(NSString*)userID
{
	NSArray* array = [session peersWithConnectionState:GKPeerStateConnected];
	
	for (NSString* ident in array)
	{
		if ([ident isEqualToString:userID])
			return YES;
	}
	
	return NO;
}

-(void) closeConnectionForUserID:(NSString*)userID
{
	[session disconnectPeerFromAllPeers:userID];
}

-(void) closeAllConnections
{
	[session disconnectFromAllPeers];
}

-(BOOL) writeToUserID:(NSString*)userID message:(NSData*)message
{
	GKSendDataMode mode = GKSendDataReliable;
	NSNumber* num = [dataModes objectForKey:userID];
	if (num == nil)
	{
		return NO;
	}
	else mode = [num intValue];
		
	NSArray* array = [NSArray arrayWithObject:userID];
	return [session sendData:message toPeers:array withDataMode:mode error:nil];
}

-(void) setPriority:(NSInteger)p
{
	priority = p;
}

-(NSInteger) priority
{
	return priority;
}

-(NSArray*) neighbours
{
	return [session peersWithConnectionState:GKPeerStateAvailable];
}

-(void) broadcast:(NSData*)message
{
	[session sendDataToAllPeers:message withDataMode:GKSendDataReliable error:nil];
}

-(BOOL) isBlockantConnection
{
	return NO;
}

-(BOOL) isAvailable
{
	return session.available;
}

-(BOOL) isUserAvailable:(NSString*)userID
{
	NSArray* array = [session peersWithConnectionState:GKPeerStateAvailable];
	
	for (NSString* ident in array)
	{
		if ([ident isEqualToString:userID])
			return YES;
	}
	
	return NO;
}

-(BOOL) isConnectionOriented
{
	return YES;
}

-(void) setServantIdentifier:(NSString *)servantID
{
}

-(void) setServant:(NSString*)multiplexingId
{
	isServant = YES;
}

-(NSString*) deviceName
{
	return @"BlueToothDevice";
}

-(NSString*) servantIdentifier
{
	return [session peerID];
}

@end
