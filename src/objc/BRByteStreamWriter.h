#import "BRUtil.h"
#import "BRTypeDefinitions.h"

/**
 * Writer for byte streams. Data types will
 * be encoded as specified by ICE (zeroc.com).
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRByteStreamWriter : NSObject {
	NSMutableData* stream;
}

@property (readonly) NSMutableData* stream;

/**
 * Allows writing to the specified byte stream
 *
 * @param str Byte stream to write to 
 */
-(void) open:(NSMutableData*)str;

/**
 * Avoids writing to the current byte stream
 */
-(void) close;

/**
 * Writes a size to the stream. A size is coded as
 * a single byte if is lower than 255. If it's greater,
 * then it uses 5 bytes: The first one is always 255 and
 * the following are the integer representation of the size.
 *
 * @param size Size to write to the stream
 */
-(void) writeSize:(NSUInteger)size;

/**
 * Adds a byte to the stream
 *
 * @param b Byte to add
 */
-(void) writeByte:(BRByte)b;

/**
 * Adds a boolean to the stream
 *
 * @param b Boolean to add
 */
-(void) writeBoolean:(BOOL)b;

/**
 * Adds a short to the stream
 *
 * @param b Short to add
 */
-(void) writeShort:(short)b;

/**
 * Adds an integer to the stream
 *
 * @param b Integer to add
 */
-(void) writeInteger:(NSInteger)b;

/**
 * Adds a long to the stream
 *
 * @param b Long to add
 */
-(void) writeLong:(long)b;

/**
 * Adds a float to the stream
 *
 * @param b Float to add
 */
-(void) writeFloat:(float)b;

/**
 * Adds a double to the stream
 *
 * @param b Double to add
 */
-(void) writeDouble:(double)b;

/**
 * Adds an string to the stream
 *
 * @param b String to add
 */
-(void) writeString:(NSString*)b;

/**
 * Adds an utf8 string to the stream
 *
 * @param b String to add
 */
-(void) writeUTF8String:(NSString*)b;

/**
 * Adds an object proxy to the stream
 *
 * @param obj Object proxy to add
 */
//-(void) writeObjectProxy:(ObjectProxy*)obj;

/**
 * Adds an object proxy to the stream without
 * identifing it. It ONLY may be used as an auxiliary
 * method for marshalling inheritance hierchies.
 *
 * @param obj Object proxy to add
 */
//-(void) writeUnidentifiedObjectProxy:(ObjectProxy*)obj;

/**
 * Adds a pointer to an object proxy to the stream. It is
 * ONLY used when it is necessary to construct graphs of
 * classes (i.e. a linked list).
 *
 * @param pointer Pointer an object proxy to add
 */
//-(void) writePointerToObjectProxy:(ObjectProxy*)pointer;

/**
 * Adds raw bytes to the stream
 *
 * @param b Bytes to add
 */
-(void) writeRawBytes:(NSData*)b;

/**
 * Adds a sequence of bytes to the stream. The NSArray
 * must only contain NSNumbers codified as unsignedcharValues.
 *
 * @param b Bytes to add
 */
-(void) writeByteSeq:(NSArray*)b;

/**
 * Adds a sequence of booleans to the stream. The NSArray
 * must only contain NSNumbers codified as booleanValues.
 *
 * @param b Booleans to add
 */
-(void) writeBooleanSeq:(NSArray*)b;

/**
 * Adds a sequence of shorts to the stream. The NSArray
 * must only contain NSNumbers codified as shortValues.
 *
 * @param b Shorts to add
 */
-(void) writeShortSeq:(NSArray*)b;

/**
 * Adds a sequence of integers to the stream. The NSArray
 * must only contain NSNumbers codified as intValues.
 *
 * @param b Integers to add
 */
-(void) writeIntegerSeq:(NSArray*)b;

/**
 * Adds a sequence of longs to the stream. The NSArray
 * must only contain NSNumbers codified as longValues.
 *
 * @param b Longs to add
 */
-(void) writeLongSeq:(NSArray*)b;

/**
 * Adds a sequence of floats to the stream. The NSArray
 * must only contain NSNumbers codified as floatValues.
 *
 * @param b Floats to add
 */
-(void) writeFloatSeq:(NSArray*)b;

/**
 * Adds a sequence of doubles to the stream. The NSArray
 * must only contain NSNumbers codified as doubleValues.
 *
 * @param b Doubles to add
 */
-(void) writeDoubleSeq:(NSArray*)b;

/**
 * Adds a sequence of strings to the stream. The NSArray
 * must only contain NSStrings.
 *
 * @param b Strings to add
 */
-(void) writeStringSeq:(NSArray*)b;

/**
 * Adds a sequence of utf8 strings to the stream. The NSArray
 * must only contain NSStrings.
 *
 * @param b Strings to add
 */
-(void) writeUTF8StringSeq:(NSArray*)b;

/**
 * Adds a dictionary to the stream. The dictionary must be of
 * of pairs (key=NSString, value=NSData).
 *
 * @param b Bytes to add
 */
-(void) writeDictionary:(NSDictionary*)b;

/**
 * Adds a data stream to the stream.
 *
 * @param data Stream of data to add
 */
-(void) writeData:(NSData*)data;

@end
