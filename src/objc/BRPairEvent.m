#import "BRPairEvent.h"


@implementation BRPairEvent

-(id) init
{
	if (self = [super initWithTopic:BR_PAIR_EVENT_TOPIC])
	{
		BRValue* zero = [[BRValue alloc] init];
		[zero setFloat:0];
		
		[self setMember:@"x" value:zero];
		[self setMember:@"y" value:zero];
		
		[zero release];
	}
	
	return self;
}

-(id) initWithX:(float)x andY:(float)y
{
	if (self = [super initWithTopic:BR_PAIR_EVENT_TOPIC])
	{
		BRValue* x_val = [[BRValue alloc] init];
		[x_val setFloat:x];
		
		BRValue* y_val = [[BRValue alloc] init];
		[y_val setFloat:y];
		
		[self setMember:@"x" value:x_val];
		[self setMember:@"y" value:y_val];
		
		[x_val release];
		[y_val release];
	}
	
	return self;
}

-(float) x
{
	return [[self member:@"x"].value getFloat]; 
}

-(float) y
{
	return [[self member:@"y"].value getFloat]; 
}

-(void) setX:(float)x
{
	BRValue* value = [[BRValue alloc] init];
	[value setFloat:x];
	
	[self setMember:@"x" value:value];
	
	[value release];
}

-(void) setY:(float)y
{
	BRValue* value = [[BRValue alloc] init];
	[value setFloat:y];
	
	[self setMember:@"y" value:value];
	
	[value release];
}

@end
