#import "BRByteStreamWriter.h"
#import "BRByteStreamReader.h"

@protocol BRMarshallable<NSObject>

-(void) marshall:(BRByteStreamWriter*)writer;
-(void) unmarshall:(BRByteStreamReader*)reader;

@end
