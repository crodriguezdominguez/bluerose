#import "BRByteStreamWriter.h"
#import "BRPredicate.h"

@implementation BRByteStreamWriter

@synthesize stream;

-(id) init
{
    self = [super init];
	if (self)
	{
		stream = nil;
	}
	
	return self;
}

-(void) dealloc
{
	if (stream != nil)
	{
		[stream release];
		stream = nil;
	}
	
	[super dealloc];
}

-(void) open:(NSMutableData*)str
{
	if (stream != str)
	{
		stream = [str retain];
	}
}

-(void) close
{
	if (stream != nil)
	{
		[stream release];
		stream = nil;
	}
}

-(void) writeSize:(NSUInteger)size
{
	if (size < 255)
	{
		BRByte p = (BRByte)size;
		[stream appendBytes:&p length:1];
	}
	else
	{
		BRByte* p = (BRByte*)&size;
		BRByte aux = 0xff;
		
		[stream appendBytes:&aux length:1];
		
		if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
		{
			[stream appendBytes:&(p[0]) length:1];
			[stream appendBytes:&(p[1]) length:1];
			[stream appendBytes:&(p[2]) length:1];
			[stream appendBytes:&(p[3]) length:1];
		}
		else
		{
			[stream appendBytes:&(p[3]) length:1];
			[stream appendBytes:&(p[2]) length:1];
			[stream appendBytes:&(p[1]) length:1];
			[stream appendBytes:&(p[0]) length:1];
		}
	}
}

-(void) writeByte:(BRByte)b
{
	[stream appendBytes:&b length:1];
}

-(void) writeBoolean:(BOOL)b
{
	BRByte aux = (b) ? (0x01) : (0x00);
	[stream appendBytes:&aux length:1];
}

-(void) writeShort:(short)b
{
	BRByte* p = (BRByte*)&b;
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		[stream appendBytes:&(p[0]) length:1];
		[stream appendBytes:&(p[1]) length:1];
	}
	else
	{
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[0]) length:1];
	}
}

-(void) writeInteger:(NSInteger)b
{
	BRByte* p = (BRByte*)&b;
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		[stream appendBytes:&(p[0]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[3]) length:1];
	}
	else
	{
		[stream appendBytes:&(p[3]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[0]) length:1];
	}
}

-(void) writeLong:(long)b
{
	BRByte* p = (BRByte*)&b;
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		[stream appendBytes:&(p[0]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[3]) length:1];
		[stream appendBytes:&(p[4]) length:1];
		[stream appendBytes:&(p[5]) length:1];
		[stream appendBytes:&(p[6]) length:1];
		[stream appendBytes:&(p[7]) length:1];
	}
	else
	{
		[stream appendBytes:&(p[7]) length:1];
		[stream appendBytes:&(p[6]) length:1];
		[stream appendBytes:&(p[5]) length:1];
		[stream appendBytes:&(p[4]) length:1];
		[stream appendBytes:&(p[3]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[0]) length:1];
	}
}

-(void) writeFloat:(float)b
{
	BRByte* p = (BRByte*)&b;
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		[stream appendBytes:&(p[0]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[3]) length:1];
	}
	else
	{
		[stream appendBytes:&(p[3]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[0]) length:1];
	}
}

-(void) writeDouble:(double)b
{
	BRByte* p = (BRByte*)&b;
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		[stream appendBytes:&(p[0]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[3]) length:1];
		[stream appendBytes:&(p[4]) length:1];
		[stream appendBytes:&(p[5]) length:1];
		[stream appendBytes:&(p[6]) length:1];
		[stream appendBytes:&(p[7]) length:1];
	}
	else
	{
		[stream appendBytes:&(p[7]) length:1];
		[stream appendBytes:&(p[6]) length:1];
		[stream appendBytes:&(p[5]) length:1];
		[stream appendBytes:&(p[4]) length:1];
		[stream appendBytes:&(p[3]) length:1];
		[stream appendBytes:&(p[2]) length:1];
		[stream appendBytes:&(p[1]) length:1];
		[stream appendBytes:&(p[0]) length:1];
	}
}

-(void) writeString:(NSString*)b
{
	NSUInteger size = [b length];
	
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		BRByte aux = (BRByte)[b characterAtIndex:i];
		[stream appendBytes:&aux length:1];
	}
}

-(void) writeUTF8String:(NSString*)b
{
	NSData* data = [b dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
	[self writeSize:[data length]];
	[stream appendData:data];
}

-(void) writeRawBytes:(NSData*)b
{
	[stream appendData:b];
}

-(void) writeByteSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		NSNumber* number = [b objectAtIndex:i];
		[self writeByte:[number unsignedCharValue]];
	}
}

-(void) writeBooleanSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];

	for (NSUInteger i=0; i<size; i++)
	{
		NSNumber* number = [b objectAtIndex:i];
		[self writeBoolean:[number boolValue]];
	}
}

-(void) writeShortSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		NSNumber* number = [b objectAtIndex:i];
		[self writeShort:[number shortValue]];
	}
}

-(void) writeIntegerSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		NSNumber* number = [b objectAtIndex:i];
		[self writeInteger:[number integerValue]];
	}
}

-(void) writeLongSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		NSNumber* number = [b objectAtIndex:i];
		[self writeLong:[number longValue]];
	}
}

-(void) writeFloatSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		NSNumber* number = [b objectAtIndex:i];
		[self writeFloat:[number floatValue]];
	}
}

-(void) writeDoubleSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		NSNumber* number = [b objectAtIndex:i];
		[self writeDouble:[number doubleValue]];
	}
}

-(void) writeStringSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		[self writeString:[b objectAtIndex:i]];
	}
}

-(void) writeUTF8StringSeq:(NSArray*)b
{
	NSUInteger size = [b count];
	[self writeSize:size];
	
	for (NSUInteger i=0; i<size; i++)
	{
		[self writeUTF8String:[b objectAtIndex:i]];
	}
}

-(void) writeDictionary:(NSDictionary*)b
{
	NSUInteger size = [b count];
	
	[self writeSize:size];
	 
	for (id key in b)
	{
		[self writeString:key];
		[self writeByteSeq:[b valueForKey:key]];
	}
}

-(void) writeData:(NSData*)data
{
    [self writeSize:[data length]];
    [stream appendData:data]; 
}

@end
