#import <dns_sd.h>
#import <pthread.h>
#import "BRInternetMobileDiscovery.h"
#import "BRInitializer.h"

static pthread_mutex_t mutex_sync_resolve = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond_resolve = PTHREAD_COND_INITIALIZER;
static NSString* resolvedName = nil;

static void resolve_sync_reply(DNSServiceRef sdRef,
							   DNSServiceFlags flags,
							   uint32_t interfaceIndex,
							   DNSServiceErrorType errorCode,
							   const char *fullname,
							   const char *hosttarget,
							   uint16_t port,
							   uint16_t txtLen,
							   const unsigned char *txtRecord,
							   void *context)
{		
	pthread_mutex_lock(&mutex_sync_resolve);
	resolvedName = [[NSString stringWithFormat:@"%s:%u", hosttarget, port] retain];
	pthread_mutex_unlock(&mutex_sync_resolve);
	
	pthread_cond_broadcast(&cond_resolve);
}

static void browser_sync_reply(DNSServiceRef sdRef, 
							   DNSServiceFlags flags,
							   uint32_t interface_index,
							   DNSServiceErrorType errorCode, 
							   const char *name, 
							   const char *regtype, 
							   const char *domain, 
							   void *context)
{
	NSMutableArray* args = (NSMutableArray*)context;
	NSString* name2 = [NSString stringWithUTF8String:name];
	NSString* regtype2 = [NSString stringWithUTF8String:regtype];
	
	if (errorCode == kDNSServiceErr_NoError)
	{
		//NSLog(@"%@=%@, %@=%@\n", [args objectAtIndex:0], name2, [args objectAtIndex:1], regtype2);
		
		if ([[args objectAtIndex:0] isEqual:name2] && [[args objectAtIndex:1] isEqual:regtype2])
		{
			DNSServiceRef sdRefResolve;
			
			DNSServiceResolve(&sdRefResolve, 0, 0, name, regtype, domain, resolve_sync_reply, NULL);
			DNSServiceProcessResult(sdRefResolve);
			DNSServiceRefDeallocate(sdRefResolve);
		}
	}
}

@implementation BRInternetMobileDiscovery

@synthesize listeners=_listeners;

-(id) init
{
    self = [super init];
	if (self)
	{
		_listeners = [[NSMutableArray alloc] init];
		refs = [[NSMutableDictionary alloc] init];
		browser = nil;
	}
	
	return self;
}

-(void) dealloc
{
	[_listeners removeAllObjects];
	
	for (NSString* key in refs)
	{
		[[refs valueForKey:key] stop];
	}
	
	[refs removeAllObjects];
	
	[_listeners release];
    _listeners = nil;
	[refs release];
	[browser release];
	
	[super dealloc];
}

-(NSString*) regType:(BRInternetMobileDiscoveryServiceType)serviceType
{
	switch(serviceType)
	{
		case BRInternetMobileDiscoveryServicePrinter:
			return @"_printer._tcp.";
		case BRInternetMobileDiscoveryServiceDAAP:
			return @"_daap._tcp.";
		case BRInternetMobileDiscoveryServiceDACP:
			return @"_dacp._tcp.";
		case BRInternetMobileDiscoveryServiceFTP:
			return @"_ftp._tcp.";
		case BRInternetMobileDiscoveryServiceHomeSharing:
			return @"_home-sharing._tcp.";
		case BRInternetMobileDiscoveryServiceHTTP:
			return @"_http._tcp.";
		case BRInternetMobileDiscoveryServiceHTTPS:
			return @"_https._tcp.";
		case BRInternetMobileDiscoveryServiceIPP:
			return @"_ipp._tcp.";
		case BRInternetMobileDiscoveryServiceJini:
			return @"_jini._tcp.";
		case BRInternetMobileDiscoveryServiceLonworks:
			return @"_lonworks._tcp.";
		case BRInternetMobileDiscoveryServiceNFS:
			return @"_nfs._tcp.";
		case BRInternetMobileDiscoveryServiceRFID:
			return @"_rfid._tcp.";
		case BRInternetMobileDiscoveryServiceRTSP:
			return @"_rtsp._tcp.";
		case BRInternetMobileDiscoveryServiceScanner:
			return @"_scanner._tcp.";
		case BRInternetMobileDiscoveryServiceSFTP:
			return @"_sftp-ssh._tcp.";
		case BRInternetMobileDiscoveryServiceSSH:
			return @"_ssh._tcp.";
		case BRInternetMobileDiscoveryServiceTelnet:
			return @"_telnet._tcp.";
		case BRInternetMobileDiscoveryServiceTftp:
			return @"_tftp._tcp.";
		case BRInternetMobileDiscoveryServiceUPnP:
			return @"_upnp._tcp.";
			
		default:
			break;
	}
	
	return @"_bluerose._tcp.";
}

-(void) addEventListener:(id<BRMobileDiscoveryListener>)listener
{
	@synchronized (_listeners) {
		[_listeners addObject:listener];
	}
}

-(void) removeEventListener:(id<BRMobileDiscoveryListener>)listener
{
	@synchronized (_listeners) {
		[_listeners removeObject:listener];
	}
}

-(BOOL) registerForDetection:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device 
{	
	NSString* servantID = servant.identifier;
	NSString* regtype = [self regType:BRInternetMobileDiscoveryServiceBluerose];
	BRIdentity* ident = servant.identity;
	
	int port = [[[BRInitializer configuration] portForService:ident.id_name category:ident.category andDevice:[device deviceName]] intValue];
	//NSLog(@"%@, %@, %d\n", servantID, regtype, port);
	
	NSNetService* service = [[NSNetService alloc] initWithDomain:@"" type:regtype name:servantID port:port];
	if (service != nil)
	{
		[service setDelegate:self];
		[service publish];
	}
	else
	{
		[service release];
		return NO;
	}
	
	[refs setValue:service forKey:servantID];
	[service release];
	
	return YES;
}

-(void) unregister:(BRObjectServant*)servant
{
	NSNetService* service = [refs valueForKey:[servant identifier]];
	[service stop];
	[refs removeObjectForKey:[servant identifier]];
}

-(void) resolveUserID:(NSNetService*)service
{
	[service setDelegate:self];
	[service resolveWithTimeout:5.0];
}

-(NSString*) resolveService:(NSString*)name serviceType:(NSString*)regtype
{
	DNSServiceRef sdRef;
	
	NSMutableArray* args = [NSMutableArray array];
	[args addObject:name];
	[args addObject:regtype];
	
	if (DNSServiceBrowse(&sdRef, 0, kDNSServiceInterfaceIndexAny, [regtype UTF8String], NULL, browser_sync_reply, (void*)args)
		!= kDNSServiceErr_NoError)
	{
		DNSServiceRefDeallocate(sdRef);
		return nil;
	}
	
	DNSServiceProcessResult(sdRef);
	
	NSString* userID = @"";
	
	pthread_mutex_lock(&mutex_sync_resolve);
	while(resolvedName == nil)
	{
		pthread_cond_wait(&cond_resolve, &mutex_sync_resolve);
	}
	
	userID = [resolvedName retain];
	[resolvedName release];
	resolvedName = nil;
	pthread_mutex_unlock(&mutex_sync_resolve);
	
	pthread_cond_broadcast(&cond_resolve);
	
	DNSServiceRefDeallocate(sdRef);
	
	return [userID autorelease];
}

-(NSString*) resolveUserIDForService:(NSString*)name serviceType:(int)serviceType
{
	return [[[self resolveService:name serviceType:[self regType:serviceType]] retain] autorelease];
}

-(BOOL) startDetection:(int)serviceType domain:(NSString*)domain
{
	if (browser == nil)
	{
		browser = [[NSNetServiceBrowser alloc] init];
		if (!browser)
			return NO;
		
		NSString* regtype = [self regType:serviceType];
		[browser setDelegate:self];
		[browser searchForServicesOfType:regtype inDomain:domain];
		
		return YES;
	}
	else return NO;
}

-(BOOL) startDetection:(int)serviceType
{
	return [self startDetection:serviceType domain:@""];
}

-(void) stopDetection
{
	if (browser != nil)
	{
		[browser stop];
		[browser release];
		browser = nil;
	}
}

- (void)netService:(NSNetService *)sender didNotPublish:(NSDictionary *)errorDict
{
	@synchronized (_listeners) {
		for (id<BRMobileDiscoveryListener> listener in _listeners)
		{
            if ([listener respondsToSelector:@selector(onElementRegistered:userID:error:)])
                [listener onElementRegistered:sender userID:nil error:YES];
		}
	}
}

- (void)netServiceDidPublish:(NSNetService *)sender
{
	NSString* userID = [self resolveService:[sender name] serviceType:[sender type]];
	
	@synchronized (_listeners) {
		for (id<BRMobileDiscoveryListener> listener in _listeners)
		{
            if ([listener respondsToSelector:@selector(onElementRegistered:userID:error:)])
                [listener onElementRegistered:sender userID:userID error:NO];
		}
	}
}

- (void)netServiceDidResolveAddress:(NSNetService *)netService
{
	NSString* userID = [NSString stringWithFormat:@"%@:%u", [netService hostName], [netService port]];
	
	@synchronized (_listeners) {
		for (id<BRMobileDiscoveryListener> listener in _listeners)
		{
            if ([listener respondsToSelector:@selector(onUserIDResolved:userID:error:)])
                [listener onUserIDResolved:netService userID:userID error:NO];
		}
	}
}

- (void)netService:(NSNetService *)netService didNotResolve:(NSDictionary *)errorDict
{
	@synchronized (_listeners) {
		for (id<BRMobileDiscoveryListener> listener in _listeners)
		{
            if ([listener respondsToSelector:@selector(onUserIDResolved:userID:error:)])
			[listener onUserIDResolved:netService userID:nil error:YES];
		}
	}
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didFindService:(NSNetService *)netService moreComing:(BOOL)moreServicesComing
{
	@synchronized (_listeners) {
		for (id<BRMobileDiscoveryListener> listener in _listeners)
		{
            if ([listener respondsToSelector:@selector(onElementDiscovered:serviceBrowser:moreComing:)])
                [listener onElementDiscovered:netService serviceBrowser:netServiceBrowser moreComing:moreServicesComing];
		}
	}
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didRemoveService:(NSNetService *)netService moreComing:(BOOL)moreServicesComing
{
	@synchronized (_listeners) {
		for (id<BRMobileDiscoveryListener> listener in _listeners)
		{
            if ([listener respondsToSelector:@selector(onElementRemoved:serviceBrowser:moreComing:)])
                [listener onElementRemoved:netService serviceBrowser:netServiceBrowser moreComing:moreServicesComing];
		}
	}
}

@end
