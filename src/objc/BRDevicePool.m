#import <pthread.h>
#import "BRDevicePool.h"
#import "BRReplyMessageStack.h"
#import "BRMessage.h"
#import "BRDiscoveryProxy.h"


@implementation BRDevicePool

static NSMutableDictionary* deviceCollection = nil; /**< Allowed transmission devices */
static id<BRICommunicationDevice> interface = nil; /**< Current transmission channel */
static NSMutableDictionary* objects = nil; /**< Registered objects servants */

+(void) initializeDevicePool
{
	signal(SIGPIPE, SIG_IGN); //evitar los mensajes SIGPIPE broken
	[BRReplyMessageStack initializeStack];
	deviceCollection = [[NSMutableDictionary alloc] init];
	objects = [[NSMutableDictionary alloc] init];
	
	interface = [BRDevicePool nextDevice];
}

+(void) destroyDevicePool
{
	[BRReplyMessageStack destroyStack];
	
	for (BRIdentity* identity in objects)
	{
		if ([BRDiscoveryProxy defaultProxy] != nil)
		{
			BRObjectServant* servant = [objects objectForKey:identity];
			[[BRDiscoveryProxy defaultProxy] removeRegistration:[servant identifier]];
		}
	}
	
	[objects release];
	[deviceCollection release];
}

+(void) addDevice:(id<BRICommunicationDevice>)device
{
	[deviceCollection setValue:device forKey:[device deviceName]];
	
	if ([deviceCollection count] == 1)
	{
		interface = [BRDevicePool nextDevice];
	}
}

+(void) registerObjectServant:(BRObjectServant*)object
{
    NSString* identity = [object.identity stringValue];
    if (identity)
    {
        [objects setObject:object forKey:identity];
    }
}

+(void) unregisterObjectServant:(BRObjectServant*)object
{
    NSString* identity = [object.identity stringValue];
    if (identity)
    {
        [objects removeObjectForKey:identity];
        
        if ([BRDiscoveryProxy defaultProxy] != nil)
        {
            [[BRDiscoveryProxy defaultProxy] removeRegistration:[object identifier]];
        }
    }
}

+(BRObjectServant*) objectServantForIdentity:(BRIdentity*)identity
{
    NSString* ident = [identity stringValue];
    for (NSString* objId in [objects allKeys])
    {
        if ([objId isEqualToString:ident])
        {
            return [objects objectForKey:objId];
        }
    }
    
    return nil;
}

+(id<BRICommunicationDevice>) nextDevice
{
	NSArray* values = [deviceCollection allValues];
	
	if ([values count] > 0)
	{
		NSInteger priority = [[values objectAtIndex:0] priority];
		id<BRICommunicationDevice> dev = [values objectAtIndex:0];
		
		for (id device in values)
		{
			if (priority < [device priority])
			{
				priority = [device priority];
				dev = device;
			}
		}
		
		return dev;
	}
	
	else return nil;
}

+(void) dispatchMessage:(NSData*)message fromUserID:(NSString*)userID
{
	BRReplyMessage* reply = [[BRReplyMessage alloc] init];
	BRRequestMessage* orig = [[BRRequestMessage alloc] initWithBytes:message];
    
	BRMethodResult* replyStatus = nil;
	
	BRReplyMessageHeader* header = (BRReplyMessageHeader*)reply.header;
	BRRequestMessageHeader* header2 = (BRRequestMessageHeader*)orig.header;
    	
	NSInteger requestId = header2.requestId;
	
	header.requestId = requestId;
	
	//identify destination object
	BRObjectServant* serv = [objects objectForKey:[header2.identity stringValue]];
	
	if (serv != nil)
	{
		replyStatus = [serv runMethod:header2.operation forUserID:userID arguments:orig.body.byteCollection];
	}
	
	header.replyStatus = replyStatus.status;
	
	if (replyStatus.status == BR_SUCCESS_STATUS)
		[reply addToEncapsulation:replyStatus.result];
	
	//write reply msg
	NSData* reply_bytes = [reply bytes];
	
	if (header2.mode == BR_TWOWAY_MODE)
	{
		[interface writeToUserID:userID message:reply_bytes];
	}
	
	[reply release];
	[orig release];
}

+(id<BRICommunicationDevice>) currentDevice
{
	return interface;
}

+(id<BRICommunicationDevice>) deviceForName:(NSString*)name
{
	return [deviceCollection valueForKey:name];
}

@end
