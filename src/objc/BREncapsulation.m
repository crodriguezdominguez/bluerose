#import "BREncapsulation.h"


@implementation BREncapsulation

@synthesize size;
@synthesize major;
@synthesize minor;
@synthesize byteCollection;

-(id) init
{
	if (self = [super init])
	{
		size = 6;
		major = 0x01;
		minor = 0x00;
		byteCollection = [[NSMutableData alloc] init];
	}
	
	return self;
}

-(void) dealloc
{
	[byteCollection release];
	[super dealloc];
}

@end

