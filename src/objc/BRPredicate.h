#import <Foundation/Foundation.h>
#import "BRTypeDefinitions.h"
#import "BRValue.h"
#import "BRMarshallable.h"

/**
 * Class that models a set of constraints over the values of a set of properties
 *
 * @author Carlos Rodriguez Dominguez
 * @date 09-02-2010
 */
@interface BRPredicate : NSObject<BRMarshallable> {
	NSMutableDictionary* constraints;
}

@property (readonly) NSMutableDictionary* constraints;

/**
 * Adds a new constraint to the set of constraints, but only if the set remains
 * consistent.
 *
 * @param property Property associated with the constraint
 * @param comp Comparison operator between the property and the value
 * @param value Value associated with the property
 * @return True if the constraint was added to the set (i.e., the set remains
 *         consistent after adding the constraint). False otherwise else.
 */
-(BOOL) addConstraint:(NSString*)property comparison:(BRComparison)comp value:(BRValue*)value;

/**
 * Removes a constraint from the set
 *
 * @param property Property associated with the constraint to remove
 */
-(void) removeConstraints:(NSString*)property;

/**
 * Checks if the set of constraints is consistent after adding "property comp value".
 * For example: "x == 5", "y <= 3", etc.
 *
 * @param property Property to check
 * @param value Value of the property
 */
-(BOOL) isConsistent:(NSString*)property comparison:(BRComparison)comp value:(BRValue*)value;

/**
 * Checks if the set of constraints is consistent after adding "property == value".
 *
 * @param property Property to check
 * @param value Value of the property
 */
-(BOOL) isConsistent:(NSString*)property value:(BRValue*)value;

/**
 * Checks if the set of constraints is empty
 */
-(BOOL) isEmpty;

/**
 * Returns the number of constraints
 *
 * @return Number of constraints
 */
-(NSUInteger) length;

@end
