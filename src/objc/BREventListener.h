#import <Foundation/Foundation.h>
#import "BRPredicate.h"
#import "BREvent.h"

@interface BREventListener : NSObject {
	NSInteger topic;
	BRPredicate* predicate;
}

@property (readonly) NSInteger topic;
@property (readonly) BRPredicate* predicate;

/**
 * Default constructor
 *
 * @param top Topic to listen
 */
-(id) initWithTopic:(NSInteger)top;

/**
 * Default constructor
 *
 * @param top Topic to listen
 * @param pred Predicate to accomplish
 */
-(id) initWithTopic:(NSInteger)top andPredicate:(BRPredicate*)pred;

/**
 * Checks whether this listener accepts a topic or not
 *
 * @param top Topic to check
 * @return YES if the topic is accepted. NO otherwise else
 */
-(BOOL) acceptsTopic:(NSInteger)top;

/**
 * Checks whether this listener accepts an event or not. It is
 * a more complex check than the check method that only accepts
 * a topic. In this method, it is assured that the event is
 * related with the topic established in the constructor. It uses
 * the predicate to check if it is accepted or not.
 *
 * @param event Event to check
 * @return YES if the event is accepted. NO otherwise else
 */
-(BOOL) acceptsEvent:(BREvent*)event;

/**
 * Action that is run whenever an appropriate event is received.
 *
 * @param evt Event that is received
 */
-(void) performAction:(BREvent*)event;

/**
 * Returns the name of the listener. It is used
 * for comparing event listeners.
 *
 * @return Name of the listener
 */
-(NSString*) listenerName;

/**
 * Checks if two listeners are equal (i.e., have the same name)
 *
 * @return YES if both are equal. NO otherwise else.
 */
-(BOOL) isEqualToListener:(BREventListener*)listener;

@end
