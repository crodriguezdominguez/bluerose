#import "BRTypeDefinitions.h"

@implementation BRMethodResult

@synthesize status;
@synthesize result;

-(id) init
{
	if (self = [super init])
	{
		result = nil;
	}
	
	return self;
}

-(void) dealloc
{
	[result release];
	
	[super dealloc];
}

@end


@implementation BRIdentity

@synthesize id_name;
@synthesize category;

-(BOOL) isEqualToIdentity:(BRIdentity*)identity
{
	if ( ([id_name isEqualToString:identity.id_name]) && 
		 ([category isEqualToString:identity.category]) )
	{
		return YES;
	}
	
	else return NO;
}

-(NSString*) stringValue
{
    return [NSString stringWithFormat:@"%@::%@", self.category, self.id_name];
}

@end

@implementation BRPair

@synthesize first;
@synthesize second;

-(id) init
{
	if (self = [super init])
	{
		first = nil;
		second = nil;
	}
	
	return self;
}

-(void) dealloc
{
	[first release];
	[second release];
	
	[super dealloc];
}

@end

