#import "BRDiscoveryProxy.h"
#import "BRByteStreamReader.h"
#import "BRByteStreamWriter.h"
#import "BRMessageHeader.h"


@implementation BRDiscoveryProxy

static BRDiscoveryProxy* proxy = nil;

+(void) initializeDiscoveryProxy:(NSString*)servID
{
	if (proxy == nil)
	{
		proxy = [[BRDiscoveryProxy alloc] initWithServant:servID];
		proxy.identity.id_name = @"Discovery";
		proxy.identity.category = @"BlueRoseService";
	}
}

+(void) initializeDiscoveryProxy:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device
{
	if (proxy == nil)
	{
		proxy = [[BRDiscoveryProxy alloc] initWithServant:servID andDevice:device];
	}
}

+(void) initializeDiscoveryProxy:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	if (proxy == nil)
	{
		proxy = [[BRDiscoveryProxy alloc] initWithServant:servID device:device waitOnline:wait];
	}
}

+(void) initializeDiscoveryProxy:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	if (proxy == nil)
	{
		proxy = [[BRDiscoveryProxy alloc] initWithServant:servID device:device waitOnline:wait parameters:pars];
	}
}

+(void) destroyDiscoveryProxy
{
	[proxy release];
}

+(BRDiscoveryProxy*) defaultProxy
{
	return proxy;
}

-(id) initWithServant:(NSString*)servID
{
	if (self = [super initWithServant:servID])
	{
		identity.id_name = @"Discovery";
		identity.category = @"BlueRoseService";
	}
	
	return self;
}

-(id) initWithServant:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device
{
	if (self = [super initWithServant:servID andDevice:device])
	{
		identity.id_name = @"Discovery";
		identity.category = @"BlueRoseService";
	}
	
	return self;
}

-(id) initWithServant:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	if (self = [super initWithServant:servID device:device waitOnline:wait])
	{
		identity.id_name = @"Discovery";
		identity.category = @"BlueRoseService";
	}
	
	return self;
}

-(id) initWithServant:(NSString*)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	if (self = [super initWithServant:servID device:device waitOnline:wait parameters:pars])
	{
		identity.id_name = @"Discovery";
		identity.category = @"BlueRoseService";
	}
	
	return self;
}

-(NSString*) typeID
{
	return @"BlueRoseService::Discovery";
}

-(void) addRegistration:(NSString*)name address:(NSString*)address
{
	NSInteger reqID;
		
	currentMode = BR_TWOWAY_MODE;
	
	NSMutableData* enc = [[NSMutableData alloc] init];
	
	pthread_mutex_lock([BRObjectProxy mutexWriter]);
	BRByteStreamWriter* writer = [BRObjectProxy writer];
	[writer open:enc];
	[writer writeString:name];
	[writer writeString:address];
	[writer close];
	pthread_mutex_unlock([BRObjectProxy mutexWriter]);
	
	reqID = [self sendRequestToServantID:servantID operation:@"addRegistration" request:enc];
	[self receiveReply:reqID];
	
	[enc release];
}

-(void) removeRegistration:(NSString*)name
{
	NSInteger reqID;
	
	currentMode = BR_TWOWAY_MODE;
	
	NSMutableData* enc = [[NSMutableData alloc] init];
	
	pthread_mutex_lock([BRObjectProxy mutexWriter]);
	BRByteStreamWriter* writer = [BRObjectProxy writer];
	[writer open:enc];
	[writer writeString:name];
	[writer close];
	pthread_mutex_unlock([BRObjectProxy mutexWriter]);
	
	reqID = [self sendRequestToServantID:servantID operation:@"removeRegistration" request:enc];
	[self receiveReply:reqID];
	
	[enc release];
}

-(NSString*) resolveName:(NSString*)name
{
	return [self resolveName:name waitOnline:NO];
}

-(NSString*) resolveName:(NSString*)name waitOnline:(BOOL)wait
{
	NSInteger reqID;

	currentMode = BR_TWOWAY_MODE;
	NSString* result = nil;

	do {
		NSMutableData* enc = [[NSMutableData alloc] init];

		pthread_mutex_lock([BRObjectProxy mutexWriter]);
		BRByteStreamWriter* writer = [BRObjectProxy writer];
		[writer open:enc];
		[writer writeString:name];
		[writer close];
		pthread_mutex_unlock([BRObjectProxy mutexWriter]);

		reqID = [self sendRequestToServantID:servantID operation:@"resolve" request:enc];
		NSData* result_bytes = [self receiveReply:reqID];

		pthread_mutex_lock([BRObjectProxy mutexReader]);
		BRByteStreamReader* reader = [BRObjectProxy reader];
		[reader open:result_bytes];
		result = [reader readString];
		[reader close];
		pthread_mutex_unlock([BRObjectProxy mutexReader]);

		if ([result isEqualToString:@""] && wait)
			[NSThread sleepForTimeInterval:1];

        [enc release];
	} while([result isEqualToString:@""] && wait);

	return result;
}

@end
