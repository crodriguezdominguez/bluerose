#import "BREventListener.h"


@implementation BREventListener

@synthesize topic;
@synthesize predicate;

-(id) init
{
	if (self = [super init])
	{
		topic = BR_DEFAULT_TOPIC;
		predicate = [[BRPredicate alloc] init];
	}
	
	return self;
}

-(id) initWithTopic:(NSInteger)top
{
	if (self = [super init])
	{
		topic = top;
		predicate = [[BRPredicate alloc] init];
	}
	
	return self;
}


-(id) initWithTopic:(NSInteger)top andPredicate:(BRPredicate*)pred
{
	if (self = [super init])
	{
		topic = top;
		predicate = [pred retain];
	}
	
	return self;
}

-(void) dealloc
{
	[predicate release];
	
	[super dealloc];
}

-(BOOL) acceptsTopic:(NSInteger)top
{
	return (topic == top);
}

-(BOOL) acceptsEvent:(BREvent*)event
{
	if ([predicate isEmpty]) return YES;
	
	NSDictionary* dict = event.nodes;
	for (NSString* key in dict)
	{
		BREventNode* node = [dict valueForKey:key];
		if (![predicate isConsistent:key value:node.value])
		{
			return NO;
		}
	}
	
	return YES;
}

-(void) performAction:(BREvent*)event
{
}

-(NSString*) listenerName
{
	return @"EventListener";
}

-(BOOL) isEqualToListener:(BREventListener*)listener
{
	return [[self listenerName] isEqualToString:[listener listenerName]];
}

@end
