#import "BRMessageHeader.h"
#import "BREncapsulation.h"

/**
 * Class for modelling an empty message.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BRMessage : NSObject {
	BRMessageHeader* header;
	BREncapsulation* body;
}

@property (readwrite, retain) BRMessageHeader* header;
@property (readwrite, retain) BREncapsulation* body;

/**
 * Returns the full message (header+body, if body exists) as a byte sequence
 *
 * @return Bytes of the message
 */
-(NSData*) bytes;

/**
 * Adds bytes to the body
 *
 * @param vct Bytes to add
 */
-(void) addToEncapsulation:(NSData*)bytes;

@end


/**
 * Batch request message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BRBatchRequestMessage : BRMessage {
	
}

/**
 * Constructor that initializes the message with the appropriate bytes
 * @param bytes Bytes that represent the message body
 */
-(id) initWithBytes:(NSData*)bytes;

@end


/**
 * Request message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BRRequestMessage : BRMessage {
	
}

/**
 * Constructor that initializes the message with the appropriate bytes
 * @param bytes Bytes that represent the message body
 */
-(id) initWithBytes:(NSData*)bytes;

@end


/**
 * Reply message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BRReplyMessage : BRMessage {
	
}

/**
 * Constructor that initializes the message with the appropriate bytes
 * @param bytes Bytes that represent the message body
 */
-(id) initWithBytes:(NSData*)bytes;

@end


/**
 * Validate connection message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BRValidateConnectionMessage : BRMessage {

}

@end


/**
 * Close connection message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BRCloseConnectionMessage : BRMessage {

}

@end


/**
 * Event message
 *
 * @author Carlos Rodriguez Dominguez
 * @date 12-10-2009
 */
@interface BREventMessage : BRMessage {
	NSMutableData* evt;
}

@property (readwrite, retain) NSMutableData* evt;

/**
 * Constructor that initializes the message with the appropriate bytes
 * @param bytes Bytes that represent the message body
 */
-(id) initWithBytes:(NSData*)bytes;
	
@end
