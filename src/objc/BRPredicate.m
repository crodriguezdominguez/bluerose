#import "BRPredicate.h"


@implementation BRPredicate

@synthesize constraints;

-(id) init
{
	if (self = [super init])
	{
		constraints = [[NSMutableDictionary alloc] init];
	}
	
	return self;
}

-(void) dealloc
{
	[constraints release];
	
	[super dealloc];
}

-(BOOL) addConstraint:(NSString*)property comparison:(BRComparison)comp value:(BRValue*)value
{
	if (![self isConsistent:property comparison:comp value:value]) return NO;
	else
	{
		NSMutableArray* array = [constraints valueForKey:property];
		BRPair* pair = [[BRPair alloc] init];
		
		if (array == nil)
		{
			array = [[NSMutableArray alloc] init];
			pair.first = [NSNumber numberWithInt:comp];
			pair.second = value;
			
			[array addObject:pair];
			
			[constraints setValue:array forKey:property];
			[array release];
		}
		else
		{
			pair.first = [NSNumber numberWithInt:comp];
			pair.second = value;
			
			[array addObject:pair];
			
			[constraints setValue:array forKey:property];
		}
		
		[pair release];
		
		return YES;
	}
}

-(void) removeConstraints:(NSString*)property
{
	[constraints removeObjectForKey:property];
}

-(BOOL) isConsistent:(NSString*)property comparison:(BRComparison)comp value:(BRValue*)value
{
	if ([constraints count] == 0) return YES;
	else
	{
		NSMutableArray* v = [constraints valueForKey:property];
		if (v == nil) return YES;
		
		for (BRPair* pair in v)
		{
			//check inconsistencies
			NSNumber* first = pair.first;
			BRValue* second = pair.second;
			int first_comp = [first intValue];
			
			if ([second compareTo:value withComparison:BR_EQUAL]) //value == pair.second
			{
				if (first_comp == BR_EQUAL)
				{
					if ( (comp == BR_LESS) ||
						(comp == BR_GREATER) ||
						(comp == BR_NOT_EQUAL) ) 
						return NO;
				}
				
				else if (first_comp == BR_NOT_EQUAL)
				{
					if (comp == BR_EQUAL) return NO;
				}
				
				else if (first_comp == BR_LESS)
				{
					if ( (comp == BR_EQUAL) ||
						(comp == BR_GREATER) ||
						(comp == BR_GREATER_EQUAL) ) 
						return NO;
				}
				
				else if (first_comp == BR_LESS_EQUAL)
				{
					if (comp == BR_GREATER) return NO;
				}
				
				else if (first_comp == BR_GREATER)
				{
					if ( (comp == BR_EQUAL) ||
						(comp == BR_LESS) ||
						(comp == BR_LESS_EQUAL) ) 
						return NO;
				}
				
				else if (first_comp == BR_GREATER_EQUAL)
				{
					if (comp == BR_LESS) return NO;
				}
			}
			else //value != pair.second
			{
				if (first_comp == BR_EQUAL) return NO;
				
				else if (first_comp == BR_NOT_EQUAL) return YES;
				
				else if ( (first_comp == BR_LESS) ||
						 (first_comp == BR_LESS_EQUAL) )
				{
					if ([value compareTo:second withComparison:BR_GREATER])
					{
						if ( (comp == BR_EQUAL) ||
							(comp == BR_GREATER) ||
							(comp == BR_GREATER_EQUAL) )
							return NO;
					}
				}
				
				else if ( (first_comp == BR_GREATER) ||
						 (first_comp == BR_GREATER_EQUAL) )
				{
					if ([value compareTo:second withComparison:BR_LESS])
					{
						if ( (comp == BR_EQUAL) ||
							(comp == BR_LESS) ||
							(comp == BR_LESS_EQUAL) )
							return NO;
					}
				}
			}
		}
		
		return YES;
	}
}

-(BOOL) isConsistent:(NSString*)property value:(BRValue*)value
{
	return ([self isConsistent:property comparison:BR_EQUAL value:value]);
}

-(BOOL) isEmpty
{
	return ([constraints count] == 0);
}

-(NSUInteger) length
{
	return [constraints count];
}

-(void) marshall:(BRByteStreamWriter*)writer
{
	[writer writeSize:[constraints count]];
	
	for (NSString* key in constraints)
	{
		[writer writeString:key];
		
		NSArray* array = [constraints valueForKey:key];
		[writer writeSize:[array count]];
		
		for (BRPair* pair in array)
		{
			NSNumber* number = pair.first;
			BRValue* value = pair.second;
			[writer writeSize:(NSUInteger)[number intValue]];
			
			[writer writeSize:value.type];
			[writer writeSize:[value.rawValue length]];
			[writer writeRawBytes:value.rawValue];
		}
	}
}

-(void) unmarshall:(BRByteStreamReader*)reader
{	
	[constraints removeAllObjects];
	
	NSUInteger size = [reader readSize];
	
	for (NSUInteger i = 0; i < size; i++)
	{
		NSString* property = [reader readString];
		
		NSUInteger size_v = [reader readSize];
		for (NSUInteger j=0; j<size_v; j++)
		{
			BRComparison comp = (BRComparison)[reader readSize];
			BRValueType type = (BRValueType)[reader readSize];
			BRValue* value = [[BRValue alloc] initWithValueType:type];
			
			//read byte array
			NSUInteger n = [reader readSize];
			
			NSMutableData* data = [[NSMutableData alloc] init];
			for (NSUInteger k=0; k<n; k++)
			{
				BRByte byte = [reader readByte];
				[data appendBytes:&byte length:1];
			}
			value.rawValue = data;
			
			[self addConstraint:property comparison:comp value:value];
			
			[value release];
			[data release];
		}
	}
}

@end
