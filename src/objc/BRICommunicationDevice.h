/**
 * Generic representation of transmission channel
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@protocol BRICommunicationDevice<NSObject>

@required

/**
 * Open a new connection between local user and a remote one
 *
 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
 * @param parameters A dictionary containing some parameters for the connection (i.e.: QoS)
 */
-(void) openConnectionForUserID:(NSString*)userID andParameters:(NSMutableDictionary*)parameters;

/**
 * Open a new connection between local user and a remote one
 *
 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
 */
-(void) openConnectionForUserID:(NSString*)userID;

/**
 * Checks whether the connection to an specific user is openned or not
 *
 * @param userID User to check whether the connection is openned with or not.
 * @return True if the connection is openned. False otherwise else.
 */
-(BOOL) isConnectionOpenned:(NSString*)userID;

/**
 * Close a connection between local user and a remote one
 *
 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
 */
-(void) closeConnectionForUserID:(NSString*)userID;

/**
 * Close all connections in this transmission channel
 */
-(void) closeAllConnections;

/**
 * Send a message to a remote user
 *
 * @param userID Receiver identification
 * @param message Message to send
 */
-(BOOL) writeToUserID:(NSString*)userID message:(NSData*)message;

/**
 * Set priority of the transmission channel in the interface pool
 *
 * @param priority Priority of the channel
 */
-(void) setPriority:(NSInteger)priority;

/**
 * Get priority of the transmission channel in the interface pool
 *
 * @return Priority
 */
-(NSInteger) priority;

/**
 * Get identificators of neighbour users
 *
 * @return Vector containing user identificators
 */
-(NSArray*) neighbours;

/**
 * Broadcast a message to all neighbour users
 *
 * @param message Message to send
 */
-(void) broadcast:(NSData*)message;

/**
 * Checks if writing or reading is blockant in this channel
 *
 * @return True if it's blockant. False otherwise else.
 */
-(BOOL) isBlockantConnection;

/**
 * Checks if the transmission of data is possible
 *
 * @return True if is available. False otherwise else.
 */
-(BOOL) isAvailable;

/**
 * Checks if the transmission of data to an specific user is possible
 *
 * @param userID Identifier of the user
 * @return True if is available. False otherwise else.
 */
-(BOOL) isUserAvailable:(NSString*)userID;

/**
 * Checks if the transmission interface is connection oriented
 */
-(BOOL) isConnectionOriented;

/**
 * Sets the current identifier of the servant (i.e. the ip address in case of TCP)
 *
 * @param servantID Identifier of the servant (i.e. the ip address in case of TCP)
 */
-(void) setServantIdentifier:(NSString*)servantID;

/**
 * Sets the current machine as a servant
 *
 * @param multiplexingId Id for multiplexing connections (i.e. a port in case of TCP)
 */
-(void) setServant:(NSString*)multiplexingId;

/**
 * Returns the name of the interface
 *
 * @return Name of the interface
 */
-(NSString*) deviceName;

/**
 * If it's a servant, it returns the identifier
 *
 * @return Identifier of the current servant or "" if it's not a servant
 */
-(NSString*) servantIdentifier;

@optional

/**
 * Receive a message from a sender, if the device supports blockant connections
 *
 * @param userID Identification of the sender
 * @return Received message or nil if the userID was not connected
 */
-(NSData*) readFromUserID:(NSString*)userID;

/**
 * Waits for connections. Only for servants and for devices with blockant connections and/or connection oriented protocols
 */
-(void) waitForConnections;

@end
