//
//  BROpenConnectionThread.m
//  BlueRose
//
//  Created by Carlos Rodriguez Dominguez on 03/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "BROpenConnectionThread.h"


@implementation BROpenConnectionThread

-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier
{
	if (self = [super init])
	{
		device = [dev retain];
		userID = [identifier retain];
		parameters = nil;
		maxRetries = NSIntegerMax;
	}
	
	return self;
}

-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier maxRetries:(NSInteger)retries
{
	if (self = [super init])
	{
		device = [dev retain];
		userID = [identifier retain];
		parameters = nil;
		maxRetries = retries;
	}
	
	return self;
}

-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev 
						   userID:(NSString*)identifier 
					   maxRetries:(NSInteger)retries 
					   parameters:(NSMutableDictionary*)pars
{
	if (self = [super init])
	{
		device = [dev retain];
		userID = [identifier retain];
		parameters = pars;
		maxRetries = retries;
	}
	
	return self;
}

-(void) dealloc
{
	[device release];
	[userID release];
	[parameters release];
	
	[super dealloc];
}

-(void) main
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	if (maxRetries == NSIntegerMax)
	{
		while(true)
		{
			@try{
				[device openConnectionForUserID:userID andParameters:parameters];
				break;
			}
			@catch(NSException* ex)
			{
			}
			
			[NSThread sleepForTimeInterval:1];
		}
	}
	else
	{
		NSInteger i;
		for (i=0; i<maxRetries; i++)
		{
			@try{
				[device openConnectionForUserID:userID andParameters:parameters];
				break;
			}
			@catch(NSException* ex)
			{
			}
				 
			[NSThread sleepForTimeInterval:1];
		}
		
		if (i>=maxRetries)
		{
			@throw [NSException exceptionWithName:@"ERROR: Impossible to establish required connection" 
										   reason:@"Required connection is not available" 
										 userInfo:nil];
		}
	}
	
	[pool release];
}

@end
