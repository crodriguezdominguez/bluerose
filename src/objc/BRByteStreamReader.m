#import "BRByteStreamReader.h"


@implementation BRByteStreamReader

@synthesize stream;

-(id) init
{
    self = [super init];
	if (self)
	{
		stream = nil;
		iterator = 0;
	}
	
	return self;
}

-(void) open:(NSData*)str
{
	if (stream != str)
	{
		stream = [str retain];
	}
	
	iterator = 0;
}

-(void) close
{
	if (stream != nil)
	{
		[stream release];
	}
	
	iterator = 0;
}

-(void) skip:(NSInteger)num
{
	iterator = iterator+num;
}

-(NSUInteger) readSize
{
	NSUInteger res = 0;
	BRByte* buffer = (BRByte*)[stream bytes];
	
	if (buffer[iterator] != 0xff)
	{
		res = (NSUInteger)buffer[iterator];
	}
	else
	{
		BRByte* p = (BRByte*)&res;
		
		iterator++;
		
		if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
		{
			p[0] = buffer[iterator];
			iterator++;
			p[1] = buffer[iterator];
			iterator++;
			p[2] = buffer[iterator];
			iterator++;
			p[3] = buffer[iterator];
		}
		
		else
		{
			p[3] = buffer[iterator];
			iterator++;
			p[2] = buffer[iterator];
			iterator++;
			p[1] = buffer[iterator];
			iterator++;
			p[0] = buffer[iterator];
		}
	}
	
	iterator++;
	
	return res;
}

-(NSData*) readToEnd
{
	NSRange range;
	range.location = iterator;
	range.length = [stream length]-iterator;
	return [stream subdataWithRange:range];
}

-(BRByte) readByte
{
	BRByte* buffer = (BRByte*)[stream bytes];
	
	BRByte res = buffer[iterator];
	iterator++;
	
	return res;
}

-(BOOL) readBoolean
{
	BRByte* buffer = (BRByte*)[stream bytes];
	
	BOOL res = buffer[iterator];
	iterator++;
	
	return ( (res == 0x00) ? NO : YES );
}

-(short) readShort
{
	short res = 0;
	BRByte* p = (BRByte*)&res;
	
	BRByte* buffer = (BRByte*)[stream bytes];
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		p[0] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
	}
	
	else
	{
		p[1] = buffer[iterator];
		iterator++;
		p[0] = buffer[iterator];
	}
	
	iterator++;
	
	return res;
}

-(NSInteger) readInteger
{
	NSInteger res = 0;
	BRByte* p = (BRByte*)&res;
	
	BRByte* buffer = (BRByte*)[stream bytes];
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		p[0] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[3] = buffer[iterator];
	}
	
	else
	{
		p[3] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[0] = buffer[iterator];
	}
	
	iterator++;
	
	return res;
}

-(long) readLong
{
	long res = 0;
	BRByte* p = (BRByte*)&res;
	
	BRByte* buffer = (BRByte*)[stream bytes];
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		p[0] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[3] = buffer[iterator];
		iterator++;
		p[4] = buffer[iterator];
		iterator++;
		p[5] = buffer[iterator];
		iterator++;
		p[6] = buffer[iterator];
		iterator++;
		p[7] = buffer[iterator];
	}
	
	else
	{
		p[7] = buffer[iterator];
		iterator++;
		p[6] = buffer[iterator];
		iterator++;
		p[5] = buffer[iterator];
		iterator++;
		p[4] = buffer[iterator];
		iterator++;
		p[3] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[0] = buffer[iterator];
	}
	
	iterator++;
	
	return res;
}

-(float) readFloat
{
	float res = 0;
	BRByte* p = (BRByte*)&res;
	
	BRByte* buffer = (BRByte*)[stream bytes];
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		p[0] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[3] = buffer[iterator];
	}
	
	else
	{
		p[3] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[0] = buffer[iterator];
	}
	
	iterator++;
	
	return res;
}

-(double) readDouble
{
	double res = 0;
	BRByte* p = (BRByte*)&res;
	
	BRByte* buffer = (BRByte*)[stream bytes];
	
	if(CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
	{
		p[0] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[3] = buffer[iterator];
		iterator++;
		p[4] = buffer[iterator];
		iterator++;
		p[5] = buffer[iterator];
		iterator++;
		p[6] = buffer[iterator];
		iterator++;
		p[7] = buffer[iterator];
	}
	
	else
	{
		p[7] = buffer[iterator];
		iterator++;
		p[6] = buffer[iterator];
		iterator++;
		p[5] = buffer[iterator];
		iterator++;
		p[4] = buffer[iterator];
		iterator++;
		p[3] = buffer[iterator];
		iterator++;
		p[2] = buffer[iterator];
		iterator++;
		p[1] = buffer[iterator];
		iterator++;
		p[0] = buffer[iterator];
	}
	
	iterator++;
	
	return res;
}

-(NSString*) readString
{
	NSUInteger size = [self readSize];
	
	BRByte* buffer = (BRByte*)[stream bytes];
	
	char* aux = (char*)malloc( (size*sizeof(char))+1 );
	
	for (NSUInteger k=0; k<size; k++)
	{
		aux[k] = (char)buffer[iterator];
		iterator++;
	}
	
	aux[size] = '\0';
	
	NSString* res = [NSString stringWithUTF8String:aux];
	
	free(aux);
	
	return res;
}

-(NSString*) readUTF8String
{
	return [self readString];
}

-(NSArray*) readByteSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	BRByte* buffer = (BRByte*)[stream bytes];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[NSNumber numberWithUnsignedChar:buffer[iterator]]];
		iterator++;
	}
	
	return [res autorelease];
}

-(NSArray*) readBooleanSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[NSNumber numberWithBool:[self readBoolean]]];
	}
	
	return [res autorelease];
}

-(NSArray*) readShortSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[NSNumber numberWithShort:[self readShort]]];
	}

	return [res autorelease];
}

-(NSArray*) readIntegerSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[NSNumber numberWithInt:[self readInteger]]];
	}
	
	return [res autorelease];
}

-(NSArray*) readLongSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[NSNumber numberWithLong:[self readLong]]];
	}
	
	return [res autorelease];
}

-(NSArray*) readFloatSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[NSNumber numberWithFloat:[self readFloat]]];
	}
	
	return [res autorelease];
}

-(NSArray*) readDoubleSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[NSNumber numberWithDouble:[self readDouble]]];
	}
	
	return [res autorelease];
}

-(NSArray*) readStringSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[self readString]];
	}
	
	return [res autorelease];
}

-(NSArray*) readUTF8StringSeq
{
	NSMutableArray* res = [[NSMutableArray alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger k=0; k<size; k++)
	{
		[res addObject:[self readUTF8String]];
	}
	
	return [res autorelease];
}

-(NSDictionary*) readDictionary
{
	NSMutableDictionary* res = [[NSMutableDictionary alloc] init];
	NSUInteger size = [self readSize];
	
	for (NSUInteger i=0; i<size; i++)
	{
		NSString* key = [self readString];
		NSArray* value = [self readByteSeq];
		
		[res setValue:value forKey:key];
	}
	
	return [res autorelease];
}

-(NSData*) readData
{
    NSUInteger size = [self readSize];
    NSRange r;
    r.location = iterator;
    r.length = size;
    iterator+=size;
    NSData* data = [stream subdataWithRange:r];
    return data;
}

@end

