#import "BRICommunicationDevice.h"

@interface BRRequestReadThread : NSThread {
	id<BRICommunicationDevice> device;
	NSString* userID;
}

-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier;

@end
