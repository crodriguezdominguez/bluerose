#import <Foundation/Foundation.h>
#import "BRICommunicationDevice.h"

/**
 * Auxiliary thread for reading replies from a transmission 
 * channel. It's used for receiving servant replies
 *
 * @author Carlos Rodriguez Dominguez
 * @date 7-10-2009
 */
@interface BRReplyReadThread : NSThread {
	id<BRICommunicationDevice> device; /**< Transmission interface for the thread */
	NSString* userID; /**< User identificator of the sender */
}

-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier;

@end
