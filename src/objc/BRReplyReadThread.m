#import "BRReplyReadThread.h"
#import "BRMessageHeader.h"
#import "BRByteStreamReader.h"
#import "BRReplyMessageStack.h"
#import "BREventHandler.h"
#import "BREvent.h"


@implementation BRReplyReadThread

-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier
{
    self = [super init];
	if (self)
	{
		device = [dev retain];
		userID = [identifier retain];
	}
	
	return self;
}

-(void) dealloc
{
	[device release];
	[userID release];
	
	[super dealloc];
}

-(void) main
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	NSData* bytes;
	NSInteger requestId;
	BRByteStreamReader* reader = [[BRByteStreamReader alloc] init];
	
	BREvent* event = [[BREvent alloc] init];
	
	while ( (bytes = [device readFromUserID:userID]) != nil ) //read messages
	{	
		const BRByte* buffer = [bytes bytes];
		BRByte type = (BRByte)buffer[8];
		
		if (type == BR_REPLY_MSG)
		{
			//add to reply stack
			[reader open:bytes];
			
			[reader skip:14];
			requestId = [reader readInteger];
			
			[BRReplyMessageStack produceForRequestId:requestId bytes:bytes];
			
			[reader close];
		}
		
		else if (type == BR_EVENT_MSG)
		{
			[reader open:bytes];
			[reader skip:14];
			
			[event unmarshall:reader];
			
			[BREventHandler onConsume:event];
			
			[reader close];
		}
		
		else if (type == BR_CLOSE_CONNECTION_MSG)
		{
			break;
		}
	}
	
	[event release];
	[reader release];
	
	[device closeConnectionForUserID:userID];
	
	[pool release];
}

@end
