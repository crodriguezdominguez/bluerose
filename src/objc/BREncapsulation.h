#import "BRTypeDefinitions.h"

/**
 * Encapsulation of the body of a message
 */
@interface BREncapsulation : NSObject {
	NSInteger size; /**< Size of the encapsulation, including this header */
	BRByte major; /**< Major allowed encoding version as defined by ICE */
	BRByte minor; /**< Minor allowed encoding version as defined by ICE  */
	NSMutableData* byteCollection; /**< Message */
}

@property (readwrite, assign) NSInteger size;
@property (readwrite, assign) BRByte major;
@property (readwrite, assign) BRByte minor;
@property (readwrite, retain) NSMutableData* byteCollection;

@end
