#import "BRUtil.h"
#import "BRTypeDefinitions.h"

/*********** Size Definitions ***********/
#define BR_SIZE_OF_INT 4
#define BR_SIZE_OF_LONG 8
#define BR_SIZE_OF_FLOAT 4
#define BR_SIZE_OF_DOUBLE 8
#define BR_SIZE_OF_BOOLEAN 1
#define BR_SIZE_OF_STRING(x) (strlen(x))
/****************************************/


/************* Types Definition *********/
#define BR_INTEGER 0x00
#define BR_LONG 0x01
#define BR_FLOAT 0x02
#define BR_DOUBLE 0x03
#define BR_STRING 0x04
#define BR_BOOLEAN 0x05
#define BR_LIST_INT 0x07
#define BR_LIST_LONG 0x08
#define BR_LIST_FLOAT 0x09
#define BR_LIST_DOUBLE 0x0A
#define BR_LIST_STRING 0x0B
#define BR_LIST_BOOLEAN 0x0C
#define BR_DICTIONARY 0x0D
/*****************************************/


/***********  Message Types **********/
#define BR_REQUEST_MSG 0x00
#define BR_BATCH_REQUEST_MSG 0x01
#define BR_REPLY_MSG 0x02
#define BR_VALIDATE_CONNECTION_MSG 0x03
#define BR_CLOSE_CONNECTION_MSG 0x04
#define BR_EVENT_MSG 0x05   //extension for natively support events
/*************************************/


/********** Reply Status ************/
#define BR_SUCCESS_STATUS 0x00
#define BR_USER_EXCEPTION_STATUS 0x01
#define BR_OBJECT_NOT_EXIST_STATUS 0x02
#define BR_FACET_NOT_EXIST_STATUS 0x03
#define BR_OPERATION_NOT_EXIST_STATUS 0x04
#define BR_UNKNOWN_LOCAL_EXCEPTION_STATUS 0x05
#define BR_UNKNOWN_USER_EXCEPTION_STATUS 0x06
#define BR_UNKNOWN_EXCEPTION_STATUS 0x07
/*************************************/


/********** Operation Mode *********/
#define BR_TWOWAY_MODE 0x00
#define BR_ONEWAY_MODE 0x01
#define BR_BATCH_ONEWAY_MODE 0x02
#define BR_DATAGRAM_MODE 0x03
#define BR_BATCH_DATAGRAM_MODE 0x04
/**********************************/


/************ Other Definitions *********/
#define BR_TRUE 0x01
#define BR_FALSE 0x00

#define BR_PROTOCOL_MINOR 0x00
#define BR_PROTOCOL_MAJOR 0x01

#define BR_ENCODING_MINOR 0x00
#define BR_ENCODING_MAJOR 0x01
/*****************************************/

/**
 * Common header for any message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRMessageHeader : NSObject {
	BRByte* magic; /**< Magic bytes: "I", "c", "e", "P" */
	BRByte protocolMajor; /**< Major version of the protocol: 0x01 */
	BRByte protocolMinor; /**< Minor version of the protocol: 0x00 */
	BRByte encodingMajor; /**< Major version of the encoding: 0x01 */
	BRByte encodingMinor; /**< Minor version of the encoding: 0x00 */
	BRByte messageType; /**< Message type */
	BRByte compressionStatus; /**< Compression of the message. Currently only 0x00 */
	NSInteger messageSize; /**< Size of the message, including the header */
}

@property (readonly) BRByte* magic;
@property (readonly) BRByte protocolMajor;
@property (readonly) BRByte protocolMinor;
@property (readonly) BRByte encodingMajor;
@property (readonly) BRByte encodingMinor;
@property (readwrite, assign) BRByte messageType;
@property (readwrite, assign) BRByte compressionStatus;
@property (readwrite, assign) NSInteger messageSize;
		   
-(NSData*) bytes;

@end


/**
 * Header for a connection validating message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRValidateConnectionMessageHeader : BRMessageHeader {
	
}

@end


/**
 * Header for a connection closing message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRCloseConnectionMessageHeader :  BRMessageHeader {

}

@end


/**
 * Header for a batch requesting message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRBatchRequestMessageHeader : BRMessageHeader {
	NSInteger numRequests; /**< Number of requests inside the message */
	BRIdentity* identity; /**< Identity of the servant */
	NSMutableArray* facet; /**< Facet of the servant */
	NSString* operation; /**< Requested operation */
	BRByte mode; /**< Operation mode */
	NSMutableDictionary* context; /**< Context of the message */
}

@property (readwrite, assign) NSInteger numRequests;
@property (readwrite, retain) BRIdentity* identity;
@property (readwrite, retain) NSMutableArray* facet;
@property (readwrite, retain) NSString* operation;
@property (readwrite, assign) BRByte mode;
@property (readwrite, retain) NSMutableDictionary* context;

-(NSData*) bytes;

@end


/**
 * Header for a requesting message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRRequestMessageHeader : BRMessageHeader {
	NSInteger requestId; /**< Identifier of the request */
	BRIdentity* identity; /**< Identity of the servant */
	NSMutableArray* facet; /**< Facet of the servant */
	NSString* operation; /**< Requested operation */
	BRByte mode; /**< Operation mode */
	NSMutableDictionary* context; /** < Context of the message */
}

@property (readwrite, assign) NSInteger requestId;
@property (readwrite, retain) BRIdentity* identity;
@property (readwrite, retain) NSMutableArray* facet;
@property (readwrite, retain) NSString* operation;
@property (readwrite, assign) BRByte mode;
@property (readwrite, retain) NSMutableDictionary* context;

-(NSData*) bytes;

@end


/**
 * Header for a replying message. 
 * Definition taken from ICE (zeroc.com) for compatibility reasons.
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BRReplyMessageHeader : BRMessageHeader {
	NSInteger requestId;
	BRByte replyStatus;
}

@property (readwrite, assign) NSInteger requestId;
@property (readwrite, assign) BRByte replyStatus;

-(NSData*) bytes;

@end


/**
 * Header for an event message. 
 * Extension over ICE Protocol (zeroc.com).
 *
 * @author Carlos Rodriguez Dominguez
 * @date 07-10-2009
 */
@interface BREventMessageHeader : BRMessageHeader {

}

@end
