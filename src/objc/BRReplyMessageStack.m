#import "BRReplyMessageStack.h"

@implementation BRReplyMessageStack

static NSMutableDictionary* stack = nil; /**< Stack as pairs of requestIds-Messages */
static BOOL waitingReply; /**< True if someone is waiting a reply */
static pthread_mutex_t mutex; /**< Semaphore for concurrently accessing the stack */
static pthread_cond_t cond; /**< Conditional variable */

+(void) initializeStack
{
	stack = [[NSMutableDictionary alloc] init];
	waitingReply = NO;
	pthread_mutex_init(&mutex, 0);
	pthread_cond_init(&cond, 0);
}

+(void) destroyStack
{
	waitingReply = NO;
	
	[stack release];
	
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

+(void) produceForRequestId:(NSInteger)requestId bytes:(NSData*)bytes
{
	NSString* key = [NSString stringWithFormat:@"%d", requestId];
	
	pthread_mutex_lock(&mutex);
	[stack setValue:bytes forKey:key];
	pthread_mutex_unlock(&mutex);
	
	pthread_cond_broadcast(&cond);
}

+(NSData*) consumeRequestId:(NSInteger)requestId
{
	NSData* res = nil;
	
	pthread_mutex_lock(&mutex);
	
	NSString* key = [NSString stringWithFormat:@"%d", requestId];
	
	while([stack objectForKey:key] == nil)
	{
		waitingReply = YES;	
		pthread_cond_wait(&cond, &mutex);
	}
	
	res = [stack objectForKey:key];
	[stack removeObjectForKey:key];
	
	waitingReply = NO;
	
	pthread_mutex_unlock(&mutex);
	
	pthread_cond_broadcast(&cond);
	
	return res;
}

+(BOOL) isWaitingReply
{
	return waitingReply;
}

@end
