#import "BREventHandler.h"
#import "BRMessageHeader.h"
#import "BRByteStreamWriter.h"
#import "BRByteStreamReader.h"
#import "BRMessage.h"

@implementation BRPubSubProxy

@synthesize subscriptions;

-(id) initDistributedProxyWithDevice:(id<BRICommunicationDevice>)device
{
    self = [super init];
    if (self)
    {
        isDistributed = YES;
        self.subscriptions = [NSMutableDictionary dictionary];
    }
    
    return self;
}

-(id) init
{
	self = [super init];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
		[self resolveInitialization:nil waitOnline:YES parameters:nil];
        isDistributed = NO;
	}
	
	return self;
}

-(id) initWithDevice:(id<BRICommunicationDevice>)device
{
	self = [super init];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
		[self resolveInitialization:device waitOnline:YES parameters:nil];
        isDistributed = NO;
	}
	
	return self;
}

-(id) initWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	self = [super init];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
		[self resolveInitialization:device waitOnline:wait parameters:nil];
        isDistributed = NO;
	}
	
	return self;
}

-(id) initWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	self = [super init];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
		[self resolveInitialization:device waitOnline:wait parameters:pars];
        isDistributed = NO;
	}
	
	return self;
}

-(id) initWithServant:(NSString*)servID
{
    self = [super initWithServant:servID];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
        isDistributed = NO;
	}
	
	return self;
}

-(id) initWithServant:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device
{
    self = [super initWithServant:servID andDevice:device];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
        isDistributed = NO;
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
    self = [super initWithServant:servID device:device waitOnline:wait];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
        isDistributed = NO;
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
    self = [super initWithServant:servID device:device waitOnline:wait parameters:pars];
	if (self)
	{
		identity.id_name = @"PubSub";
		identity.category = @"BlueRoseService";
        isDistributed = NO;
	}
	
	return self;
}

-(void) dealloc
{
    self.subscriptions = nil;
    [super dealloc];
}

-(NSString*) typeID
{
	return @"BlueRoseService::PubSub";
}

-(void) subscribeToTopic:(NSInteger)topic
{
    if (!isDistributed)
    {
        NSInteger reqID;
        
        currentMode = BR_ONEWAY_MODE;
        
        NSMutableData* enc = [[NSMutableData alloc] init];
        
        pthread_mutex_lock([BRObjectProxy mutexWriter]);
        BRByteStreamWriter* writer = [BRObjectProxy writer];
        [writer open:enc];
        [writer writeInteger:topic];
        [writer close];
        pthread_mutex_unlock([BRObjectProxy mutexWriter]);
        
        if (!isDistributed)
        {
            reqID = [self sendRequestToServantID:servantID operation:@"subscribe0" request:enc];
            [self receiveReply:reqID];
        }
        else
        {
            [self broadcastRequestWithOperation:@"subscribe0" request:enc];
        }
        
        [enc release];
    }
}

-(void) subscribeToTopic:(NSInteger)topic andPredicate:(BRPredicate*)predicate
{
    if (!isDistributed)
    {
        NSInteger reqID;
        
        currentMode = BR_ONEWAY_MODE;
        
        NSMutableData* enc = [[NSMutableData alloc] init];
        
        pthread_mutex_lock([BRObjectProxy mutexWriter]);
        BRByteStreamWriter* writer = [BRObjectProxy writer];
        [writer open:enc];
        [writer writeInteger:topic];
        [predicate marshall:writer];
        [writer close];
        pthread_mutex_unlock([BRObjectProxy mutexWriter]);

        if (!isDistributed)
        {
            reqID = [self sendRequestToServantID:servantID operation:@"subscribe1" request:enc];
            [self receiveReply:reqID];
        }
        else
        {
            [self broadcastRequestWithOperation:@"subscribe1" request:enc];
        }
        
        [enc release];
    }
}

-(void) unsubscribe
{
    if (!isDistributed)
    {
        NSInteger reqID;
        
        currentMode = BR_ONEWAY_MODE;

        NSMutableData* enc = [[NSMutableData alloc] init];
        
        if (!isDistributed)
        {
            reqID = [self sendRequestToServantID:servantID operation:@"unsubscribe0" request:enc];
            [self receiveReply:reqID];
        }
        else
        {
            [self broadcastRequestWithOperation:@"unsubscribe0" request:enc];
        }
        
        [enc release];
    }
}

-(void) unsubscribe:(NSInteger)topic
{
    if (!isDistributed)
    {
        NSInteger reqID;
        
        currentMode = BR_ONEWAY_MODE;
        
        NSMutableData* enc = [[NSMutableData alloc] init];
        
        pthread_mutex_lock([BRObjectProxy mutexWriter]);
        BRByteStreamWriter* writer = [BRObjectProxy writer];
        [writer open:enc];
        [writer writeInteger:topic];
        [writer close];
        pthread_mutex_unlock([BRObjectProxy mutexWriter]);
        
        if (!isDistributed)
        {
            reqID = [self sendRequestToServantID:servantID operation:@"unsubscribe1" request:enc];
            [self receiveReply:reqID];
        }
        else
        {
            [self broadcastRequestWithOperation:@"unsubscribe1" request:enc];
        }
        
        [enc release];
    }
}

-(BOOL) isSubscribed:(NSString*)userID event:(BREvent*)templateEvent
{
    if (!isDistributed)
    {
        NSInteger reqID;
        
        currentMode = BR_TWOWAY_MODE;
        
        NSMutableData* enc = [[NSMutableData alloc] init];
        
        pthread_mutex_lock([BRObjectProxy mutexWriter]);
        BRByteStreamWriter* writer = [BRObjectProxy writer];
        [writer open:enc];
        [writer writeString:userID];
        [templateEvent marshall:writer];
        [writer close];
        pthread_mutex_unlock([BRObjectProxy mutexWriter]);
        
        reqID = [self sendRequestToServantID:servantID operation:@"isSubscribed" request:enc];
        NSData* result_bytes = [self receiveReply:reqID];
        
        pthread_mutex_lock([BRObjectProxy mutexReader]);
        BRByteStreamReader* reader = [BRObjectProxy reader];
        [reader open:result_bytes];
        BOOL result = [reader readBoolean];
        [reader close];
        pthread_mutex_unlock([BRObjectProxy mutexReader]);
        
        [enc release];
        
        return result;
    }
    else
    {
        return YES;
    }
}

-(NSArray*) subscribers
{
    if (!isDistributed)
    {
        NSInteger reqID;
        
        currentMode = BR_TWOWAY_MODE;
        
        NSMutableData* enc = [[NSMutableData alloc] init];
        
        reqID = [self sendRequestToServantID:servantID operation:@"getSubscribers0" request:enc];
        NSData* result_bytes = [self receiveReply:reqID];
        
        pthread_mutex_lock([BRObjectProxy mutexReader]);
        BRByteStreamReader* reader = [BRObjectProxy reader];
        [reader open:result_bytes];
        NSArray* result = [reader readStringSeq];
        [reader close];
        pthread_mutex_unlock([BRObjectProxy mutexReader]);
        
        [enc release];
        
        return result;
    }
    else
    {
        return [interface neighbours];
    }
}

-(NSArray*) subscribers:(BREvent*)templateEvent
{
    if (!isDistributed)
    {
        NSInteger reqID;
        
        currentMode = BR_TWOWAY_MODE;
        
        NSMutableData* enc = [[NSMutableData alloc] init];
        
        pthread_mutex_lock([BRObjectProxy mutexWriter]);
        BRByteStreamWriter* writer = [BRObjectProxy writer];
        [writer open:enc];
        [templateEvent marshall:writer];
        [writer close];
        pthread_mutex_unlock([BRObjectProxy mutexWriter]);
        
        reqID = [self sendRequestToServantID:servantID operation:@"getSubscribers1" request:enc];
        NSData* result_bytes = [self receiveReply:reqID];
        
        pthread_mutex_lock([BRObjectProxy mutexReader]);
        BRByteStreamReader* reader = [BRObjectProxy reader];
        [reader open:result_bytes];
        NSArray* result = [reader readStringSeq];
        [reader close];
        pthread_mutex_unlock([BRObjectProxy mutexReader]);
        
        [enc release];
        
        return result;
    }
    else
    {
        return [interface neighbours];
    }
}

-(void)publish:(BREvent*)event
{
	[self publish:event comeback:YES];
}

-(void)publish:(BREvent*)event comeback:(BOOL)comeback
{
	NSInteger reqID;
	
	currentMode = BR_TWOWAY_MODE;
	
	NSMutableData* enc = [[NSMutableData alloc] init];
    
    if (!isDistributed)
    {
        pthread_mutex_lock([BRObjectProxy mutexWriter]);
        BRByteStreamWriter* writer = [BRObjectProxy writer];
        [writer open:enc];
        [event marshall:writer];
        [writer writeBoolean:comeback];
        [writer close];
        pthread_mutex_unlock([BRObjectProxy mutexWriter]);
        
        reqID = [self sendRequestToServantID:servantID operation:@"publish" request:enc];
        [self receiveReply:reqID];
    }
    else
    {
        pthread_mutex_lock([BRObjectProxy mutexWriter]);
        BRByteStreamWriter* writer = [BRObjectProxy writer];
        [writer open:enc];
        [event marshall:writer];
        [writer close];
        pthread_mutex_unlock([BRObjectProxy mutexWriter]);
        
        BREventMessage* evtMessage = [[BREventMessage alloc] initWithBytes:enc];
        [self broadcastMessage:[evtMessage bytes]];
        [evtMessage release];
    }
	
	[enc release];
}

@end

@implementation BREventHandler

static BRPubSubProxy* proxy = nil;
static NSMutableDictionary* listeners = nil;

+(void) initializeDistributedEventHandlerWithDevice:(id<BRICommunicationDevice>)device
{
    listeners = [[NSMutableDictionary alloc] init];
    proxy = [[BRPubSubProxy alloc] initDistributedProxyWithDevice:device];
}

+(void) initializeEventHandlerWithDevice:(id<BRICommunicationDevice>)device
{
	listeners = [[NSMutableDictionary alloc] init];
	proxy = [[BRPubSubProxy alloc] initWithDevice:device];
}

+(void) initializeEventHandlerWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	listeners = [[NSMutableDictionary alloc] init];
	proxy = [[BRPubSubProxy alloc] initWithDevice:device waitOnline:wait];
}

+(void) initializeEventHandlerWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	listeners = [[NSMutableDictionary alloc] init];
	proxy = [[BRPubSubProxy alloc] initWithDevice:device waitOnline:wait parameters:pars];
}

+(void) initializeEventHandlerWithServant:(NSString*)servID
{
	listeners = [[NSMutableDictionary alloc] init];
	proxy = [[BRPubSubProxy alloc] initWithServant:servID];
}

+(void) initializeEventHandlerWithServant:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device
{
	listeners = [[NSMutableDictionary alloc] init];
	proxy = [[BRPubSubProxy alloc] initWithServant:servID andDevice:device];
}

+(void) initializeEventHandlerWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	listeners = [[NSMutableDictionary alloc] init];
	proxy = [[BRPubSubProxy alloc] initWithServant:servID device:device waitOnline:wait];
}

+(void) initializeEventHandlerWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	listeners = [[NSMutableDictionary alloc] init];
	proxy = [[BRPubSubProxy alloc] initWithServant:servID device:device waitOnline:wait parameters:pars];
}

+(void) destroyEventHandler
{
	[listeners release];
	[proxy unsubscribe];
	[proxy release];
}

+(void) onConsume:(BREvent*)event
{
	NSInteger topic = event.topic;
	NSArray* array = [listeners objectForKey:[NSNumber numberWithInt:topic]];
	if (array != nil)
	{
		for (BREventListener* listener in array)
		{
			if ([listener acceptsTopic:topic])
			{
				if ([listener acceptsEvent:event])
				{
					[listener performAction:event];
				}
			}
		}
	}
}

+(void) addEventListener:(BREventListener*)listener
{
	NSInteger topic = listener.topic;
	NSNumber* top = [NSNumber numberWithInt:topic];
	
	NSArray* array = [listeners objectForKey:top];
	if (array == nil)
	{
		NSMutableArray* v = [[NSMutableArray alloc] init];
		[v addObject:listener];
		
		[listeners setObject:v forKey:top];
		
		[v release];
	}
	else
	{
		NSMutableArray* arr = [NSMutableArray arrayWithArray:array];
		[arr addObject:listener];
		[listeners setObject:arr forKey:top];
	}
	
	if (listener.predicate == nil)
		[proxy subscribeToTopic:topic];
	
	else [proxy subscribeToTopic:topic andPredicate:listener.predicate];
	
	BREvent* evt = [[BREvent alloc] init];
	
	[proxy publish:evt];
	
	[evt release];
}

+(void) removeEventListener:(BREventListener*)listener
{
	[self removeEventListenerWithTopic:listener.topic andName:[listener listenerName]];
}

+(void) removeEventListeners:(NSInteger)topic
{
	NSNumber* top = [NSNumber numberWithInt:topic];
	
	NSArray* array = [listeners objectForKey:top];
	if (array != nil)
	{
		[proxy unsubscribe:topic];
		[listeners removeObjectForKey:top];
	}
}

+(void) removeEventListenerWithTopic:(NSInteger)topic andName:(NSString*)name
{
	NSNumber* top = [NSNumber numberWithInt:topic];
	
	NSMutableArray* array = [listeners objectForKey:top];
	if (array != nil)
	{
		for (BREventListener* listener in array)
		{
			if ([[listener listenerName] isEqualToString:name])
			{
				[array removeObject:listener];
				
				[listeners setObject:array forKey:top];
				
				if ([array count] == 0)
				{
					[self removeEventListeners:topic];
				}
				
				return;
			}
		}
	}
}

+(void)publish:(BREvent*)event
{
	[proxy publish:event];
}

+(void)publish:(BREvent*)event comeback:(BOOL)comeback
{
	[proxy publish:event comeback:comeback];
}

+(BRPubSubProxy*) pubSubProxy
{
	return proxy;
}

@end
