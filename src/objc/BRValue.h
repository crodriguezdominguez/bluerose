#import <Foundation/Foundation.h>
#import "BRTypeDefinitions.h"
#import "BRMarshallable.h"

typedef enum __BRValueType {
	BR_BYTE_TYPE, BR_SHORT_TYPE, BR_INTEGER_TYPE, BR_LONG_TYPE,
	BR_BOOLEAN_TYPE,
	BR_FLOAT_TYPE, BR_DOUBLE_TYPE,
	BR_STRING_TYPE,
	BR_SEQ_BYTE_TYPE, BR_SEQ_SHORT_TYPE, BR_SEQ_INTEGER_TYPE, BR_SEQ_LONG_TYPE,
	BR_SEQ_BOOLEAN_TYPE,
	BR_SEQ_FLOAT_TYPE, BR_SEQ_DOUBLE_TYPE,
	BR_SEQ_STRING_TYPE,
	BR_OTHER_TYPE, BR_NULL_TYPE
} BRValueType;

/**
 * Category for comparing some NSArrays
 *
 * @author Carlos Rodriguez Dominguez
 * @date 23-12-2009
 */
@interface NSArray (BRComparing)

/**
 * Compares two arrays in lexicographical order
 */
-(NSComparisonResult) compare:(NSArray*)otherArray;

@end

@interface BRValue : NSObject<BRMarshallable, NSCopying> {
	BRValueType type; /**< Type of the value */
	NSMutableData* rawValue; /**< Value */
}

@property (assign) BRValueType type;
@property (copy) NSMutableData* rawValue;

-(id) initWithValueType:(BRValueType)value_type;

-(BOOL) isLessThan:(BRValue*)value;
-(BOOL) isGreaterThan:(BRValue*)value;
-(BOOL) isLessEqualThan:(BRValue*)value;
-(BOOL) isGreaterEqualThan:(BRValue*)value;
-(BOOL) isEqualTo:(BRValue*)value;
-(BOOL) compareTo:(BRValue*)value withComparison:(BRComparison)comparison;

-(void) setByte:(BRByte)value;
-(void) setInteger:(NSInteger)value;
-(void) setShort:(short)value;
-(void) setLong:(long)value;

-(void) setBoolean:(BOOL)value;

-(void) setFloat:(float)value;
-(void) setDouble:(double)value;

-(void) setString:(NSString*)value;

-(void) setByteSeq:(NSArray*)value;
-(void) setData:(NSData*)value;
-(void) setIntegerSeq:(NSArray*)value;
-(void) setShortSeq:(NSArray*)value;
-(void) setLongSeq:(NSArray*)value;

-(void) setBooleanSeq:(NSArray*)value;

-(void) setFloatSeq:(NSArray*)value;
-(void) setDoubleSeq:(NSArray*)value;

-(void) setStringSeq:(NSArray*)value;

-(BRByte) getByte;
-(NSInteger) getInteger;
-(short) getShort;
-(long) getLong;

-(BOOL) getBoolean;

-(float) getFloat;
-(double) getDouble;

-(NSString*) getString;

-(NSData*) getData;
-(NSArray*) getByteSeq;
-(NSArray*) getIntegerSeq;
-(NSArray*) getShortSeq;
-(NSArray*) getLongSeq;

-(NSArray*) getBooleanSeq;

-(NSArray*) getFloatSeq;
-(NSArray*) getDoubleSeq;

-(NSArray*) getStringSeq;

@end
