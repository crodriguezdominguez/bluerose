#import "BRSocketCollection.h"
#import <sys/socket.h>
#import <unistd.h>

@implementation BRSocketCollection

@synthesize sockets;

-(id) init
{
	if (self = [super init])
	{
		sockets = [[NSMutableDictionary alloc] init];
		pthread_mutex_init(&mutex, 0);
	}

	return self;
}

-(void) dealloc
{
	[sockets release];
	pthread_mutex_destroy(&mutex);
	
	[super dealloc];
}

-(void) addSocket:(int)socket userID:(NSString*)userID
{
	pthread_mutex_lock(&mutex);
	[sockets setValue:[NSNumber numberWithInt:socket] forKey:userID];
	pthread_mutex_unlock(&mutex);
}

-(void) removeSocketForUserID:(NSString*)userID
{
	pthread_mutex_lock(&mutex);
	
	NSNumber* socket = [sockets valueForKey:userID];
	if (socket != nil)
	{
		shutdown([socket intValue], SHUT_RDWR);
		close([socket intValue]);
		[sockets removeObjectForKey:userID];
	}
	
	pthread_mutex_unlock(&mutex);
}

-(void) clear
{
	pthread_mutex_lock(&mutex);
	for (id key in sockets)
	{
		NSNumber* socket = [sockets objectForKey:key];
		shutdown([socket intValue], SHUT_RDWR);
		close([socket intValue]);
	}
	
	[sockets removeAllObjects];
	pthread_mutex_unlock(&mutex);
}

-(void) lockCollection
{
	pthread_mutex_lock(&mutex);
}

-(void) unlockCollection
{
	pthread_mutex_unlock(&mutex);
}

-(int) getSocketForUserID:(NSString*)userID
{
	int dummy = -1;
	
	NSNumber* socket = [sockets valueForKey:userID];
	if (socket != nil)
	{
		dummy = [socket intValue];
	}
	
	return dummy;
}

-(NSArray*) users
{
	return [sockets allKeys];
}

-(BOOL) someoneConnected
{
	BOOL res = NO;
	
	pthread_mutex_lock(&mutex);
	if ([sockets count] > 0) res = YES;
	pthread_mutex_unlock(&mutex);
	
	return res;
}

@end
