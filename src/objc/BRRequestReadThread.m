#import "BRRequestReadThread.h"
#import "BRMessage.h"
#import "BRMessageHeader.h"
#import "BRTypeDefinitions.h"
#import "BRDevicePool.h"


@implementation BRRequestReadThread

-(id) initWithCommunicationDevice:(id<BRICommunicationDevice>)dev userID:(NSString*)identifier
{
	if (self = [super init])
	{
		device = [dev retain];
		userID = [identifier retain];
	}
	
	return self;
}

-(void) dealloc
{
	[device release];
	[userID release];
	
	[super dealloc];
}

-(void) main
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	//send validate connection msg
	BRValidateConnectionMessage* msg = [[BRValidateConnectionMessage alloc] init];
	NSData* bytes = [msg bytes];
	[device writeToUserID:userID message:bytes];
	[msg release];

	while ( (bytes = [device readFromUserID:userID]) != nil ) //read messages
	{
		const BRByte* buffer = [bytes bytes];
		BRByte type = (BRByte)buffer[8];
		
		if (type == BR_CLOSE_CONNECTION_MSG)
		{
			break;
		}
		
		if (type == BR_REQUEST_MSG)
		{
			//process message
			[BRDevicePool dispatchMessage:bytes fromUserID:userID];
		}
	}
	
	NSLog(@"Connection closed: %@\n", userID);
	
	[device closeConnectionForUserID:userID];
	
	[pool release];
}

@end
