#import <pthread.h>
#import "BRMessageHeader.h"
#import "BRByteStreamWriter.h"

@implementation BRMessageHeader

@synthesize magic;
@synthesize protocolMajor;
@synthesize protocolMinor;
@synthesize encodingMajor;
@synthesize encodingMinor;
@synthesize messageType;
@synthesize compressionStatus;
@synthesize messageSize;

static BRByteStreamWriter* writer = nil;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

-(id) init
{
    self = [super init];
	if (self)
	{
		BRByte mmagic[4];
		magic = &mmagic[0];
		mmagic[0] = 0x49;  //IceP
		mmagic[1] = 0x63;
		mmagic[2] = 0x65;
		mmagic[3] = 0x50;
		
		protocolMajor = BR_PROTOCOL_MAJOR;
		protocolMinor = BR_PROTOCOL_MINOR;
		encodingMajor = BR_ENCODING_MAJOR;
		encodingMinor = BR_ENCODING_MINOR;
		messageType = BR_VALIDATE_CONNECTION_MSG;
		compressionStatus = 0x00;
		
		messageSize = 14;
		
		if (writer == nil)
			writer = [[BRByteStreamWriter alloc] init];
	}
	
	return self;
}

-(void) dealloc
{
	[super dealloc];
}

-(NSData*) bytes
{
	NSMutableData* res = [[NSMutableData alloc] init];
	
	pthread_mutex_lock(&mutex);
	
	[writer open:res];
	
	[writer writeByte:magic[0]];
	[writer writeByte:magic[1]];
	[writer writeByte:magic[2]];
	[writer writeByte:magic[3]];
	
	[writer writeByte:protocolMajor];
	[writer writeByte:protocolMinor];
	[writer writeByte:encodingMajor];
	[writer writeByte:encodingMinor];
	
    [writer writeByte:messageType];
    [writer writeByte:compressionStatus];
	
	[writer writeInteger:messageSize];
	
	[writer close];
	
	pthread_mutex_unlock(&mutex);
	
	return [res autorelease];
}

@end

@implementation BRValidateConnectionMessageHeader

-(id) init
{
    self = [super init];
	if (self)
	{
		messageType = BR_VALIDATE_CONNECTION_MSG;
	}
	
	return self;
}

@end

@implementation BRCloseConnectionMessageHeader

-(id) init
{
    self = [super init];
	if (self)
	{
		messageType = BR_CLOSE_CONNECTION_MSG;
	}
	
	return self;
}

@end

@implementation BRBatchRequestMessageHeader

@synthesize numRequests;
@synthesize identity;
@synthesize facet;
@synthesize operation;
@synthesize mode;
@synthesize context;

-(id) init
{
    self = [super init];
	if (self)
	{
		messageType = BR_BATCH_REQUEST_MSG;
		numRequests = 0;
		identity = [[BRIdentity alloc] init];
		facet = [[NSMutableArray alloc] init];
		operation = nil;
		context = [[NSMutableDictionary alloc] init];
		mode = BR_TWOWAY_MODE;
	}
	
	return self;
}

-(void) dealloc
{
	[identity release];
	[facet release];
	[operation release];
	[context release];
	
	[super dealloc];
}

-(NSData*) bytes
{
	NSMutableData* mutable_data = [NSMutableData dataWithData:[super bytes]];
	
	pthread_mutex_lock(&mutex);
	[writer open:mutable_data];
	
	//numRquests
	[writer writeInteger:numRequests];
	
	//insertar Identity
	[writer writeString:identity.id_name];
	[writer writeString:identity.category];
	
	//insertar facet
	[writer writeStringSeq:facet];
	
	//insertar operation
	[writer writeString:operation];
	
	//insertar mode
	[writer writeByte:mode];
	
	//insertar context
	[writer writeSize:[context count]];
	
	for (id key in context)
	{
		[writer writeString:key];
		[writer writeString:[context valueForKey:key]];
	}
	
	[writer close];
	 
	pthread_mutex_unlock(&mutex);
	
	return mutable_data;
}

@end

@implementation BRRequestMessageHeader

@synthesize requestId;
@synthesize identity;
@synthesize facet;
@synthesize operation;
@synthesize mode;
@synthesize context;

-(id) init
{
    self = [super init];
	if (self)
	{
		messageType = BR_REQUEST_MSG;
		requestId = 0;
		identity = [[BRIdentity alloc] init];
		operation = nil;
		facet = [[NSMutableArray alloc] init];
		context = [[NSMutableDictionary alloc] init];
		mode = BR_TWOWAY_MODE;
	}
	
	return self;
}

-(void) dealloc
{
	[identity release];
	[facet release];
	[operation release];
	[context release];
	
	[super dealloc];
}

-(NSData*) bytes
{
	NSMutableData* mutable_data = [NSMutableData dataWithData:[super bytes]];
	
	pthread_mutex_lock(&mutex);
	[writer open:mutable_data];
	
	//numRquests
	[writer writeInteger:requestId];
	
	//insertar Identity
	[writer writeString:identity.id_name];
	[writer writeString:identity.category];
	
	//insertar facet
	[writer writeStringSeq:facet];
	
	//insertar operation
	[writer writeString:operation];
	
	//insertar mode
	[writer writeByte:mode];
	
	//insertar context
	[writer writeSize:[context count]];
	
	for (id key in context)
	{
		[writer writeString:key];
		[writer writeString:[context valueForKey:key]];
	}
	
	[writer close];
	
	pthread_mutex_unlock(&mutex);
	
	return mutable_data;
}

@end;

@implementation BRReplyMessageHeader

@synthesize requestId;
@synthesize replyStatus;

-(id) init
{
    self = [super init];
	if (self)
	{
		messageType = BR_REPLY_MSG;
		requestId = 0;
		replyStatus = BR_SUCCESS_STATUS;
		messageSize = 19;
	}
	
	return self;
}

-(NSData*) bytes
{
	NSMutableData* mutable_data = [NSMutableData dataWithData:[super bytes]];
	
	pthread_mutex_lock(&mutex);
	[writer open:mutable_data];
	
	[writer writeInteger:requestId];
	[writer writeByte:replyStatus];
	
	[writer close];
	pthread_mutex_unlock(&mutex);
	
	return mutable_data;
}

@end


@implementation BREventMessageHeader

-(id) init
{
    self = [super init];
	if (self)
	{
		messageType = BR_EVENT_MSG;
	}
	
	return self;
}

@end

