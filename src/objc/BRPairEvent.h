#import <Foundation/Foundation.h>
#import "BREvent.h"

#define BR_PAIR_EVENT_TOPIC 1

@interface BRPairEvent : BREvent {

}

-(id) initWithX:(float)x andY:(float)y;

-(float) x;
-(float) y;

-(void) setX:(float)x;
-(void) setY:(float)y;

@end
