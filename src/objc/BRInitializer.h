#import <Foundation/Foundation.h>
#import "BRConfiguration.h"
#import "BRObjectServant.h"
#import "BRICommunicationDevice.h"
#import "BRIMobileDiscovery.h"


@interface BRInitializer : NSObject {
	
}

/**
 * Initiazes the whole BlueRose subsystem.
 */
+(void) initialize:(NSString*)configFile;

/**
 * Initializes a client associated with a device
 */
+(void) initializeClient:(id<BRICommunicationDevice>)device;

/**
 * Initializes a client associated with a device
 */
+(void) initializeClient:(id<BRICommunicationDevice>)device withDiscoveryServant:(BOOL)discoveryServant;

/**
 * Initializes a client associated with a device
 */
+(void) initializeClient:(id<BRICommunicationDevice>)device withDiscoveryServant:(BOOL)discoveryServant withEventHandler:(BOOL)withEventHandler;

/**
 * Creates a servant in the current thread
 */
+(void) initializeServant:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device;

/**
 * Creates a servant in the current thread
 */
+(void) initializeServant:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device registerAtDiscoveryServant:(BOOL)registerAtDiscoveryServant;

/**
 * Creates a mobile servant in the current thread
 */
+(void) initializeMobileServant:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device discoverer:(id<BRIMobileDiscovery>)discoverer serviceType:(int)serviceType;

/**
 * Free all the resources
 */
+(void) destroy;

/**
 * Returns the configuration of bluerose
 */
+(BRConfiguration*) configuration;

@end
