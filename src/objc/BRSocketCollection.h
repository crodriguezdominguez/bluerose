#import <Foundation/Foundation.h>
#import <pthread.h>

@interface BRSocketCollection : NSObject {
	NSMutableDictionary* sockets;
	pthread_mutex_t mutex;
}

@property (readonly) NSMutableDictionary* sockets;

-(void) addSocket:(int)socket userID:(NSString*)userID;
-(void) removeSocketForUserID:(NSString*)userID;
-(void) clear;
-(void) lockCollection;
-(void) unlockCollection;
-(int) getSocketForUserID:(NSString*)userID;
-(NSArray*) users;
-(BOOL) someoneConnected;

@end
