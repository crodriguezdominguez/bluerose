#import <sys/types.h>
#import <sys/sysctl.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <netinet/tcp.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h> 
#import <fcntl.h>
#import <unistd.h>

#import "BRInternetDevice.h"
#import "BRMessage.h"
#import "BRReplyReadThread.h"
#import "BRRequestReadThread.h"

@implementation BRInternetDevice

-(id) init
{
    self = [super init];
	if (self)
	{
		priority = 0;
		isServant = NO;
		sockets = [[BRSocketCollection alloc] init];
		servantThreads = [[NSMutableDictionary alloc] init];
		clientThreads = [[NSMutableDictionary alloc] init];
		openConnRefCounter = [[NSMutableDictionary alloc] init];
		
		pthread_mutex_init(&mutex, NULL);
	}
	
	return self;
}

-(void) dealloc
{
	[sockets release];
	[servantThreads release];
	[clientThreads release];
	[openConnRefCounter release];
	[servantAddress release];
	
	if (isServant) close(servantSocket);
	
	pthread_mutex_destroy(&mutex);
	
	[super dealloc];
}

-(void) openConnectionForUserID:(NSString*)userID andParameters:(NSMutableDictionary*)parameters
{
	//TODO: parameters
	
	//avoid reopen already opened connection
	BOOL connectionOpenned = [self isConnectionOpenned:userID];
	if (connectionOpenned)
	{
		pthread_mutex_lock(&mutex);
		NSNumber* number = [openConnRefCounter valueForKey:userID];
		int n = [number intValue]+1;
		[openConnRefCounter setValue:[NSNumber numberWithInt:n] forKey:userID];
		pthread_mutex_unlock(&mutex);
	}
	else
	{
		pthread_mutex_lock(&mutex);
		[openConnRefCounter setValue:[NSNumber numberWithInt:1] forKey:userID];
		pthread_mutex_unlock(&mutex);
		
		//establish connection
		struct sockaddr_in serv_addr;
		struct hostent *server;
		
		NSArray* results = [userID componentsSeparatedByString:@":"];
		
		NSString* str = [results objectAtIndex:1];
		int port = [str intValue];
		NSString* host = [results objectAtIndex:0];
		int yes = 1;
		int delay = 1;
		
		int sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0) 
		{
			@throw [NSException exceptionWithName:@"ERROR: socket creation failed" 
										   reason:@"Socket was not created" 
										 userInfo:nil];
		}
		
		//no delay
		if (setsockopt(sockfd, IPPROTO_TCP, 
					   TCP_NODELAY, 
					   (char*)&delay, sizeof(delay)) < 0)
		{
			@throw [NSException exceptionWithName:@"ERROR: setting socket options" 
										   reason:@"Socket was not created" 
										 userInfo:nil];
		}
		
		//keep connection alive
		if (setsockopt(sockfd, SOL_SOCKET, 
					   SO_KEEPALIVE, 
					   (char*)&yes, sizeof(yes)) < 0)
		{
			@throw [NSException exceptionWithName:@"ERROR: setting socket options" 
										   reason:@"Socket was not created" 
										 userInfo:nil];
		}
		
		//initialize connection
	    server = gethostbyname([host UTF8String]);
	    if (server == NULL) 
		{
			@throw [NSException exceptionWithName:@"ERROR: host not found" 
										   reason:@"Host was unreacheable" 
										 userInfo:nil];
	    }
		
	    bzero((char *) &serv_addr, sizeof(serv_addr));
		
	    serv_addr.sin_family = AF_INET;
		
	    bcopy((char *)server->h_addr, 
			  (char *)&serv_addr.sin_addr.s_addr,
			  server->h_length);
		
	    serv_addr.sin_port = htons(port);
		
	    if (connect(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr)) < 0)
		{
			@throw [NSException exceptionWithName:@"ERROR: connection failed" 
										   reason:@"Host was unreacheable" 
										 userInfo:nil];
		}
		
		[sockets addSocket:sockfd userID:userID];
		
		//read validate connection msg
		[self readFromUserID:userID];
	}
}

-(void) openConnectionForUserID:(NSString*)userID
{
	[self openConnectionForUserID:userID andParameters:nil];
}

-(BOOL) isConnectionOpenned:(NSString*)userID
{
	pthread_mutex_unlock(&mutex); //TODO: check this deadlock bug...
	
	BOOL openned = NO;
	
	[sockets lockCollection];
	if ([sockets getSocketForUserID:userID] != -1)
	{
		openned = YES;
	}
	[sockets unlockCollection];
	
	return openned;
}

-(void) closeConnectionForUserID:(NSString*)userID
{
	//send CloseConnectionMessage
	if ([self isConnectionOpenned:userID])
	{
		pthread_mutex_lock(&mutex);
		NSNumber* number = [openConnRefCounter valueForKey:userID];
		int n = [number intValue]-1;
		[openConnRefCounter setValue:[NSNumber numberWithInt:n] forKey:userID];
		if (n > 0)
		{
			pthread_mutex_unlock(&mutex);
			return;
		}
		pthread_mutex_unlock(&mutex);
		
		if (isServant)
		{
			BRCloseConnectionMessage* msg = [[BRCloseConnectionMessage alloc] init];
			
			[self writeToUserID:userID message:[msg bytes]];
			
			[msg release];
		}
		
		[sockets removeSocketForUserID:userID];
		
		pthread_mutex_lock(&mutex);
		
		if (isServant)
		{
			[servantThreads removeObjectForKey:userID];
		}
		else
		{
			[clientThreads removeObjectForKey:userID];
		}
		
		pthread_mutex_unlock(&mutex);
		
		[sockets removeSocketForUserID:userID];
		
		pthread_mutex_lock(&mutex);
		//if (sockets.find(userID) != sockets.end()) 
		//	sockets.erase(sockets.find(userID));
		
		[openConnRefCounter removeObjectForKey:userID];
		
		//ips.erase(ips.find(userID));
		pthread_mutex_unlock(&mutex);
	}
}

-(void) closeAllConnections
{
	[servantThreads removeAllObjects];
	[clientThreads removeAllObjects];
	[openConnRefCounter removeAllObjects];
	[sockets clear];
}

-(BOOL) writeToUserID:(NSString*)userID message:(NSData*)message
{
	BOOL found = YES;
	
	int sock = [sockets getSocketForUserID:userID];
	if (sock == -1) found = NO;
	
	if (!found)
	{
		return NO;
	}
	
	pthread_mutex_lock(&mutex);
	BRByte* buffer = (BRByte*)[message bytes];
	
	if (!isServant && buffer[8] == BR_REQUEST_MSG)
	{
		BRRequestMessage* msg = [[BRRequestMessage alloc] initWithBytes:message];
		BRRequestMessageHeader* hd = (BRRequestMessageHeader*)msg.header;
		
		if (hd.mode == BR_TWOWAY_MODE)
		{
			BRReplyReadThread* thread = [clientThreads valueForKey:userID];
			if (thread == nil)
			{
				thread = [[BRReplyReadThread alloc] initWithCommunicationDevice:self userID:userID];
				[clientThreads setValue:thread forKey:userID];
				[thread start];
				[thread release];
			}
		}
		
		[msg release];
	}
	pthread_mutex_unlock(&mutex);
	
	NSInteger number_of_bytes = (NSInteger)[message length];
	int r;
	char* data_to_write = (char*) &buffer[0];
	
	//force flush
	while (number_of_bytes) 
	{
		r = write(sock,data_to_write,number_of_bytes);
	  	if (r <= 0)
		{
			return false;
		}
		data_to_write += r;
		number_of_bytes -= r;
	}
	
	[sockets unlockCollection];
	return true;
}

-(NSData*) readFromUserID:(NSString*)userID
{
	BOOL found = YES;
	
	int sockfd = [sockets getSocketForUserID:userID];
	if (sockfd == -1) found = NO;
	
	if (!found)
	{
		//sockets.unlockCollection();
		return NO;
	}
	
	int n;
	int size = -1;
	int buffer_size = 14;
	
	BRByte buffer_init[15];
	
	n = read(sockfd, buffer_init, 14);
	if (n < 14)
	{
		return nil;
	}
	else
	{
		NSMutableData* msg = [[NSMutableData alloc] init];
		[msg appendBytes:buffer_init length:n];
		
		size = 0;
		BRByte* p = (BRByte*)&size;
	
		if (CFByteOrderGetCurrent() == CFByteOrderLittleEndian)
		{
			p[0] = buffer_init[10];
			p[1] = buffer_init[11];
			p[2] = buffer_init[12];
			p[3] = buffer_init[13];
		}
		else
		{
			p[3] = buffer_init[10];
			p[2] = buffer_init[11];
			p[1] = buffer_init[12];
			p[0] = buffer_init[13];
		}
		
		buffer_size = size-n;
		
		if (buffer_size == 0)
		{
			//sockets.unlockCollection();
			return [msg autorelease];
		}
		else
		{
			BRByte* buffer = (BRByte*)malloc((buffer_size+1) * sizeof(BRByte));
			
			BOOL fin = NO;
			while(!fin)
			{
				n = read(sockfd, buffer, buffer_size);
				if (n <= 0)
				{
					free(buffer);
					//sockets.unlockCollection();
					[msg release];
					return nil;
				}
				
				buffer_size -= n;
				
				if (buffer_size == 0)
				{
					fin = YES;
				}
				
				[msg appendBytes:buffer length:n];
			}
			
			free(buffer);
			
			//sockets.unlockCollection();
			return [msg autorelease];
		}
	}
}

-(void) setPriority:(NSInteger)p
{
	priority = p;
}

-(NSInteger) priority
{
	return priority;
}

-(NSArray*) neighbours
{
	return [sockets users];
}

-(void) broadcast:(NSData*)message
{
	NSArray* neighbours = [sockets users];
	for (NSString* userID in neighbours)
	{
		[self writeToUserID:userID message:message];
	}
}

-(BOOL) isBlockantConnection
{
	return YES;
}

-(BOOL) isAvailable
{
	[sockets lockCollection];
	
	BRValidateConnectionMessage* msg = [[BRValidateConnectionMessage alloc] init];
	NSData* bytes = [msg bytes];
	
	NSArray* array = [sockets users];
	
	for (NSString* userID in array)
	{
		if ([self writeToUserID:userID message:bytes])
		{
			[sockets unlockCollection];
			[msg release];
			return YES;
		}
	}
	
	[sockets unlockCollection];
	[msg release];
	return NO;
}

-(BOOL) isUserAvailable:(NSString*)userID
{
	[sockets lockCollection];
	
	BOOL found = YES;
	
	int sock = [sockets getSocketForUserID:userID];
	
	if (sock == -1) found = NO;
	if (!found)
	{
		[sockets unlockCollection];
		return NO;
	}
	
	BRValidateConnectionMessage* msg = [[BRValidateConnectionMessage alloc] init];
	
	if (![self writeToUserID:userID message:[msg bytes]])
	{
		[sockets unlockCollection];
		[msg release];
		return false;
	}
	
	[sockets unlockCollection];
	[msg release];
	
	return YES;
}

-(BOOL) isConnectionOriented
{
	return YES;
}

-(void) setServantIdentifier:(NSString *)servantID
{
	[servantAddress release];
	servantAddress = [servantID retain];
}

-(void) setServant:(NSString*)multiplexingId
{
	isServant = YES;
	servantPort = [multiplexingId intValue];
	
	//Get localhost IP
	struct ifaddrs* ifAddrStruct=NULL;
	struct in_addr* tmpAddrPtr=NULL;
	char substr[5];
	char* address = NULL;
	
	if ([servantAddress isEqualToString:@""] || [servantAddress isEqualToString:@"localhost"] || [servantAddress isEqualToString:@"127.0.0.1"])
	{
		getifaddrs(&ifAddrStruct);
		
		while (ifAddrStruct!=NULL) 
		{
			// check it is IP4 and not lo0
			if (ifAddrStruct->ifa_addr->sa_family==AF_INET && strcmp(ifAddrStruct->ifa_name, "lo0")!=0 &&
				strlen(ifAddrStruct->ifa_name) <= 3) 
			{
				// is a valid IP4 Address
				tmpAddrPtr=&((struct sockaddr_in *)ifAddrStruct->ifa_addr)->sin_addr;
				address = inet_ntoa(*tmpAddrPtr);
				servantAddress = [NSString stringWithFormat:@"%s:%d", address, servantPort];
				
				//it is not a 127.x.x.x address...
				substr[0] = address[0]; substr[1] = address[1]; substr[2] = address[2]; substr[3] = '\0';
				if (strcmp(substr, "127") != 0)
				{
					if (strcmp(address, "0.0.0.0") != 0) break;
				}
			}
			
			ifAddrStruct=ifAddrStruct->ifa_next;
		}
		
		if (ifAddrStruct == NULL)
		{
			NSLog(@"Warning: Only local connections will be allowed. Please, check your network configuration");
		}
	}
	else
	{
		//check if the port is not established...
		NSArray* results = [servantAddress componentsSeparatedByString:@":"];
		if ([results count] < 2)
		{
			NSString* auxiliary = [NSString stringWithString:servantAddress];
			[servantAddress release];
			servantAddress = [[NSString alloc] initWithFormat:@"%@:%d", auxiliary, servantPort];
		}
	}
	
	[self start];
}

-(void) waitForConnections
{
	while(![self isFinished])
	{
		[NSThread sleepForTimeInterval:0.5];
	}
}

-(NSString*) deviceName
{
	return @"InternetDevice";
}

-(NSString*) servantIdentifier
{
	return servantAddress;
}

-(void) main
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	int newsockfd;
	socklen_t clilen;
	
	struct sockaddr_in serv_addr, cli_addr;
	int yes = 1, delay=1;
	
	servantSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (servantSocket < 0)
	{
		@throw [NSException exceptionWithName:@"ERROR: creating socket" 
									   reason:@"Servant socket was not created" 
									 userInfo:nil];
	}
	
	//avoid port used error
	if (setsockopt(servantSocket, SOL_SOCKET, 
				   SO_REUSEADDR, 
				   (char*)&yes, sizeof(yes)) < 0)
	{
		@throw [NSException exceptionWithName:@"ERROR: setting socket options" 
									   reason:@"Socket was not created" 
									 userInfo:nil];
	}
	
	//no-delay
	if (setsockopt(servantSocket, IPPROTO_TCP, 
				   TCP_NODELAY, 
				   (char*)&delay, sizeof(delay)) < 0)
	{
		@throw [NSException exceptionWithName:@"ERROR: setting socket options" 
									   reason:@"Socket was not created" 
									 userInfo:nil];
	}
	
	//keep alive connections
	if (setsockopt(servantSocket, SOL_SOCKET, 
				   SO_KEEPALIVE, 
				   (char*)&yes, sizeof(yes)) < 0)
	{
		@throw [NSException exceptionWithName:@"ERROR: setting socket options" 
									   reason:@"Socket was not created" 
									 userInfo:nil];
	}
	
	if (setsockopt(servantSocket, SOL_SOCKET, 
				   SO_NOSIGPIPE, 
				   (char*)&yes, sizeof(yes)) < 0)
	{
		@throw [NSException exceptionWithName:@"ERROR: setting socket options" 
									   reason:@"Socket was not created" 
									 userInfo:nil];				
	}
	
	bzero((char *) &serv_addr, sizeof(serv_addr));
	
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(servantPort);
	if (bind(servantSocket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		@throw [NSException exceptionWithName:@"ERROR: binding socket port" 
									   reason:@"Socket port is busy" 
									 userInfo:nil];
	}
	
	listen(servantSocket, SOMAXCONN);
	
	BOOL run = YES;
	while(run)
	{
		newsockfd = accept(servantSocket, 
						   (struct sockaddr*)&cli_addr, 
						   &clilen);
		
		if (newsockfd >= 0)
		{
			NSString* aux = [NSString stringWithFormat:@"%s:%d", inet_ntoa(cli_addr.sin_addr), cli_addr.sin_port];
			NSLog(@"Connection opened: %@\n", aux);
			
			pthread_mutex_lock(&mutex);
			
			//sockets[buffer] = newsockfd;
			[sockets addSocket:newsockfd userID:aux];
			//ips[buffer] = inet_ntoa(cli_addr.sin_addr);
			
			BRRequestReadThread* thread = [[BRRequestReadThread alloc] initWithCommunicationDevice:self userID:aux];
			[servantThreads setValue:thread forKey:aux];
			[thread start];
			
			[thread release];
			
			pthread_mutex_unlock(&mutex);
		}
	}
	
	[pool drain];
}

@end
