#import <libxml/parser.h>
#import <libxml/tree.h>


@interface BRConfiguration : NSObject {
	xmlDocPtr doc;
}
		
-(id) initWithFile:(NSString*)xmlFile;
-(NSMutableArray*) devices;
-(NSString*) moduleForDevice:(NSString*)device;
-(NSMutableArray*) categories;
-(NSMutableArray*) servicesForCategory:(NSString*)category;
-(NSMutableArray*) devicesForService:(NSString*)service andCategory:(NSString*)category;
-(NSString*) portForService:(NSString*)service category:(NSString*)category andDevice:(NSString*)device;
-(NSString*) addressForService:(NSString*)service category:(NSString*)category andDevice:(NSString*)device;
-(NSString*) userIDForService:(NSString*)service category:(NSString*)category andDevice:(NSString*)device;

@end
