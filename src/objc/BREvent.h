#import <Foundation/Foundation.h>
#import "BRValue.h"
#import "BRMarshallable.h"

#define BR_DEFAULT_TOPIC 0

@class BREventNode;

@interface BREvent : NSObject<BRMarshallable> {
	NSInteger topic; /**< Event topic */
	NSMutableDictionary* nodes; /**< Map of identifiers and associated nodes */
}

@property (readwrite, assign) int topic;
@property (retain) NSMutableDictionary* nodes;

-(id) initWithEvent:(BREvent*)event;
-(id) initWithTopic:(NSInteger)top;

-(BOOL) existsMember:(NSString*)member;
-(NSUInteger) length;

-(BREventNode*) member:(NSString*)member;
-(void) setMember:(NSString*)member node:(BREventNode*)node;
-(void) setMember:(NSString*)member value:(BRValue*)value;

@end


@interface BREventNode : BREvent<NSCopying> {
	NSString* identifier;
	BRValue* value;
}

@property (retain) NSString* identifier;
@property (copy) BRValue* value;

-(id) initWithIdentifier:(NSString*)ident value:(BRValue*)val;

@end

