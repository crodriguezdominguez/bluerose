#import <Foundation/Foundation.h>
#import "BRMobileDiscoveryListener.h"
#import "BRICommunicationDevice.h"
#import "BRObjectServant.h"

/**
 * Interface to implement device discovery based on Zeroconf (dns-sd.org).
 *
 * @author Carlos Rodriguez Dominguez
 * @date 15-06-2010
 */
@protocol BRIMobileDiscovery<NSObject>

/**
 * Adds a new listener to the device discovery.
 *
 * @param listener Listener to add
 */
-(void) addEventListener:(id<BRMobileDiscoveryListener>)listener;

/**
 * Removes a listener from the device discovery.
 *
 * @param listener Listener to remove
 */
-(void) removeEventListener:(id<BRMobileDiscoveryListener>)listener;

/**
 * Registers a servant for its discovery. That servant must be associated with a device.
 * When the register is complete, "onElementRegistered" method will be called on all the
 * listeners.
 *
 * @param servant Servant to register.
 * @param device Device associated with the servant.
 */
-(BOOL) registerForDetection:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device;

/**
 * Unregisters a servant from discovery.
 *
 * @param servant Servant to unregister.
 */
-(void) unregister:(BRObjectServant*)servant;

/**
 * Resolves the userID of a discovered service or device. When the resolution is complete,
 * "onUserIDResolved" method will be called on all the listeners.
 *
 * @param name Name provided by "onElementDiscovered" or "onElementRegistered" listener methods.
 */
-(void) resolveUserID:(NSNetService*)service;

/**
 * Returns the userID of a service from its name. It does not call any method on the listeners.
 *
 * @param name Name of the service
 * @param serviceType Type of the service to resolve
 * @return UserID of the service.
 */
-(NSString*) resolveUserIDForService:(NSString*)name serviceType:(int)serviceType;

/**
 * Start event detection. It will run on background on a separate thread from the caller.
 * The serviceType will be determined by each implementation of the discovery.
 * It will be useful in order to select different types of services.
 * This method can NOT be called more than once if the method "stopDetection" is not called.
 * In order to detect more than one type of service, more than one instance of a IMobileDiscovery
 * class will be needed.
 *
 * @param serviceType Type of service as determined by the implementation of the discoverer
 * @param domain Domain in which services will be discovered (see dns-sd.org for further information)
 * @return True if the detection started correctly. False otherwise else.
 */
-(BOOL) startDetection:(int)serviceType domain:(NSString*)domain;

/**
 * Overload of the startDetection method but with a local domain always
 *
 * @param serviceType Type of service as determined by the implementation of the discoverer
 * @return True if the detection started correctly. False otherwise else.
 */
-(BOOL) startDetection:(int)serviceType;

/**
 * Stops the detection of events.
 */
-(void) stopDetection;

@end
