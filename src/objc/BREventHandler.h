#import "BREvent.h"
#import "BREventListener.h"
#import "BRICommunicationDevice.h"
#import "BRObjectProxy.h"

@class BRPubSubProxy;
@class BREventHandler;

@interface BRPubSubProxy : BRObjectProxy {
	BOOL isDistributed;
    NSMutableDictionary* subscriptions;
}

@property (nonatomic, retain) NSMutableDictionary* subscriptions;

-(id) initDistributedProxyWithDevice:(id<BRICommunicationDevice>)device;

/**
 * Adds a new subscription to a topic
 *
 * @param _topic Topic to subscribe to
 */
-(void) subscribeToTopic:(NSInteger)topic;

/**
 * Adds a new subscription to a topic, but with a set of constraints
 *
 * @param _topic Topic to subscribe to
 * @param _pred Set of constraints over the properties of the events
 */
-(void) subscribeToTopic:(NSInteger)topic andPredicate:(BRPredicate*)predicate;

/**
 * Removes all the subscriptions
 */
-(void) unsubscribe;

/**
 * Remove a subscription to a topic
 *
 * @param _topic Topic associated with the subscription to remove
 */
-(void) unsubscribe:(NSInteger)topic;

/**
 * Checks if any of the user subscriptions allow him/her to receive the event
 * that is specified by the template
 *
 * @param userID User whose subscription is going to be checked
 * @param templateEvent Event template
 * @return YES if the user may receive the event specified by the template.
 *         NO otherwise else.
 */
-(BOOL) isSubscribed:(NSString*)userID event:(BREvent*)templateEvent;

/**
 * Returns all the subscribers
 *
 * @return Vector of subscribers
 */
-(NSArray*) subscribers;

/**
 * Returns all the subscribers that may receive the specified event
 *
 * @param templateEvent Event
 * @return Vector of subscribers
 */
-(NSArray*) subscribers:(BREvent*)templateEvent;

/**
 * Publishes an event to all its subscribers (including the publisher, if it's subscribed
 * to the event)
 *
 * @param event Event to be published
 *
 */
-(void)publish:(BREvent*)event;

/**
 * Publishes an event to all its subscribers
 *
 * @param event Event to be published
 * @param comeBack True if the event has to be published to the publisher because
 *                 it is subscribed. False if the event will not be published to
 *                 the published even if it is subscribed to it.
 */
-(void)publish:(BREvent*)event comeback:(BOOL)comeback;

@end


@interface BREventHandler : NSObject {

}

+(void) initializeDistributedEventHandlerWithDevice:(id<BRICommunicationDevice>)device;

+(void) initializeEventHandlerWithDevice:(id<BRICommunicationDevice>)device;
+(void) initializeEventHandlerWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait;
+(void) initializeEventHandlerWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars;

+(void) initializeEventHandlerWithServant:(NSString*)servID;
+(void) initializeEventHandlerWithServant:(NSString*)servID andDevice:(id<BRICommunicationDevice>)device;
+(void) initializeEventHandlerWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait;
+(void) initializeEventHandlerWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars;

/**
 * Destructor
 */
+(void) destroyEventHandler;

/**
 * Action to be performed whenever an event is consumed
 *
 * @param event Received event
 */
+(void) onConsume:(BREvent*)event;

/**
 * Adds an event listener to the listeners. The event handler
 * manages the memory of the listener that is passed as an argument.
 *
 * @param listener Event listener to add
 */
+(void) addEventListener:(BREventListener*)listener;

/**
 * Removes an event listener
 *
 * @param listener EventListener to remove
 */
+(void) removeEventListener:(BREventListener*)listener;

/**
 * Removes all the event listener that are related with a topic
 *
 * @param topic Topic to remove
 */
+(void) removeEventListeners:(NSInteger)topic;

/**
 * Removes an event listener from the listeners
 *
 * @param topic Topic of the listener to remove
 * @param name Name of the listener to remove
 */
+(void) removeEventListenerWithTopic:(NSInteger)topic andName:(NSString*)name;

/**
 * Publishes an event to all its subscribers (including the publisher, if it's subscribed
 * to the event)
 *
 * @param event Event to be published
 *
 */
+(void)publish:(BREvent*)event;

/**
 * Publishes an event to all its subscribers
 *
 * @param event Event to be published
 * @param comeBack True if the event has to be published to the publisher because
 *                 it is subscribed. False if the event will not be published to
 *                 the published even if it is subscribed to it.
 */
+(void)publish:(BREvent*)event comeback:(BOOL)comeback;

/**
 * Returns the proxy to the pub/sub service
 *
 * @return Proxy to the pub/sub service
 */
+(BRPubSubProxy*) pubSubProxy;

@end
