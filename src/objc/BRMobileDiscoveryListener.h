#import <Foundation/Foundation.h>

@protocol BRMobileDiscoveryListener<NSObject>

@optional

/**
 * Method that will be called whenever a new device or service is discovered.
 *
 * @param name Name of the discovered element
 * @param regtype Type of registration as specified by Zeroconf standard
 * @param domain Domain of the element. Usually it will be "_local.".
 * @param moreComing It specifies if any event is coming inmediatly after this event.
 *                   This paramater has to be checked in order to avoid unnecesary UI updates.
 * @param error It specifies if an error ocurred.
 */
-(void) onElementDiscovered:(id)element serviceBrowser:(id)serviceBrowser moreComing:(BOOL)moreComing;

/**
 * Method that will be called whenever a new device or service is registered.
 *
 * @param fullname Name of the registered device or service
 * @param userID userID of the registered device or service. It can be used
 *        directly with a ICommunicationDevice instance in order to exchange information.
 * @param records TXT records as specified in dns-sd.org. It includes extra information about
 *                the service or device.
 * @param moreComing It specifies if any event is coming inmediatly after this event.
 *                   This paramater has to be checked in order to avoid unnecesary UI updates.
 * @param error It specifies if an error ocurred.
 */
-(void) onElementRegistered:(id)element userID:(NSString*)userID error:(BOOL)error;

/**
 * Method that will be called whenever a device or service disappears.
 *
 * @param name Name of the discovered element
 * @param regtype Type of registration as specified by Zeroconf standard
 * @param domain Domain of the element. Usually it will be "_local.".
 * @param moreComing It specifies if any event is coming inmediatly after this event.
 *                   This paramater has to be checked in order to avoid unnecesary UI updates.
 * @param error It specifies if an error ocurred.
 */
-(void) onElementRemoved:(id)element serviceBrowser:(id)serviceBrowser moreComing:(BOOL)moreComing;

/**
 * Method that will be called whenever a device or service is resolved.
 *
 * @param fullname Name of the registered device or service
 * @param userID userID of the registered device or service. It can be used
 *        directly with a ICommunicationDevice instance in order to exchange information.
 * @param records TXT records as specified in dns-sd.org. It includes extra information about
 *                the service or device.
 * @param moreComing It specifies if any event is coming inmediatly after this event.
 *                   This paramater has to be checked in order to avoid unnecesary UI updates.
 * @param error It specifies if an error ocurred.
 */
-(void) onUserIDResolved:(id)element userID:(NSString*)userID error:(BOOL)error;

@end
