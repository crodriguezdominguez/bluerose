#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import <pthread.h>
#import "BRICommunicationDevice.h"
#import "BRMessage.h"

@class BRBlueToothDevice;

@protocol BRBlueToothEventListener<NSObject>

@optional
-(void) connectionToUserIDFailed:(NSString*)userID withError:(NSError*)error device:(BRBlueToothDevice*)device;
-(void) clientFailed:(BRBlueToothDevice*)device withError:(NSError*)error;
-(void) connectionRequestFromUserID:(NSString*)userID device:(BRBlueToothDevice*)device;
-(void) userID:(NSString*)userID stateChanged:(GKPeerConnectionState)state device:(BRBlueToothDevice*)device;

@end

@interface BRBlueToothDevice : NSObject<BRICommunicationDevice, GKSessionDelegate> {
	GKSession* session;
	NSMutableDictionary* dataModes;
	NSMutableArray* listeners;
	BOOL isServant;
	int priority;
	pthread_mutex_t mutex;
    
    BOOL dataReceived;
    NSData* lastDataReceived;
    NSString* dataReceptionUserID;
}

@property (readonly) GKSession* session;
@property (nonatomic, retain) NSData* lastDataReceived;
@property (nonatomic, retain) NSString* dataReceptionUserID;

-(void) addEventListener:(id<BRBlueToothEventListener>)listener;
-(void) removeEventListener:(id<BRBlueToothEventListener>)listener;

-(void) setupSession;
-(void) destroySession;

-(void) willTerminate:(NSNotification*)notification;
-(void) willResume:(NSNotification*)notification;

@end
