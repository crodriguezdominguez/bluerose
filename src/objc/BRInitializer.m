#import "BRInitializer.h"
#import "BRDevicePool.h"
#import "BRDiscoveryProxy.h"
#import "BREventHandler.h"

@implementation BRInitializer

static BRConfiguration* configuration = nil;

+(void) initialize:(NSString*)configFile
{
	configuration = [[BRConfiguration alloc] initWithFile:configFile];
}

+(void) initializeClient:(id<BRICommunicationDevice>)device
{
	[self initializeClient:device withDiscoveryServant:YES withEventHandler:YES];
}

+(void) initializeClient:(id<BRICommunicationDevice>)device withDiscoveryServant:(BOOL)discoveryServant
{
	[self initializeClient:device withDiscoveryServant:discoveryServant withEventHandler:YES];
}

+(void) initializeClient:(id<BRICommunicationDevice>)device withDiscoveryServant:(BOOL)discoveryServant withEventHandler:(BOOL)withEventHandler
{
	[BRDevicePool initializeDevicePool];
	[BRDevicePool addDevice:device];
	
	if (discoveryServant)
	{
		if ([BRDiscoveryProxy defaultProxy] == nil)
		{
			NSString* discoveryAddress = [configuration userIDForService:@"Discovery" category:@"BlueRoseService" andDevice:[device deviceName]];
			
			if (discoveryAddress != nil)
			{
				[BRDiscoveryProxy initializeDiscoveryProxy:discoveryAddress andDevice:device];
			}
			else
			{
				@throw [NSException exceptionWithName:@"ERROR: Discovery servant not configured in config file" 
											   reason:@"Config file does not contain information about the Discovery servant" 
											 userInfo:nil];
			}
		}
	}
	
	if (withEventHandler)
	{
		if (discoveryServant)
			[BREventHandler initializeEventHandlerWithDevice:device];
		
		else //extract from config file
		{
			NSString* pub_sub = [configuration userIDForService:@"PubSub" category:@"BlueRoseService" andDevice:[device deviceName]];
			
			if (pub_sub != nil)
			{
				[BREventHandler initializeEventHandlerWithServant:pub_sub andDevice:device];
			}
			else
			{
				@throw [NSException exceptionWithName:@"ERROR: PubSub servant not configured in config file" 
											   reason:@"Config file does not contain information about the PubSub servant" 
											 userInfo:nil];
			}
		}
	}
}

+(void) initializeMobileServant:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device discoverer:(id<BRIMobileDiscovery>)discoverer serviceType:(int)serviceType
{
	[BRDevicePool initializeDevicePool];
	[BRDevicePool addDevice:device];
	
	BRIdentity* identity = servant.identity;
	NSString* port = [configuration portForService:identity.id_name category:identity.category andDevice:[device deviceName]];
	
	if ([port isEqualToString:@""])
	{
		@throw [NSException exceptionWithName:@"ERROR: Port not specified in config file" 
									   reason:@"Port not specified in config file"
									 userInfo:nil];
	}
	
	//set as servant
	[device setServant:port];
	
	if ([BRDiscoveryProxy defaultProxy] == nil)
	{
		//search for the discoveryProxy with the discoverer
		NSString* discoveryAddress = [discoverer resolveUserIDForService:@"BlueRoseService::Discovery" serviceType:serviceType];
		
		if (![discoveryAddress isEqualToString:@""])
		{
			[BRDiscoveryProxy initializeDiscoveryProxy:discoveryAddress andDevice:device];
		}
		else
		{
			@throw [NSException exceptionWithName:@"ERROR: Discovery servant not initialized" 
										   reason:@"Discovery servant not configured in config file" 
										 userInfo:nil];
		}
	}
	
	[discoverer registerForDetection:servant device:device];
}

+(void) initializeServant:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device
{
	[self initializeServant:servant device:device registerAtDiscoveryServant:YES];
}

+(void) initializeServant:(BRObjectServant*)servant device:(id<BRICommunicationDevice>)device registerAtDiscoveryServant:(BOOL)registerAtDiscoveryServant
{
	[BRDevicePool initializeDevicePool];
	[BRDevicePool addDevice:device];
	
	BRIdentity* identity = servant.identity;
	NSString* address = [configuration addressForService:identity.id_name category:identity.category andDevice:[device deviceName]];
	NSString* port = [configuration portForService:identity.id_name category:identity.category andDevice:[device deviceName]];	
	if (port == nil)
	{
		@throw [NSException exceptionWithName:@"ERROR: Port not specified in config file" 
									   reason:@"Config file does not contain information about the port of the specified servant" 
									 userInfo:nil];
	}
	
	//set as servant
	[device setServantIdentifier:address];
	[device setServant:port];
	
	if (registerAtDiscoveryServant)
	{
		if ([BRDiscoveryProxy defaultProxy] == nil)
		{
			NSString* discoveryAddress = [configuration userIDForService:@"Discovery" category:@"BlueRoseService" andDevice:[device deviceName]];
			
			if (discoveryAddress != nil)
			{
				[BRDiscoveryProxy initializeDiscoveryProxy:discoveryAddress andDevice:device];
			}
			else
			{
				@throw [NSException exceptionWithName:@"ERROR: Discovery servant not configured in config file" 
											   reason:@"Config file does not contain information about the Discovery servant" 
											 userInfo:nil];
			}
		}
		
		[[BRDiscoveryProxy defaultProxy] addRegistration:[servant identifier] address:[device servantIdentifier]];
	}
}

+(void) destroy
{
	[configuration release];
	
	[BRDevicePool destroyDevicePool];
	[BRDiscoveryProxy destroyDiscoveryProxy];
	[BREventHandler destroyEventHandler];
}

+(BRConfiguration*) configuration
{
	return configuration;
}

@end
