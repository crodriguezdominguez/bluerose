// Author: Carlos Rodríguez Domínguez
// Date: Jan. 2013
// Description: Client JS API for webBlueRose

/*
* Constructor of a BlueRose object
* @param server_host URL of the BlueRose server to connect to, WITHOUT prefix (i.e., "http://...")
* @param opts An associative array containing the following optional params:
*				- encrypt: If True, the connection is encrypted
*				- auth: If True, it is mandatory to set "auth_name" and "auth_key". These values
*						are sent to the server in order to authenticate the user
* @return A new BlueRose object. You can inmediatly call connect method or establish "onstart",
*			"onfinalize", "onerror" and "onmessage" event handlers.
*/
function BlueRose(server_host, opts) {
	this.options = opts;
	this.listeners = {};
	this.server_host = server_host;
	this.connection = null;
};

/**
* Function triggered when the connection to the server is established.
*/
BlueRose.prototype.onstart = function() {
	//can be overriden
};

/**
* Function triggered when the connection to the server is closed.
*/
BlueRose.prototype.onfinalize = function(error) {
	//can be overriden
};

/**
* Function triggered when an error happened in the connection to the server.
*/
BlueRose.prototype.onerror = function(error) {
	//can be overriden
};

/**
* Function triggered when a message from the server is received.
*/
BlueRose.prototype.onmessage = function(message) {
	//can be overriden
};

/**
* Creates a connection to the server, using the parameters established in the constructor
* @pre The connection could not be previously established
*/
BlueRose.prototype.connect = function() {
	var prefix = "ws://";
	if (this.options.encrypt)
	{
		prefix = "wss://";
	}
	//establish a websocket connection
	this.connection = new WebSocket(prefix+this.server_host);
	var d = this;
	d.connection.onopen = function () {
		//send auth data
		if (d.options.auth)
		{
			d.listeners['auth-autorization'] = function(message) {
				if (!message.authorized) {
					d.onerror('The authorization was rejected by the server. The connection will be closed.');
					d.finalize();
				}
			}
			var auth = {'type': 'auth', 'contents': {'auth_name': d.options.auth_name, 'auth_key': d.options.auth_key}};
			try {
				d.connection.send(JSON.stringify(auth));
			} catch(exception)
			{
				this.onerror(exception.message);
			}
		}
		
		//call the event handler
		d.onstart();
	};
		
    d.connection.onerror = function (error) {
		// an error occurred when sending/receiving data
		d.onerror(error);
		d.finalize();
	};
		    
    d.connection.onclose = function (error) {
    	//the connection was closed
		d.finalize();
		d.onfinalize(error);
	};
		
	d.connection.onmessage = function (message) {
		//analyze received data and decide what to do with it
		var obj = JSON.parse(message.data);
		if (obj.type == 'evt')
		{
			//iterate over the listeners
			var event_type = obj.contents.type;
			for(key in d.listeners)
			{
				if (key == event_type)
				{
					d.listeners[key](obj.contents); //call the function
				}
			}
		}
		
		//send the message to the user handler
		d.onmessage(obj);
	};
};

/**
* Subscribes to an event
* @param event_type The type of the event to be subscribed to
* @param triggered_function The function to be called whenever an event typed
*        as "event_type" is received. The function can have the received event
*		 as an argument. The received event will have three fields:
*            - event_type: The type of the event
*			 - data: The contents of the event
*			 - timestamp: The moment in which the event was originally published
* @post The events associated to "event_type" begin to be received
*/
BlueRose.prototype.subscribe = function(event_type, triggered_function) {
	if (!this.listeners[event_type])
	{
		//send the subscription to the server
		var subs = {'type': 'subs', 'contents': {'type': event_type, 'timestamp': new Date()}};
		try {
			this.connection.send(JSON.stringify(subs));
		} catch (exception)
		{
			this.onerror(exception.message);
		}
	}
	
	//associate the triggered_function with the event_type in the listeners
	this.listeners[event_type] = triggered_function;
};

/**
* Unsubscribes to an event
* @param event_type The type of the event to be unsubscribed of
* @pre There should exist a subscription to "event_type" before unsubscribing to it
* @post The events associated to "event_type" are not received anymore
*/
BlueRose.prototype.unsubscribe = function(event_type) {
	delete this.listeners[event_type]; //remove the triggered function
	
	//send the unsubscription to the server
	var subs = {'type': 'unsubs', 'contents': {'type': event_type, 'timestamp': new Date()}};
	try {
		this.connection.send(JSON.stringify(subs));
	}catch(exception)
	{
		this.onerror(exception.message);
	}
};

/**
* Publishes an event associated to "event_type"
* @param event_type The type to be associated to the event
* @param event The contents of the event to be delivered
* @param comeback If True, then if the user is susbscribed to "event_type", the
* 		 event is also received and the corresponding listener triggered.
*/
BlueRose.prototype.publish = function(event_type, event, comeback) {
	//send the event to the server
	var evt = {'type': 'evt', 'contents': {'type': event_type, 'data': event, 'timestamp': new Date()}, 'comeback': comeback};
	try {
		this.connection.send(JSON.stringify(evt));
	}catch(exception)
	{
		this.onerror(exception.message);
	}
};

/**
* Finalizes a connection to the BlueRose server
* @pre The connection should be previously established by calling "connect" method
* @post It is not possible anymore to operate with the connection without
*       calling "connect" again
*/
BlueRose.prototype.finalize = function() {
	if (this.connection)
	{
		this.connection.close();
		this.connection = null;
	}
	this.listeners = {};
};
