// http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
 
// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'node-mp-pubsub-server';
 
// Port where we'll run the websocket server
var webSocketsServerPort = 8080;
 
// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');
var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

var children = [];

if (cluster.isMaster) {
	for (var i = 0; i < numCPUs; i++) {
    	var child = cluster.fork();
    	children.push(child);
    	
    	//when a message is received from a children, deliver it to the other children
    	child.on('message', function(message){
			var index = children.indexOf(this);
    		for (var i=0; i<children.length; i++)
    		{
				if (i!=index) children[i].send(message);
	    	}
    	});
    }

	cluster.on('exit', function(worker, code, signal) {
	    console.log('worker ' + worker.process.pid + ' died');
	});
}
else
{
	/**
	 * Global variables
	 */
	// latest 100 messages
	var history = [ ];
	// list of currently connected clients (users)
	var clients = [ ];
	
	// list of subscriptions
	var subscriptions = { };
	 
	/**
	 * Helper function for escaping input strings
	 */
	function htmlEntities(str) {
	    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;')
	                      .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
	
	/**
	* Function to check if a client is subscribed to an event
	*/
	function isSubscribed(client, event_type) {
		//look for client in subscriptions
		var str = stringifyConnection(client);
		if (subscriptions[str])
		{
			var subs = subscriptions[str];
			if (subs.indexOf('all') != -1) return true;
			else if (subs.indexOf(event_type) != -1) return true;
			else return false;
		}
		else return false;
	}
	
	function stringifyConnection(connection) {
		return connection.socket.remoteAddress+":"+connection.socket.remotePort;
	}
	 
	/**
	 * HTTP server
	 */
	var server = http.createServer(function(request, response) {
	    // Not important for us. We're writing WebSocket server, not HTTP server
	});
	server.listen(webSocketsServerPort, function() {
	    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
	});
	 
	/**
	 * WebSocket server
	 */
	var wsServer = new webSocketServer({
	    // WebSocket server is tied to a HTTP server. WebSocket request is just
	    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
	    httpServer: server
	});
	
	//when the worker receives a message, it broadcasts it to all the clients
	process.on('message', function(message) {
    	// broadcast message to all subscribed clients
    	var event = message.event;
		var event_type = event.contents.type;
        var comeback = event.comeback;
        
        var json = message.utf8Data;
        var origin_conn = message.connection;
        for (var i=0; i < clients.length; i++) {
        	var send = true;
        	if (origin_conn == stringifyConnection(clients[i]) && !comeback)
        	{
            	send = false;
        	}
        	if (!isSubscribed(clients[i], event_type))
        	{
            	send = false;
        	}
            if (send) clients[i].sendUTF(json);
        }
    });
	 
	// This callback function is called every time someone
	// tries to connect to the WebSocket server
	wsServer.on('request', function(request) {
	    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
	 
	    // accept connection - you should check 'request.origin' to make sure that
	    // client is connecting from your website
	    // (http://en.wikipedia.org/wiki/Same_origin_policy)
	    var connection = request.accept(null, request.origin); 
	    
	    clients.push(connection);
	 
	    console.log((new Date()) + ' Connection accepted: '+connection.remoteAddress);
	 
	    // send back event history
	    /*if (history.length > 0) {
	        connection.sendUTF(JSON.stringify( { type: 'evt-history', data: history} ));
	    }*/
	 
	    // user sent some message
	    connection.on('message', function(message) {
	        if (message.type === 'utf8') { // accept only text
	        	var messageData = JSON.parse(message.utf8Data);
	        	if (messageData.type == 'evt')
	        	{
			    	var event = messageData;
			    
			        console.log((new Date()) + ' Received event from '
			                    + stringifyConnection(connection));
			        
			        // we want to keep history of all sent messages
			        var obj = {
			            time: (new Date()).getTime(),
			            text: htmlEntities(event)
			        };
			        history.push(obj);
			        history = history.slice(-100);
			
			        //broadcast the event
					var event_type = event.contents.type;
					var comeback = event.comeback;
					var index = clients.indexOf(connection);
					var json = message.utf8Data;
					var origin_conn = message.connection;
					for (var i=0; i < clients.length; i++) {
						var send = true;
						if (i == index && !comeback)
						{
					    		send = false;
						}
						if (!isSubscribed(clients[i], event_type))
						{
					    		send = false;
						}
					    if (send) clients[i].sendUTF(json);
					}

			        //notify the master about the event to be broadcasted by other workers
			        process.send({connection: stringifyConnection(connection), event: event, utf8Data:message.utf8Data});
			    }
			    else if (messageData.type == 'subs')
			    {
				    //subscribe to event
				    var str = stringifyConnection(connection);
				    if (!subscriptions[str])
				    {
					    subscriptions[str] = new Array();
				    }
				    
				    if (subscriptions[str].indexOf(messageData.contents.type) == -1)
				    {
				    	subscriptions[str].push(messageData.contents.type);
				    }
			    }
			    else if (messageData.type == 'unsubs')
			    {
				    //subscribe to event
				    var str = stringifyConnection(connection);
				    if (isSubscribed(connection, messageData.contents.type))
				    {
				    	var index = subscriptions[str].indexOf(messageData.contents.type);
				    	if (index >= 0) subscriptions[str].splice(index, 1);
				    }
			    }
			    else if (messageData.type == 'auth')
			    {
				    //TODO
			    }
			    else 
			    {
				    console.log('Not supported message received from '+connection.remoteAddress+', contents: '+message.utf8Data);
			    }
	        }
	    });
	 
	    // user disconnected
	    connection.on('close', function(socket) {
	        console.log((new Date()) + " Peer "
	            + connection.remoteAddress + " disconnected.");
	        // remove user from the list of connected clients
	        var index = clients.indexOf(connection);
	        if (index !== -1) {
	        	// remove the subscriptions of the client
	        	delete subscriptions[stringifyConnection(connection)];
	        	
	            // remove the connection from clients
	            clients.splice(index, 1);
	        }
	    });
	 
	});
}
