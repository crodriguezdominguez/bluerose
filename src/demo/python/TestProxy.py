# encoding: utf-8
"""
TestProxy.py

Created by Carlos Rodriguez Dominguez on 2010-04-24.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.ObjectProxy import *
from bluerose.AsyncMethodCallback import *
from bluerose.ByteStreamReader import *

class AddCallback(AsyncMethodCallback):
	def __init__(self):
		AsyncMethodCallback.__init__(self)
	
	def callback(self, message):
		reader = ByteStreamReader(message)
		self.addCallback(reader.readInteger())
	
	def addCallback(self, result):
		print("Resultado suma async: %d" % result)

class TestProxy(ObjectProxy):
	
	def __init__(self, servantID=None, device=None, waitOnline=True, parameters=dict()):
		ObjectProxy.__init__(self, servantID, device, waitOnline, parameters)
		self.identity.idName = "TestServant"
		self.identity.category = "Test"
		
		if servantID==None:
			self.resolveInitialization(device, waitOnline, parameters)
			
	def getTypeID(self):
		return "Test::TestServant"
	
	def addAsync(self, x, y):
		self.currentMode = MessageHeader.TWOWAY_MODE
		
		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeInteger(x)
		ObjectProxy.writer.writeInteger(y)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()
		
		reqID = self.sendRequest(self.servantID, "add", data)
		
		callback = AddCallback()
		self.receiveCallback(reqID, callback)
	
	def add(self, x, y):
		self.currentMode = MessageHeader.TWOWAY_MODE
		
		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeInteger(x)
		ObjectProxy.writer.writeInteger(y)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()
		
		reqID = self.sendRequest(self.servantID, "add", data)
		result_bytes = self.receiveReply(reqID)
		
		reader = ByteStreamReader(result_bytes)
		return reader.readInteger()
	
	def println(self, message):
		self.currentMode = MessageHeader.ONEWAY_MODE
		
		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeUTF8String(message)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()
		
		reqID = self.sendRequest(self.servantID, "print", data)
		self.receiveReply(reqID)
	
	def eventTest(self, evt):
		self.currentMode = MessageHeader.ONEWAY_MODE
		
		ObjectProxy.staticMutex.acquire()
		ObjectProxy.writer.writeObject(evt)
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()
		
		reqID = self.sendRequest(self.servantID, "event_test", data)
		self.receiveReply(reqID)
		