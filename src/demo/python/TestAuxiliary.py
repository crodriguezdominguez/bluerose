#!/usr/bin/env python
# encoding: utf-8
"""
TestAuxiliary.py

Created by Carlos Rodriguez Dominguez on 2010-04-24.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

import threading
import time
import random
from TestProxy import *
from bluerose.events.EventListener import *
from bluerose.events.EventHandler import *
from bluerose.events.Event import *

MAX_ITER = 1000

class TestThread(threading.Thread):
	"""
	Thread for testing concurrent access to 
	a proxy object.
	"""
	mutex = threading.Lock() #allows "print" mutual exclusion
	
	def __init__(self, testProxy, index):
		self.obj = testProxy
		self.index = index
		
		threading.Thread.__init__(self)
	
	def run(self):
		for i in range(10):
			try:
				res = self.obj.add(i, i*i)
				TestThread.mutex.acquire()
				print("Index: %d,  %d + %d = %d" % (self.index, i, i*i, res))
				TestThread.mutex.release()
			except Exception as e:
				TestThread.mutex.acquire()
				print(e)
				TestThread.mutex.release()
				#esperar que exista la conexion
				self.obj.waitOnlineMode()

class TestPairEventListener(EventListener):
	"""Listener for pair events."""
	
	def __init__(self):
		EventListener.__init__(self, 1) #subscribe to pair topic (number 1)
		self.k=0
	
	def performAction(self, event):
		print("Pair Event %d Received: %s" % (self.k, str(event)))
		self.k += 1

def local_add(x, y):
	"""Add x and y (locally) and return the result."""
	
	return x+y

def test_local():
	"""Run MAX_ITER calls to local_add function."""
	
	for i in range(MAX_ITER):
		inicio = time.time()
		result = local_add(i, i+1)
		fin = time.time()
		print("Result: %d + %d = %d   (%f)" % (i, i+1, result, fin-inicio))

def test_objetos():
	"""Create and destroy MAX_ITER proxy objects."""
	
	test_objs = [] #test objs are stored in order to prevent their deallocation
	
	inicio = time.time()
	for i in range(MAX_ITER):
		test = TestProxy()
		test_objs.append(test)
	fin = time.time()
	
	print("Time to create %d test proxies: %f" % (MAX_ITER, fin-inicio))

def test_publish_subscribe():
	"""Publish events (pair events) for one second."""
	
	random.seed()
	
	evt_listener = TestPairEventListener()
	EventHandler.addEventListener(evt_listener)
	
	evt = Event(1) #create event and establish topic to 1 (pair event topic)
	inicio = time.time()
	while True:
		x = Value()
		x.setFloat(random.random())
		y = Value()
		y.setFloat(random.random())
		
		evt["x"] = x
		evt["y"] = y
		
		EventHandler.publish(evt)
		fin = time.time()
		
		if (fin-inicio) >= 1.0:
			break

	EventHandler.removeEventListener(evt_listener)
