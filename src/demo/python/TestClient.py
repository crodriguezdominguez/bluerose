#!/usr/bin/env python
# encoding: utf-8
"""
TestClient.py

Created by Carlos Rodriguez Dominguez on 2010-04-24.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

import bluerose
import time

from bluerose.devices.TcpCompatibleDevice import *
from bluerose.Initializer import *
from bluerose.events.Event import *
from TestAuxiliary import *

def main():
		contador_tiempo = 0.0

		#initialize the middleware
		cli = TcpCompatibleDevice()
		Initializer.initialize("config.xml")
		Initializer.initializeClient(cli)

		#work with objects
		test_objetos()

		#work with an specific object whose servant is discovered dynamically
		dcalc = TestProxy()

		test_local()

		print("------------------------------------")

		#add i and i+1 from 0 to MAX_ITER
		for i in range(MAX_ITER):
			inicio = time.time()
			result = dcalc.add(i, i+1)
			fin = time.time()

			contador_tiempo += (fin-inicio)

			print("Result: %d + %d = %d   (%f)" % (i, i+1, result, fin-inicio))

		#add i and i+1 from 0 to MAX_ITER. Receive the result asynchronously
		for i in range(MAX_ITER):
			inicio = time.time()
			dcalc.addAsync(i, i+1)
			fin = time.time()

			contador_tiempo += (fin-inicio)

			print("Async: %d + %d    (%f)" % (i, i+1, fin-inicio))

		#print a message at the server
		for i in range(MAX_ITER):
			inicio = time.time()
			dcalc.println(u'hello world ñññáéíóù!!!')
			fin = time.time()

			contador_tiempo += (fin-inicio)

			print("Print %d: (%f)" % (i, fin-inicio))

		#check a proxy shared between several threads
		inicio = time.time()
		threads = []
		for i in range(20):
			aux = TestThread(dcalc, i)
			threads.append(aux)
			aux.start()

		for i in range(20):
			threads[i].join()

		#event test
		for i in range(20):
			x = Value()
			x.setFloat(i)
			y = Value()
			y.setFloat(i+1)
			
			evt = Event()
			evt["x"] = x
			evt["y"] = y
			
			dcalc.eventTest(evt)

		#work with pub/sub paradigm
		test_publish_subscribe()

		fin = time.time()

		#shutdown bluerose
		Initializer.destroy()

		contador_tiempo += (fin-inicio)

		print("Execution Time: %f" % contador_tiempo)
		
		return 0
	
if __name__ == '__main__':
	sys.exit(main())

