#!/usr/bin/env python
# encoding: utf-8
"""
TestServer.py

Created by Carlos Rodriguez Dominguez on 2010-04-24.
Copyright (c) 2010 GEDES Research Group. All rights reserved.
"""

from bluerose.ByteStreamReader import *
from bluerose.Initializer import *
from bluerose.ObjectServant import *
from bluerose.devices.TcpCompatibleDevice import *
from bluerose.events.Event import *
from bluerose.messages.MessageHeader import *

class TestServer(ObjectServant):
	def __init__(self):
		ObjectServant.__init__(self)
		self.identity.idName = "TestServant"
		self.identity.category = "Test"
	
	def add(self, x, y):
		return x+y;

	def println(self, string):
		print(string)

	def eventTest(self, evt):
		print("Event received: %d (%s, %s)" % (evt.topic, str(evt.getMemberValue("x")), str(evt.getMemberValue("y"))))
		
	def runMethod(self, methodName, userID, arguments):
		reader = ByteStreamReader(arguments)
		
		if methodName == "add":
			ObjectServant.mutex.acquire()
			
			result = self.add(reader.readInteger(), reader.readInteger());
			ObjectServant.writer.writeInteger(result)
			result = ObjectServant.writer.toList()
				
			ObjectServant.writer.reset()
			ObjectServant.mutex.release()
			
			return (result, MessageHeader.SUCCESS_STATUS)
		
		elif methodName == "print":
			res = reader.readUTF8String()
			self.println(res)
			return ([], MessageHeader.SUCCESS_STATUS)
		
		elif methodName == "event_test":
			evt = Event()
			evt.unmarshall(reader)
			self.eventTest(evt)
			return ([], MessageHeader.SUCCESS_STATUS)
		
		return ([], MessageHeader.OPERATION_NOT_EXIST_STATUS)

def main():
	#initialize transmission devices
	device = TcpCompatibleDevice()
	
	#initialize objects
	servant = TestServer()
	
	#initialize bluerose and the servant		
	#try:
	Initializer.initialize("config.xml")
	Initializer.initializeServant(servant, device)
	#except Exception as e:
	#	print(e)
	
	#wait for connections to the device
	device.waitForConnections()
	
	#cleanups
	Initializer.destroy()
	
	return 0
	
if __name__ == '__main__':
	sys.exit(main())
