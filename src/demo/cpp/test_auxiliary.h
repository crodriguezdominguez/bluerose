#ifndef _TEST_AUXILIARY_H_
#define _TEST_AUXILIARY_H_

#include <vector>
#include "EventHandler.h"
#include "TestProxy.h"
#include "PairEvent.h"

#define MAX_ITER 1000

using namespace Test;

/**
* adds x and y (locally) and returns the result
*/
int local_add(int x, int y);

/**
* It runs MAX_ITER calls to local_add function
*/
void test_local();

/**
* It creates and destroys 10000 proxy objects
*/
void test_objetos();

/**
* It publishes events (pair events) for one second
*/
void test_publish_subscribe();

/**
* Thread for testing concurrent access to 
* a proxy object
*/
class TestThread;

/**
* Listener for pair events
*/
class TestPairEventListener;



/***************************************
*******     IMPLEMENTATIONS      *******
***************************************/

class TestThread : public BlueRose::Thread
{
	TestProxy* obj;
	int index;
	
public:
	TestThread(TestProxy* _obj, int _index)
	{
		obj = _obj;
		index = _index;
	}
	
	void run()
	{
		for (int i=0; i<10; i++)
		{
			try{
				int res = obj->add(i, i*i);
				printf("Index: %d,  %d + %d = %d\n", index, i, i*i, res);
				fflush(stdout);
			}
			catch(char* str)
			{
				printf("%s\n", str);

				//esperar que exista la conexion
				obj->waitOnlineMode();
			}
		}
	}
};

class TestPairEventListener : public BlueRose::IEventListener {
private:
	int k;
public:
	TestPairEventListener() : IEventListener(PAIR_EVENT_TOPIC)
	{
		k=0;
	}
	
	std::string getName()
	{
		return "TestListener";
	}
	
	void action(const BlueRose::Event* evt)
	{
		BlueRose::PairEvent* event = (BlueRose::PairEvent*)evt;
		
		printf("Pair Event %d Received: %f, %f   (%d members)\n", k, event->getX(), event->getY(), (int)event->size());	
		k++;
	}
};

int local_add(int x, int y)
{
	return x+y;
}

void test_local()
{
	int result;
	
	for (int i=0; i<MAX_ITER; i++)
	{
		clock_t inicio = clock();
		result = local_add(i, i+1);
		clock_t fin = clock();
		
		printf("Result: %d + %d = %d   (%ld)\n", i, i+1, result, fin-inicio);
	}
}

void test_objetos()
{
	std::vector<TestProxy*> test_objs;
	
	clock_t inicio = clock();
	for (int i=0; i<10000; i++)
	{
		TestProxy* test = new TestProxy();
		test_objs.push_back(test);
	}
	clock_t fin = clock();
	
	printf("Time to create 10000 test proxies: %f\n", (float)(fin-inicio)/(float)CLOCKS_PER_SEC);
	fflush(stdout);
	
	for (int i=0; i<10000; i++)
	{
		delete test_objs[i];
	}
	
	test_objs.clear();
}

void test_publish_subscribe()
{
	clock_t ini, fin;
	srand(time(0));
	
	TestPairEventListener* evt_listener = new TestPairEventListener();
	BlueRose::EventHandler::addEventListener(evt_listener); //the handler manages the memory of the listener
	
	BlueRose::PairEvent evt;
	ini = clock();
	while(true)
	{
		evt.setX(rand());
		evt.setY(rand());
		
		BlueRose::EventHandler::publish(evt);
		fin = clock();
		
		if ( ((fin-ini)/CLOCKS_PER_SEC) >= 1) break;
	}

	BlueRose::EventHandler::removeEventListener(evt_listener);
}

#endif

