#include <cstdio>
#include <ctime>
#include "BlueRose.h"
#include "test_auxiliary.h"

using namespace Test;

int main(int argc, char** argv)
{
	//some auxiliary variables
	int result;
	clock_t inicio,fin,ini_general,contador_tiempo;
	
	ini_general = clock();
	contador_tiempo = 0;
		
	//initialize the middleware
	BlueRose::TcpCompatibleDevice cli;
	BlueRose::Initializer::initialize("config.xml");
	BlueRose::Initializer::initializeClient(&cli);
	
	//work with objects
	test_objetos();
	
	//work with an specific object whose servant is discovered dynamically
	TestProxy* dcalc = new TestProxy();
	
	test_local();
	
	printf("------------------------------------\n");
	
	//add i and i+1 from 0 to MAX_ITER
	for (int i=0; i<MAX_ITER; i++)
	{
		inicio = clock();
		result = dcalc->add(i, i+1);
		fin = clock();
		
		contador_tiempo += (fin-inicio);
		
		printf("Result: %d + %d = %d   (%ld)\n", i, i+1, result, fin-inicio);
	}
	
	//add i and i+1 from 0 to MAX_ITER. Receive the result asynchronously
	for (int i=0; i<MAX_ITER; i++)
	{
		inicio = clock();
		dcalc->add_async(i, i+1);
		fin = clock();
		
		contador_tiempo += (fin-inicio);
		
		printf("Async: %d + %d    (%ld)\n", i, i+1, fin-inicio);
	}
	
	//print a message at the server
	for (int i=0; i<MAX_ITER; i++)
	{
		inicio = clock();
		dcalc->print(L"hello world ñññáéíóù!!!");
		fin = clock();
		
		contador_tiempo += (fin-inicio);
	
		printf("Print %d: (%ld)\n", i, fin-inicio);
	}
	
	//check a proxy shared between several threads
	inicio = clock();
	std::vector<TestThread*> threads;
	for (int i=0; i<20; i++)
	{
		TestThread* aux = new TestThread(dcalc, i);
		threads.push_back(aux);
		aux->start();
	}
	
	for (int i=1; i<20; i++)
	{
		threads[i]->join();
	}
	
	for (int i=1; i<20; i++)
	{
		delete threads[i];
	}
	
	//event test
	for (int i=0; i<20; i++)
	{
		BlueRose::PairEvent pair_evt(i, i+1);
		dcalc->event_test(pair_evt);
	}
	
	//free object
	delete dcalc;
	
	//work with pub/sub paradigm
	test_publish_subscribe();
	
	fin = clock();
	
	//shutdown bluerose
	BlueRose::Initializer::destroy();
	
	contador_tiempo += (fin-inicio);
	
	printf("Execution Time: %f\n", (float)(contador_tiempo-ini_general)/(float)CLOCKS_PER_SEC);
	
	return 0;
}

