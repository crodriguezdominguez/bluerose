#include <vector>
#include <cstdlib>
#include <cstdio>
#include "BlueRose.h"

using namespace BlueRose;

int main(int argc, char** argv)
{
	Configuration config("config.xml");
	
	std::vector<std::string> res = config.getDevices();
	
	for (unsigned int i=0; i<res.size(); i++)
	{
		printf("%s\n", res[i].c_str());
		printf("-->%s\n", config.getModuleForDevice(res[i]).c_str());
	}
	
	res = config.getServicesForCategory("BlueRoseService");
	
	for (unsigned int i=0; i<res.size(); i++)
	{
		printf("%s\n", res[i].c_str());
		//printf("-->%s\n", config.getModuleForDevice(res[i]).c_str());
	}
	
	res = config.getDevicesForService("BlueRoseService", "Discovery");
	for (unsigned int i=0; i<res.size(); i++)
	{
		printf("%s\n", res[i].c_str());
		printf("-->%s\n", config.getPort("BlueRoseService", "Discovery", res[i]).c_str());
		printf("-->%s\n", config.getAddress("BlueRoseService", "Discovery", res[i]).c_str());
		printf("-->%s\n", config.getUserID("BlueRoseService", "Discovery", res[i]).c_str());
	}
	
	return 0;
}

