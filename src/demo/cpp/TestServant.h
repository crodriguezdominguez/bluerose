/**
* YOU SHOULD NOT MODIFY THIS FILE
*/

#ifndef _BLUEROSE_TEST_CALC_SERVANT_H_
#define _BLUEROSE_TEST_CALC_SERVANT_H_

#include "BlueRose.h"
#include "PairEvent.h"

namespace Test {

class TestServant : public BlueRose::ObjectServant {
public:
	TestServant() : ObjectServant()
	{
		identity.name = "TestServant";
		identity.category = "Test";
		
		BlueRose::DevicePool::registerObjectServant(this);
	}
	
	~TestServant()
	{
		BlueRose::DevicePool::unregisterObjectServant(this);
	}
	
	BlueRose::byte runMethod(const std::string& userID,
							const std::string& method, 
							const std::vector<BlueRose::byte>& args, 
							std::vector<BlueRose::byte>& result)
	{
		pthread_mutex_lock(&mutex_writer);
		pthread_mutex_lock(&mutex_reader);
		reader.open(args);
		writer.open(result);
		
		if (method == "add")
		{
			writer.writeInteger(add(reader.readInteger(), reader.readInteger()));

			pthread_mutex_unlock(&mutex_reader);
			pthread_mutex_unlock(&mutex_writer);
			return BR_SUCCESS_STATUS;
		}
		
		else if (method == "print")
		{
			std::wstring str;
			reader.readUTF8String(str);
			print(str);

			pthread_mutex_unlock(&mutex_reader);
			pthread_mutex_unlock(&mutex_writer);
			return BR_SUCCESS_STATUS;
		}
		
		else if (method == "event_test")
		{
			BlueRose::PairEvent evt;
			evt.unmarshall(reader);
			event_test(evt);

			pthread_mutex_unlock(&mutex_reader);
			pthread_mutex_unlock(&mutex_writer);
			return BR_SUCCESS_STATUS;
		}
		
		writer.close();
		reader.close();

		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		
		return BR_OPERATION_NOT_EXIST_STATUS;
	}
	
	int add(int x, int y);
	
	void print(const std::wstring& str);
	
	void event_test(BlueRose::PairEvent& evt);
};

};

#endif


