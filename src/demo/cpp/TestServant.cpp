#include <iostream>
#include "TestServant.h"

namespace Test {

int TestServant::add(int x, int y)
{
	return x+y;
}

void TestServant::print(const std::wstring& str)
{
	std::wcout << str << std::endl;
}

void TestServant::event_test(BlueRose::PairEvent& evt)
{
	printf("Event received: %d  ---> (%f, %f)\n", evt.getTopic(), evt.getX(), evt.getY());
}

};
