#include <vector>
#include <cstdio>
#include "BlueRose.h"
#include "TestServant.h"

int main()
{
	//initialize transmission devices
	BlueRose::TcpCompatibleDevice device;
	
	//initialize objects
	Test::TestServant* servant = new Test::TestServant();
	
	//initialize bluerose and the servant
	BlueRose::Initializer::initialize("config.xml");
	BlueRose::Initializer::initializeServant(servant, &device);
	
	//wait for connections to the device
	device.waitForConnections();
	
	//cleanups
	BlueRose::Initializer::destroy();
	delete servant;
	
	return 0;
}

