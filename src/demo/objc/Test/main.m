#import <UIKit/UIKit.h>
#import "BlueRose.h"
#import "TestAuxiliary.h"
#import "BRInternetMobileDiscovery.h"

int main(int argc, char *argv[]) {
    
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

	//some tests...
	
	//some auxiliary variables
	int result;
	clock_t inicio,fin,ini_general,contador_tiempo;
	
	ini_general = clock();
	contador_tiempo = 0;
	
	//initialize the middleware
	BRInternetDevice* cli = [[BRInternetDevice alloc] init];
	NSString* configFile = [[NSBundle mainBundle] pathForResource:@"bluerose_config" ofType:@"xml"];
	[BRInitializer initialize:configFile];
	[BRInitializer initializeClient:cli];
	
	//work with objects
	[TestAuxiliary testObjects];
	
	//work with an specific object whose servant is discovered dynamically
	TestProxy* dcalc =  [[TestProxy alloc] init];
	
	[TestAuxiliary testLocal];
	
	NSLog(@"------------------------------------");
	
	//add i and i+1 from 0 to MAX_ITER
	for (int i=0; i<MAX_ITER; i++)
	{
		inicio = clock();
		result = [dcalc addX:i andY:(i+1)];
		fin = clock();
		
		contador_tiempo += (fin-inicio);
		
		NSLog(@"Result: %d + %d = %d   (%ld)", i, i+1, result, fin-inicio);
	}
	
	//add i and i+1 from 0 to MAX_ITER. Receive the result asynchronously
	for (int i=0; i<MAX_ITER; i++)
	{
		inicio = clock();
		[dcalc addAsyncX:i andY:(i+1)];
		fin = clock();
		
		contador_tiempo += (fin-inicio);
		
		NSLog(@"Async: %d + %d    (%ld)", i, i+1, fin-inicio);
	}
	
	//print a message at the server
	for (int i=0; i<MAX_ITER; i++)
	{
		inicio = clock();
		[dcalc print:@"hello world ñññáéíóù!!!"];
		fin = clock();
		
		contador_tiempo += (fin-inicio);
		
		NSLog(@"Print %d: (%ld)", i, fin-inicio);
	}
	
	//check a proxy shared between several threads
	inicio = clock();
	NSMutableArray* threads = [[NSMutableArray alloc] init];
	for (int i=0; i<20; i++)
	{
		TestThread* aux = [[TestThread alloc] initWithProxy:dcalc andIndex:i];
		[threads addObject:aux];
		[aux start];
		
		[aux release];
	}

	for (int i=0; i<20; i++)
	{
		while(![[threads objectAtIndex:i] isFinished])
		{
			[NSThread sleepForTimeInterval:0.5];
		}
	}
	
	[threads release];
	
	//free object
	[dcalc release];
	
	//work with pub/sub paradigm
	[TestAuxiliary testPubSub];
	
	fin = clock();
	
	contador_tiempo += (fin-inicio);
	
	NSLog(@"Execution Time: %f", (float)(contador_tiempo-ini_general)/(float)CLOCKS_PER_SEC);
	
	//Launch app
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    
	//shutdown bluerose
	[BRInitializer destroy];
	[cli release];
	
	[pool release];
	
    return retVal;
}
