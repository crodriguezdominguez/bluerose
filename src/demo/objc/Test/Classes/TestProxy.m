#import "TestProxy.h"

@implementation AddCallback

-(void) callback:(NSData *)message
{
	BRByteStreamReader* reader = [[BRByteStreamReader alloc] init];
	[reader open:message];
	[self addCallback:[reader readInteger]];
	[reader close];
	
	[reader release];
}

-(void) addCallback:(NSInteger)result
{
	NSLog(@"Resultado suma async: %d", result);
}

@end

@implementation TestProxy

-(id) init
{
	if (self = [super init])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
		
		[self resolveInitialization:nil waitOnline:YES parameters:nil];
	}
	
	return self;
}

-(id) initWithDevice:(id<BRICommunicationDevice>)device
{
	if (self = [super init])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
		
		[self resolveInitialization:device waitOnline:YES parameters:nil];
	}
	
	return self;
}

-(id) initWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	if (self = [super init])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
		
		[self resolveInitialization:device waitOnline:wait parameters:nil];
	}
	
	return self;
}

-(id) initWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars
{
	if (self = [super init])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
		
		[self resolveInitialization:device waitOnline:wait parameters:pars];
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID
{
	if (self = [super initWithServant:servID])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID andDevice:(id<BRICommunicationDevice>)device
{
	if (self = [super initWithServant:servID andDevice:device])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait
{
	if (self = [super initWithServant:servID device:device waitOnline:wait])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
	}
	
	return self;
}

-(id) initWithServant:(NSString *)servID device:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary *)pars
{
	if (self = [super initWithServant:servID device:device waitOnline:wait parameters:pars])
	{
		identity.id_name = @"TestServant";
		identity.category = @"Test";
	}
	
	return self;
}

-(void) addAsyncX:(NSInteger)x andY:(NSInteger)y
{
	NSInteger reqID;
	BRByteStreamWriter* writer = [[BRByteStreamWriter alloc] init];
	currentMode = BR_TWOWAY_MODE;
	
	NSMutableData* enc = [[NSMutableData alloc] init];
	
	[writer open:enc];
	[writer writeInteger:x];
	[writer writeInteger:y];
	[writer close];
	
	reqID = [self sendRequestToServantID:servantID operation:@"add" request:enc];
	AddCallback* callback = [[AddCallback alloc] init];
	[self receiveCallback:reqID callback:callback];
	
	[writer release];
	[enc release];
	[callback release];
}

-(NSString*) typeID
{
	return @"Test::TestServant";
}

-(NSInteger) addX:(NSInteger)x andY:(NSInteger)y
{
	NSInteger reqID;
	BRByteStreamWriter* writer = [[BRByteStreamWriter alloc] init];
	BRByteStreamReader* reader = [[BRByteStreamReader alloc] init];
	currentMode = BR_TWOWAY_MODE;
	
	NSMutableData* enc = [[NSMutableData alloc] init];
	
	[writer open:enc];
	[writer writeInteger:x];
	[writer writeInteger:y];
	[writer close];
	
	reqID = [self sendRequestToServantID:servantID operation:@"add" request:enc];
	NSData* result_bytes = [self receiveReply:reqID];
	
	[reader open:result_bytes];
	NSInteger result = [reader readInteger];
	[reader close];
	
	[reader release];
	[writer release];
	[enc release];
	
	return result;
}

-(void) print:(NSString*)message
{
	NSInteger reqID;
	BRByteStreamWriter* writer = [[BRByteStreamWriter alloc] init];
	
	currentMode = BR_ONEWAY_MODE;
	
	NSMutableData* enc = [[NSMutableData alloc] init];
	
	[writer open:enc];
	[writer writeUTF8String:message];
	[writer close];
	
	reqID = [self sendRequestToServantID:servantID operation:@"print" request:enc];
	[self receiveReply:reqID];
	
	[enc release];
	[writer release];
}

@end
