#import <Foundation/Foundation.h>
#import "BlueRose.h"

@interface AddCallback : NSObject<BRAsyncMethodCallback> {
}

-(void) addCallback:(NSInteger)result;

@end

@interface TestProxy : BRObjectProxy {

}

-(id) initWithDevice:(id<BRICommunicationDevice>)device;
-(id) initWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait;
-(id) initWithDevice:(id<BRICommunicationDevice>)device waitOnline:(BOOL)wait parameters:(NSMutableDictionary*)pars;

-(void) addAsyncX:(NSInteger)x andY:(NSInteger)y;
-(NSInteger) addX:(NSInteger)x andY:(NSInteger)y;
-(void) print:(NSString*)message;

@end
