#import <Foundation/Foundation.h>
#import "BlueRose.h"
#import "TestProxy.h"
#import "BRPairEvent.h"

#define MAX_ITER 1000

@interface TestAuxiliary : NSObject {
}

/**
 * adds x and y (locally) and returns the result
 */
+(int) localAddX:(int)x y:(int)y;

/**
 * It runs MAX_ITER calls to local_add function
 */
+(void) testLocal;

/**
 * It creates and destroys 10000 proxy objects
 */
+(void) testObjects;

/**
 * It publishes events (pair events) for one second
 */
+(void) testPubSub;

@end


@interface TestThread : NSThread {
	TestProxy* proxy;
	NSInteger index;
}

-(id) initWithProxy:(TestProxy*)prox andIndex:(NSInteger)indx;

@end

@interface TestPairEventListener : BREventListener {
	int k;
}

@end
