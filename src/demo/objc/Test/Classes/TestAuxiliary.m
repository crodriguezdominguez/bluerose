#import "TestAuxiliary.h"

@implementation TestAuxiliary

+(int) localAddX:(int)x y:(int)y;
{
	return x+y;
}

+(void) testLocal;
{
	int result;
	
	for (int i=0; i<MAX_ITER; i++)
	{
		clock_t inicio = clock();
		result = [TestAuxiliary localAddX:i y:(i+1)];
		clock_t fin = clock();
		
		NSLog(@"Result: %d + %d = %d   (%ld)\n", i, i+1, result, fin-inicio);
	}
}

+(void) testObjects;
{
	NSMutableArray* test_objs = [[NSMutableArray alloc] init];
	
	clock_t inicio = clock();
	for (int i=0; i<10000; i++)
	{
		TestProxy* test = [[TestProxy alloc] init];
		[test_objs addObject:test];
		
		[test release];
	}
	clock_t fin = clock();
	
	NSLog(@"Time to create 10000 test proxies: %f\n", (float)(fin-inicio)/(float)CLOCKS_PER_SEC);
	
	[test_objs release];
}

+(void) testPubSub;
{
	clock_t ini, fin;
	srand(time(0));
	
	TestPairEventListener* evt_listener = [[TestPairEventListener alloc] init];
	[BREventHandler addEventListener:evt_listener];
	
	BRPairEvent* evt = [[BRPairEvent alloc] init];
	ini = clock();
	while(1)
	{
		[evt setX:rand()];
		[evt setY:rand()];
		
		[BREventHandler publish:evt];
		fin = clock();
		
		if ( ((fin-ini)/CLOCKS_PER_SEC) >= 1) break;
	}
	
	[BREventHandler removeEventListener:evt_listener];
	
	[evt_listener release];
	[evt release];
}

@end

@implementation TestPairEventListener

-(id) init
{
	if (self = [super initWithTopic:BR_PAIR_EVENT_TOPIC])
	{
		k = 0;
	}
	
	return self;
}

-(NSString*) listenerName
{
	return @"TestPairEventListener";
}

-(void) performAction:(BREvent *)event
{
	BRPairEvent* evt = [[BRPairEvent alloc] initWithEvent:event];
	
	NSLog(@"Pair Event %d Received: %f, %f   (%d members)\n", k, [evt x], [evt y], (int)[evt length]);	
	k++;
	
	[evt release];
}

@end

@implementation TestThread

-(id) initWithProxy:(TestProxy*)prox andIndex:(NSInteger)indx
{
	if (self = [super init])
	{
		proxy = [prox retain];
		index = indx;
	}
	
	return self;
}

-(void) dealloc
{
	[proxy release];
	
	[super dealloc];
}

-(void) main
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	for (int i=0; i<10; i++)
	{
		@try{
			int res = [proxy addX:i andY:i*i];
			NSLog(@"Index: %d,  %d + %d = %d", index, i, i*i, res);
		}
		@catch(NSException* ex)
		{
			NSLog(@"%@", [ex reason]);
			
			//esperar que exista la conexion
			[proxy waitOnlineMode];
		}
	}
	
	[pool release];
}

@end
