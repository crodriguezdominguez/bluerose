package org.ugr.bluerose.test;

import org.ugr.bluerose.Initializer;
import org.ugr.bluerose.devices.TcpCompatibleDevice;
import org.ugr.bluerose.events.Event;
import org.ugr.bluerose.events.Value;

public class Test {
	public static void main(String[] args)
	{   
		int MAX_ITER = 1000;
    	//some tests...
    	
    	//some auxiliary variables
    	long inicio,fin,contador_tiempo;
    	
    	//ini_general = System.currentTimeMillis();
    	contador_tiempo = 0;
    	
    	//initialize the middleware
    	TcpCompatibleDevice cli = new TcpCompatibleDevice();
    	
    	try {
    		Initializer.initialize(new java.io.File(Test.class.getResource("/bluerose_config.xml").getPath()));
			Initializer.initializeClient(cli);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
    	//work with objects
    	TestAuxiliary.testObjects();
    	
    	//work with an specific object whose servant is discovered dynamically
    	TestProxy dcalc = null;
		try {
			dcalc = new TestProxy();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	TestAuxiliary.testLocal();
    	
    	//add i and i+1 from 0 to MAX_ITER
    	for (int i=0; i<MAX_ITER; i++)
    	{
    		inicio = System.currentTimeMillis();
    		int result = dcalc.add(i, i+1);
    		fin = System.currentTimeMillis();
    		
    		contador_tiempo += (fin-inicio);
    		
    		System.out.println("Result: "+i+"+"+(i+1)+"="+result+"   "+((fin-inicio)/1000.0));
    	}
    	
    	//add i and i+1 from 0 to MAX_ITER. Receive the result asynchronously
    	for (int i=0; i<MAX_ITER; i++)
    	{
    		inicio = System.currentTimeMillis();
    		dcalc.addAsync(i, i+1);
    		fin = System.currentTimeMillis();
    		
    		contador_tiempo += (fin-inicio);
    		
    		System.out.println("Async: "+i+"+"+(i+1)+"   "+((fin-inicio)/1000.0));
    	}
    	
    	//print MAX_ITER messages at the server
    	for (int i=0; i<MAX_ITER; i++)
    	{
    		inicio = System.currentTimeMillis();
    		dcalc.print("hello world ��������!!!");
    		fin = System.currentTimeMillis();
    		
    		contador_tiempo += (fin-inicio);
    		
    		System.out.println("Print "+i+"   "+": "+((fin-inicio)/1000.0));
    	}
    	
    	//check a proxy shared between several threads
    	inicio = System.currentTimeMillis();
    	java.util.Vector<Thread> threads = new java.util.Vector<Thread>();
    	for (int i=0; i<20; i++)
    	{
    		TestThread aux = new TestThread(dcalc, i);
    		threads.add(aux);
    		aux.start();
    	}

    	for (int i=0; i<20; i++)
    	{
    		try {
				threads.get(i).join();
			} catch (InterruptedException e) {
			}
    	}
    	
    	threads.clear();
    	
    	//event test
		for (int i=0; i<20; i++)
		{
			Value x = new Value();
			x.setFloat(i);
			Value y = new Value();
			y.setFloat(i+1);
			
			Event evt = new Event();
			evt.setMember("x", x);
			evt.setMember("y", y);
			dcalc.eventTest(evt);
		}
    	
    	//work with pub/sub paradigm
    	TestAuxiliary.testPubSub();
    	
		Initializer.destroy();
		
		System.exit(0);
	}
}
