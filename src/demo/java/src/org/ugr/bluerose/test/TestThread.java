package org.ugr.bluerose.test;

public class TestThread extends Thread {
	
	protected TestProxy proxy;
	protected int index;
	
	public TestThread(TestProxy prxy, int idx)
	{
		super();
		proxy = prxy;
		index = idx;
	}
	
	@Override
	public void run()
	{
		for (int i=0; i<10; i++)
		{
			try{
				int res = proxy.add(i, i*i);
				System.out.println("Index: "+index+"  "+i+"+"+(i*i)+"="+res);
			}
			catch(Exception ex)
			{
				
			}
		}
	}
}
