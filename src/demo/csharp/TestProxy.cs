
using System;
using System.Collections.Generic;

using BlueRose;
using BlueRose.Events;
using BlueRose.Messages;

namespace BlueRoseTest
{


	class AddCallback : AsyncMethodCallback {
		public void OnAdd(int result)
		{
			Console.WriteLine("Resultado suma async: "+result);
		}
		
		public void Callback(List<byte> message)
		{
			ByteStreamReader reader = new ByteStreamReader(message);
			this.OnAdd(reader.ReadInteger());
		}
	}
	
	
	public class TestProxy : ObjectProxy {
	
		public TestProxy() : base()
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
				
			this.ResolveInitialization(null, true, null);
		}
		
		public TestProxy(ICommunicationDevice device) : base()
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
				
			this.ResolveInitialization(device, true, null);
		}
		
		public TestProxy(ICommunicationDevice device, bool wait) : base()
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
				
			this.ResolveInitialization(device, wait, null);
		}
		
		public TestProxy(ICommunicationDevice device, bool wait, Dictionary<string, List<byte>> pars) : base()
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
				
			this.ResolveInitialization(device, wait, pars);
		}
		
		public TestProxy(string servID) : base(servID)
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
		}
		
		public TestProxy(string servID, ICommunicationDevice device) : base(servID, device)
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
		}
		
		public TestProxy(string servID, ICommunicationDevice device, bool wait) : base(servID, device, wait)
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
		}
		
		public TestProxy(String servID, ICommunicationDevice device, bool wait, Dictionary<string, List<byte>> pars) : base(servID, device, wait, pars)
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
		}
	
		public void AddAsync(int x, int y)
		{
			currentMode = RequestOperationMode.TwoWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteInteger(x);
			writer.WriteInteger(y);
			
			int reqID = this.SendRequest(servantID, "add", writer.ToVector());
			AddCallback callback = new AddCallback();
			this.ReceiveCallback(reqID, callback);
		}
	
		public int Add(int  x, int y)
		{
			currentMode = RequestOperationMode.TwoWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteInteger(x);
			writer.WriteInteger(y);
			
			int reqID = this.SendRequest(servantID, "add", writer.ToVector());
			List<byte> result_bytes = this.ReceiveReply(reqID);
			
			ByteStreamReader reader = new ByteStreamReader(result_bytes);
			return reader.ReadInteger();
		}
		
		public void EventTest(Event evt)
		{
			currentMode = RequestOperationMode.OneWay;
		
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteObject(evt);
			
			int reqID = this.SendRequest(servantID, "event_test", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		public void Print(string message)
		{
			currentMode = RequestOperationMode.OneWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteUTF8String(message);
			
			int reqID = this.SendRequest(servantID, "print", writer.ToVector());
			this.ReceiveReply(reqID);
		}
		
		public override String GetTypeID() 
		{
			return "Test::TestServant";
		}
	}

}
