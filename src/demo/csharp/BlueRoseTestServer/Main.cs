using System;
using System.IO;

using BlueRose;
using BlueRose.Devices;

namespace BlueRoseTestServer
{
	class MainClass
	{		
		public static void Main(string[] args)
		{
			//initialize transmission devices
			TcpCompatibleDevice device = new TcpCompatibleDevice();
			
			//initialize objects
			TestServer servant = new TestServer();

			//initialize bluerose and the servant	
			try {
	    			Initializer.Initialize(new StreamReader("config.xml").BaseStream);
	    			Initializer.InitializeServant(servant, device);
			} catch (Exception e) {
				Console.WriteLine(e.ToString());
			}
			
			//wait for connections to the device
			device.WaitForConnections();
			
			//cleanups
			Initializer.Destroy();
		}
	}
}
