
using System;
using System.Collections.Generic;
using BlueRose;
using BlueRose.Events;
using BlueRose.Messages;

namespace BlueRoseTestServer
{


	public class TestServer : ObjectServant
	{

		int Add(int x, int y)
		{
			return x+y;
		}
	
		void Print(String str)
		{
			Console.WriteLine(str);
		}
	
		void EventTest(Event evt)
		{
			Console.WriteLine("Event received: "+evt.Topic+"  ---> ("+evt.GetMemberValue("x").ToString() +", "+evt.GetMemberValue("y").ToString()+")");
		}
	
		/**
		 * DO NOT MODIFY THIS
		 */
		
		public TestServer()
		{
			identity.Name = "TestServant";
			identity.Category = "Test";
		}
		
		public override MethodResult RunMethod(string method, string userID, List<byte> args)
		{
			ByteStreamReader reader = new ByteStreamReader(args);
			ByteStreamWriter writer = new ByteStreamWriter();
			
			MethodResult result = new MethodResult();
			
			if (method.Equals("add"))
			{
				writer.WriteInteger(Add(reader.ReadInteger(), reader.ReadInteger()));
				result.Status = ReplyStatus.Success;
				result.Result = writer.ToVector();
					
				return result;
			}
			
			else if (method.Equals("print"))
			{
				String res = reader.ReadUTF8String();
				Print(res);
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method.Equals("event_test"))
			{
				Event evt = new Event();
				evt.Unmarshall(reader);
				EventTest(evt);
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			result.Status = ReplyStatus.OperationNotExist;
			
			return result;
		}
	}
}
