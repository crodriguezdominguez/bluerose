using System;
using System.IO;
using System.Collections.Generic;

using BlueRose;
using BlueRose.Devices;
using BlueRose.Events;

namespace BlueRoseTest
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int MAX_ITER = 1000;
			
			//some tests...
    	
		    	//some auxiliary variables
		    	long inicio,fin,contador_tiempo;
		    	
		    	//ini_general = DateTime.Now.Millisecond;
		    	contador_tiempo = 0;
		    	
		    	//initialize the middleware
		    	TcpCompatibleDevice cli = new TcpCompatibleDevice();
		    	
		    	try {                                  
		    		Initializer.Initialize(new StreamReader("config.xml").BaseStream);
				Initializer.InitializeClient(cli);
			} catch (Exception e) {
				Console.WriteLine(e.StackTrace);
			}
			
		    	//work with objects
		    	TestAuxiliary.TestObjects();
		    	
		    	//work with an specific object whose servant is discovered dynamically
		    	TestProxy dcalc = null;
				try {
					dcalc = new TestProxy();
				} catch (Exception e) {
					Console.WriteLine(e.StackTrace);
				}
		    	
		    	TestAuxiliary.TestLocal();
		    	
		    	//add i and i+1 from 0 to MAX_ITER
		    	for (int i=0; i<MAX_ITER; i++)
		    	{
		    		inicio = DateTime.Now.Millisecond;
		    		int result = dcalc.Add(i, i+1);
		    		fin = DateTime.Now.Millisecond;
		    		
		    		contador_tiempo += (fin-inicio);
		    		
		    		Console.WriteLine("Result: "+i+"+"+(i+1)+"="+result+"   "+((fin-inicio)/1000.0));
		    	}
		    	
		    	//add i and i+1 from 0 to MAX_ITER. Receive the result asynchronously
		    	for (int i=0; i<MAX_ITER; i++)
		    	{
		    		inicio = DateTime.Now.Millisecond;
		    		dcalc.AddAsync(i, i+1);
		    		fin = DateTime.Now.Millisecond;
		    		
		    		contador_tiempo += (fin-inicio);
		    		
		    		Console.WriteLine("Async: "+i+"+"+(i+1)+"   "+((fin-inicio)/1000.0));
		    	}
		    	
		    	//print MAX_ITER messages at the server
		    	for (int i=0; i<MAX_ITER; i++)
		    	{
		    		inicio = DateTime.Now.Millisecond;
		    		dcalc.Print("hello world ñññáéíóù!!!");
		    		fin = DateTime.Now.Millisecond;
		    		
		    		contador_tiempo += (fin-inicio);
		    		
		    		Console.WriteLine("Print "+i+"   "+": "+((fin-inicio)/1000.0));
		    	}
		    	
		    	//check a proxy shared between several threads
		    	inicio = DateTime.Now.Millisecond;
		    	List<TestThread> threads = new List<TestThread>();
		    	for (int i=0; i<20; i++)
		    	{
		    		TestThread aux = new TestThread(dcalc, i);
		    		threads.Add(aux);
		    		aux.Start();
		    	}
		
		    	for (int i=0; i<20; i++)
		    	{
		    		try {
					threads[i].Join();
				} 
				catch (Exception) {
				}
		    	}
		    	
		    	threads.Clear();
			
			//event test
			for (int i=0; i<20; i++)
			{
				Value x = new Value();
				x.SetFloat(i);
				Value y = new Value();
				y.SetFloat(i+1);
				
				Event evt = new Event();
				evt.SetMember("x", x);
				evt.SetMember("y", y);
				dcalc.EventTest(evt);
			}
		    	
		    	//work with pub/sub paradigm
		    	TestAuxiliary.TestPubSub();
		    	
			Initializer.Destroy();
			
			System.Environment.Exit(0);
		}
	}
}
