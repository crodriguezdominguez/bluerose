
using System;
using System.Threading;

namespace BlueRoseTest
{


	public class TestThread
	{
		protected TestProxy proxy;
		protected int index;
		protected Thread thread;
		
		public TestThread(TestProxy prxy, int idx)
		{
			proxy = prxy;
			index = idx;
		}
		
		public void Join()
		{
			thread.Join();
		}
		
		public void Start()
		{
			thread = new Thread(new ThreadStart(Run));
			thread.Start();
		}
		
		protected void Run()
		{
			for (int i=0; i<10; i++)
			{
				try{
					int res = proxy.Add(i, i*i);
					Console.WriteLine("Index: "+index+"  "+i+"+"+(i*i)+"="+res);
				}
				catch(Exception)
				{
					
				}
			}
		}
	}
}
