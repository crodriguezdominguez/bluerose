using System;
using System.Collections.Generic;

using BlueRose;
using BlueRose.Events;

namespace BlueRoseTest {

	class TestEventListener : EventListener {
	
		protected int k=0;
		public TestEventListener() : base(1)
		{
			
		}
		
		public override void PerformAction(Event evt) {
			Value x = evt.GetMemberValue("x");
			Value y = evt.GetMemberValue("y");
			Console.WriteLine("Recibido "+k+": "+x.GetFloat()+", "+y.GetFloat());
			
			k++;
		}
		
	}
	
	public class TestAuxiliary {
		public static void TestObjects()
		{
			List<ObjectProxy> test_objs = new List<ObjectProxy>();
			
			long inicio = DateTime.Now.Millisecond;
			for (int i=0; i<10; i++)
			{
				TestProxy test = null;
				try {
					test = new TestProxy();
				} catch (Exception) {
				}
				if (test != null) test_objs.Add(test);
			}
			long fin = DateTime.Now.Millisecond;
			
			Console.WriteLine("Time to create 10 test proxies: "+(float)((fin-inicio)/1000.0));
		}
		
		public static void TestLocal()
		{
			int result = 0;
			for (int i=0; i<100; i++)
			{
				result = i+i+1;
				Console.WriteLine("Local  "+i+"+"+(i+1)+"="+result);
			}
		}
		
		public static void TestPubSub()
		{	
			EventListener evt_listener = new TestEventListener();
			BlueRose.Events.EventHandler.AddEventListener(evt_listener);
			
			Event evt = new Event();
			evt.Topic = 1;
			
			Random rand = new Random(DateTime.Now.Millisecond);
			DateTime ini = DateTime.Now;
			DateTime dt = ini.AddSeconds(1);
			while(true)
			{
				Value x = new Value();
				x.SetFloat((float)rand.NextDouble());
				Value y = new Value();
				y.SetFloat((float)rand.NextDouble());
				evt.SetMember("x", x);
				evt.SetMember("y", y);
				
				BlueRose.Events.EventHandler.Publish(evt);
				DateTime fin = DateTime.Now;
				
				if ( fin.Equals(dt) || fin.CompareTo(dt) == 1 ) break;
			}
			
			BlueRose.Events.EventHandler.RemoveEventListener(evt_listener);
		}
	}
}