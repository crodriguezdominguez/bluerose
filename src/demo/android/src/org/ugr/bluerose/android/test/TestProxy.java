package org.ugr.bluerose.android.test;

import org.ugr.bluerose.AsyncMethodCallback;
import org.ugr.bluerose.ByteStreamReader;
import org.ugr.bluerose.ICommunicationDevice;
import org.ugr.bluerose.ObjectProxy;
import org.ugr.bluerose.messages.MessageHeader;

class AddCallback implements AsyncMethodCallback {
	public void addCallback(int result)
	{
		//Log.e("testAsync", "Resultado suma async: "+result);
	}
	
	public void callback(java.util.Vector<Byte> message)
	{
		ByteStreamReader reader = new ByteStreamReader(message);
		this.addCallback(reader.readInteger());
	}
}


public class TestProxy extends ObjectProxy {

	public TestProxy() throws Exception
	{
		super();
		identity.id_name = "TestServant";
		identity.category = "Test";
			
		this.resolveInitialization(null, true, null);
	}
	
	public TestProxy(boolean wait) throws Exception
	{
		super();
		
		identity.id_name = "TestServant";
		identity.category = "Test";
			
		this.resolveInitialization(null, wait, null);
	}
	
	public TestProxy(ICommunicationDevice device) throws Exception
	{
		super();
		identity.id_name = "TestServant";
		identity.category = "Test";
			
		this.resolveInitialization(device, true, null);
	}
	
	public TestProxy(ICommunicationDevice device, boolean wait) throws Exception
	{
		super();
		identity.id_name = "TestServant";
		identity.category = "Test";
			
		this.resolveInitialization(device, wait, null);
	}
	
	public TestProxy(ICommunicationDevice device, boolean wait, java.util.Dictionary<String, java.util.Vector<Byte>> pars) throws Exception
	{
		super();
		identity.id_name = "TestServant";
		identity.category = "Test";
			
		this.resolveInitialization(device, wait, pars);
	}
	
	public TestProxy(String servID) throws Exception
	{
		super(servID);
		identity.id_name = "TestServant";
		identity.category = "Test";
	}
	
	public TestProxy(String servID, ICommunicationDevice device) throws Exception
	{
		super(servID, device);
		identity.id_name = "TestServant";
		identity.category = "Test";
	}
	
	public TestProxy(String servID, ICommunicationDevice device, boolean wait) throws Exception
	{
		super(servID, device, wait);
		identity.id_name = "TestServant";
		identity.category = "Test";
	}
	
	public TestProxy(String servID, ICommunicationDevice device, boolean wait, java.util.Dictionary<String, java.util.Vector<Byte>> pars)
		throws Exception
	{
		super(servID, device, wait, pars);
		identity.id_name = "TestServant";
		identity.category = "Test";
	}

	public void addAsync(int x, int y)
	{
		int reqID;
		
		currentMode = MessageHeader.TWOWAY_MODE;
		
		synchronized(mutex) {
			writer.writeInteger(x);
			writer.writeInteger(y);
		
			reqID = this.sendRequest(servantID, "add", writer.toVector());
			
			writer.reset();
		}
		
		AddCallback callback = new AddCallback();
		this.receiveCallback(reqID, callback);
	}

	public int add(int  x, int y)
	{
		currentMode = MessageHeader.TWOWAY_MODE;
		
		java.util.Vector<Byte> result_bytes = null;
		
		synchronized(mutex) {
			writer.writeInteger(x);
			writer.writeInteger(y);
		
			int reqID = this.sendRequest(servantID, "add", writer.toVector());
			result_bytes = this.receiveReply(reqID);
			
			writer.reset();
		}
		
		ByteStreamReader reader = new ByteStreamReader(result_bytes);
		return reader.readInteger();
	}

	public void print(String message)
	{
		currentMode = MessageHeader.ONEWAY_MODE;
		
		synchronized(mutex) {
			writer.writeUTF8String(message);
		
			int reqID = this.sendRequest(servantID, "print", writer.toVector());
			this.receiveReply(reqID);
			
			writer.reset();
		}
	}
	
	@Override
	public String getTypeID() 
	{
		return "Test::TestServant";
	}
}
