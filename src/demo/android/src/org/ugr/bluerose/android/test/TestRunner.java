package org.ugr.bluerose.android.test;

import org.ugr.bluerose.Initializer;
import org.ugr.bluerose.devices.TcpCompatibleDevice;

import android.content.Context;
import android.content.SharedPreferences;

public class TestRunner
{
    public interface TestRunnerListener
    {
        public void onResult(int test, String resultText);
        public void onError(int test, String resultText);
        public void onTestFinished(long millis);
        public void onTestStopped();
    }
    
    private static final String LOGTAG = "TestRunner";
    private static final String PREFERENCES_TAG = "BlueRoseDemoPreferences";
    private static final String PREF_TAG_IP = "ip";
    private static final String PREF_DEFAULT_IP = "192.168.2.105";
    private static final String PREF_TAG_PORT = "port";
    private static final String PREF_DEFAULT_PORT = "10003";
    private static final int NUMBER_OF_TESTS = 6;
    
    private final Context mContext;
    private String mAddress, mPort;
    private boolean mIsRunning = false;

    private SharedPreferences mPrefs;
    
    public TestRunner(Context c)
    {
        mContext = c;
        mPrefs = c.getSharedPreferences(PREFERENCES_TAG, Context.MODE_PRIVATE);
        
        if( mPrefs.contains(PREF_TAG_IP) ){
            mAddress = mPrefs.getString(PREF_TAG_IP, PREF_DEFAULT_IP);
            mPort = mPrefs.getString(PREF_TAG_PORT, PREF_DEFAULT_PORT);
        } else { // primera ejecución, crear fichero XML
            mAddress = PREF_DEFAULT_IP;
            mPort = PREF_DEFAULT_PORT;
        }
    }
    
    public void setIpAndPort(String ip, String port)
    {
        mAddress = ip;
        mPort = port;

        /* Guardar preferencias del programa */
        SharedPreferences.Editor e = mPrefs.edit();
        e.putString(PREF_TAG_IP, ip);
        e.putString(PREF_TAG_PORT, port);

        e.commit();
    }
    
    public String getAddress()
    {
        return mAddress;
    }
    
    public String getPort()
    {
        return mPort;
    }
    
    public int getNumberOfTests()
    {
        return NUMBER_OF_TESTS;
    }
    
    public void runTests(TestRunnerListener listener)
    {
        mIsRunning = true;
        
        //some auxiliary variables
        long inicio,fin,contador_tiempo, ini_general;
        
        ini_general = System.currentTimeMillis();
        contador_tiempo = 0;
        
        //initialize the middleware
        
        /**
         * TEST 0: connection
         * */
        try {
            Initializer.initialize( mContext.getResources().openRawResource(R.raw.bluerose_config) );
            org.ugr.bluerose.Configuration cfg = org.ugr.bluerose.Initializer.configuration;
            cfg.setAddressForService("BlueRoseService", "Discovery", "TcpCompatibleDevice", mAddress);
            cfg.setPortForService("BlueRoseService", "Discovery", "TcpCompatibleDevice", mPort);
            
            TcpCompatibleDevice device = new TcpCompatibleDevice();
            org.ugr.bluerose.Initializer.initializeNonWaitingClient(device);
        } catch (Exception e) {
            StackTraceElement [] el = e.getStackTrace();
            String es="";
            for(StackTraceElement i : el)
                es += i.toString()+"\n";
            listener.onError(0, es );
            return;
        }
        
        /**
         * TEST 1: work with objects
         * */
        TestProxy dcalc = null;
        if(mIsRunning){
            try {
                TestAuxiliary.testObjects();
                //work with an specific object whose servant is discovered dynamically
                dcalc = new TestProxy();
            } catch(Exception e) {
                listener.onError(1, e.getStackTrace()[0].toString() );
                return;
            }
        } else {
            Initializer.destroy();
            listener.onTestStopped();
            return;
        }
        
        listener.onResult(1, "Object test OK" );
        
        /**
         * TEST 2: 100 synchronous calculations
         * */
        String s = "";
        if(mIsRunning){
            //add i and i+1 from 0 to MAX_ITER
            for (int i=0; i<100; i++)
            {
                inicio = System.currentTimeMillis();
                int result = dcalc.add(i, i+1);
                fin = System.currentTimeMillis();
                
                contador_tiempo += (fin-inicio);
                
                s+= "Result:\t"+i+"+"+(i+1)+"="+result+"\t"+((fin-inicio)/1000.0)+"\n";
            }
            listener.onResult(2, s );
        } else {
            Initializer.destroy();
            listener.onTestStopped();
            return;
        }
        
        
        /**
         * TEST 3: 100 asynchronous calculations
         * */
        if(mIsRunning){
            s = "";
            for (int i=0; i<10; i++)
            {
                inicio = System.currentTimeMillis();
                dcalc.addAsync(i, i+1);
                fin = System.currentTimeMillis();
                
                contador_tiempo += (fin-inicio);
                
                s += "Async: "+i+"+"+(i+1)+"   "+((fin-inicio)/1000.0)+"\n";
            }
    
            listener.onResult(3, s );
        } else {
            Initializer.destroy();
            listener.onTestStopped();
            return;
        }
        
        /**
         * TEST 4: print message on the server
         * */
        if(mIsRunning){
            s = "";
            //print a message at the server
            for (int i=0; i<100; i++)
            {
                inicio = System.currentTimeMillis();
                dcalc.print("hello world!!!");
                fin = System.currentTimeMillis();
                
                contador_tiempo += (fin-inicio);
                
                s += "Print "+i+"   "+": "+((fin-inicio)/1000.0)+"\n";
            }
            
            listener.onResult(4, s );
        } else {
            Initializer.destroy();
            listener.onTestStopped();
            return;
        }
        
        /**
         * TEST 5: check a proxy shared between several threads
         * */
        if(mIsRunning){
            inicio = System.currentTimeMillis();
            java.util.Vector<Thread> threads = new java.util.Vector<Thread>();
            for (int i=0; i<20; i++)
            {
                org.ugr.bluerose.android.test.TestThread aux = new org.ugr.bluerose.android.test.TestThread(dcalc, i);
                threads.add(aux);
                aux.start();
            }
    
            for (int i=0; i<20; i++)
            {
                try {
                    threads.get(i).join();
                } catch (InterruptedException e) {
                    listener.onError(5, e.getStackTrace()[0].toString() );
                    return;
                }
            }
            
            threads.clear();
            
            listener.onResult(5, "Shared proxy test OK" );
        } else {
            Initializer.destroy();
            listener.onTestStopped();
            return;
        }
        
        /**
         * TEST 6: work with pub/sub paradigm
         * */
        if(mIsRunning){
            TestAuxiliary.testPubSub();
            listener.onResult(5, "Pub/sub test OK" );
            listener.onTestFinished(System.currentTimeMillis()-ini_general);

        } else {
            listener.onTestStopped();
            return;
        }

        Initializer.destroy();
        mIsRunning = false;
    }
    
    public synchronized void stop()
    {
        mIsRunning = false;;
    }
}
