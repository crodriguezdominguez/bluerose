package org.ugr.bluerose.android.test;

import org.ugr.bluerose.android.test.R;
import org.ugr.bluerose.android.test.TestRunner.TestRunnerListener;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class TestActivity extends Activity implements TestRunnerListener
{
    private static final String TAG = "TestActivity";
    private static final String ERROR_PREFIX = "\n\n----¡¡¡ERROR!!!----\n";
    private static final String ERROR_SUFFIX = "\n------------------------\n\n";
    private static final String SEPARATOR =  "\n\n------------------------\n\n";

    private static final int MODE_STOPPED  = 0;
    private static final int MODE_EDITING  = 1;
    private static final int MODE_RUNNING  = 2;
    private static final int MODE_STOPPING = 3;
    
    private class ResultUpdater implements Runnable
    {
        public static final int MODE_RESULT = 0;
        public static final int MODE_ERROR = 1;
        public static final int MODE_FINISH = 2;
        
        protected int progress;
        protected String msg;
        protected int type;
        
        public ResultUpdater(){
            msg = "";
        }
        
        public ResultUpdater(int p, String s, int t){
            progress = p;
            msg = s;
            type = t;
        }
        
        public void run(){
            if(type==MODE_RESULT){
                TestActivity.this.mProgressBar.setProgress(progress);
                TestActivity.this.mOutputText.append(msg);
            } else if(type==MODE_ERROR) {
                TestActivity.this.mProgressBar.setProgress(progress);
                TestActivity.this.mOutputText.append(msg);
                TestActivity.this.setMode(MODE_STOPPED);
            } else if(type==MODE_FINISH) {
                TestActivity.this.mProgressBar.setProgress(mProgressBar.getMax());
                Toast.makeText(TestActivity.this, msg, Toast.LENGTH_LONG).show();
                TestActivity.this.setMode(MODE_STOPPED);
            }
        }
    }
    private final Handler mViewUpdaterHandler = new Handler();

    private Button mStartButton;
    private ProgressBar mProgressBar;
    private Button mEditButton;
    
    private LinearLayout mEditBar;
    private EditText mAddressEdit;
    private EditText mPortEdit;
    private Button mSaveButton;

    private TextView mOutputText;
    
    private int mMode;
    private TestRunner mTestRunner;
    private final ResultUpdater ru = new ResultUpdater();
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mStartButton = (Button) findViewById(R.id.startButton);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mEditButton = (Button) findViewById(R.id.editButton);
        
        mEditBar = (LinearLayout) findViewById(R.id.editBar);
        mAddressEdit = (EditText) findViewById(R.id.ipEdit);
        mPortEdit = (EditText) findViewById(R.id.portEdit);
        mSaveButton = (Button) findViewById(R.id.saveButton);
        
        mOutputText = (TextView) findViewById(R.id.text);
        
        
        mStartButton.setOnClickListener(startListener);
        mEditButton.setOnClickListener(editListener);
        mSaveButton.setOnClickListener(saveListener);
        
        mTestRunner = new TestRunner(this);
        mProgressBar.setMax(mTestRunner.getNumberOfTests());
        mAddressEdit.setText(mTestRunner.getAddress());
        mPortEdit.setText(mTestRunner.getPort());
        
        setMode(MODE_STOPPED);
    }
    
    public void onResult(int test, String resultText)
    {
        ru.progress = test;
        ru.msg = resultText+SEPARATOR;
        ru.type = ResultUpdater.MODE_RESULT;
        
        mViewUpdaterHandler.post( ru );
    }
    
    public void onError(int test, String resultText)
    {
        ru.progress = test;
        ru.msg = ERROR_PREFIX + resultText + ERROR_SUFFIX;
        ru.type = ResultUpdater.MODE_ERROR;
        
        mViewUpdaterHandler.post( ru );
    }
    
    
    public void onTestFinished(long millis)
    {
        ru.msg ="\nTotal time: "+millis+ " ms\n";
        ru.type = ResultUpdater.MODE_RESULT;

        mViewUpdaterHandler.post( ru );
        mViewUpdaterHandler.post( new ResultUpdater(0, "Test finished: everything looks OK!!!", ResultUpdater.MODE_FINISH) );
    }
    
    public void onTestStopped()
    {
        ru.msg = SEPARATOR + "Test stopped" + SEPARATOR;
        ru.type = ResultUpdater.MODE_ERROR;
        mViewUpdaterHandler.post( ru );
    }
    
    public void setMode(int mode)
    {
        mMode = mode;
        mEditButton.setEnabled(mode!=MODE_RUNNING && mode!=MODE_STOPPING);
        mEditBar.setVisibility( mode==MODE_EDITING ? View.VISIBLE : View.GONE );
        
        int id = (mode==MODE_RUNNING || mode==MODE_STOPPING) ? R.drawable.action_cancel : R.drawable.action_start;
        String str;
        if(mode==MODE_RUNNING)
            str = "Stop";
        else if(mode==MODE_STOPPING)
            str = "Stopping...";
        else
            str = "Start";
        Drawable left = getResources().getDrawable(id);
        left.setBounds(0, 0, 32, 32);
        mStartButton.setCompoundDrawables(left,
                                          null, null, null);
        mStartButton.setText(str);
        if(mode !=MODE_STOPPED)
            mProgressBar.setProgress(0);
        mStartButton.setEnabled(mode!=MODE_STOPPING);
    }
        
    private android.view.View.OnClickListener startListener = 
        new android.view.View.OnClickListener() {
            public void onClick(View v)
            {
                if(mMode==MODE_EDITING){
                    mTestRunner.setIpAndPort(mAddressEdit.getText().toString(), mPortEdit.getText().toString());
                    setMode(MODE_STOPPED);
                }
                
                if(mMode==MODE_STOPPED){
                    setMode(MODE_RUNNING);
                    mOutputText.setText("");
                    Thread t =
                        new Thread()
                        {
                            @Override
    						public void run()
                            {
                                mTestRunner.runTests(TestActivity.this);
                            }
                        };
                    t.start();
                    Toast.makeText(TestActivity.this, "Test started.", Toast.LENGTH_SHORT).show();
                } else {
                    mTestRunner.stop();
                    setMode(MODE_STOPPING);
                }
            }
        
        };
        
    private android.view.View.OnClickListener editListener = 
        new android.view.View.OnClickListener() {
            public void onClick(View v)
            {
                if(mMode == MODE_EDITING)
                    setMode(MODE_STOPPED);
                else
                    setMode(MODE_EDITING);
            }
        
        };

    private android.view.View.OnClickListener saveListener = 
        new android.view.View.OnClickListener()
        {
            public void onClick(View v)
            {
                mTestRunner.setIpAndPort(mAddressEdit.getText().toString(), mPortEdit.getText().toString());
                setMode(MODE_STOPPED);
            }
            
        };
}