#!/usr/bin/env python
# encoding: utf-8
"""
pythonWeb.py

Created by Carlos Rodriguez Dominguez on 2011-06-27.
Copyright (c) 2011 GEDES GROUP. All rights reserved.
"""

import sys
import os
import types
import string,cgi,time
import json
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

import bluerose
from bluerose.devices.TcpCompatibleDevice import *
from bluerose.Initializer import *
from bluerose.events.Event import *
from bluerose.ObjectProxy import *

class UniProxy(ObjectProxy):
	
	def __init__(self, idName, idCategory, servantID=None, device=None, waitOnline=True, parameters=dict()):
		ObjectProxy.__init__(self, servantID, device, waitOnline, parameters)
		self.identity.idName = idName
		self.identity.category = idCategory
		
		if servantID==None:
			self.resolveInitialization(device, waitOnline, parameters)
			
	def getTypeID(self):
		return str(self.identity.category)+"::"+str(self.identity.idName)
	
	def requestOperation(self, operation, args, resultType):
		self.currentMode = MessageHeader.TWOWAY_MODE
		
		ObjectProxy.staticMutex.acquire()
		
		for arg in args:
			if type(arg) == types.IntType:
				ObjectProxy.writer.writeInteger(arg)
			elif type(arg) == types.BooleanType:
				ObjectProxy.writer.writeBoolean(arg)
			elif type(arg) == types.LongType:
				ObjectProxy.writer.writeLong(arg)
			elif type(arg) == types.FloatType:
				ObjectProxy.writer.writeFloat(arg)
			elif type(arg) == types.StringType:
				ObjectProxy.writer.writeString(arg)
			elif type(arg) == types.UnicodeType:
				ObjectProxy.writer.writeUTF8String(arg)
			elif type(arg) == types.DictType:
				ObjectProxy.writer.writeDictionary(arg)
			elif type(arg) == types.ListType:
				if type(arg[0]) == types.IntType:
					ObjectProxy.writer.writeIntegerSeq(arg)
				elif type(arg[0]) == types.BooleanType:
					ObjectProxy.writer.writeBooleanSeq(arg)
				elif type(arg[0]) == types.LongType:
					ObjectProxy.writer.writeLongSeq(arg)
				elif type(arg[0]) == types.FloatType:
					ObjectProxy.writer.writeFloatSeq(arg)
				elif type(arg[0]) == types.StringType:
					ObjectProxy.writer.writeStringSeq(arg)
				elif type(arg[0]) == types.UnicodeType:
					ObjectProxy.writer.writeUTF8StringSeq(arg)
				else:
					raise Exception()
			else:
				raise Exception()
				
		data = ObjectProxy.writer.toList()
		ObjectProxy.writer.reset()
		ObjectProxy.staticMutex.release()
		
		reqID = self.sendRequest(self.servantID, operation, data)
		result_bytes = self.receiveReply(reqID)
		
		reader = ByteStreamReader(result_bytes)
		if resultType == "integer":
			return reader.readInteger()
		elif resultType == "float":
			return reader.readFloat()
		elif resultType == "boolean":
			return reader.readBoolean()
		elif resultType == "long":
			return reader.readLong()
		elif resultType == "string":
			return reader.readString()
		elif resultType == "utf8string":
			return reader.readUTF8String()
		elif resultType == "dictionary":
			return reader.readDictionary()
		elif resultType == "integerlist":
			return reader.readIntegerSeq()
		elif resultType == "floatlist":
			return reader.readFloatSeq()
		elif resultType == "booleanlist":
			return reader.readBooleanSeq()
		elif resultType == "longlist":
			return reader.readLongSeq()
		elif resultType == "stringlist":
			return reader.readStringSeq()
		elif resultType == "utf8stringlist":
			return reader.readUTF8StringSeq()
		else: return reader.readToEnd()
			
def main():
	cli = TcpCompatibleDevice()
	Initializer.initialize("config.xml")
	Initializer.initializeClient(cli)
	
	input_data = cgi.FieldStorage()
	print "Content-type: text/plain"
	
	try:
		params = json.loads(input_data)
		idName = params["identity"]
		namespace = params["namespace"]
		operation = params["operation"]
		args = params["args"]
		resultType = params["resultType"]
		uniProxy = UniProxy(idName, namespace)
		print str(uniProxy.requestOperation(operation, args, resultType))
	except:
		print "Error: Failed to Decode Request"
	
	Initializer.destroy()
	return 0

if __name__ == '__main__':
	main()

