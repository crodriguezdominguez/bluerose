#include "ObjectServant.h"
#include "DiscoveryProxy.h"
#include "DevicePool.h"

namespace BlueRose {

ByteStreamWriter ObjectServant::writer;
ByteStreamReader ObjectServant::reader;
pthread_mutex_t ObjectServant::mutex_writer = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ObjectServant::mutex_reader = PTHREAD_MUTEX_INITIALIZER;

ObjectServant::ObjectServant()
{
	
}

ObjectServant::~ObjectServant()
{
	
}

bool ObjectServant::checkIdentity(const Identity& id)
{
	if ( (id.name == identity.name) && 
		 (id.category == identity.category) )
	{
		return true;
	}
	
	else return false;
}

Identity& ObjectServant::getIdentity()
{
	return identity;
}

std::string ObjectServant::getIdentifier()
{
	return (identity.category+"::")+identity.name;
}

};

