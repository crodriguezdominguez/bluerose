#include "OpenConnectionThread.h"

namespace BlueRose {


OpenConnectionThread::OpenConnectionThread(ICommunicationDevice* iface, const std::string& _userID, int mRetries, const DictionaryDataType& pars)
{
	interface = iface;
	userID = _userID;
	parameters = pars;
	maxRetries = mRetries;
}

void OpenConnectionThread::run()
{	
	if (maxRetries == INT_MAX)
	{
		while(true)
		{
			try{
				interface->openConnection(userID, parameters);
				break;
			}
			catch(char* str)
			{
			}
			
			this->sleep(1);
		}
	}
	else
	{
		int i;
		for (i=0; i<maxRetries; i++)
		{
			try{
				interface->openConnection(userID, parameters);
				break;
			}
			catch(char* str)
			{
			}
		
			this->sleep(1);
		}
		
		if (i>=maxRetries)
		{
			throw "ERROR: Impossible to establish required connection";
		}
	}
}


};
