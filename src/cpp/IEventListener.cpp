#include "IEventListener.h"

namespace BlueRose {

IEventListener::IEventListener(int _topic)
{
	topic = _topic;
}

IEventListener::IEventListener(int _topic, Predicate _pred)
{
	topic = _topic;
	predicate = _pred;
}

IEventListener::~IEventListener()
{
	
}

bool IEventListener::check(int _topic)
{
	return (topic == _topic);
}

int IEventListener::getTopic()
{
	return topic;
}

Predicate IEventListener::getPredicate()
{
	return predicate;
}

bool IEventListener::check(const Event& evt)
{
	if (predicate.isEmpty()) return true;
	
	for (EventIterator it = evt.begin(); it != evt.end(); ++it)
	{
		if (!predicate.isConsistent(it->first, it->second.getValue()))
		{
			return false;
		}
	}
	
	return true;
}

bool IEventListener::hasPredicate()
{
	return !(predicate.isEmpty());
}
	
};
