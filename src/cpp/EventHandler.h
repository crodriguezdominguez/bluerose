#ifndef _BLUEROSE_EVENT_HANDLER_H_
#define _BLUEROSE_EVENT_HANDLER_H_

#include <map>
#include <vector>
#include "ObjectProxy.h"
#include "Event.h"
#include "Predicate.h"
#include "IEventListener.h"

namespace BlueRose {

	class PubSubProxy;
	class EventHandler;

	class EventHandler {
		protected:
			static PubSubProxy* proxy; /**< Proxy for the publish/subcribe service */
			static std::map<int, std::vector<IEventListener*> > listeners; /**< Event listeners */
			
		public:
			static void initEventHandler(ICommunicationDevice* _iface = NULL,
										bool waitOnline = true, 
										const DictionaryDataType& connParameters = DictionaryDataType());
			
			static void initEventHandler(const std::string& _servantID,
										ICommunicationDevice* _iface = NULL,
										bool waitOnline = true, 
										const DictionaryDataType& connParameters = DictionaryDataType());
			
			/**
			* Destructor
			*/
			static void destroyEventHandler();
		
			/**
			* Action to be performed whenever an event is consumed
			*
			* @param evt Received event
			*/
			static void onConsume(const Event& evt);
			
			/**
			* Adds an event listener to the listeners. The event handler
			* manages the memory of the listener that is passed as an argument.
			*
			* @param listener Event listener to add
			*/
			static void addEventListener(IEventListener* listener);
			
			/**
			* Removes an event listener
			*
			* @param listener EventListener to remove
			*/
			static void removeEventListener(IEventListener* listener);
			
			/**
			* Removes all the event listener that are related with a topic
			*
			* @param topic Topic to remove
			*/
			static void removeEventListeners(int topic);
			
			/**
			* Removes an event listener from the listeners
			*
			* @param topic Topic of the listener to remove
			* @param name Name of the listener to remove
			*/
			static void removeEventListener(int topic, const std::string& name);
			
			/**
			* Publishes an event to all its subscribers
			*
			* @param evt Event to be published
			* @param comeBack True if the event has to be published to the publisher because
			*                 it is subscribed. False if the event will not be published to
			*                 the published even if it is subscribed to it.
			*/
			static void publish(const Event& evt, bool comeBack=true);
			
			/**
			* Returns the proxy to the pub/sub service
			*
			* @return Proxy to the pub/sub service
			*/
			static PubSubProxy* getPubSubProxy();
	};
	
	class PubSubProxy : ObjectProxy {
		public:
			PubSubProxy(ICommunicationDevice* _iface = NULL,
						bool waitOnline = true, 
						const DictionaryDataType& connParameters = DictionaryDataType());
			
			PubSubProxy(const std::string& _servantID,
						ICommunicationDevice* _iface = NULL,
						bool waitOnline = true, 
						const DictionaryDataType& connParameters = DictionaryDataType());
							
			/**
			* Destructor
			*/
			~PubSubProxy();
		
			/**
			* Adds a new subscription to a topic
			*
			* @param _topic Topic to subscribe to
			*/
			void subscribe(int _topic);
		
			/**
			* Adds a new subscription to a topic, but with a set of constraints
			*
			* @param _topic Topic to subscribe to
			* @param _pred Set of constraints over the properties of the events
			*/
			void subscribe(int _topic, Predicate _pred);
			
			/**
			* Removes all the subscriptions
			*/
			void unsubscribe();
			
			/**
			* Remove a subscription to a topic
			*
			* @param _topic Topic associated with the subscription to remove
			*/
			void unsubscribe(int _topic);
		
			/**
			* Overload of the function that returns the type identifier.
			* (@see ObjectProxy.h)
			*
			* @return Type identifier
			*/
			std::string getTypeID() const;
		
			/**
			* Checks if any of the user subscriptions allow him/her to receive the event
			* that is specified by the template
			*
			* @param _userID User whose subscription is going to be checked
			* @param _template Event template
			* @return True if the user may receive the event specified by the template.
			*         False otherwise else.
			*/
			bool isSubscribed(const std::string& _userID, const Event& _template);
			
			/**
			* Returns all the subscribers
			*
			* @return Vector of subscribers
			*/
			std::vector<std::string> getSubscribers();
			
			/**
			* Returns all the subscribers that may receive the specified event
			*
			* @param _template Event
			* @return Vector of subscribers
			*/
			std::vector<std::string> getSubscribers(const Event& _template);
			
			/**
			* Publishes an event to all its subscribers
			*
			* @param evt Event to be published
			* @param comeBack True if the event has to be published to the publisher because
			*                 it is subscribed. False if the event will not be published to
			*                 the published even if it is subscribed to it.
			*/
			void publish(const Event& evt, bool comeBack=true);
	};
	
};

#endif

