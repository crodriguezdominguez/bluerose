#ifndef _BLUEROSE_REPLY_READ_THREAD_H_
#define _BLUEROSE_REPLY_READ_THREAD_H_

#include "Thread.h"
#include "ICommunicationDevice.h"

namespace BlueRose {

	/**
	* Auxiliary thread for reading replies from a transmission 
	* channel. It's used for receiving servant replies
	*
	* @author Carlos Rodriguez Dominguez
	* @date 7-10-2009
	*/
	class ReplyReadThread : public Thread
	{
		protected:
			ICommunicationDevice* interface; /**< Transmission interface for the thread */
			std::string userID; /**< User identificator of the sender */
		
		public:
		
			/**
			* Constructor
			*
			* @param iface Transmission interface
			*/
			ReplyReadThread(ICommunicationDevice* iface, const std::string _userID);
		
			/**
			* Overload of the main method of the thread
			*/
			void run();
	};
	
};

#endif

