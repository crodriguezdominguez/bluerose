#ifndef __BLUEROSE_THREAD_H__
#define __BLUEROSE_THREAD_H__

#include <pthread.h>

namespace BlueRose {

	/**
	* Wrapper for threads.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 05-11-2008
	*/
	class Thread 
	{
		public:
			/**
			* Default constructor. If this constructor is used,
			* then "run" method has to be overloaded.
			*/
			Thread();
	
			/**
			* Constructor with parameters
			*
			* @param start_routine Pointer to the function associated with the thread
			* @param args Array containing the arguments of the function
			*/
			Thread(void* (*start_routine)(void*), void* args);
	
			/**
			* Starts the execution of the thread
			*/
			void start();
	
			/**
			* Cancels the execution of the thread. The thread is not itself
			* destroyed, so "start" method can be called again.
			*/
			void cancel();
	
			/**
			* Avoids the end of main thread until this thread has finished
			* its execution.
			*/
			void join();
	
			/**
			* In case of using the default constructor, this method
			* has to be overloaded, as it will serve as the code that
			* the thread is going to execute. "start" method has to be
			* called for correctly executing the thread with this code.
			*/
			virtual void run();
			
			/**
			* Puts thread to sleep
			*
			* @param seconds Seconds to sleep
			*/
			void sleep(int seconds);
	
		private:
			pthread_t _thread; /**< POSIX thread */
			void* (*routine)(void*); /**< Pointer to the function to execute */
			void* _args; /**< Arguments of the function to execute */
	};

};

#endif
