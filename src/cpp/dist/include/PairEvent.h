#ifndef _PAIR_EVENT_H_
#define _PAIR_EVENT_H_

#include "BlueRose.h"

#define PAIR_EVENT_TOPIC 1

namespace BlueRose {

class PairEvent : public BlueRose::Event {
public:
	PairEvent();
	PairEvent(float x, float y);
	
	float getX();
	float getY();
	
	void setX(float x);
	void setY(float y);
};

};

#endif
