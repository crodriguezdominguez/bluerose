#ifndef _BLUEROSE_CONFIGURATION_H_
#define _BLUEROSE_CONFIGURATION_H_

#include <vector>
#include <string>
#include <libxml/parser.h>
#include <libxml/tree.h>

namespace BlueRose {
	
	class Configuration {
		private:
			xmlDocPtr doc;
			
		public:
			Configuration(const std::string& XMLfile);
			~Configuration();
			
			std::vector<std::string> getDevices();
			std::string getModuleForDevice(const std::string& device);
			std::vector<std::string> getCategories();
			std::vector<std::string> getServicesForCategory(const std::string& category);
			std::vector<std::string> getDevicesForService(const std::string& category, const std::string& service);
			std::string getPort(const std::string& category, const std::string& service, const std::string& device);
			std::string getAddress(const std::string& category, const std::string& service, const std::string& device);
			std::string getUserID(const std::string& category, const std::string& service, const std::string& device);
	};

};

#endif

