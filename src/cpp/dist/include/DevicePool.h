#ifndef _BLUEROSE_POOL_H_
#define _BLUEROSE_POOL_H_

#include <string>
#include <list>
#include <map>
#include <pthread.h>
#include "ICommunicationDevice.h"
#include "Thread.h"
#include "ObjectServant.h"

namespace BlueRose {

	struct identityPredicate;

	/**
	* Pool of interfaces. It also includes servant objects, as they may be
	* considered as an abstraction over an specific interface.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class DevicePool
	{
		protected:
			static std::map<std::string, ICommunicationDevice*> deviceCollection; /**< Allowed transmission devices */
			static ICommunicationDevice* interface; /**< Current transmission channel */
			static std::map<Identity, ObjectServant*, identityPredicate> objects; /**< Registered objects servants */
		
		public:
			
			/**
			* Adds a new interface to the collection of interfaces
			*
			* @param _interface Interface to add
			*/
			static void addDevice(ICommunicationDevice* _interface);
			
			/**
			* Registers a new object into the pool
			*
			* @param obj Object to register
			*/
			static void registerObjectServant(ObjectServant* obj);
			
			/**
			* Unregisters an object from the pool
			*
			* @param obj Object to unregister
			*/
			static void unregisterObjectServant(ObjectServant* obj);
	
			/**
			* Selects a new interface for transmitting data based upon the priority of the channels
			*
			* @return New transmission interface
			*/
			static ICommunicationDevice* nextDevice();
			
			/**
			* Starts the pool, selecting the current interface
			*/
			static void init();
			
			/**
			* Shutdown the interface pool, releasing all resources.
			*/
			static void destroy();
			
			/**
			* Message Dispatcher for asynchronous calls and/or server requests
			*
			* @param userId Sender user id
			* @param msg Message to dispatch
			*/
			static void dispatchMessage(const std::string& userId, const std::vector<byte>& msg);
	
			/**
			* Retrieves the current interface
			*/
			static ICommunicationDevice* currentDevice();
			
			/**
			* Returns the transmission interface for the provided name
			*
			* @param name Name of the transmission interface
			*
			* @return Transmission interface
			*/
			static ICommunicationDevice* deviceForName(const std::string& name);
	};

};

#endif

