#ifndef _BLUEROSE_READ_THREAD_H_
#define _BLUEROSE_READ_THREAD_H_

#include <string>
#include "Thread.h"
#include "ICommunicationDevice.h"
#include "DevicePool.h"

namespace BlueRose {
	
	/**
	* Auxiliary thread for reading requests from a
	* transmission channel. It's used for receiving requests
	*
	* @author Carlos Rodriguez Dominguez
	* @date 7-10-2009
	*/
	class RequestReadThread : public Thread
	{
		protected:
			ICommunicationDevice* interface; /**< Transmission interface for the thread */
			std::string userID; /**< User identificator of the sender */
		
		public:
		
			/**
			* Constructor
			*
			* @param iface Transmission interface
			*/
			RequestReadThread(ICommunicationDevice* iface, const std::string _userID);
		
			/**
			* Overload of the main method of the thread
			*/
			void run();
	};
	
};

#endif

