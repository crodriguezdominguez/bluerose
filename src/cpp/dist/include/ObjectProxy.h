#ifndef _BLUEROSE_OBJECT_PROXY_H_
#define _BLUEROSE_OBJECT_PROXY_H_

#include <string>
#include <vector>
#include <list>
#include <map>
#include "OpenConnectionThread.h"
#include "Thread.h"
#include "TypeDefinitions.h"
#include "Message.h"
#include "ICommunicationDevice.h"

#define MAX_REQUEST_ID 1000000

namespace BlueRose {

	/**
	* Proxy for transparently connecting to a servant
	*
	* @author Carlos Rodriguez Dominguez
	* @date 11-11-2009
	*/
	class ObjectProxy;
	
	/**
	* Callback method for an asynchronous request
	*
	* @author Carlos Rodriguez Dominguez
	* @date 15-11-2009
	*/
	class AsyncCallback;
	
	/**
	* Thread for waiting an asynchronous reply
	*
	* @author Carlos Rodriguez Dominguez
	* @date 15-11-2009
	*/
	class AsyncMethodCallback;


	class ObjectProxy {
		protected:
			ICommunicationDevice* interface; /**< Interface for transferring information */
			OpenConnectionThread* openThread; /**< Thread for openning connections in case the servant is disconnected */
			std::string servantID; /**< Id of the servant */
			Identity identity; /**< Identity of the object */
			byte currentMode; /**< Current operational mode. @see MessageHeader.h */
			pthread_mutex_t mutex; /**< Semaphore for concurrently accesing the proxy */
			static AsyncCallback* asyncThread; /**< Callback thread for asynchronous methods */
			static int numInstances; /**< Number of instances of the object */
			static pthread_mutex_t shared_mutex; /**< Mutex for static data */
			static int requestId; /**< Request Id */
			static ByteStreamWriter writer; /**< Shared writer */
			static pthread_mutex_t mutex_writer; /**< Shared mutex for the writer */
			static ByteStreamReader reader; /**< Shared reader */
			static pthread_mutex_t mutex_reader; /**< Shared mutex for the reader */
			
			/**
			* Sends a request to a servant
			* 
			* @param servantID ID of the servant to send the request to
			* @param operation Operation of the request
			* @param enc Message of the request
			* @return Request Identifier for receiving the reply message
			*/
			int sendRequest(const std::string& servantID, const std::string& operation, const std::vector<byte>& enc);
			
			/**
			* Receives a reply. Is a blockant method.
			*
			* @param requestId Request Identifier of the request message
			* @param result Received Message
			*/
			void receiveReply(int requestId, std::vector<byte>& result);
			
			/**
			* Receives a message with the callback thread and treats the received
			* message with the specified AsyncMethodCallback
			*
			* @param requestId Request Identifier of the request message
			* @param _amc Async method callback for treating the received message
			*/
			void receiveCallback(int requestId, AsyncMethodCallback* _amc);
			
			void resolveInitialization(ICommunicationDevice* _iface = NULL, 
										bool waitOnline = true, 
										const DictionaryDataType& connParameters = DictionaryDataType());
			
		public:
			/**
			* Default constructor. It uses the discovery proxy for retrieving
			* the servant. It throws an exception if the discovery proxy is not
			* previously initialized or if the servant is not registered in the proxy
			*/
			ObjectProxy();
			
			/**
			* Constructor. Uses the current interface of the DevicePool.
			* If that interface changes, it also changes for the object.
			*
			* @param _servantID Id of the servant to connect to
			* @param _iface Interface to connect to. It it's NULL, it will connect to the current interface in the
			*               interface pool.
			* @param waitOnline If it's true, it will wait for the servant to be alive
			* @param connParameters Connection parameters
			*/
			ObjectProxy(const std::string& _servantID, 
						ICommunicationDevice* _iface = NULL, 
						bool waitOnline = true, 
						const DictionaryDataType& connParameters = DictionaryDataType());
			
			/**
			* Virtual destructor
			*/
			virtual ~ObjectProxy();

			/**
			* Waits for a Servant to be online
			*/
			void waitOnlineMode();
			
			/**
			* Gets the type ID of the proxy
			*
			* @return String that represents the type ID
			*/
			virtual std::string getTypeID() const = 0;
			
			/**
			* Marshalls the proxy
			*
			* @param res Result of the marshalling
			* @param reduced Sets if the proxy must be marshalled in the reduced way
			* @param typeID Sets the type id if the reduced way is on
			*/
			virtual void marshall(std::vector<byte>& res);
			
			/**
			* Unmarshalls the proxy from a byte vector. The
			* marshalled proxy musts start at byte 0.
			*
			* @param vec Vector with the proxy marshalled
			*/
			virtual void unmarshall(const std::vector<byte>& vec);
	};

	class AsyncMethodCallback {
		public:
			virtual ~AsyncMethodCallback(){}
			
			/**
			* Callback method that is called when an specific asynchronous
			* reply is received
			*
			* @param result Received reply message
			*/
			virtual void callback(const std::vector<byte>& result) = 0;
	};
	
	class AsyncCallback : public Thread {
		protected:
			std::map<int, AsyncMethodCallback*> requestsId_callbacks; /**< stack of requestIds and asynchronous method callbacks */
			pthread_mutex_t mutex; /**< Mutex */
			pthread_cond_t cond; /**< Conditional variable */
			
		public:
			/**
			* Default constructor
			*/
			AsyncCallback();
			
			/**
			* Destructor
			*/
			~AsyncCallback();
			
			/**
			* Method for the AsyncCallback receiver thread
			*/
			void run();
			
			/**
			* Adds a new request id to the async method callback stack
			*
			* @param reqId Request identifier for the reply
			* @param amc Callback method that is called when the reply is received
			*/
			void pushRequestId(int reqId, AsyncMethodCallback* amc);
	};
	
};

#endif

