#ifndef _BLUEROSE_PRODUCER_CONSUMER_STACK_H_
#define _BLUEROSE_PRODUCER_CONSUMER_STACK_H_

#include <map>
#include <pthread.h>
#include "Message.h"

namespace BlueRose {

	/**
	* Stack where all received messages are stored
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class ReplyMessageStack {
		protected:
			static std::map<int, std::vector<byte> > stack; /**< Stack as pairs of requestIds-Messages */
			static bool waitingReply; /**< True if someone is waiting a reply */
			static pthread_mutex_t mutex; /**< Semaphore for concurrently accessing the stack */
			static pthread_cond_t cond; /**< Conditional variable */
		
		public:
			/**
			* Initializes the stack
			*/
			static void init();
			
			/**
			* Destroys the stack
			*/
			static void destroy();
			
			/**
			* Adds a new message to the stack
			*
			* @param requestId Id of the request
			* @param msg Message
			*/
			static void produce(int requestId, const std::vector<byte>& msg);
			
			/**
			* Removes a message from the stack. If the message does not exist,
			* it blocks undefinitely.
			*
			* @param requestId Id of the request
			* @param res Message removed from the stack
			*/
			static void consume(int requestId, std::vector<byte>& res);
			
			/**
			* Returns whether somebody is waiting a reply or not
			*
			* @return True if someone is waiting. False otherwise else.
			*/
			static bool isWaitingReply();
	};
	
};

#endif

