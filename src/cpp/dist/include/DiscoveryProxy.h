#ifndef _BLUEROSE_DISCOVERY_PROXY_H_
#define _BLUEROSE_DISCOVERY_PROXY_H_

#include <string>
#include "ObjectProxy.h"

namespace BlueRose {

	class DiscoveryProxy : public ObjectProxy{
		protected:
			static DiscoveryProxy* proxy; /**< Singleton pattern applied */
		
			/**
			* Default constructor. (@see ObjectProxy.h)
			*/
			DiscoveryProxy(const std::string& _servantID, 
							ICommunicationDevice* _iface = NULL, 
							bool waitOnline = true, 
							const DictionaryDataType& connParameters = DictionaryDataType());
			
			/**
			* Destructor
			*/
			~DiscoveryProxy();
		
		public:
			static void init(const std::string& _servantID, 
							ICommunicationDevice* _iface = NULL,
			 				bool waitOnline = true, 
							const DictionaryDataType& connParameters = DictionaryDataType());
									
			static void destroy();
			
			static DiscoveryProxy* defaultProxy();
			
			/**
			* Overload of the function that returns the type identifier.
			* (@see ObjectProxy.h)
			*
			* @return Type identifier
			*/
			std::string getTypeID() const;
		
			/**
			* Registers a servant name and an address
			*
			* @param name Name of the servant to register
			* @param address Address of the servant
			*/
			void addRegistration(const std::string& name, const std::string& address);
		
			/**
			* Unregisters the address that is associated with a name
			*
			* @param name Name of the servant to unregister
			*/
			void removeRegistration(const std::string& name);
		
			/**
			* Returns the physical address that is associated with a servant name
			*
			* @param name Name of the servant to resolve
			* @param waitOnline Waits for the specified servant to be registered
			* @return Physical address of the servant to resolve
			*/
			std::string resolve(const std::string& name, bool waitOnline = false);
	};
	
};

#endif

