#ifndef _BLUEROSE_DISCOVERY_SERVICE_H_
#define _BLUEROSE_DISCOVERY_SERVICE_H_

#include <string>
#include <map>
#include <vector>
#include "BlueRose.h"

namespace BlueRose {
	
	namespace Service {
	
		/**
		* Implements the pub/sub service. It runs (by default)
		* on port 10002.
		*/
		int main(int argc, char** argv);
	
		/**
		* Servant for the discovery service requests
		*
		* @author Carlos Rodriguez Dominguez
		* @date 09-02-2010
		*/
		class DiscoveryServant : public ObjectServant {
			protected:
				/**
				* Map that associates a servant name with its physical addresses (it may be replicated)
				*/
				std::map<std::string, std::string> registrations;
			
			public:
				/**
				* Default constructor
				*/
				DiscoveryServant();
				
				/**
				* Destructor
				*/
				~DiscoveryServant();
				
				/**
				* Overload of the correspondent method of the ObjectServant class (@see ObjectServant.h).
				*/
				byte runMethod(const std::string& userID,
								const std::string& method,
								const std::vector<byte>& args, 
								std::vector<byte>& result);
				
				/**
				* Registers a servant name and an address
				*
				* @param name Name of the servant to register
				* @param address Address of the servant
				*/
				void addRegistration(const std::string& name, const std::string& address);
				
				/**
				* Unregisters all the names that are associated with a name
				*
				* @param name Name of the servant to unregister
				*/
				void removeRegistration(const std::string& name);
				
				/**
				* Returns the physical address that is associated with a servant name
				*
				* @param name Name of the servant to resolve
				* @return Physical address of the servant to resolve
				*/
				std::string resolve(const std::string& name);
		};
		
		/**
		* Implementation of a discoverer of services or devices. Whenever it detects
		* a new element, it adds it to the registrations of the discovery service.
		*
		* @author Carlos Rodriguez Dominguez
		* @date 21-06-2010
		*/
		/*class DiscoveryServantEventListener : public MobileDiscoveryListener {
			protected:
				DiscoveryServant* servant;

			public:
				DiscoveryServantEventListener(IMobileDiscovery* tcpd, DiscoveryServant* serv);

				void onElementDiscovered(const std::string& name, 
										const std::string& regtype, 
										const std::string& domain,
										bool moreComing, bool error);

				void onElementRegistered(const std::string& fullname, 
										const std::string& userID,
										const TXTRecord& records,
										bool moreComing, bool error);

				void onElementRemoved(const std::string& name, 
										const std::string& regtype, 
										const std::string& domain,
										bool moreComing, bool error);

				void onUserIDResolved(const std::string& fullname, 
										const std::string& userID, 
										const TXTRecord& records,
										bool moreComing, bool error);
		};*/
	};
};

#endif

