#ifndef _BLUEROSE_INITIALIZER_H_
#define _BLUEROSE_INITIALIZER_H_

#include <string>
#include "Configuration.h"
#include "ObjectServant.h"
//#include "IMobileDiscovery.h"
#include "ICommunicationDevice.h"

namespace BlueRose {
	
	class Initializer {
		protected:
			static Configuration* configuration;
			
		public:
			/**
			* Initiazes the whole BlueRose subsystem.
			*/
			static void initialize(const std::string& configFile);
			
			/**
			* Initializes a client associated with a device
			*/
			static void initializeClient(ICommunicationDevice* device, bool withDiscoveryServant=true, bool withEventHandler=true);
			
			/**
			* Creates a servant in the current thread
			*/
			static void initializeServant(ObjectServant* servant, ICommunicationDevice* device, bool registerAtDiscoveryServant=true);
			
			/**
			* Creates a mobile servant in the current thread
			*/
			//static void initializeMobileServant(ObjectServant* servant, ICommunicationDevice* device, IMobileDiscovery* discoverer, int serviceType);
			
			/**
			* Returns the configuration of the middleware
			*/
			static Configuration* getConfiguration();
			
			/**
			* Free all the resources
			*/
			static void destroy();
	};
};

#endif

