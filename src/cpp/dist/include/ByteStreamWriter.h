#ifndef _BLUEROSE_BYTE_STREAM_WRITER_H_
#define _BLUEROSE_BYTE_STREAM_WRITER_H_

#include <vector>
#include "BRUtil.h"
#include "TypeDefinitions.h"

namespace BlueRose {

	/**
	* Writer for byte streams. Data types will
	* be encoded as specified by ICE (zeroc.com)
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class ByteStreamWriter
	{
		protected:
			std::vector<byte>* stream; /**< Byte stream to write to */
	
		public:
			
			/**
			* Constructor
			*/
			ByteStreamWriter();
		
			/**
			* Allows writing to the specified byte stream
			*
			* @param str Byte stream to write to 
			*/
			void open(std::vector<byte>& str);
			
			/**
			* Avoids writing to the current byte stream
			*/
			void close();
		
			/**
			* Retrieves the current byte stream
			*
			* @return Current byte stream
			*/
			std::vector<byte>& bytes();
	
			/**
			* Writes a size to the stream. A size is coded as
			* a single byte if is lower than 255. If it's greater,
			* then it uses 5 bytes: The first one is always 255 and
			* the following are the integer representation of the size.
			*
			* @param size Size to write to the stream
			*/
			void writeSize(unsigned int size);
	
			/**
			* Adds a byte to the stream
			*
			* @param b Byte to add
			*/
			void writeByte(byte b);
			
			/**
			* Adds a boolean to the stream
			*
			* @param b Boolean to add
			*/
			void writeBoolean(bool b);
			
			/**
			* Adds a short to the stream
			*
			* @param b Short to add
			*/
			void writeShort(short b);
			
			/**
			* Adds an integer to the stream
			*
			* @param b Integer to add
			*/
			void writeInteger(int b);
			
			/**
			* Adds a long to the stream
			*
			* @param b Long to add
			*/
			void writeLong(long b);
			
			/**
			* Adds a float to the stream
			*
			* @param b Float to add
			*/
			void writeFloat(float b);
			
			/**
			* Adds a double to the stream
			*
			* @param b Double to add
			*/
			void writeDouble(double b);
			
			/**
			* Adds an utf8 string to the stream
			*
			* @param b String to add
			*/
			void writeUTF8String(const std::wstring& b);
			
			/**
			* Adds an string to the stream
			*
			* @param b String to add
			*/
			void writeString(const std::string& b);
		
			/**
			* Adds raw bytes to the stream
			*
			* @param b Bytes to add
			*/
			void writeRawBytes(const std::vector<byte>& b);
		
			/**
			* Adds a sequence of bytes to the stream
			*
			* @param b Bytes to add
			*/
			void writeByteSeq(const std::vector<byte>& b);
			
			/**
			* Adds a sequence of booleans to the stream
			*
			* @param b Booleans to add
			*/
			void writeBooleanSeq(const std::vector<bool>& b);
			
			/**
			* Adds a sequence of shorts to the stream
			*
			* @param b Shorts to add
			*/
			void writeShortSeq(const std::vector<short>& b);
			
			/**
			* Adds a sequence of integers to the stream
			*
			* @param b Integers to add
			*/
			void writeIntegerSeq(const std::vector<int>& b);
			
			/**
			* Adds a sequence of longs to the stream
			*
			* @param b Longs to add
			*/
			void writeLongSeq(const std::vector<long>& b);
			
			/**
			* Adds a sequence of floats to the stream
			*
			* @param b Floats to add
			*/
			void writeFloatSeq(const std::vector<float>& b);
			
			/**
			* Adds a sequence of doubles to the stream
			*
			* @param b Doubles to add
			*/
			void writeDoubleSeq(const std::vector<double>& b);
			
			/**
			* Adds a sequence of strings to the stream
			*
			* @param b Strings to add
			*/
			void writeStringSeq(const std::vector<std::string>& b);
			
			/**
			* Adds a sequence of utf8 strings to the stream
			*
			* @param b Strings to add
			*/
			void writeUTF8StringSeq(const std::vector<std::wstring>& b);
		
			/**
			* Adds a dictionary to the stream
			*
			* @param b Bytes to add
			*/
			void writeDictionary(const DictionaryDataType& b);
	};

};

#endif

