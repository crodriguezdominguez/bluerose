#ifndef _BLUEROSE_PREDICATE_H_
#define _BLUEROSE_PREDICATE_H_

#include <map>
#include <string>
#include "Value.h"
#include "TypeDefinitions.h"
#include "Marshallable.h"

namespace BlueRose {

	typedef std::map<std::string, std::vector<std::pair<Comparison, Value> > >::const_iterator PredicateIterator;

	/**
	* Class that models a set of constraints over the values of a set of properties
	*
	* @author Carlos Rodriguez Dominguez
	* @date 09-02-2010
	*/
	class Predicate : public Marshallable {
		protected:
			/**
			* Associations between properties and their comparisons with a value
			*/
			std::map<std::string, std::vector<std::pair<Comparison, Value> > > constraints;
		
		public:
			/**
			* Default constructor
			*/
			Predicate();
			
			/**
			* Adds a new constraint to the set of constraints, but only if the set remains
			* consistent.
			*
			* @param property Property associated with the constraint
			* @param comp Comparison operator between the property and the value
			* @param value Value associated with the property
			* @return True if the constraint was added to the set (i.e., the set remains
			*         consistent after adding the constraint). False otherwise else.
			*/
			bool addConstraint(const std::string& property, Comparison comp, const Value& value);
			
			/**
			* Removes a constraint from the set
			*
			* @param property Property associated with the constraint to remove
			*/
			void removeConstraints(const std::string& property);
			
			/**
			* Checks if the set of constraints is consistent after adding "property comp value".
			* For example: "x == 5", "y <= 3", etc.
			*
			* @param property Property to check
			* @param value Value of the property
			*/
			bool isConsistent(const std::string& property, Comparison comp, const Value& value) const;
			
			/**
			* Checks if the set of constraints is consistent after adding "property == value".
			*
			* @param property Property to check
			* @param value Value of the property
			*/
			bool isConsistent(const std::string& property, const Value& value) const;
			
			/**
			* Checks if the set of constraints is empty
			*/
			bool isEmpty() const;
			
			/**
			* Returns the number of constraints
			*
			* @return Number of constraints
			*/
			unsigned int size() const;
			
			/**
			* Returns an iterator to the beginning of the set of constraints
			*
			* @return Iterator to the beginning of the set of constraints
			*/
			PredicateIterator begin() const;
			
			/**
			* Returns an iterator to the end of the set of constraints
			*
			* @return Iterator to the end of the set of constraints
			*/
			PredicateIterator end() const;
			
			virtual void marshall(ByteStreamWriter& writer) const;
			virtual void unmarshall(ByteStreamReader& reader);
	};
	
};

#endif
