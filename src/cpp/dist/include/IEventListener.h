#ifndef _BLUEROSE_IEVENTLISTENER_H_
#define _BLUEROSE_IEVENTLISTENER_H_

#include "Event.h"
#include "Predicate.h"

namespace BlueRose {

	/**
	* Abstract Listener for events.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 15-02-2010
	*/
	class IEventListener {
		protected:
			int topic; /**< Topic that the listener accepts */
			Predicate predicate; /**< Predicate associated with the listener */
		
		public:
			/**
			* Default constructor
			*
			* @param _topic Topic to listen
			*/
			IEventListener(int _topic);
			
			/**
			* Default constructor
			*
			* @param _topic Topic to listen
			* @param _pred Predicate to accomplish
			*/
			IEventListener(int _topic, Predicate _pred);
			
			virtual ~IEventListener();
			
			/**
			* Checks whether this listener accepts a topic or not
			*
			* @param _topic Topic to check
			* @return True if the topic is accepted. False otherwise else
			*/
			bool check(int _topic);
			
			/**
			* Checks whether this listener accepts an event or not. It is
			* a more complex check than the check method that only accepts
			* a topic. In this method, it is assured that the event is
			* related with the topic established in the constructor. It uses
			* the predicate to check if it is accepted or not.
			*
			* @param evt Event to check
			* @return True if the event is accepted. False otherwise else
			*/
			bool check(const Event& evt);
			
			/**
			* Returns the topic that is associated with the listener
			*
			* @return Topic that is associated with the listener
			*/
			int getTopic();
			
			/**
			* Returns the predicate that is associated with the listener
			*
			* @return Predicate associated with the listener
			*/
			Predicate getPredicate();
			
			/**
			* Returns whether the predicate is empty or not
			*/
			bool hasPredicate();
			
			/**
			* Action that is run whenever an appropriate event is received.
			*
			* @param evt Event that is received
			*/
			virtual void action(const Event* evt) = 0;
			
			/**
			* Returns the name of the listener. It is used
			* for comparing event listeners.
			*
			* @return Name of the listener
			*/
			virtual std::string getName() = 0;
	};

};

#endif

