#ifndef _BLUEROSE_OBJECT_SERVANT_H_
#define _BLUEROSE_OBJECT_SERVANT_H_

#include <vector>
#include <string>
#include <pthread.h>
#include "ByteStreamWriter.h"
#include "ByteStreamReader.h"
#include "TypeDefinitions.h"

namespace BlueRose {

	class ObjectServant {
		protected:
			Identity identity;
			static ByteStreamWriter writer;
			static ByteStreamReader reader;
			static pthread_mutex_t mutex_writer;
			static pthread_mutex_t mutex_reader;
		
		public:
			ObjectServant();
			virtual ~ObjectServant();
		
			bool checkIdentity(const Identity& id);
			Identity& getIdentity();
			std::string getIdentifier();
		
			virtual byte runMethod(const std::string& userdID, 
									const std::string& method, 
									const std::vector<byte>& args, 
									std::vector<byte>& result) = 0;
	};

};


#endif

