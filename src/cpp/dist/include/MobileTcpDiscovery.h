#ifndef _BLUEROSE_MOBILE_TCP_DISCOVERY_H_
#define _BLUEROSE_MOBILE_TCP_DISCOVERY_H_

#include <map>
#include <string>
#include <dns_sd.h> //zeroconf: Apple Bonjour for windows and macosx. For linux, use avahi+libdnssd1
#include "Thread.h"
#include "ICommunicationDevice.h"
#include "ObjectServant.h"
#include "IMobileDiscovery.h"


namespace BlueRose {

	class MobileTcpDiscoveryThread;
	class MobileTcpDiscovery;

	class MobileTcpDiscovery : public IMobileDiscovery {
		protected:
			std::map<ObjectServant*, MobileTcpDiscoveryThread*> threads;
			std::map<ObjectServant*, DNSServiceRef> refs;
			std::vector<MobileDiscoveryListener*> listeners;
			MobileTcpDiscoveryThread* detectionThread;
			
			std::string getRegType(int serviceType);
		
		public:
			enum ServiceType {
				PRINTER,
				BLUEROSE,
				DAAP,
				DACP,
				FTP,
				HOME_SHARING,
				HTTP,
				HTTPS,
				IPP,
				JINI,
				LONWORKS,
				NFS,
				RFID,
				RTSP,
				SCANNER,
				SFTP,
				SSH,
				TELNET,
				TFTP,
				UPNP
			};
			
			MobileTcpDiscovery();
			~MobileTcpDiscovery();
			
			void addEventListener(MobileDiscoveryListener* listener);
			void removeEventListener(MobileDiscoveryListener* listener);
			
			bool registerForDetection(ObjectServant* servant, ICommunicationDevice* device);
			void unregister(ObjectServant* servant);
			void resolveUserID(const std::string& name, const std::string& regtype, const std::string& domain);
			std::string resolveUserID(const std::string& name, int serviceType);
			bool startDetection(int serviceType, const std::string& domain);
			bool startDetection(int serviceType);
			void stopDetection();
	};
	
	class MobileTcpDiscoveryThread : public Thread {
		protected:
			DNSServiceRef sdRef;
			MobileTcpDiscovery* tcpdiscovery;
		
		public:
			MobileTcpDiscoveryThread(MobileTcpDiscovery* tcpd, DNSServiceRef ref);
			void run();
	};
};

#endif

