#ifndef _BLUEROSE_I_MOBILE_DISCOVERY_H_
#define _BLUEROSE_I_MOBILE_DISCOVERY_H_

#include <map>
#include <string>
#include "TypeDefinitions.h"
#include "ObjectServant.h"
#include "ICommunicationDevice.h"

namespace BlueRose {

	class MobileDiscoveryListener;
	class IMobileDiscovery;
	class TXTRecord;
	
	typedef std::map<std::string, std::string> RecordMap; /**< Auxiliary type definition for TXTRecord */

	/**
	* Interface to implement device discovery based on Zeroconf (dns-sd.org).
	*
	* @author Carlos Rodriguez Dominguez
	* @date 15-06-2010
	*/
	class IMobileDiscovery {
		public:
			/**
			* Adds a new listener to the device discovery.
			*
			* @param listener Listener to add
			*/
			virtual void addEventListener(MobileDiscoveryListener* listener) = 0;
			
			/**
			* Removes a listener from the device discovery.
			*
			* @param listener Listener to remove
			*/
			virtual void removeEventListener(MobileDiscoveryListener* listener) = 0;

			/**
			* Registers a servant for its discovery. That servant must be associated with a device.
			* When the register is complete, "onElementRegistered" method will be called on all the
			* listeners.
			*
			* @param servant Servant to register.
			* @param device Device associated with the servant.
			*/
			virtual bool registerForDetection(ObjectServant* servant, ICommunicationDevice* device) = 0;
			
			/**
			* Unregisters a servant from discovery.
			*
			* @param servant Servant to unregister.
			*/
			virtual void unregister(ObjectServant* servant) = 0;
			
			/**
			* Resolves the userID of a discovered service or device. When the resolution is complete,
			* "onUserIDResolved" method will be called on all the listeners.
			*
			* @param name Name provided by "onElementDiscovered" or "onElementRegistered" listener methods.
			*/
			virtual void resolveUserID(const std::string& name, const std::string& regtype, const std::string& domain) = 0;
			
			/**
			* Returns the userID of a service from its name. It does not call any method on the listeners.
			*
			* @param name Name of the service
			* @param serviceType Type of the service to resolve
			* @return UserID of the service.
			*/
			virtual std::string resolveUserID(const std::string& name, int serviceType) = 0;
			
			/**
			* Start event detection. It will run on background on a separate thread from the caller.
			* The serviceType will be determined by each implementation of the discovery.
			* It will be useful in order to select different types of services.
			* This method can NOT be called more than once if the method "stopDetection" is not called.
			* In order to detect more than one type of service, more than one instance of a IMobileDiscovery
			* class will be needed.
			*
			* @param serviceType Type of service as determined by the implementation of the discoverer
			* @param domain Domain in which services will be discovered (see dns-sd.org for further information)
			* @return True if the detection started correctly. False otherwise else.
			*/
			virtual bool startDetection(int serviceType, const std::string& domain) = 0;
			
			/**
			* Overload of the startDetection method but with a local domain always
			*
			* @param serviceType Type of service as determined by the implementation of the discoverer
			* @return True if the detection started correctly. False otherwise else.
			*/
			virtual bool startDetection(int serviceType) = 0;
			
			/**
			* Stops the detection of events.
			*/
			virtual void stopDetection() = 0;
	};

	/**
	* Abstract class. Any of its methods can be overloaded
	* in order to listen to discovery events
	*
	* @author Carlos Rodriguez Dominguez
	* @date 15-06-2010
	*/
	class MobileDiscoveryListener {
		private:
			IMobileDiscovery* discovery; /**< Reference to the discoverer */
	
		public:
			/**
			* Constructor.
			*
			* @param d Reference to the discoverer that stores the listener.
			*/
			MobileDiscoveryListener(IMobileDiscovery* d);
			
			/**
			* Destructor
			*/
			virtual ~MobileDiscoveryListener();
		
			/**
			* Method that will be called whenever a new device or service is discovered.
			*
			* @param name Name of the discovered element
			* @param regtype Type of registration as specified by Zeroconf standard
			* @param domain Domain of the element. Usually it will be "_local.".
			* @param moreComing It specifies if any event is coming inmediatly after this event.
			*                   This paramater has to be checked in order to avoid unnecesary UI updates.
			* @param error It specifies if an error ocurred.
			*/
			virtual void onElementDiscovered(const std::string& name, 
										const std::string& regtype, 
										const std::string& domain,
										bool moreComing, bool error);
	
			/**
			* Method that will be called whenever a new device or service is registered.
			*
			* @param fullname Name of the registered device or service
			* @param userID userID of the registered device or service. It can be used
			*        directly with a ICommunicationDevice instance in order to exchange information.
			* @param records TXT records as specified in dns-sd.org. It includes extra information about
			*                the service or device.
			* @param moreComing It specifies if any event is coming inmediatly after this event.
			*                   This paramater has to be checked in order to avoid unnecesary UI updates.
			* @param error It specifies if an error ocurred.
			*/
			virtual void onElementRegistered(const std::string& fullname, 
											const std::string& userID,
											const TXTRecord& records,
											bool moreComing, bool error);
			
			/**
			* Method that will be called whenever a device or service disappears.
			*
			* @param name Name of the discovered element
			* @param regtype Type of registration as specified by Zeroconf standard
			* @param domain Domain of the element. Usually it will be "_local.".
			* @param moreComing It specifies if any event is coming inmediatly after this event.
			*                   This paramater has to be checked in order to avoid unnecesary UI updates.
			* @param error It specifies if an error ocurred.
			*/
			virtual void onElementRemoved(const std::string& name, 
										const std::string& regtype, 
										const std::string& domain,
										bool moreComing, bool error);

			
			/**
			* Method that will be called whenever a device or service is resolved.
			*
			* @param fullname Name of the registered device or service
			* @param userID userID of the registered device or service. It can be used
			*        directly with a ICommunicationDevice instance in order to exchange information.
			* @param records TXT records as specified in dns-sd.org. It includes extra information about
			*                the service or device.
			* @param moreComing It specifies if any event is coming inmediatly after this event.
			*                   This paramater has to be checked in order to avoid unnecesary UI updates.
			* @param error It specifies if an error ocurred.
			*/
			virtual void onUserIDResolved(const std::string& fullname, 
											const std::string& userID, 
											const TXTRecord& records,
											bool moreComing, bool error);
										
			/**
			* Returns the discoverer associated with the listener.
			*
			* @return Discoverer associated with the listener.
			*/							
			IMobileDiscovery* getMobileDiscovery();
	};
	
	/**
	* TXT Record as specified for DNS records (dns-sd.org).
	*
	* @author Carlos Rodriguez Dominguez
	* @date 16-06-2010
	*/
	class TXTRecord {
		protected:		
			RecordMap records; /**< Dictionary of string keys and string values */
			
			/**
			* Default constructor. It creates an empty TXT record
			*/
			TXTRecord();
			
		public:
			/**
			* Returns the number of keys.
			*/
			unsigned int size() const;
			
			/**
			* Returns the value of a key.
			*/
			std::string getValue(const std::string& key) const;
			
			/**
			* Returns an iterator over the first key of the records.
			*/
			RecordMap::const_iterator begin() const;
			
			/**
			* Returns an iterator over the last+1 key of the records.
			*/
			RecordMap::const_iterator end() const;
			
			/**
			* Creates a TXTRecord with the number of bytes of the records and a
			* string formatted as specified in dns-sd.org (num_bytes data num_bytes data ...).
			*
			* @param txtLen Number of bytes of the records' string
			* @param txtRecords String that stores all the records.
			*/
			static TXTRecord parse(uint16_t txtLen, const byte* txtRecords);
	};

};

#endif
