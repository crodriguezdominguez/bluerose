#ifndef _BLUEROSE_EVENT_H_
#define _BLUEROSE_EVENT_H_

#include <string>
#include <map>
#include <vector>
#include "TypeDefinitions.h"
#include "Value.h"
#include "Marshallable.h"

#define DEFAULT_TOPIC 0

namespace BlueRose {
	
	class Event;
	class EventNode;
	
	typedef std::map<std::string, EventNode>::const_iterator EventIterator;
	
	class Event : public Marshallable {
		protected:
			int topic; /**< Event topic */
			std::map<std::string, EventNode> nodes; /**< Map of identifiers and associated nodes */
			
			bool existsMember(const std::string& member) const;
			
		public:
			Event();
			Event(int _topic);
			
			unsigned int size() const;
			
			EventNode getMember(const std::string& member);
			Value getMemberValue(const std::string& member);
			void setMember(const std::string& member, EventNode& node);
			void setMemberValue(const std::string& member, Value& value);
			int getTopic() const;
			void setTopic(int _topic);
			
			bool isNull() const;
			
			EventIterator begin() const;
			EventIterator end() const;
			
			virtual void marshall(ByteStreamWriter& writer) const;
			virtual void unmarshall(ByteStreamReader& reader);
	};
	
	class EventNode : public Event {
		protected:
			std::string identifier;
			Value value;
			
		public:
			EventNode();
			EventNode(const std::string& _identifier, Value& _value);
			
			std::string getIdentifier() const;
			Value getValue() const;
			
			void setIdentifier(const std::string& _identifier);
			void setValue(const Value& _value);
			
			virtual void marshall(ByteStreamWriter& writer) const;
			virtual void unmarshall(ByteStreamReader& reader);
	};
	
};

#endif
