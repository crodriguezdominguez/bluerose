#ifndef _BLUEROSE_SOCKET_COLLECTION_H_
#define _BLUEROSE_SOCKET_COLLECTION_H_

#include <map>
#include <string>
#include <vector>

namespace BlueRose {

	typedef std::map<std::string, int>::const_iterator SocketIterator;
	
	class SocketCollection {
		protected:
			std::map<std::string, int> sockets;
			pthread_mutex_t mutex;
		
		public:
			SocketCollection();
			~SocketCollection();
		
			void addSocket(const std::string& userID, int socket);
			void removeSocket(const std::string& userID);
			void clear();
			void lockCollection();
			void unlockCollection();
			int getSocket(const std::string& userID);
			std::vector<std::string> getUsers();
			bool someoneConnected();
			
			SocketIterator begin();
			SocketIterator end();
	};
	
	/*class SocketCollection {
		protected:
			std::map<std::string, Socket*> sockets;
			pthread_mutex_t mutex;
		
		public:
			SocketCollection();
			~SocketCollection();
		
			void addSocket(Socket* socket);
			void removeSocket(const std::string& userID);
			void clear();
			void lockCollection();
			void unlockCollection();
			Socket* getSocket(const std::string& userID);
			std::vector<std::string> getUsers();
			bool someoneConnected();
	};*/
	
};

#endif
