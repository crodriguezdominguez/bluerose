#ifndef _BLUEROSE_VALUE_H_
#define _BLUEROSE_VALUE_H_

#include <vector>
#include <string>
#include <pthread.h>
#include "TypeDefinitions.h"
#include "Marshallable.h"

namespace BlueRose {
	
	typedef enum __ValueType {
		BYTE, SHORT, INTEGER, LONG,
		BOOLEAN,
		FLOAT, DOUBLE,
		STRING,
		SEQ_BYTE, SEQ_SHORT, SEQ_INTEGER, SEQ_LONG,
		SEQ_BOOLEAN,
		SEQ_FLOAT, SEQ_DOUBLE,
		SEQ_STRING,
		OTHER, NULL_TYPE
	} ValueType;
	
	class Value : public Marshallable {
		protected:
			ValueType type; /**< Type of the value */
			std::vector<byte> rawValue; /**< Value */
			
			static ByteStreamWriter writer;
			static ByteStreamReader reader;
			static pthread_mutex_t mutex;
			static pthread_mutex_t mutex_reader;
			
			bool less_than(const Value& value) const;
			bool less_equal_than(const Value& value) const;
			bool greater_than(const Value& value) const;
			bool greater_equal_than(const Value& value) const;
		
		public:
			Value();
			Value(ValueType _type);
			
			ValueType getType() const;
			void setType(ValueType _type);
			
			std::vector<byte> getRawBytes() const;
			void setRawBytes(const std::vector<byte>& _value);
			
			void setByte(byte value);
			void setInteger(int value);
			void setShort(short value);
			void setLong(long value);
			
			void setBoolean(bool value);
		
			void setFloat(float value);
			void setDouble(double value);
		
			void setString(std::wstring& value);
		
			void setByteSeq(std::vector<byte>& value);
			void setIntegerSeq(std::vector<int>& value);
			void setShortSeq(std::vector<short>& value);
			void setLongSeq(std::vector<long>& value);
			
			void setBooleanSeq(std::vector<bool>& value);
		
			void setFloatSeq(std::vector<float>& value);
			void setDoubleSeq(std::vector<double>& value);
		
			void setStringSeq(std::vector<std::wstring>& value);
		
			//some useful getters
			byte getByte() const;
			int getInteger() const;
			short getShort() const;
			long getLong() const;
			
			bool getBoolean() const;
		
			float getFloat() const;
			double getDouble() const;
		
			std::wstring getString() const;
		
			std::vector<byte> getByteSeq() const;
			std::vector<int> getIntegerSeq() const;
			std::vector<short> getShortSeq() const;
			std::vector<long> getLongSeq() const;
			
			std::vector<bool> getBooleanSeq() const;
		
			std::vector<float> getFloatSeq() const;
			std::vector<double> getDoubleSeq() const;
		
			std::vector<std::wstring> getStringSeq() const;
			
			bool isNull() const;
			
			bool compare(Comparison comp, const Value& value) const;
			
			virtual void marshall(ByteStreamWriter& writer) const;
			virtual void unmarshall(ByteStreamReader& reader);
	};
	
};

#endif

