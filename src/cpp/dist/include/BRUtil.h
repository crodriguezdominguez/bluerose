#ifndef _BLUEROSE_UTIL_H_
#define _BLUEROSE_UTIL_H_

#include <vector>
#include <sys/types.h>
#include <wchar.h>
#include "TypeDefinitions.h"

#if defined(__i386)     || defined(_M_IX86) || defined(__x86_64)  || \
    defined(_M_X64)     || defined(_M_IA64) || defined(__alpha__) || \
    defined(__MIPSEL__)
#define BR_LITTLE_ENDIAN
#elif defined(__sparc) || defined(__sparc__) || defined(__hppa)      || \
      defined(__ppc__) || defined(__powerpc) || defined(_ARCH_COM) || \
      defined(__MIPSEB__)
#define BR_BIG_ENDIAN
#else
#   error "Unknown architecture"
#endif

#define UTF8_IGNORE_ERROR		0x01
#define UTF8_SKIP_BOM			0x02

namespace BlueRose {
	
	/**
	* Initializes an encapsulation structure
	*
	* @param enc Encapsulation to initialize
	*/
	void makeEncapsulation(Encapsulation* enc);
	
	/**
	* Splits an string by any char in delim
	*
	* @param str String to split
	* @param delim Delimiter characters
	* @param results Splitted strings
	*/
	void splitString(const std::string& str, const std::string& delim, std::vector<std::string>& results);
	
	/**
	* Sends a ping to a target
	*
	* @param target IP of the target
	*
	* @return Milliseconds of the ping. 0 or -1 if an error happened.
	*/
	int sendPing(const std::string& target);
	
	/**
	* Prints a byte array as integers
	*
	* @param v Array to print
	*/
	void printVector(const std::vector<byte>& v);
	
	/**
	* Generates an unique identifier as specified
	* by RFC 4122 (using random number).
	*
	* @return Unique identifier
	*/
	std::string generateUUID();
	
	/*
	 * Copyright (c) 2007 Alexey Vatchenko <av@bsdua.org>
	 *
	 * Permission to use, copy, modify, and/or distribute this software for any
	 * purpose with or without fee is hereby granted, provided that the above
	 * copyright notice and this permission notice appear in all copies.
	 *
	 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
	 */
	
	/**
	 *	This function translates UTF-8 string into UCS-4 string (all symbols
	 *	will be in local machine byte order).
	 *
	 *	@param in Input UTF-8 string. It can be null-terminated.
	 *	@param insize Size of input string in bytes.
	 *	@param out Result buffer for UCS-4 string. If out is NULL,
	 *		function returns size of result buffer.
	 *	@param outsize Size of out buffer in wide characters.
	 *
	 *	@return Size of result buffer (in wide characters).
	 *	Zero is returned in case of error.
	 *
	 * @post If UTF-8 string contains zero symbols, they will be translated
	 *	   as regular symbols.
	 * @post If UTF8_IGNORE_ERROR or UTF8_SKIP_BOM flag is set, sizes may vary
	 *	   when `out' is NULL and not NULL. It's because of special UTF-8
	 *	   sequences which may result in forbitten (by RFC3629) UNICODE
	 *	   characters.  So, the caller must check return value every time and
	 *	   not prepare buffer in advance (\0 terminate) but after calling this
	 *	   function.
	 */
	size_t utf8_to_wchar(const char *in, size_t insize, wchar_t *out, size_t outsize, int flags);
	
	/**
	 * Translates UCS-4 symbols (given in local machine
	 *	byte order) into UTF-8 string.
	 *
	 *	@param in Input unicode string. It can be null-terminated.
	 *	@param insize Size of input string in wide characters.
	 *	@param out Result buffer for utf8 string. If out is NULL,
	 *		function returns size of result buffer.
	 *	@param outsize Size of result buffer.
	 *
	 *	@return Size of result buffer (in bytes). Zero is returned
	 *	in case of error.
	 *
	 * @post If UCS-4 string contains zero symbols, they will be translated
	 *	as regular symbols.
	 */
	size_t wchar_to_utf8(const wchar_t *in, size_t insize, char *out, size_t outsize, int flags);
};

#endif

