#ifndef _BLUEROSE_BYTE_STREAM_READER_H_
#define _BLUEROSE_BYTE_STREAM_READER_H_

#include <vector>
#include "BRUtil.h"
#include "TypeDefinitions.h"

namespace BlueRose {


	/**
	* Reader for byte streams. Data types will
	* be decoded as specified by ICE (zeroc.com)
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class ByteStreamReader
	{
		protected:
			std::vector<byte>::iterator iterator; /**< Current position in the stream */
			std::vector<byte> stream; /**< Byte stream to read from */
	
		public:
			
			/**
			* Constructor
			*/
			ByteStreamReader();
		
			/**
			* Allows reading from the specified byte stream
			*
			* @param str Byte stream to read from 
			*/
			void open(const std::vector<byte>& str);
			
			/**
			* Avoids reading from the current byte stream
			*/
			void close();
		
			/**
			* Skips a number of bytes from the stream
			*
			* @param num Number of bytes to skip
			*/
			void skip(int num);
	
			/**
			* Reads a size from the stream
			*
			* @return Size extracted from the stream
			*/
			unsigned int readSize();
		
			/**
			* Reads from the current position to the end, but
			* without modifying the current position.
			*
			* @param bytes Bytes extracted
			*/
			void readToEnd(std::vector<byte>& bytes);
	
			/**
			* Reads a byte from the stream
			*
			* @return Byte extracted from the stream
			*/
			byte readByte();
			
			/**
			* Reads a boolean from the stream
			*
			* @return Boolean extracted from the stream
			*/
			bool readBoolean();
			
			/**
			* Reads a short from the stream
			*
			* @return Short extracted from the stream
			*/
			short readShort();
			
			/**
			* Reads an integer from the stream
			*
			* @return Integer extracted from the stream
			*/
			int readInteger();
			
			/**
			* Reads a long from the stream
			*
			* @return Long extracted from the stream
			*/
			long readLong();
			
			/**
			* Reads a float from the stream
			*
			* @return Float extracted from the stream
			*/
			float readFloat();
			
			/**
			* Reads a double from the stream
			*
			* @return Double extracted from the stream
			*/
			double readDouble();
			
			/**
			* Reads an string from the stream
			*
			* @param res String extracted from the stream
			*/
			void readString(std::string& res);
			
			/**
			* Reads an utf8 string from the stream
			*
			* @param res String extracted from the stream
			*/
			void readUTF8String(std::wstring& res);
		
			/**
			* Reads a byte sequence from the stream
			*
			* @return Byte sequence extracted from the stream
			*/
			void readByteSeq(std::vector<byte>& res);
			
			/**
			* Reads a boolean sequence from the stream
			*
			* @return Boolean sequence extracted from the stream
			*/
			void readBooleanSeq(std::vector<bool>& res);
			
			/**
			* Reads a short sequence from the stream
			*
			* @return Short sequence extracted from the stream
			*/
			void readShortSeq(std::vector<short>& res);
			
			/**
			* Reads an integer sequence from the stream
			*
			* @return Integer sequence extracted from the stream
			*/
			void readIntegerSeq(std::vector<int>& res);
			
			/**
			* Reads a long sequence from the stream
			*
			* @return Long sequence extracted from the stream
			*/
			void readLongSeq(std::vector<long>& res);
			
			/**
			* Reads a float sequence from the stream
			*
			* @return Float sequence extracted from the stream
			*/
			void readFloatSeq(std::vector<float>& res);
			
			/**
			* Reads a double sequence from the stream
			*
			* @return Double sequence extracted from the stream
			*/
			void readDoubleSeq(std::vector<double>& res);
			
			/**
			* Reads a string sequence from the stream
			*
			* @return String sequence extracted from the stream
			*/
			void readStringSeq(std::vector<std::string>& res);
			
			/**
			* Reads an utf8 string sequence from the stream
			*
			* @param res String extracted from the stream
			*/
			void readUTF8StringSeq(std::vector<std::wstring>& res);
		
			/**
			* Reads a dictionary from the stream
			*
			* @return Dictionary extracted from the stream
			*/
			void readDictionary(DictionaryDataType& res);
	};

};

#endif
