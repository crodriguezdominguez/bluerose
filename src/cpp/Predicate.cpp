#include "Predicate.h"
#include "ByteStreamWriter.h"
#include "ByteStreamReader.h"

namespace BlueRose {

Predicate::Predicate()
{
	
}

bool Predicate::addConstraint(const std::string& property, Comparison comp, const Value& value)
{
	if (!isConsistent(property, comp, value)) return false;
	else
	{
		std::pair<Comparison, Value> pair(comp, value);
		constraints[property].push_back(pair);
		
		return true;
	}
}

void Predicate::removeConstraints(const std::string& property)
{
	std::map<std::string, std::vector<std::pair<Comparison, Value> > >::iterator it = constraints.find(property);
	
	if (it != constraints.end())
	{
		constraints.erase(it);
	}
}

bool Predicate::isConsistent(const std::string& property, Comparison comp, const Value& value) const
{
	if (constraints.size() == 0) return true;
	else
	{
		PredicateIterator iter = constraints.find(property);
		if (iter == constraints.end()) return true;
		
		const std::vector<std::pair<Comparison, Value> > v = iter->second;
		for (unsigned int i=0; i<v.size(); i++)
		{
			//check inconsistencies
			const std::pair<Comparison, Value> pair = v[i];
			if (pair.second.compare(EQUAL, value)) //value == pair.second
			{
				if (pair.first == EQUAL)
				{
					if ( (comp == LESS) ||
					     (comp == GREATER) ||
					     (comp == NOT_EQUAL) ) 
						return false;
				}
				
				else if (pair.first == NOT_EQUAL)
				{
					if (comp == EQUAL) return false;
				}
				
				else if (pair.first == LESS)
				{
					if ( (comp == EQUAL) ||
					     (comp == GREATER) ||
						 (comp == GREATER_EQUAL) ) 
						return false;
				}
				
				else if (pair.first == LESS_EQUAL)
				{
					if (comp == GREATER) return false;
				}
					
				else if (pair.first == GREATER)
				{
					if ( (comp == EQUAL) ||
					     (comp == LESS) ||
						 (comp == LESS_EQUAL) ) 
						return false;
				}
				
				else if (pair.first == GREATER_EQUAL)
				{
					if (comp == LESS) return false;
				}
			}
			else //value != pair.second
			{
				if (pair.first == EQUAL) return false;
				
				else if (pair.first == NOT_EQUAL) return true;
				
				else if ( (pair.first == LESS) ||
						  (pair.first == LESS_EQUAL) )
				{
					if (value.compare(GREATER, pair.second))
					{
						if ( (comp == EQUAL) ||
						     (comp == GREATER) ||
							 (comp == GREATER_EQUAL) )
							return false;
					}
				}
				
				else if ( (pair.first == GREATER) ||
						  (pair.first == GREATER_EQUAL) )
				{
					if (value.compare(LESS, pair.second))
					{
						if ( (comp == EQUAL) ||
						     (comp == LESS) ||
							 (comp == LESS_EQUAL) )
							return false;
					}
				}
			}
		}
		
		return true;
	}
}

bool Predicate::isConsistent(const std::string& property, const Value& value) const
{
	return isConsistent(property, EQUAL, value);
}

bool Predicate::isEmpty() const
{
	return (constraints.size() == 0);
}

unsigned int Predicate::size() const
{
	return constraints.size();
}

PredicateIterator Predicate::begin() const
{
	return constraints.begin();
}

PredicateIterator Predicate::end() const
{
	return constraints.end();
}

void Predicate::marshall(ByteStreamWriter& writer) const
{	
	writer.writeSize(constraints.size());
	
	for (PredicateIterator it = this->begin(); it != this->end(); ++it)
	{
		writer.writeString(it->first);
		
		std::vector<std::pair<Comparison, Value> > cons = it->second;
		writer.writeSize(cons.size());
		for (unsigned int i=0; i<cons.size(); i++)
		{
			std::pair<Comparison, Value> pair = cons[i];
			writer.writeSize((unsigned int)pair.first);
			Value value = pair.second;
			writer.writeSize(value.getType());
			writer.writeByteSeq(value.getRawBytes());
		}
	}
}

void Predicate::unmarshall(ByteStreamReader& reader)
{
	unsigned int size = reader.readSize();
	std::string property;
	Comparison comp;
	Value value;
	ValueType type;
	std::vector<byte> rawBytes;
	
	constraints.clear();
	for (unsigned int i = 0; i < size; i++)
	{
		property = "";
		reader.readString(property);
		
		unsigned int size_v = reader.readSize();
		for (unsigned int j=0; j<size_v; j++)
		{
			comp = (Comparison)reader.readSize();
			type = (ValueType)reader.readSize();
			value.setType(type);
			reader.readByteSeq(rawBytes);
			value.setRawBytes(rawBytes);
			
			this->addConstraint(property, comp, value);
		}
	}
}
	
};
