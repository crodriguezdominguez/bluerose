#include "ReplyMessageStack.h"

namespace BlueRose {

std::map<int, std::vector<byte> > ReplyMessageStack::stack;
bool ReplyMessageStack::waitingReply = false;
pthread_mutex_t ReplyMessageStack::mutex;
pthread_cond_t ReplyMessageStack::cond;

void ReplyMessageStack::init()
{
	pthread_mutex_init(&mutex, 0);
	pthread_cond_init(&cond, 0);
}

void ReplyMessageStack::destroy()
{
	waitingReply = false;
	stack.clear();
	
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

void ReplyMessageStack::produce(int requestId, const std::vector<byte>& msg)
{
	pthread_mutex_lock(&mutex);
	stack[requestId] = msg;
	pthread_mutex_unlock(&mutex);
	
	pthread_cond_broadcast(&cond);
}

void ReplyMessageStack::consume(int requestId, std::vector<byte>& res)
{
	std::map<int, std::vector<byte> >::iterator it;
	
	pthread_mutex_lock(&mutex);
	
	while((it = stack.find(requestId)) == stack.end())
	{
		waitingReply = true;	
		pthread_cond_wait(&cond, &mutex);
	}
	
	res = it->second;
	stack.erase(it);
	waitingReply = false;
	
	pthread_mutex_unlock(&mutex);
	
	pthread_cond_broadcast(&cond);
}

bool ReplyMessageStack::isWaitingReply()
{
	return waitingReply;
}

};

