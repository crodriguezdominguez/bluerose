#include "DiscoveryProxy.h"
#include <unistd.h>

namespace BlueRose {

DiscoveryProxy* DiscoveryProxy::proxy = NULL;

DiscoveryProxy::DiscoveryProxy(const std::string& _servantID,
								ICommunicationDevice* _iface,
								bool waitOnline, 
								const DictionaryDataType& connParameters)
 : ObjectProxy(_servantID, _iface, waitOnline, connParameters)
{
	identity.name = "Discovery";
	identity.category = "BlueRoseService";
}

DiscoveryProxy::~DiscoveryProxy()
{
	
}

std::string DiscoveryProxy::getTypeID() const
{
	return "BlueRoseService::Discovery";
}

void DiscoveryProxy::init(const std::string& _servantID, 
							ICommunicationDevice* _iface,
 							bool waitOnline, 
							const DictionaryDataType& connParameters)
{
	if (proxy != NULL)
	{
		delete proxy;
	}
	
	proxy = new DiscoveryProxy(_servantID, _iface, waitOnline, connParameters);
}

void DiscoveryProxy::destroy()
{
	if (proxy != NULL)
	{
		delete proxy;
		proxy = NULL;
	}
}

DiscoveryProxy* DiscoveryProxy::defaultProxy()
{
	return proxy;
}

void DiscoveryProxy::addRegistration(const std::string& name, const std::string& address)
{
	int reqId;

	currentMode = BR_TWOWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;
	
	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	writer.writeString(name);
	writer.writeString(address);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "addRegistration", enc);
	receiveReply(reqId, result_bytes);
}

void DiscoveryProxy::removeRegistration(const std::string& name)
{
	int reqId;

	currentMode = BR_TWOWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;
	
	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	writer.writeString(name);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "removeRegistration", enc);
	receiveReply(reqId, result_bytes);
}

std::string DiscoveryProxy::resolve(const std::string& name, bool waitOnline)
{
	int reqId;

	currentMode = BR_TWOWAY_MODE;
	std::string result = "";

	do {
		
		std::vector<BlueRose::byte> result_bytes;
		std::vector<BlueRose::byte> enc;
	
		pthread_mutex_lock(&mutex_writer);
		writer.open(enc);
		writer.writeString(name);
		writer.close();
		pthread_mutex_unlock(&mutex_writer);

		reqId = sendRequest(servantID, "resolve", enc);
		receiveReply(reqId, result_bytes);
	
		pthread_mutex_lock(&mutex_reader);
		reader.open(result_bytes);
		reader.readString(result);
		reader.close();
		pthread_mutex_unlock(&mutex_reader);
		
		if (result == "" && waitOnline) sleep(1);
		
	} while(result == "" && waitOnline);
	
	return result;
}

};

