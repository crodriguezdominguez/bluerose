#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <unistd.h>

#include <iostream>

#include "Connector.h"

ReadThread::ReadThread(int sfd, Connector* conn) : Thread()
{
	sockfd = sfd;
	connector = conn;
}

void ReadThread::run()
{
	std::string str;
	char buffer[256];
	int n;
	
	pthread_detach(pthread_self());
	
	str = "";
	while ((n = read(sockfd,buffer,255)) > 0) //leer mensajes
	{
		str = str + buffer;
		
		if (buffer[n-1] == '\0') //ha llegado el final de la cadena
		{
			//ACCION QUE SE REALIZA AL RECIBIR UN MENSAJE
			connector->callBack(str);
			
			//reiniciar la cadena final a vacia para leer 
			//una nueva correctamente
			str = "";
		}
	}
	
	if (n == 0)
	{
		std::cerr << "ERROR conexion a servicio perdida" << std::endl;
		
		//cerrar el socket
		close(sockfd);
	}
}

Connector::Connector(const std::string& sh, int p)
{
	port = p;
	serviceHost = sh;
}

Connector::~Connector()
{
	close(sockfd);
	delete readThread;
}

void Connector::openConnection()
{
	int n, k;
    struct sockaddr_in serv_addr;
    struct hostent *server;
	
    //char buffer[256];
	
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
	{
		std::cerr << "ERROR abriendo el socket" << std::endl;
		exit(1);
	}
	
    server = gethostbyname(serviceHost.c_str());
    if (server == NULL) 
	{
		std::cerr << "ERROR host no encontrado" << std::endl;
		exit(1);
    }
	
    bzero((char *) &serv_addr, sizeof(serv_addr));
	
    serv_addr.sin_family = AF_INET;
	
    bcopy((char *)server->h_addr, 
		  (char *)&serv_addr.sin_addr.s_addr,
		  server->h_length);
	
    serv_addr.sin_port = htons(port);
	
    if (connect(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr)) < 0)
	{
		std::cerr << "ERROR conexion al host fallida" << std::endl;
		exit(1);
	}
	
	readThread = new ReadThread(sockfd, this);
	readThread->start();
}

void Connector::closeConnection()
{
	close(sockfd);
}

void Connector::write(const std::string& msg)
{
	::write(sockfd, msg.c_str(), msg.length()+1);
}
