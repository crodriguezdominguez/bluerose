#ifndef _TYPES_COMPATIBILITY_H_
#define _TYPES_COMPATIBILITY_H_

#include <vector>
#include <string>

const char* vectorToBuffer(const std::vector<char>& vector);

std::vector<char> bufferToVector(const char* buffer, int size);

std::string bufferToString(const char* buffer);

bool isLittleEndian();

std::vector<char> fromInt(int n);

std::vector<char> fromLong(long n);

std::vector<char> fromShort(short n);

std::vector<char> fromFloat(float n);

std::vector<char> fromDouble(double n);

int toInt(const std::vector<char>& b);

long toLong(const std::vector<char>& b);

short toShort(const std::vector<char>& b);

float toFloat(const std::vector<char>& b);

double toDouble(const std::vector<char>& b);

int toInt(const char* b);

long toLong(const char* b);

short toShort(const char* b);

float toFloat(const char* b);

double toDouble(const char* b);

#endif
