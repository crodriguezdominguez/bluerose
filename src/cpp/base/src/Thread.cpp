#include <pthread.h>
#include "Thread.h"

/*
* Funcion tipo C usada para habilitar la compatibilidad
* con hebras POSIX. (No todas las implementaciones de hebras
* POSIX admite usar punteros a metodos de una clase de C++).
*/
void* __thread_auxiliary_function(void* args)
{
	Thread* th = (Thread*)args;
	th->run();
	
	return NULL;
}

Thread::Thread()
{
	routine = &__thread_auxiliary_function;
	_args = (void*)this;
}

Thread::Thread(void* (*start_routine)(void*), void* args)
{
	routine = start_routine;
	_args = args;
}

Thread::~Thread()
{
	routine = NULL;
	pthread_cancel(_thread);
	pthread_detach(_thread);
}

void Thread::run()
{
	
}

void Thread::start()
{
	pthread_create(&_thread, NULL, routine, _args);
}

void Thread::cancel()
{
	pthread_cancel(_thread);
}

void Thread::join()
{
	pthread_join(_thread, NULL);
}

