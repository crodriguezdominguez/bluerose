#include "TypesCompatibility.h"

const char* vectorToBuffer(const std::vector<char>& vector)
{
	std::string t(vector.begin(), vector.end());
	
	return t.c_str();
}

std::vector<char> bufferToVector(const char* buffer, int size)
{
	std::vector<char> res;
	
	for(int i=0; i<size; i++)
		res.push_back(buffer[i]);
	
	return res;
}

std::string bufferToString(const char* buffer)
{
	std::string str = buffer;
	
	return str;
}

bool isLittleEndian()
{
	int one = 1;
	return (*((char*)(&one)));
}

std::vector<char> fromInt(int n)
{
	std::vector<char> res;
	char* p = (char*)&n;
	
	if (isLittleEndian())
	{
		res.push_back(p[0]);
		res.push_back(p[1]);
		res.push_back(p[2]);
		res.push_back(p[3]);
	}
	else
	{
		res.push_back(p[3]);
		res.push_back(p[2]);
		res.push_back(p[1]);
		res.push_back(p[0]);
	}
	
	return res;
}

std::vector<char> fromLong(long n)
{
	return fromInt((int)n);
}

std::vector<char> fromShort(short n)
{
	std::vector<char> res;
	char* p = (char*)&n;
	
	if (isLittleEndian())
	{
		res.push_back(p[0]);
		res.push_back(p[1]);
	}
	else
	{
		res.push_back(p[1]);
		res.push_back(p[0]);
	}
	
	return res;
}

std::vector<char> fromFloat(float n)
{
	std::vector<char> res;
	char* p = (char*)&n;
	
	if (isLittleEndian())
	{
		res.push_back(p[0]);
		res.push_back(p[1]);
		res.push_back(p[2]);
		res.push_back(p[3]);
	}
	else
	{
		res.push_back(p[3]);
		res.push_back(p[2]);
		res.push_back(p[1]);
		res.push_back(p[0]);
	}
	
	return res;
}

std::vector<char> fromDouble(double n)
{
	return fromFloat((float)n);
}

int toInt(const std::vector<char>& b)
{
	int i;
    char *p = (char *)&i;
	
    if (isLittleEndian()) 
	{
        p[0] = b[0];
        p[1] = b[1];
        p[2] = b[2];
        p[3] = b[3];
    } 
	else 
	{
        p[0] = b[3];
        p[1] = b[2];
        p[2] = b[1];
        p[3] = b[0];
    }
	
    return i;
}

long toLong(const std::vector<char>& b)
{
	return (long)toInt(b);
}

short toShort(const std::vector<char>& b)
{
	short s;
    char *p = (char *)&s;
	
    if (isLittleEndian()) 
	{
        p[0] = b[0];
        p[1] = b[1];
    } 
	else 
	{
        p[0] = b[1];
        p[1] = b[0];
    }
	
    return s;
}

float toFloat(const std::vector<char>& b)
{
	float i;
    char *p = (char *)&i;
	
    if (isLittleEndian()) 
	{
        p[0] = b[0];
        p[1] = b[1];
        p[2] = b[2];
        p[3] = b[3];
    } 
	else 
	{
        p[0] = b[3];
        p[1] = b[2];
        p[2] = b[1];
        p[3] = b[0];
    }
	
    return i;
}

double toDouble(const std::vector<char>& b)
{
	return (double)toDouble(b);
}

int toInt(const char* b)
{
	int i;
    char *p = (char *)&i;
	
    if (isLittleEndian()) 
	{
        p[0] = b[0];
        p[1] = b[1];
        p[2] = b[2];
        p[3] = b[3];
    } 
	else 
	{
        p[0] = b[3];
        p[1] = b[2];
        p[2] = b[1];
        p[3] = b[0];
    }
	
    return i;
}

long toLong(const char* b)
{
	return (long)toInt(b);
}

short toShort(const char* b)
{
	short s;
    char *p = (char *)&s;
	
    if (isLittleEndian()) 
	{
        p[0] = b[0];
        p[1] = b[1];
    } 
	else 
	{
        p[0] = b[1];
        p[1] = b[0];
    }
	
    return s;
}

float toFloat(const char* b)
{
	float i;
    char *p = (char *)&i;
	
    if (isLittleEndian()) 
	{
        p[0] = b[0];
        p[1] = b[1];
        p[2] = b[2];
        p[3] = b[3];
    } 
	else 
	{
        p[0] = b[3];
        p[1] = b[2];
        p[2] = b[1];
        p[3] = b[0];
    }
	
    return i;
}

double toDouble(const char* b)
{
	return (double)toDouble(b);
}

