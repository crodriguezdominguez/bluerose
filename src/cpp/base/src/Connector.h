#ifndef _CONNECTOR_H_
#define _CONNECTOR_H_

#include <string>
#include "Thread.h"

//declaraciones adelantadas
class Connector;
class ReadThread;

class ReadThread : public Thread {
public:
	ReadThread(int sfd, Connector* conn);
	
	void run();
	
protected:
	int sockfd;
	Connector* connector;
};

class Connector {
public:
	Connector(const std::string& sh, int p);
	~Connector();
	
	/**
	* Funcion que se ejecuta al recibir un mensaje
	* desde el servicio
	*
	* @param msg Mensaje recibido
	*/
	virtual void callBack(const std::string& msg) = 0;
	void write(const std::string& msg);
	
	void openConnection();
	void closeConnection();
	
protected:
	int sockfd;
	std::string serviceHost;
	int port;
	ReadThread* readThread;
};

#endif
