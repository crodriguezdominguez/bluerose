#ifndef _SERVICE_H_
#define _SERVICE_H_

#include <list>
#include "Thread.h"

class Service;
class ClientThread;

class ClientThread : public Thread {
public:
	ClientThread(int sfd, Service* s);
	void run();

protected:
	int sockfd;
	Service* service;
};

class Service : public Thread {
public:
	Service(int p, bool trace=true);
	~Service();
	
	void run();
	void waitToEnd();
	void broadCast(const std::string& msg);
	void removeClient(int socket);
	
protected:
	int sockfd;
	int port;
	pthread_mutex_t mutex;
	std::list<struct sockaddr_in> clients_data;
	std::list<int> clients;
	bool trace;
};

#endif

