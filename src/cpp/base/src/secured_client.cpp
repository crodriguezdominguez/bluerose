#include <iostream>
#include <cstdlib>
#include <cstring>
#include "SecuredConnector.h"

class ConnectorTest : public SecuredConnector {
public:
	ConnectorTest(const std::string& host, int port) : SecuredConnector(host, port)
	{
	}
	
	void callBack(const std::string& msg)
	{
		std::cout << "Mensaje recibido: " << msg << std::endl;
	}
};

int main(int argc, char *argv[])
{	
	char buffer[256];
	
	if (argc < 3) {
		std::cerr << "Uso: " << argv[0] << " <host> <puerto>" << std::endl;
		exit(0);
    }
	
	ConnectorTest conn(argv[1], atoi(argv[2]));
	conn.openConnection();
	
	do {
		std::cin.getline(buffer, 256);
		if (strcmp(buffer, "FIN") != 0) conn.write(buffer);
	} while(strcmp(buffer, "FIN") != 0);
	
	conn.closeConnection();
	
	conn.openConnection();
	
	conn.write("He terminado");
	
	conn.closeConnection();
	
    return 0;
}
