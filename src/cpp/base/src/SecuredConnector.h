#ifndef _SECURED_CONNECTOR_H_
#define _SECURED_CONNECTOR_H_

#include <string>
#include <openssl/ssl.h>
#include "Connector.h"

class SecuredReadThread : public ReadThread {
public:
	SecuredReadThread(SSL* ssl_, int sfd, Connector* conn);
	
	void run();
	
private:
	SSL* ssl;
};

class SecuredConnector : public Connector {
public:
	SecuredConnector(const std::string& sh, int p);
	~SecuredConnector();
	
	virtual void callBack(const std::string& msg) = 0;
	void write(const std::string& msg);
	
	void openConnection();
	
protected:
	SSL_CTX* ctx;
	SSL* ssl;
};

#endif
