#ifndef __AUXILIARY___THREAD_H__
#define __AUXILIARY___THREAD_H__

#include <pthread.h>

/**
* Clase wrapper para hebras del sistema. Si se desea portar el codigo
* a sistemas que no usen hebras POSIX, tan solo se debera modificar esta
* clase concreta.
*
* @author Carlos Rodriguez Dominguez
* @date 05/11/2008
*/
class Thread {
public:
	/**
	* Constructor por defecto. Si se usa este
	* constructor, entonces se debera heredar de esta
	* clase y reimplementar el metodo "run".
	*/
	Thread();
	
	/**
	* Constructor con parametros
	*
	* @param start_routine Puntero a la funcion asociada a la hebra
	* @param args Vector con los argumentos de la funcion
	*/
	Thread(void* (*start_routine)(void*), void* args);
	
	/**
	* Destructor de la clase. Cancelara la ejecucion de la hebra
	* y la eliminara.
	*/
	virtual ~Thread();
	
	/**
	* Comienza la ejecucion de la hebra
	*/
	virtual void start();
	
	/**
	* Cancela la ejecucion de la hebra. No la destruye,
	* con lo que se puede usar el metodo start para iniciarla
	* de nuevo.
	*/
	virtual void cancel();
	
	/**
	* Evita el fin de la aplicacion que ejecuta la hebra hasta
	* que esta no termine.
	*/
	virtual void join();
	
	/**
	* Metodo a reimplementar en caso de heredar de esta clase
	* y usar el constructor por defecto. En caso de heredar
	* de esta clase, no conviene realizar llamadas explicitas
	* a este metodo. Usar el metodo "start" para que se encargue
	* de llamarlo correctamente.
	*/
	virtual void run();
	
private:
	pthread_t _thread; /**< Hebra POSIX. */
	void* (*routine)(void*); /**< Puntero a la funcion a ejecutar */
	void* _args; /**< Argumentos de la funcion a ejecutar */
};

#endif
