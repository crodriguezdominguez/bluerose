#ifndef _SECURED_SERVICE_H_
#define _SECURED_SERVICE_H_

#include <list>
#include <openssl/ssl.h>
#include "Thread.h"
#include "Service.h"

class SecuredClientThread : public ClientThread {
public:
	SecuredClientThread(SSL* ssl_, int sfd, Service* s);
	void run();
	
protected:
	SSL* ssl;
};

class SecuredService : public Service {
public:
	SecuredService(int p, bool trace=true);
	~SecuredService();
	
	void removeClient(SSL* ssl);
	void run();
	void broadCast(const std::string& msg);
	
protected:
	std::list<SSL*> ssl_clients;
	SSL* ssl;
	SSL_CTX* ctx;
};

#endif

