#include <iostream>
#include <cstdlib>

#include "Service.h"


int main(int argc, char *argv[])
{
	if (argc < 2) {
		std::cerr << "Uso: " << argv[0] << " <puerto>" << std::endl;
		exit(0);
    }
	
	int port = atoi(argv[1]);
	Service serv(port);
	serv.start();
	serv.waitToEnd();
	
	return 0; 
}
