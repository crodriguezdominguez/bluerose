#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include "SocketCollection.h"

namespace BlueRose {

SocketCollection::SocketCollection()
{
	pthread_mutex_init(&mutex, 0);
}

SocketCollection::~SocketCollection()
{
	this->clear();
	pthread_mutex_destroy(&mutex);
}

void SocketCollection::addSocket(const std::string& userID, int socket)
{
	pthread_mutex_lock(&mutex);
	sockets[userID] = socket;
	pthread_mutex_unlock(&mutex);
}

void SocketCollection::removeSocket(const std::string& userID)
{
	pthread_mutex_lock(&mutex);

	std::map<std::string, int>::iterator it = sockets.find(userID);
	if (it != sockets.end())
	{
		shutdown(it->second, SHUT_RDWR);
		close(it->second);
		sockets.erase(it);
	}

	pthread_mutex_unlock(&mutex);
}

void SocketCollection::clear()
{
	pthread_mutex_lock(&mutex);
	for (std::map<std::string, int>::iterator it = sockets.begin(); it != sockets.end(); ++it)
	{
		close(it->second);
	}

	sockets.clear();
	pthread_mutex_unlock(&mutex);
}

int SocketCollection::getSocket(const std::string& userID)
{
	int dummy = -1;

	std::map<std::string, int>::iterator it = sockets.find(userID);
	if (it != sockets.end())
	{
		dummy = it->second;
	}

	return dummy;
}

void SocketCollection::lockCollection()
{
	pthread_mutex_lock(&mutex);
}

void SocketCollection::unlockCollection()
{
	pthread_mutex_unlock(&mutex);
}

std::vector<std::string> SocketCollection::getUsers()
{
	std::vector<std::string> res;

	pthread_mutex_lock(&mutex);

	for (std::map<std::string, int>::iterator it = sockets.begin(); it != sockets.end(); ++it)
	{
		res.push_back(it->first);
	}

	pthread_mutex_unlock(&mutex);

	return res;
}

bool SocketCollection::someoneConnected()
{
	bool res = false;
	
	pthread_mutex_lock(&mutex);
	if (sockets.size() > 0) res = true;
	pthread_mutex_unlock(&mutex);
	
	return res;
}

SocketIterator SocketCollection::begin()
{
	return sockets.begin();
}

SocketIterator SocketCollection::end()
{
	return sockets.end();
}

/*
SocketCollection::SocketCollection()
{
	pthread_mutex_init(&mutex, 0);
}

SocketCollection::~SocketCollection()
{
	this->clear();
	pthread_mutex_destroy(&mutex);
}

void SocketCollection::addSocket(Socket* socket)
{
	if (socket != NULL)
	{
		pthread_mutex_lock(&mutex);
		sockets[socket->getUserID()] = socket;
		pthread_mutex_unlock(&mutex);
	}
}

void SocketCollection::removeSocket(const std::string& userID)
{
	pthread_mutex_lock(&mutex);
	
	std::map<std::string, Socket*>::iterator it = sockets.find(userID);
	if (it != sockets.end())
	{
		delete it->second;
		sockets.erase(it);
	}
	
	pthread_mutex_unlock(&mutex);
}

void SocketCollection::clear()
{
	pthread_mutex_lock(&mutex);
	for (std::map<std::string, Socket*>::iterator it = sockets.begin(); it != sockets.end(); ++it)
	{
		delete it->second;
	}
	
	sockets.clear();
	pthread_mutex_unlock(&mutex);
}

Socket* SocketCollection::getSocket(const std::string& userID)
{
	Socket* dummy = NULL;
	
	std::map<std::string, Socket*>::iterator it = sockets.find(userID);
	if (it != sockets.end())
	{
		dummy = it->second;
	}
	
	return dummy;
}

void SocketCollection::lockCollection()
{
	pthread_mutex_lock(&mutex);
}

void SocketCollection::unlockCollection()
{
	pthread_mutex_unlock(&mutex);
}

std::vector<std::string> SocketCollection::getUsers()
{
	std::vector<std::string> res;
	
	pthread_mutex_lock(&mutex);
	
	for (std::map<std::string, Socket*>::iterator it = sockets.begin(); it != sockets.end(); ++it)
	{
		res.push_back(it->first);
	}
	
	pthread_mutex_unlock(&mutex);
	
	return res;
}

bool SocketCollection::someoneConnected()
{
	pthread_mutex_lock(&mutex);
	
	for (std::map<std::string, Socket*>::iterator it = sockets.begin(); it != sockets.end(); ++it)
	{
		if (it->second->isConnected())
		{
			pthread_mutex_unlock(&mutex);
			return true;
		}
	}
	
	pthread_mutex_unlock(&mutex);
	return false;
}*/

};
