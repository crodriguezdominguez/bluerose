#include "Initializer.h"
#include "DevicePool.h"
#include "DiscoveryProxy.h"
#include "EventHandler.h"

namespace BlueRose {

Configuration* Initializer::configuration = NULL;

void Initializer::initialize(const std::string& configFile)
{
	configuration = new Configuration(configFile);
}

void Initializer::initializeClient(ICommunicationDevice* device, bool withDiscoveryServant, bool withEventHandler)
{
	DevicePool::addDevice(device);
	DevicePool::init();
	
	if (withDiscoveryServant)
	{
		if (DiscoveryProxy::defaultProxy() == NULL)
		{
			std::string discoveryAddress = configuration->getUserID("BlueRoseService", "Discovery", device->getName());
			
			if (discoveryAddress != "")
			{
				DiscoveryProxy::defaultProxy()->init(discoveryAddress, device);
			}
			else
			{
				throw "Discovery servant not configured in config file";
			}
		}
	}
	
	if (withEventHandler)
	{
		if (withDiscoveryServant)
			EventHandler::initEventHandler(device);
			
		else //extract from config file
		{
			std::string pub_sub = configuration->getUserID("BlueRoseService", "PubSub", device->getName());
			
			if (pub_sub != "")
			{
				EventHandler::initEventHandler(pub_sub, device);
			}
			else
			{
				throw "PubSub servant not configured in config file";
			}
		}
	}
}

/*void Initializer::initializeMobileServant(ObjectServant* servant, ICommunicationDevice* device, IMobileDiscovery* discoverer, int serviceType)
{
	DevicePool::addDevice(device);
	DevicePool::init();
	
	Identity id = servant->getIdentity();
	std::string port = configuration->getPort(id.category, id.name, device->getName());
	
	if (port == "")
	{
		throw "Port not specified in config file";
	}
	
	//set as servant
	device->setServant(port);
	
	if (DiscoveryProxy::defaultProxy() == NULL)
	{
		//search for the discoveryProxy with the discoverer
		std::string discoveryAddress = discoverer->resolveUserID("BlueRoseService::Discovery", serviceType);
		
		if (discoveryAddress != "")
		{
			DiscoveryProxy::defaultProxy()->init(discoveryAddress, device);
		}
		else
		{
			throw "Discovery servant not configured in config file";
		}
	}

	discoverer->registerForDetection(servant, device);
}*/

void Initializer::initializeServant(ObjectServant* servant, ICommunicationDevice* device, bool registerAtDiscoveryServant)
{
	DevicePool::addDevice(device);
	DevicePool::init();
	
	Identity id = servant->getIdentity();
	std::string address = configuration->getAddress(id.category, id.name, device->getName());
	std::string port = configuration->getPort(id.category, id.name, device->getName());
	
	if (port == "")
	{
		throw "Port not specified in config file";
	}
	
	//set as servant
	device->setServantIdentifier(address);
	device->setServant(port);
	
	if (registerAtDiscoveryServant)
	{
		if (DiscoveryProxy::defaultProxy() == NULL)
		{
			std::string discoveryAddress = configuration->getUserID("BlueRoseService", "Discovery", device->getName());
			
			if (discoveryAddress != "")
			{
				DiscoveryProxy::defaultProxy()->init(discoveryAddress, device);
			}
			else
			{
				throw "Discovery servant not configured in config file";
			}
		}

		DiscoveryProxy::defaultProxy()->addRegistration(servant->getIdentifier(), device->servantIdentifier());
	}
}

Configuration* Initializer::getConfiguration()
{
	return configuration;
}

void Initializer::destroy()
{
	if (configuration != NULL)
		delete configuration;
		
	DevicePool::destroy();
	
	if (DiscoveryProxy::defaultProxy() != NULL)
	{
		DiscoveryProxy::destroy();
	}
	
	BlueRose::EventHandler::destroyEventHandler();
}

};
