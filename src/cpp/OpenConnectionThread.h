#ifndef _BLUEROSE_OPEN_CONNECTION_THREAD_H_
#define _BLUEROSE_OPEN_CONNECTION_THREAD_H_

#include <limits.h>
#include "Thread.h"
#include "ICommunicationDevice.h"

namespace BlueRose {

/**
* Thread for openning a new connection whenever connection
* to a service is lost.
*
* @author Carlos Rodriguez Dominguez
* @date 15-12-2009
*/
class OpenConnectionThread : public Thread
{
	protected:
		ICommunicationDevice* interface; /**< Transmission interface for the thread */
		std::string userID; /**< User identificator of the sender */
		DictionaryDataType parameters;
		int maxRetries;
		
	public:
		/**
		* Default Constructor
		*
		* @param iface Interface for openning the connection
		* @param _userID Id of the user to open the connection with
		* @param mRetries Max number of retries before ending the thread. if mRetries==INT_MAX, it will retry forever.
		* @param pars Parameters of the connection 
		*/
		OpenConnectionThread(ICommunicationDevice* iface, const std::string& _userID, int mRetries=INT_MAX, const DictionaryDataType& pars=DictionaryDataType());
		
		/**
		* Overload of the main method of the thread
		*/
		void run();
};

};

#endif
