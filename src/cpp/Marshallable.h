#ifndef _BLUEROSE_MARSHALLABLE_H_
#define _BLUEROSE_MARSHALLABLE_H_

#include <vector>
#include "ByteStreamReader.h"
#include "ByteStreamWriter.h"
#include "TypeDefinitions.h"

namespace BlueRose {

	class Marshallable {
		public:
			virtual void marshall(ByteStreamWriter& writer) const = 0;
			virtual void unmarshall(ByteStreamReader& reader) = 0;
	};
	
};

#endif
