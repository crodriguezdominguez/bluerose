#include <cstdio>
#include "Message.h"
#include "RequestReadThread.h"
#include "DevicePool.h"

namespace BlueRose {


RequestReadThread::RequestReadThread(ICommunicationDevice* iface, const std::string _userID)
{
	interface = iface;
	userID = _userID;
}

void RequestReadThread::run()
{
	pthread_detach(pthread_self());
	
	std::vector<byte> bytes;
	
	//send validate connection msg
	ValidateConnectionMessage msg;
	msg.bytes(bytes);
	interface->write(userID, bytes);
	bytes.clear();
	
	while ( interface->read(userID, bytes) ) //read messages
	{
		if (bytes[8] == BR_CLOSE_CONNECTION_MSG)
		{
			break;
		}
		
		if (bytes[8] == BR_REQUEST_MSG)
		{
			//process message
			DevicePool::dispatchMessage(userID, bytes);
		}
		
		bytes.clear();
	}
	
	printf("Connection closed: %s\n", userID.c_str());

	interface->closeConnection(userID);
}


};


