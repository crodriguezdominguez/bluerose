#ifndef _BLUEROSE_I_COMM_DEVICE_H_
#define _BLUEROSE_I_COMM_DEVICE_H_

#include <string>
#include <vector>

#include "TypeDefinitions.h"
#include "Thread.h"

namespace BlueRose {

	/**
	* Generic representation of transmission channel
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class ICommunicationDevice : public Thread
	{
		public:
			virtual ~ICommunicationDevice(){}
			
			/**
			* Open a new connection between local user and a remote one
			*
			* @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
			* @param parameters A dictionary containing some parameters for the connection (i.e.: QoS)
			*/
			virtual void openConnection(std::string userID, DictionaryDataType parameters = DictionaryDataType()) = 0;
			
			/**
			* Checks whether the connection to an specific user is openned or not
			*
			* @param userID User to check whether the connection is openned with or not.
			* @return True if the connection is openned. False otherwise else.
			*/
			virtual bool isConnectionOpenned(std::string userID) = 0;
			
			/**
			* Close a connection between local user and a remote one
			*
			* @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
			*/
			virtual void closeConnection(std::string userID) = 0;
			
			/**
			* Close all connections in this transmission channel
			*/
			virtual void closeAllConnections() = 0;
			
			/**
			* Send a message to a remote user
			*
			* @param readerID Receiver identification
			* @param data Message to send
			*/
			virtual bool write(std::string readerID, std::vector<byte> msg) = 0;
			
			/**
			* Receive a message from a sender
			*
			* @param senderID Identification of the sender
			* @return Received message
			*/
			virtual bool read(std::string senderID, std::vector<byte>& msg) = 0;
			
			/**
			* Set priority of the transmission channel in the interface pool
			*
			* @param priority Priority of the channel
			*/
			virtual void setPriority(int priority) = 0;
			
			/**
			* Get priority of the transmission channel in the interface pool
			*
			* @return Priority
			*/
			virtual int getPriority() = 0;
			
			/**
			* Broadcast a message to all neighbour users
			*
			* @param data Message to send
			*/
			virtual void broadcast(std::vector<byte> data) = 0;
			
			/**
			* Checks if writing or reading is blockant in this channel
			*
			* @return True if it's blockant. False otherwise else.
			*/
			virtual bool isBlockantConnection() = 0;
			
			/**
			* Checks if the transmission of data is possible
			*
			* @return True if is available. False otherwise else.
			*/
			virtual bool isAvailable() = 0;
			
			/**
			* Checks if the transmission of data to an specific user is possible
			*
			* @return True if is available. False otherwise else.
			*/
			virtual bool isAvailable(std::string userID) = 0;
			
			/**
			* Checks if the transmission interface is connection oriented
			*/
			virtual bool isConnectionOriented() = 0;
			
			/**
			* Sets the current identifier of the servant (i.e. the ip address in case of TCP)
			*
			* @param servantID Identifier of the servant (i.e. the ip address in case of TCP)
			*/
			virtual void setServantIdentifier(std::string servantID) = 0;
			
			/**
			* Sets the current machine as a servant
			*
			* @param multiplexingId Id for multiplexing connections (i.e. a port in case of TCP)
			*/
			virtual void setServant(std::string multiplexingId) = 0;
			
			/**
			* Waits for connections. Only for servants.
			*/
			virtual void waitForConnections() = 0;
			
			/**
			* Returns the name of the interface
			*
			* @return Name of the interface
			*/
			virtual std::string getName() = 0;
			
			/**
			* If it's a servant, it returns the identifier
			*
			* @return Identifier of the current servant or "" if it's not a servant
			*/
			virtual std::string servantIdentifier() = 0;
	};

};

#endif

