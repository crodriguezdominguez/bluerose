#include <vector>
#include <dns_sd.h>
#include "IMobileDiscovery.h"
#include "BRUtil.h"

namespace BlueRose {

MobileDiscoveryListener::MobileDiscoveryListener(IMobileDiscovery* d)
{
	discovery = d;
}

MobileDiscoveryListener::~MobileDiscoveryListener()
{
}

void MobileDiscoveryListener::onElementDiscovered(const std::string& name, 
							const std::string& regtype, 
							const std::string& domain,
							bool moreComing, bool error)
{
}
	
void MobileDiscoveryListener::onElementRegistered(const std::string& fullname, 
								const std::string& userID,
								const TXTRecord& records, 
								bool moreComing, bool error)
{
}
		
void MobileDiscoveryListener::onElementRemoved(const std::string& name, 
							const std::string& regtype, 
							const std::string& domain,
							bool moreComing, bool error)
{
}
		
void MobileDiscoveryListener::onUserIDResolved(const std::string& fullname, 
								const std::string& userID,
								const TXTRecord& records,
								bool moreComing, bool error)
{
}
													
IMobileDiscovery* MobileDiscoveryListener::getMobileDiscovery()
{
	return discovery;
}

TXTRecord::TXTRecord()
{
	
}

unsigned int TXTRecord::size() const
{
	return records.size();
}

std::string TXTRecord::getValue(const std::string& key) const
{
	RecordMap::const_iterator it = records.find(key);
	
	if (it != records.end())
	{
		return it->second;
	}
	
	return "";
}

RecordMap::const_iterator TXTRecord::begin() const
{
	return records.begin();
}

RecordMap::const_iterator TXTRecord::end() const
{
	return records.end();
}

TXTRecord TXTRecord::parse(uint16_t txtLen, const unsigned char* txtRecords)
{
	TXTRecord rec;
	std::vector<std::string> results;
	char key[256];
	void* value;
	
	uint16_t size = TXTRecordGetCount(txtLen, txtRecords);
	for (uint16_t i=0; i<size; i++)
	{
		uint8_t tamValue = 0;
		TXTRecordGetItemAtIndex(txtLen, txtRecords, i, 255, key, &tamValue, (const void**)&value);
		
		std::string val = "";
		for (uint8_t j=0; j<tamValue; j++)
			val += (char)(*(((unsigned char*)value)+j));
			
		rec.records[key] = val;
	}
	
	return rec;
}

};
