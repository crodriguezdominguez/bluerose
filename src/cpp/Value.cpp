#include "Value.h"
#include "ByteStreamReader.h"
#include "ByteStreamWriter.h"

namespace BlueRose {

ByteStreamWriter Value::writer;
pthread_mutex_t Value::mutex = PTHREAD_MUTEX_INITIALIZER;
ByteStreamReader Value::reader;
pthread_mutex_t Value::mutex_reader = PTHREAD_MUTEX_INITIALIZER;

Value::Value()
{
	type = NULL_TYPE;
}

Value::Value(ValueType _type)
{
	type = _type;
}

ValueType Value::getType() const
{
	return type;
}

void Value::setType(ValueType _type)
{
	type = _type;
}

std::vector<byte> Value::getRawBytes() const
{
	return rawValue;
}

void Value::setRawBytes(const std::vector<byte>& _value)
{
	rawValue = _value;
}

void Value::setByte(byte value)
{
	type = BYTE;
	rawValue.clear();
	
	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeByte(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setInteger(int value)
{
	type = INTEGER;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeInteger(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setShort(short value)
{
	type = SHORT;
	rawValue.clear();
	
	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeShort(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setLong(long value)
{
	type = LONG;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeLong(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setBoolean(bool value)
{
	type = BOOLEAN;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeBoolean(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setFloat(float value)
{
	type = FLOAT;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeFloat(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setDouble(double value)
{
	type = DOUBLE;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeDouble(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setString(std::wstring& value)
{
	type = STRING;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeUTF8String(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setByteSeq(std::vector<byte>& value)
{
	type = SEQ_BYTE;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeByteSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setIntegerSeq(std::vector<int>& value)
{
	type = SEQ_INTEGER;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeIntegerSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setShortSeq(std::vector<short>& value)
{
	type = SEQ_SHORT;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeShortSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setLongSeq(std::vector<long>& value)
{
	type = SEQ_LONG;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeLongSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setBooleanSeq(std::vector<bool>& value)
{
	type = SEQ_BOOLEAN;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeBooleanSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setFloatSeq(std::vector<float>& value)
{
	type = SEQ_FLOAT;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeFloatSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setDoubleSeq(std::vector<double>& value)
{
	type = SEQ_DOUBLE;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeDoubleSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

void Value::setStringSeq(std::vector<std::wstring>& value)
{
	type = SEQ_STRING;
	rawValue.clear();

	pthread_mutex_lock(&mutex);
	writer.open(rawValue);
	writer.writeUTF8StringSeq(value);
	writer.close();
	pthread_mutex_unlock(&mutex);
}

byte Value::getByte() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	byte res = reader.readByte();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

int Value::getInteger() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	int res = reader.readInteger();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

short Value::getShort() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	short res = reader.readShort();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

long Value::getLong() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	long res = reader.readLong();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

bool Value::getBoolean() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	bool res = reader.readBoolean();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

float Value::getFloat() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	float res = reader.readFloat();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

double Value::getDouble() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	double res = reader.readDouble();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::wstring Value::getString() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::wstring res;
	reader.readUTF8String(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<byte> Value::getByteSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<byte> res;
	reader.readByteSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<int> Value::getIntegerSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<int> res;
	reader.readIntegerSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<short> Value::getShortSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<short> res;
	reader.readShortSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<long> Value::getLongSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<long> res;
	reader.readLongSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<bool> Value::getBooleanSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<bool> res;
	reader.readBooleanSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<float> Value::getFloatSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<float> res;
	reader.readFloatSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<double> Value::getDoubleSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<double> res;
	reader.readDoubleSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

std::vector<std::wstring> Value::getStringSeq() const
{
	pthread_mutex_lock(&mutex_reader);
	reader.open(rawValue);
	std::vector<std::wstring> res;
	reader.readUTF8StringSeq(res);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return res;
}

bool Value::isNull() const
{
	return (type == NULL_TYPE);
}

bool Value::less_than(const Value& value) const
{
	//"switch" avoided for performance gainings
	if (type == BYTE) return (getByte() < value.getByte());
	else if (type == SHORT) return (getShort() < value.getShort());
	else if (type == INTEGER) return (getInteger() < value.getInteger());
	else if (type == LONG) return (getLong() < value.getLong());
	else if (type == FLOAT) return (getFloat() < value.getFloat());
	else if (type == DOUBLE) return (getDouble() < value.getDouble());
	else if (type == STRING) return (getString() < value.getString());
	else if (type == SEQ_BYTE) return (getByteSeq() < value.getByteSeq());
	else if (type == SEQ_SHORT) return (getShortSeq() < value.getShortSeq());
	else if (type == SEQ_INTEGER) return (getIntegerSeq() < value.getIntegerSeq());
	else if (type == SEQ_LONG) return (getLongSeq() < value.getLongSeq());
	else if (type == SEQ_FLOAT) return (getFloatSeq() < value.getFloatSeq());
	else if (type == SEQ_DOUBLE) return (getDoubleSeq() < value.getDoubleSeq());
	else if (type == SEQ_STRING) return (getStringSeq() < value.getStringSeq());
	else return false;
}

bool Value::less_equal_than(const Value& value) const
{
	return !greater_than(value);
}

bool Value::greater_than(const Value& value) const
{
	//"switch" avoided for performance gainings
	if (type == BYTE) return (getByte() > value.getByte());
	else if (type == SHORT) return (getShort() > value.getShort());
	else if (type == INTEGER) return (getInteger() > value.getInteger());
	else if (type == LONG) return (getLong() > value.getLong());
	else if (type == FLOAT) return (getFloat() > value.getFloat());
	else if (type == DOUBLE) return (getDouble() > value.getDouble());
	else if (type == STRING) return (getString() > value.getString());
	else if (type == SEQ_BYTE) return (getByteSeq() > value.getByteSeq());
	else if (type == SEQ_SHORT) return (getShortSeq() > value.getShortSeq());
	else if (type == SEQ_INTEGER) return (getIntegerSeq() > value.getIntegerSeq());
	else if (type == SEQ_LONG) return (getLongSeq() > value.getLongSeq());
	else if (type == SEQ_FLOAT) return (getFloatSeq() > value.getFloatSeq());
	else if (type == SEQ_DOUBLE) return (getDoubleSeq() > value.getDoubleSeq());
	else if (type == SEQ_STRING) return (getStringSeq() > value.getStringSeq());
	else return false;
}

bool Value::greater_equal_than(const Value& value) const
{
	return !less_than(value);
}

bool Value::compare(Comparison comp, const Value& value) const
{
	if (type != value.getType()) return false;

	if (comp == EQUAL)
	{
		if (rawValue == value.rawValue) return true;
		else return false;
	}

	if (comp == NOT_EQUAL)
	{
		if (rawValue != value.rawValue) return true;
		else return false;
	}

	if (comp == LESS) return (less_than(value));
	else if (comp == LESS_EQUAL) return (less_equal_than(value));
	else if (comp == GREATER) return (greater_than(value));
	else if (comp == GREATER_EQUAL) return (greater_equal_than(value));
	else return false;
}

void Value::marshall(ByteStreamWriter& writer) const
{
	writer.writeSize((int)type);
	writer.writeByteSeq(rawValue);
}

void Value::unmarshall(ByteStreamReader& reader)
{
	type = (ValueType)reader.readSize();
	rawValue.clear();
	reader.readByteSeq(rawValue);
}
	
};
