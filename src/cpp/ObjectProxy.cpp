#include <new>
#include "ObjectProxy.h"
#include "DevicePool.h"
#include "ReplyMessageStack.h"
#include "MessageHeader.h"
#include "OpenConnectionThread.h"
#include "ByteStreamWriter.h"
#include "DiscoveryProxy.h"

namespace BlueRose {


/**************************************/
/*               Object               */
/**************************************/

int ObjectProxy::requestId = 0;
AsyncCallback* ObjectProxy::asyncThread = NULL;
int ObjectProxy::numInstances = 0;
pthread_mutex_t ObjectProxy::shared_mutex = PTHREAD_MUTEX_INITIALIZER;

ByteStreamWriter ObjectProxy::writer;
pthread_mutex_t ObjectProxy::mutex_writer = PTHREAD_MUTEX_INITIALIZER;
ByteStreamReader ObjectProxy::reader;
pthread_mutex_t ObjectProxy::mutex_reader = PTHREAD_MUTEX_INITIALIZER;

ObjectProxy::ObjectProxy()
{
	interface = NULL;
	openThread = NULL;
	
	pthread_mutex_lock(&shared_mutex);
	numInstances++;
	pthread_mutex_unlock(&shared_mutex);
	
	//interface->openConnection(_servantID, connParameters);
	currentMode = BR_TWOWAY_MODE;
	pthread_mutex_init(&mutex, 0);
}

ObjectProxy::ObjectProxy(const std::string& _servantID, ICommunicationDevice* _iface, bool waitOnline, const DictionaryDataType& connParameters)
{
	servantID = _servantID;
	interface = _iface;
	openThread = NULL;
	
	pthread_mutex_lock(&shared_mutex);
	numInstances++;
	pthread_mutex_unlock(&shared_mutex);
	
	ICommunicationDevice* iface_aux = (_iface != NULL) ? interface : DevicePool::currentDevice();
	if (!iface_aux->isConnectionOpenned(servantID))
	{
		openThread = new OpenConnectionThread(iface_aux, servantID, INT_MAX, connParameters);
		openThread->start();
	
		if (waitOnline)
		{ 
			openThread->join();
			delete openThread;
			openThread = NULL;
		}
	}
	else
	{
		iface_aux->openConnection(servantID);
	}
	
	//interface->openConnection(_servantID, connParameters);
	currentMode = BR_TWOWAY_MODE;
	pthread_mutex_init(&mutex, 0);
}

ObjectProxy::~ObjectProxy()
{
	delete openThread;
	
	pthread_mutex_lock(&shared_mutex);
	numInstances--;
	if (numInstances <= 0)
	{
		delete asyncThread;
		asyncThread = NULL;
		numInstances = 0;
	}
	pthread_mutex_unlock(&shared_mutex);
	
	//if (interface == NULL) DevicePool::currentInterface()->closeConnection(servantID);
	//else interface->closeConnection(servantID);

	interface = NULL;
	
	pthread_mutex_destroy(&mutex);
}

void ObjectProxy::resolveInitialization(ICommunicationDevice* _iface, 
									bool waitOnline, 
									const DictionaryDataType& connParameters)
{
	if (DiscoveryProxy::defaultProxy() == NULL)
	{
		throw "DiscoveryProxy not initialized";
	}
	
	std::string _servantID = DiscoveryProxy::defaultProxy()->resolve(this->getTypeID());
	
	if (_servantID == "")
	{
		std::string msg = "Servant for the ";
		msg += this->getTypeID();
		msg += " proxy has not been found";
		throw msg.c_str();
	}
	
	servantID = _servantID;
	interface = _iface;
	openThread = NULL;
	
	ICommunicationDevice* iface_aux = (_iface != NULL) ? interface : DevicePool::currentDevice();
	if (!iface_aux->isConnectionOpenned(servantID))
	{
		openThread = new OpenConnectionThread(iface_aux, servantID, INT_MAX, connParameters);
		openThread->start();
	
		if (waitOnline)
		{ 
			openThread->join();
			delete openThread;
			openThread = NULL;
		}
	}
	else
	{
		iface_aux->openConnection(servantID);
	}						
}

int ObjectProxy::sendRequest(const std::string& servantID, const std::string& operation, const std::vector<byte>& enc)
{
	bool res;
	
	RequestMessage msg;
	RequestMessageHeader* hd = (RequestMessageHeader*)msg.header;
	std::vector<byte> msg_bytes;
	
	pthread_mutex_lock(&shared_mutex);
	requestId = ((requestId+1) % MAX_REQUEST_ID);
	int req = requestId;
	pthread_mutex_unlock(&shared_mutex);
	
	hd->requestId = req;
	hd->operation = operation;
	
	pthread_mutex_lock(&mutex);
	hd->mode = currentMode;
	hd->id = identity;
	pthread_mutex_unlock(&mutex);
	
	msg.addToEncapsulation(enc);
	
	msg.bytes(msg_bytes);
	
	if (interface == NULL)
		res = DevicePool::currentDevice()->write(servantID, msg_bytes);
		
	else res = interface->write(servantID, msg_bytes);
	
	if (!res)
	{
		pthread_mutex_lock(&mutex);
		/**
		* TODO: Crear una cache para tratar de proveer algunos mensajes
		*/
		if (openThread == NULL)
		{
			if (interface != NULL)
				openThread = new OpenConnectionThread(interface, servantID, INT_MAX);	
			else openThread = new OpenConnectionThread(DevicePool::currentDevice(), servantID, INT_MAX);
				
			openThread->start();
		}
		pthread_mutex_unlock(&mutex);
		
		//throw "Service Not Available";
	}
	else
	{
		pthread_mutex_lock(&mutex);
		if (openThread != NULL)
		{
			delete openThread;
			openThread = NULL;
		}
		pthread_mutex_unlock(&mutex);
	}
	
	return req;
}

void ObjectProxy::receiveReply(int reqId, std::vector<byte>& result)
{
	pthread_mutex_lock(&mutex);
	byte currMode = currentMode;
	pthread_mutex_unlock(&mutex);
	
	if (currMode == BR_TWOWAY_MODE)
	{
		std::vector<byte> recv_bytes;
		
		ReplyMessageStack::consume(reqId, recv_bytes);
		ReplyMessage msg(recv_bytes);
		result = msg.body->byteCollection;
	}
}


void ObjectProxy::receiveCallback(int reqId, AsyncMethodCallback* _amc)
{
	pthread_mutex_lock(&shared_mutex);
	
	if (asyncThread == NULL)
	{
		asyncThread = new AsyncCallback();
		asyncThread->start();
	}
	asyncThread->pushRequestId(reqId, _amc);
	
	pthread_mutex_unlock(&shared_mutex);
}

void ObjectProxy::waitOnlineMode()
{
	pthread_mutex_lock(&mutex);
	bool wait = (openThread != NULL);
	pthread_mutex_unlock(&mutex);
	
	if (!wait) return;
	else
	{
		pthread_mutex_lock(&mutex);
		
		openThread->join();
		delete openThread;
		openThread = NULL;
		
		pthread_mutex_unlock(&mutex);
	}
}

void ObjectProxy::marshall(std::vector<byte>& res)
{
	#warning "ObjectProxy not marshallable yet!!!"
	
	pthread_mutex_lock(&mutex_writer);
	writer.open(res);
	
	writer.writeInteger(4+servantID.size());
	writer.writeString(servantID);
	
	writer.close();
	pthread_mutex_unlock(&mutex_writer);
}

void ObjectProxy::unmarshall(const std::vector<byte>& vec)
{
	#warning "ObjectProxy not unmarshallable yet!!!"
}

	
/*************************************/
/*           AsyncThread             */
/*************************************/
AsyncCallback::AsyncCallback()
{
	pthread_mutex_init(&mutex, 0);
	pthread_cond_init(&cond, 0);
}

AsyncCallback::~AsyncCallback()
{
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

void AsyncCallback::run()
{	
	std::vector<byte> recv_bytes;
	std::map<int, AsyncMethodCallback*>::iterator it;
	
	while(1)
	{
		pthread_mutex_lock(&mutex);
		
		while(requestsId_callbacks.size()<=0)
		{
			pthread_cond_wait(&cond, &mutex);
		}
		
		unsigned int size = requestsId_callbacks.size();
		pthread_mutex_unlock(&mutex);
		
		if (size > 0)
		{
			pthread_mutex_lock(&mutex);
			it = requestsId_callbacks.begin();
			int requestId = it->first;
			AsyncMethodCallback* amc = it->second;
			requestsId_callbacks.erase(it);
			pthread_mutex_unlock(&mutex);
			
			ReplyMessageStack::consume(requestId, recv_bytes);	
			ReplyMessage msg(recv_bytes);
			recv_bytes = msg.body->byteCollection;
			if (recv_bytes.size() > 0)
			{
				amc->callback(recv_bytes);
				delete amc;
			}
		}
	}
}

void AsyncCallback::pushRequestId(int reqId, AsyncMethodCallback* amc)
{
	pthread_mutex_lock(&mutex);
	requestsId_callbacks[reqId] = amc;
	pthread_mutex_unlock(&mutex);
	
	pthread_cond_broadcast(&cond);
}

};

