#ifndef _BLUEROSE_MESSAGE_HEADER_H_
#define _BLUEROSE_MESSAGE_HEADER_H_

#include <cstring>
#include <string>
#include <vector>
#include <pthread.h>
#include "ByteStreamWriter.h"
#include "BRUtil.h"
#include "TypeDefinitions.h"


/*********** Size Definitions ***********/
#define BR_SIZE_OF_INT 4
#define BR_SIZE_OF_LONG 8
#define BR_SIZE_OF_FLOAT 4
#define BR_SIZE_OF_DOUBLE 8
#define BR_SIZE_OF_BOOLEAN 1
#define BR_SIZE_OF_STRING(x) (strlen(x))
/****************************************/


/************* Types Definition *********/
#define BR_INTEGER 0x00
#define BR_LONG 0x01
#define BR_FLOAT 0x02
#define BR_DOUBLE 0x03
#define BR_STRING 0x04
#define BR_BOOLEAN 0x05
#define BR_LIST_INT 0x07
#define BR_LIST_LONG 0x08
#define BR_LIST_FLOAT 0x09
#define BR_LIST_DOUBLE 0x0A
#define BR_LIST_STRING 0x0B
#define BR_LIST_BOOLEAN 0x0C
#define BR_DICTIONARY 0x0D
/*****************************************/


/***********  Message Types **********/
#define BR_REQUEST_MSG 0x00
#define BR_BATCH_REQUEST_MSG 0x01
#define BR_REPLY_MSG 0x02
#define BR_VALIDATE_CONNECTION_MSG 0x03
#define BR_CLOSE_CONNECTION_MSG 0x04
#define BR_EVENT_MSG 0x05   //extension for natively support events
/*************************************/


/********** Reply Status ************/
#define BR_SUCCESS_STATUS 0x00
#define BR_USER_EXCEPTION_STATUS 0x01
#define BR_OBJECT_NOT_EXIST_STATUS 0x02
#define BR_FACET_NOT_EXIST_STATUS 0x03
#define BR_OPERATION_NOT_EXIST_STATUS 0x04
#define BR_UNKNOWN_LOCAL_EXCEPTION_STATUS 0x05
#define BR_UNKNOWN_USER_EXCEPTION_STATUS 0x06
#define BR_UNKNOWN_EXCEPTION_STATUS 0x07
/*************************************/


/********** Operation Mode *********/
#define BR_TWOWAY_MODE 0x00
#define BR_ONEWAY_MODE 0x01
#define BR_BATCH_ONEWAY_MODE 0x02
#define BR_DATAGRAM_MODE 0x03
#define BR_BATCH_DATAGRAM_MODE 0x04
/**********************************/


/************ Other Definitions *********/
#define BR_TRUE 0x01
#define BR_FALSE 0x00

#define BR_PROTOCOL_MINOR 0x00
#define BR_PROTOCOL_MAJOR 0x01

#define BR_ENCODING_MINOR 0x00
#define BR_ENCODING_MAJOR 0x01
/*****************************************/

namespace BlueRose {

	/**
	* Common header for any message. 
	* Definition taken from ICE (zeroc.com) for compatibility reasons.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class MessageHeader 
	{
		protected:
			static ByteStreamWriter writer;
			static pthread_mutex_t mutex;
		
		public:
			MessageHeader();
			virtual ~MessageHeader();
	
			byte magic[4]; /**< Magic bytes: "I", "c", "e", "P" */
			byte protocolMajor; /**< Major version of the protocol: 0x01 */
			byte protocolMinor; /**< Minor version of the protocol: 0x00 */
			byte encodingMajor; /**< Major version of the encoding: 0x01 */
			byte encodingMinor; /**< Minor version of the encoding: 0x00 */
			byte messageType; /**< Message type */
			byte compressionStatus; /**< Compression of the message. Currently only 0x00 */
			int messageSize; /**< Size of the message, including the header */
	
			/**
			* Byte representation of the header
			*
			* @param bytes Result of representing the header as bytes
			*/
			virtual void bytes(std::vector<byte>& bytes);
	};


	/**
	* Header for a connection validating message. 
	* Definition taken from ICE (zeroc.com) for compatibility reasons.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class ValidateConnectionMessageHeader : public MessageHeader
	{
		public:
			ValidateConnectionMessageHeader();
	};


	/**
	* Header for a connection closing message. 
	* Definition taken from ICE (zeroc.com) for compatibility reasons.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class CloseConnectionMessageHeader : public MessageHeader
	{
		public:
			CloseConnectionMessageHeader();
	};


	/**
	* Header for a batch requesting message. 
	* Definition taken from ICE (zeroc.com) for compatibility reasons.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class BatchRequestMessageHeader : public MessageHeader
	{
		public:
			BatchRequestMessageHeader();
			
			int numRequests; /**< Number of requests inside the message */
			Identity id; /**< Identity of the servant */
			std::vector<std::string> facet; /**< Facet of the servant */
			std::string operation; /**< Requested operation */
			byte mode; /**< Operation mode */
			Context context; /**< Context of the message */

			void bytes(std::vector<byte>& bytes);
	};


	/**
	* Header for a requesting message. 
	* Definition taken from ICE (zeroc.com) for compatibility reasons.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class RequestMessageHeader : public MessageHeader
	{
		public:
			RequestMessageHeader();
			
			int requestId; /**< Identifier of the request */
			Identity id; /**< Identity of the servant */
			std::vector<std::string> facet; /**< Facet of the servant */
			std::string operation; /**< Requested operation */
			byte mode; /**< Operation mode */
			Context context; /** < Context of the message */
	
			void bytes(std::vector<byte>& bytes);
	};


	/**
	* Header for a replying message. 
	* Definition taken from ICE (zeroc.com) for compatibility reasons.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class ReplyMessageHeader : public MessageHeader
	{
		public:
			ReplyMessageHeader();
			
			int requestId; /**< Identifier of the request */
			byte replyStatus; /**< Status of the reply */
		
			void bytes(std::vector<byte>& bytes);
	};
	
	/**
	* Header for an event message. 
	* Extension over ICE Protocol (zeroc.com).
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	class EventMessageHeader : public MessageHeader
	{
		public:
			EventMessageHeader();
			void bytes(std::vector<byte>& bytes);
	};
	
};

#endif

