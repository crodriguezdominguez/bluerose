#include <stdlib.h>
#include "BRUtil.h"
#include "ByteStreamWriter.h"

namespace BlueRose {

ByteStreamWriter::ByteStreamWriter()
{
	stream = NULL;
}

void ByteStreamWriter::writeSize(unsigned int size)
{
	if (size < 255)
	{
		stream->push_back((byte)size);
	}
	else
	{
		byte* p = (byte*)&size;
		
		stream->push_back(0xff);
		
		#ifdef BR_LITTLE_ENDIAN
			stream->push_back(p[0]);
			stream->push_back(p[1]);
			stream->push_back(p[2]);
			stream->push_back(p[3]);
		#else
			stream->push_back(p[3]);
			stream->push_back(p[2]);
			stream->push_back(p[1]);
			stream->push_back(p[0]);
		#endif
	}
}

void ByteStreamWriter::open(std::vector<byte>& str)
{
	//objectIdentity = 1;
	//currentTypeID = 1;
	//marshalledObjects.clear();
	//marshalledInstances.clear();
	stream = &str;
}

void ByteStreamWriter::close()
{
	//objectIdentity = 1;
	//currentTypeID = 1;
	//marshalledObjects.clear();
	//marshalledInstances.clear();
	stream = NULL;
}

std::vector<byte>& ByteStreamWriter::bytes()
{
	return *stream;
}

void ByteStreamWriter::writeByte(byte b)
{
	stream->push_back(b);
}

void ByteStreamWriter::writeBoolean(bool b)
{
	stream->push_back( ( b ) ? 0x01 : 0x00 );
}

void ByteStreamWriter::writeShort(short b)
{
	byte* p = (byte*)&b;

	#ifdef BR_LITTLE_ENDIAN
		stream->push_back(p[0]);
		stream->push_back(p[1]);
	#else
		stream->push_back(p[1]);
		stream->push_back(p[0]);
	#endif
}

void ByteStreamWriter::writeInteger(int b)
{
	byte* p = (byte*)&b;

	#ifdef BR_LITTLE_ENDIAN
		stream->push_back(p[0]);
		stream->push_back(p[1]);
		stream->push_back(p[2]);
		stream->push_back(p[3]);
	#else
		stream->push_back(p[3]);
		stream->push_back(p[2]);
		stream->push_back(p[1]);
		stream->push_back(p[0]);
	#endif
}

void ByteStreamWriter::writeLong(long b)
{
	byte* p = (byte*)&b;

	#ifdef BR_LITTLE_ENDIAN
		stream->push_back(p[0]);
		stream->push_back(p[1]);
		stream->push_back(p[2]);
		stream->push_back(p[3]);
		stream->push_back(p[4]);
		stream->push_back(p[5]);
		stream->push_back(p[6]);
		stream->push_back(p[7]);
	#else
		stream->push_back(p[7]);
		stream->push_back(p[6]);
		stream->push_back(p[5]);
		stream->push_back(p[4]);
		stream->push_back(p[3]);
		stream->push_back(p[2]);
		stream->push_back(p[1]);
		stream->push_back(p[0]);
	#endif
}

void ByteStreamWriter::writeFloat(float b)
{
	byte* p = (byte*)&b;

	#ifdef BR_LITTLE_ENDIAN
		stream->push_back(p[0]);
		stream->push_back(p[1]);
		stream->push_back(p[2]);
		stream->push_back(p[3]);
	#else
		stream->push_back(p[3]);
		stream->push_back(p[2]);
		stream->push_back(p[1]);
		stream->push_back(p[0]);
	#endif
}

void ByteStreamWriter::writeDouble(double b)
{
	byte* p = (byte*)&b;

	#ifdef BR_LITTLE_ENDIAN
		stream->push_back(p[0]);
		stream->push_back(p[1]);
		stream->push_back(p[2]);
		stream->push_back(p[3]);
		stream->push_back(p[4]);
		stream->push_back(p[5]);
		stream->push_back(p[6]);
		stream->push_back(p[7]);
	#else
		stream->push_back(p[7]);
		stream->push_back(p[6]);
		stream->push_back(p[5]);
		stream->push_back(p[4]);
		stream->push_back(p[3]);
		stream->push_back(p[2]);
		stream->push_back(p[1]);
		stream->push_back(p[0]);
	#endif
}

void ByteStreamWriter::writeString(const std::string& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		stream->push_back(b[i]);
	}
}

void ByteStreamWriter::writeUTF8String(const std::wstring& b)
{
	const wchar_t* inbuf = b.c_str();
	size_t insize = b.size();
	size_t outsize = wchar_to_utf8(inbuf, insize, NULL, 0, UTF8_IGNORE_ERROR);
	char* outbuf = (char*)malloc( (outsize+1)*sizeof(char) );
	
	outsize = wchar_to_utf8(inbuf, insize, outbuf, outsize+1, UTF8_IGNORE_ERROR);
	
	writeSize(outsize);
	for (size_t i=0; i<outsize; i++)
	{
		writeByte((byte)outbuf[i]);
	}
	
	free(outbuf);
}

//void ByteStreamWriter::writeObjectProxy(ObjectProxy& obj)
//{
	/*writeInteger(objectIdentity);
	writeUnidentifiedObjectProxy(obj);
	objectIdentity++;*/
//}

//void ByteStreamWriter::writeUnidentifiedObjectProxy(ObjectProxy& obj)
//{	
	/*std::string typeID = obj.getTypeID();
	std::map<std::string, unsigned int>::iterator it = marshalledObjects.find(typeID);
	if (it == marshalledObjects.end()) //it wasn't in the table before
	{
		writeBoolean(false);
		writeString(typeID);
		
		marshalledObjects[typeID] = currentTypeID;
		currentTypeID++;
	}
	else
	{
		writeBoolean(true);
		writeSize(marshalledObjects[typeID]);
	}
	
	std::vector<byte> res;
	obj.marshall(res);
	
	marshalledInstances.push_back(obj);
	
	writeRawBytes(res);*/
//}

//void ByteStreamWriter::writePointerToObjectProxy(ObjectProxy* pointer)
//{
	/*
	if (pointer == NULL)
	{
		writeInteger(0);
	}
	else
	{
		//check if the instance has been marshalled before
		std::vector<ObjectProxy>::iterator it;
		bool marshalled = false;
		for (it = marshalledInstances.begin(); it != marshalledInstances.end(); ++it)
		{
			if (&(*it) == pointer)
			{
				marshalled = true;
				break;
			}
		}
		
		if (marshalled) //point to the marshalled instance
		{
			int ident = marshalledObjects[pointer->getTypeID()];
			writeInteger(-ident); //negative identifier
			writeBoolean(true); //the marshalled object existed before
		}
		else
		{
			writeInteger(-objectIdentity); //negative identifier
			writeBoolean(false); //the marshalled object NOT existed before
			writeObjectProxy(*pointer); //marshall the object
		}
	}
	*/
//}

void ByteStreamWriter::writeRawBytes(const std::vector<byte>& b)
{
	stream->insert(stream->end(), b.begin(), b.end());
}

void ByteStreamWriter::writeByteSeq(const std::vector<byte>& b)
{
	writeSize(b.size());	
	stream->insert(stream->end(), b.begin(), b.end());
}

void ByteStreamWriter::writeBooleanSeq(const std::vector<bool>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeBoolean(b[i]);
	}
}

void ByteStreamWriter::writeShortSeq(const std::vector<short>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeShort(b[i]);
	}
}

void ByteStreamWriter::writeIntegerSeq(const std::vector<int>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeInteger(b[i]);
	}
}

void ByteStreamWriter::writeLongSeq(const std::vector<long>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeLong(b[i]);
	}
}

void ByteStreamWriter::writeFloatSeq(const std::vector<float>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeFloat(b[i]);
	}
}

void ByteStreamWriter::writeDoubleSeq(const std::vector<double>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeDouble(b[i]);
	}
}

void ByteStreamWriter::writeStringSeq(const std::vector<std::string>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeString(b[i]);
	}
}

void ByteStreamWriter::writeUTF8StringSeq(const std::vector<std::wstring>& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (unsigned int i=0; i<size; i++)
	{
		writeUTF8String(b[i]);
	}
}

void ByteStreamWriter::writeDictionary(const DictionaryDataType& b)
{
	unsigned int size = b.size();
	
	writeSize(size);
	
	for (DictionaryDataType::const_iterator iter = b.begin(); iter != b.end(); ++iter)
	{
		writeString(iter->first);
		writeByteSeq(iter->second);
	}
}

};

