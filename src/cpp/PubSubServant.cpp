#include "PubSubServant.h"

namespace BlueRose {
	
namespace Service {

PubSubServant::PubSubServant()
 : ObjectServant()
{
	identity.name = "PubSub";
	identity.category = "BlueRoseService";
	
	BlueRose::DevicePool::registerObjectServant(this);
}

PubSubServant::~PubSubServant()
{
	BlueRose::DevicePool::unregisterObjectServant(this);
}

byte PubSubServant::runMethod(const std::string& userID,
									const std::string& method, 
									const std::vector<byte>& args, 
									std::vector<byte>& result)
{
	pthread_mutex_lock(&mutex_writer);
	pthread_mutex_lock(&mutex_reader);
	reader.open(args);
	writer.open(result);
	
	if (method == "subscribe0")
	{
		subscribe(reader.readInteger(), userID);
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "subscribe1")
	{
		int _topic = reader.readInteger();
		Predicate _pred;
		_pred.unmarshall(reader);
		subscribe(_topic, _pred, userID);
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "isSubscribed")
	{
		std::string _userID;
		Event _template;
		reader.readString(_userID);
		_template.unmarshall(reader);
		
		writer.writeBoolean(isSubscribed(_userID, _template));
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "unsubscribe0")
	{	
		unsubscribe(userID);
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "unsubscribe1")
	{	
		int _topic = reader.readInteger();
		unsubscribe(_topic, userID);
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "getSubscribers0")
	{	
		writer.writeStringSeq(getSubscribers());
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "getSubscribers1")
	{	
		Event _template;
		_template.unmarshall(reader);
		writer.writeStringSeq(getSubscribers(_template));
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "publish")
	{
		Event evt;
		evt.unmarshall(reader);
		bool comeBack = reader.readBoolean();
		publish(evt, comeBack, userID);
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	writer.close();
	reader.close();
	
	pthread_mutex_unlock(&mutex_reader);
	pthread_mutex_unlock(&mutex_writer);
	
	return BR_OPERATION_NOT_EXIST_STATUS;							
}

void PubSubServant::subscribe(int _topic, const std::string& senderID)
{
	Predicate pred;
	subscribe(_topic, pred, senderID);
}

void PubSubServant::subscribe(int _topic, Predicate _pred, const std::string& senderID)
{
	std::pair<int, Predicate> sub_pair(_topic, _pred);
	
	if (subscriptions.find(senderID) != subscriptions.end()) //other subscriptions existed
	{
		subscriptions[senderID].push_back(sub_pair);
	}
	else
	{
		std::vector<std::pair<int, Predicate> > sub;
		sub.push_back(sub_pair);
		
		subscriptions[senderID] = sub;
	}
}

void PubSubServant::unsubscribe(const std::string& senderID)
{
	std::map<std::string, std::vector<std::pair<int, Predicate> > >::iterator it = subscriptions.find(senderID);
	if (it != subscriptions.end())
	{
		subscriptions.erase(it);
	}
}

void PubSubServant::unsubscribe(int _topic, const std::string& senderID)
{
	std::map<std::string, std::vector<std::pair<int, Predicate> > >::iterator it = subscriptions.find(senderID);
	if (it != subscriptions.end())
	{
		std::vector<std::pair<int, Predicate> > v = it->second;
		std::vector<std::pair<int, Predicate> > res;
		
		std::vector<std::pair<int, Predicate> >::iterator j;
		for (j=v.begin(); j!=v.end(); ++j)
		{
			if (j->first != _topic)
			{
				res.push_back(*j);
			}
		}
		
		it->second = res;
	}
}

bool PubSubServant::isSubscribed(const std::string& _userID, const Event& _template)
{
	std::map<std::string, std::vector<std::pair<int, Predicate> > >::iterator it = subscriptions.find(_userID);
	if (it == subscriptions.end()) return false;
	else
	{
		int topic = _template.getTopic();
		std::vector<std::pair<int, Predicate> > v = it->second;
		
		for (unsigned int i=0; i<v.size(); i++)
		{
			if (v[i].first == topic)
			{
				//check that all the values of the event are consistent with the predicate
				for (EventIterator it = _template.begin(); it != _template.end(); ++it)
				{
					if (!v[i].second.isConsistent(it->first, EQUAL, it->second.getValue()))
					{
						return false;
					}
					
					//foreach event node, repeat the process...
					for (EventIterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
					{
						isSubscribed(_userID, it->second);
					}
				}
			}
		}
		
		return true;
	}
}

std::vector<std::string> PubSubServant::getSubscribers()
{
	std::map<std::string, std::vector<std::pair<int, Predicate> > >::iterator it;
	std::vector<std::string> res;
	for (it = subscriptions.begin(); it != subscriptions.end(); ++it)
	{
		res.push_back(it->first);
	}
	
	return res;
}

std::vector<std::string> PubSubServant::getSubscribers(const Event& _template)
{
	std::map<std::string, std::vector<std::pair<int, Predicate> > >::iterator it;
	std::vector<std::string> res;
	for (it = subscriptions.begin(); it != subscriptions.end(); ++it)
	{
		if (isSubscribed(it->first, _template)) 
			res.push_back(it->first);
	}
	
	return res;
}

void PubSubServant::publish(const Event& evt, bool comeBack, const std::string& senderID)
{
	std::vector<std::string> subscribers = getSubscribers(evt);
	
	//raw write...
	std::vector<byte> bytes;
	std::vector<byte> send_msg;
	
	ByteStreamWriter writer;
	writer.open(bytes);
	evt.marshall(writer);
	writer.close();
	
	EventMessage msg(bytes);
	msg.bytes(send_msg);
	
	for (unsigned int i=0; i<subscribers.size(); i++)
	{
		//printf("EVENT PUBLISHED to: %s\n", subscribers[i].c_str());
		//fflush(stdout);
		
		//send an event msg
		if ( (senderID != subscribers[i]) || (senderID == subscribers[i] && comeBack) )
		{
			if (!DevicePool::currentDevice()->write(subscribers[i], send_msg))
			{
				unsubscribe(subscribers[i]);
			}
		}
	}
}

};
	
};

int main(int argc, char** argv)
{
	//initialize transmission interfaces
	BlueRose::TcpCompatibleDevice device;
	
	//initialize objects
	BlueRose::Service::PubSubServant* servant = new BlueRose::Service::PubSubServant();
	
	//initialize bluerose
	if (argc > 1) BlueRose::Initializer::initialize(argv[1]);
	else BlueRose::Initializer::initialize("config.xml");
	
	//initialize the servant
	BlueRose::Initializer::initializeServant(servant, &device);
	
	//wait for connections to the device
	device.waitForConnections();
	
	//cleanups
	BlueRose::Initializer::destroy();
	delete servant;

	return 0;
}

