#include "MessageHeader.h"

namespace BlueRose {

ByteStreamWriter MessageHeader::writer;
pthread_mutex_t MessageHeader::mutex = PTHREAD_MUTEX_INITIALIZER;

/**************************************/
/*            MessageHeader           */
/**************************************/

MessageHeader::MessageHeader()
{
	magic[0] = 0x49;  //IceP
	magic[1] = 0x63;
	magic[2] = 0x65;
	magic[3] = 0x50;
	
	protocolMajor = BR_PROTOCOL_MAJOR;
	protocolMinor = BR_PROTOCOL_MINOR;
	encodingMajor = BR_ENCODING_MAJOR;
	encodingMinor = BR_ENCODING_MINOR;
	messageType = BR_VALIDATE_CONNECTION_MSG;
	compressionStatus = 0x00;
	
	messageSize = 14;
}

MessageHeader::~MessageHeader()
{
	
}

void MessageHeader::bytes(std::vector<byte>& bytes)
{
	pthread_mutex_lock(&mutex);
	writer.open(bytes);
	
	writer.writeByte(magic[0]);
	writer.writeByte(magic[1]);
	writer.writeByte(magic[2]);
	writer.writeByte(magic[3]);
	
	writer.writeByte(protocolMajor);
	writer.writeByte(protocolMinor);
	writer.writeByte(encodingMajor);
	writer.writeByte(encodingMinor);
	
	writer.writeByte(messageType);
	writer.writeByte(compressionStatus);
	
	writer.writeInteger(messageSize);
	
	writer.close();
	pthread_mutex_unlock(&mutex);
}


/**************************************/
/*   ValidateConnectionMessageHeader  */
/**************************************/

ValidateConnectionMessageHeader::ValidateConnectionMessageHeader()
	: MessageHeader()
{
	messageType = BR_VALIDATE_CONNECTION_MSG;
}



/**************************************/
/*     CloseConnectionMessageHeader   */
/**************************************/

CloseConnectionMessageHeader::CloseConnectionMessageHeader()
	: MessageHeader()
{
	messageType = BR_CLOSE_CONNECTION_MSG;
}



/**************************************/
/*      BatchRequestMessageHeader     */
/**************************************/

BatchRequestMessageHeader::BatchRequestMessageHeader()
	: MessageHeader()
{
	messageType = BR_BATCH_REQUEST_MSG;
	numRequests = 0;
	id.name = "";
	id.category = "";
	operation = "";
	mode = BR_TWOWAY_MODE;
}

void BatchRequestMessageHeader::bytes(std::vector<byte>& bytes)
{
	pthread_mutex_lock(&mutex);
	writer.open(bytes);
	
	writer.writeByte(magic[0]);
	writer.writeByte(magic[1]);
	writer.writeByte(magic[2]);
	writer.writeByte(magic[3]);
	
	writer.writeByte(protocolMajor);
	writer.writeByte(protocolMinor);
	writer.writeByte(encodingMajor);
	writer.writeByte(encodingMinor);
	
	writer.writeByte(messageType);
	writer.writeByte(compressionStatus);
	
	writer.writeInteger(messageSize);
	
	//numRquests
	writer.writeInteger(numRequests);
	
	//insertar Identity
	writer.writeString(id.name);
	writer.writeString(id.category);
	
	//insertar facet
	writer.writeStringSeq(facet);
	
	//insertar operation
	writer.writeString(operation);
	
	//insertar mode
	writer.writeByte(mode);
	
	//insertar context
	writer.writeSize(context.size());
	
	for (Context::iterator iter = context.begin(); iter != context.end(); ++iter)
	{
		writer.writeString(iter->first);
		writer.writeString(iter->second);
	}
	
	writer.close();
	pthread_mutex_unlock(&mutex);
}



/**************************************/
/*        RequestMessageHeader        */
/**************************************/

RequestMessageHeader::RequestMessageHeader()
	: MessageHeader()
{
	messageType = BR_REQUEST_MSG;
	requestId = 0;
	
	id.name = "";
	id.category = "";
	operation = "";
	mode = BR_TWOWAY_MODE;
}

void RequestMessageHeader::bytes(std::vector<byte>& bytes)
{
	pthread_mutex_lock(&mutex);
	writer.open(bytes);
	
	writer.writeByte(magic[0]);
	writer.writeByte(magic[1]);
	writer.writeByte(magic[2]);
	writer.writeByte(magic[3]);
	
	writer.writeByte(protocolMajor);
	writer.writeByte(protocolMinor);
	writer.writeByte(encodingMajor);
	writer.writeByte(encodingMinor);
	
	writer.writeByte(messageType);
	writer.writeByte(compressionStatus);
	
	writer.writeInteger(messageSize);
	
	//requestId
	writer.writeInteger(requestId);
	
	//insertar Identity
	writer.writeString(id.name);
	writer.writeString(id.category);
	
	//insertar facet
	writer.writeStringSeq(facet);
	
	//insertar operation
	writer.writeString(operation);
	
	//insertar mode
	writer.writeByte(mode);
	
	//insertar context
	writer.writeSize(context.size());
	
	for (Context::iterator iter = context.begin(); iter != context.end(); ++iter)
	{
		writer.writeString(iter->first);
		writer.writeString(iter->second);
	}
	
	writer.close();
	pthread_mutex_unlock(&mutex);
}




/**************************************/
/*         ReplyMessageHeader         */
/**************************************/

ReplyMessageHeader::ReplyMessageHeader()
	: MessageHeader()
{
	messageType = BR_REPLY_MSG;
	requestId = 0;
	replyStatus = BR_SUCCESS_STATUS;
	
	messageSize = 19;
}

void ReplyMessageHeader::bytes(std::vector<byte>& bytes)
{
	pthread_mutex_lock(&mutex);
	writer.open(bytes);
	
	writer.writeByte(magic[0]);
	writer.writeByte(magic[1]);
	writer.writeByte(magic[2]);
	writer.writeByte(magic[3]);
	
	writer.writeByte(protocolMajor);
	writer.writeByte(protocolMinor);
	writer.writeByte(encodingMajor);
	writer.writeByte(encodingMinor);
	
	writer.writeByte(messageType);
	writer.writeByte(compressionStatus);
	
	writer.writeInteger(messageSize);
	
	writer.writeInteger(requestId);
	
	writer.writeByte(replyStatus);
	
	writer.close();
	pthread_mutex_unlock(&mutex);
}

/**************************************/
/*         EventMessageHeader         */
/**************************************/

EventMessageHeader::EventMessageHeader()
	: MessageHeader()
{
	messageType = BR_EVENT_MSG;
}

void EventMessageHeader::bytes(std::vector<byte>& bytes)
{
	pthread_mutex_lock(&mutex);
	writer.open(bytes);
	
	writer.writeByte(magic[0]);
	writer.writeByte(magic[1]);
	writer.writeByte(magic[2]);
	writer.writeByte(magic[3]);
	
	writer.writeByte(protocolMajor);
	writer.writeByte(protocolMinor);
	writer.writeByte(encodingMajor);
	writer.writeByte(encodingMinor);
	
	writer.writeByte(messageType);
	writer.writeByte(compressionStatus);
	
	writer.writeInteger(messageSize);
	
	writer.close();
	pthread_mutex_unlock(&mutex);
}

};
