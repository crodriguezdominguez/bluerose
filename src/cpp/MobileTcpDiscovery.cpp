#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <netdb.h>
#include <sys/select.h>
#include "DevicePool.h"
#include "Initializer.h"
#include "MobileTcpDiscovery.h"

namespace BlueRose {

static pthread_mutex_t mutex_mobile_discovery = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t mutex_sync_resolve = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond_resolve = PTHREAD_COND_INITIALIZER;
static std::string resolvedName = "";

static void resolve_reply_register(DNSServiceRef sdRef,
								DNSServiceFlags flags,
								uint32_t interfaceIndex,
								DNSServiceErrorType errorCode,
								const char *fullname,
								const char *hosttarget,
								uint16_t port,
								uint16_t txtLen,
								const unsigned char *txtRecord,
								void *context)
{
	//create a copy...
	pthread_mutex_lock(&mutex_mobile_discovery);
	std::vector<MobileDiscoveryListener*> listeners(*((std::vector<MobileDiscoveryListener*>*)context));
	pthread_mutex_unlock(&mutex_mobile_discovery);
	
	bool error = false;
	
	if (errorCode != kDNSServiceErr_NoError) error = true;
	
	std::string userID = hosttarget;
	char buffer[256];
	sprintf(buffer, ":%u", port);
	userID += buffer;
	
	bool moreComing = false;
	if (flags & kDNSServiceFlagsMoreComing) moreComing = true;
	
	TXTRecord rec = TXTRecord::parse(txtLen, txtRecord);
	
	for (unsigned int i=0; i<listeners.size(); i++)
	{
		listeners[i]->onElementRegistered(fullname, userID, rec, moreComing, error);
	}
}
	
static void register_reply(DNSServiceRef sdRef, 
							DNSServiceFlags flags, 
							DNSServiceErrorType errorCode, 
							const char *name, 
		   					const char *regtype, 
		   					const char *domain, 
							void *context)
{
	if (errorCode != kDNSServiceErr_NoError)
	{
		DNSServiceRef sdRefResolve;
		
		DNSServiceResolve(&sdRefResolve, 0, 0, name, regtype, domain, resolve_reply_register, context);
		DNSServiceProcessResult(sdRefResolve);
		DNSServiceRefDeallocate(sdRefResolve);
	}
}

static void resolve_reply(DNSServiceRef sdRef,
							DNSServiceFlags flags,
							uint32_t interfaceIndex,
							DNSServiceErrorType errorCode,
							const char *fullname,
							const char *hosttarget,
							uint16_t port,
							uint16_t txtLen,
							const unsigned char *txtRecord,
							void *context)
{
	if (gethostbyname(hosttarget) == NULL) return;
		
	//create a copy...
	pthread_mutex_lock(&mutex_mobile_discovery);
	std::vector<MobileDiscoveryListener*> listeners(*((std::vector<MobileDiscoveryListener*>*)context));
	pthread_mutex_unlock(&mutex_mobile_discovery);
	
	bool error = false;
	
	if (errorCode != kDNSServiceErr_NoError) error = true;
	
	std::string userID = hosttarget;
	char buffer[256];
	sprintf(buffer, ":%u", port);
	userID += buffer;
	
	bool moreComing = false;
	if (flags & kDNSServiceFlagsMoreComing) moreComing = true;
	
	TXTRecord rec = TXTRecord::parse(txtLen, txtRecord);
	
	for (unsigned int i=0; i<listeners.size(); i++)
	{
		listeners[i]->onUserIDResolved(fullname, userID, rec, moreComing, error);
	}
}

static void resolve_sync_reply(DNSServiceRef sdRef,
							DNSServiceFlags flags,
							uint32_t interfaceIndex,
							DNSServiceErrorType errorCode,
							const char *fullname,
							const char *hosttarget,
							uint16_t port,
							uint16_t txtLen,
							const unsigned char *txtRecord,
							void *context)
{	
	char buffer[256];
	sprintf(buffer, "%s:%u", hosttarget, port);
	
	pthread_mutex_lock(&mutex_sync_resolve);
	resolvedName = buffer;
	pthread_mutex_unlock(&mutex_sync_resolve);
	
	pthread_cond_broadcast(&cond_resolve);
}

static void browser_sync_reply(DNSServiceRef sdRef, 
							DNSServiceFlags flags,
							uint32_t interface_index,
							DNSServiceErrorType errorCode, 
							const char *name, 
		   					const char *regtype, 
		   					const char *domain, 
		   					void *context)
{
	std::vector<std::string>* args = (std::vector<std::string>*)context;
	std::string name2 = name;
	std::string regtype2 = regtype;
		
	if (errorCode == kDNSServiceErr_NoError)
	{
		//printf("%s=%s, %s=%s\n", args->at(0).c_str(), name2.c_str(), args->at(1).c_str(), regtype2.c_str());
		//fflush(stdout);
		
		if (args->at(0) == name2 && args->at(1) == regtype2)
		{
			DNSServiceRef sdRefResolve;

			DNSServiceResolve(&sdRefResolve, 0, 0, name, regtype, domain, resolve_sync_reply, NULL);
			DNSServiceProcessResult(sdRefResolve);
			DNSServiceRefDeallocate(sdRefResolve);
		}
	}
}

static void browser_reply(DNSServiceRef sdRef, 
						DNSServiceFlags flags,
						uint32_t interface_index,
						DNSServiceErrorType errorCode, 
						const char *name, 
	   					const char *regtype, 
	   					const char *domain, 
	   					void *context)
{
	//create a copy...
	pthread_mutex_lock(&mutex_mobile_discovery);
	std::vector<MobileDiscoveryListener*> listeners(*((std::vector<MobileDiscoveryListener*>*)context));
	pthread_mutex_unlock(&mutex_mobile_discovery);
	
	bool error = false;
	bool moreComing = false;
	
	if (flags & kDNSServiceFlagsMoreComing) moreComing = true;
	if (errorCode != kDNSServiceErr_NoError) error = true;

	if (flags & kDNSServiceFlagsAdd)
	{
		for (unsigned int i=0; i<listeners.size(); i++)
		{
			listeners[i]->onElementDiscovered(name, regtype, domain, moreComing, error);
		}
	}
	else
	{
		for (unsigned int i=0; i<listeners.size(); i++)
		{
			listeners[i]->onElementRemoved(name, regtype, domain, moreComing, error);
		}
	}
}

MobileTcpDiscovery::MobileTcpDiscovery()
{
	detectionThread = NULL;
}

MobileTcpDiscovery::~MobileTcpDiscovery()
{
	std::map<ObjectServant*, DNSServiceRef>::iterator it;
	for (it = refs.begin(); it != refs.end(); ++it)
	{
		DNSServiceRefDeallocate(it->second);
	}
	
	for (unsigned int i=0; i<listeners.size(); i++)
	{
		delete listeners[i];
	}
	
	stopDetection();
}

std::string MobileTcpDiscovery::getRegType(int serviceType)
{
	switch(serviceType)
	{
		case PRINTER:
			return "_printer._tcp.";
		case DAAP:
			return "_daap._tcp.";
		case DACP:
			return "_dacp._tcp.";
		case FTP:
			return "_ftp._tcp.";
		case HOME_SHARING:
			return "_home-sharing._tcp.";
		case HTTP:
			return "_http._tcp.";
		case HTTPS:
			return "_https._tcp.";
		case IPP:
			return "_ipp._tcp.";
		case JINI:
			return "_jini._tcp.";
		case LONWORKS:
			return "_lonworks._tcp.";
		case NFS:
			return "_nfs._tcp.";
		case RFID:
			return "_rfid._tcp.";
		case RTSP:
			return "_rtsp._tcp.";
		case SCANNER:
			return "_scanner._tcp.";
		case SFTP:
			return "_sftp-ssh._tcp.";
		case SSH:
			return "_ssh._tcp.";
		case TELNET:
			return "_telnet._tcp.";
		case TFTP:
			return "_tftp._tcp.";
		case UPNP:
			return "_upnp._tcp.";
			
		default:
			break;
	}
	
	return "_bluerose._tcp.";
}

void MobileTcpDiscovery::addEventListener(MobileDiscoveryListener* listener)
{
	pthread_mutex_lock(&mutex_mobile_discovery);
	listeners.push_back(listener);
	pthread_mutex_unlock(&mutex_mobile_discovery);
}

void MobileTcpDiscovery::removeEventListener(MobileDiscoveryListener* listener)
{
	pthread_mutex_lock(&mutex_mobile_discovery);
	std::vector<MobileDiscoveryListener*>::iterator it;
	for (it=listeners.begin(); it!=listeners.end(); ++it)
	{
		if (*it == listener)
		{
			listeners.erase(it);
			break;
		}
	}
	pthread_mutex_unlock(&mutex_mobile_discovery);
}

bool MobileTcpDiscovery::registerForDetection(ObjectServant* servant, ICommunicationDevice* device)
{
	DNSServiceRef sdRef;
	
	std::string servantID = servant->getIdentifier();
	std::string regtype = getRegType(BLUEROSE);
	Identity id = servant->getIdentity();
	
	int port = atoi(Initializer::getConfiguration()->getPort(id.category, id.name, device->getName()).c_str());
	//printf("%s, %s, %d\n", servantID.c_str(), regtype.c_str(), port);
	//fflush(stdout);
	
	int result;
	if ((result=DNSServiceRegister(&sdRef, 0, kDNSServiceInterfaceIndexAny, servantID.c_str(), regtype.c_str(), NULL, NULL, port, 
							0, NULL, register_reply, (void*)&listeners)) != kDNSServiceErr_NoError)
	{
		DNSServiceRefDeallocate(sdRef);
		return false;
	}
	
	refs[servant] = sdRef;
	MobileTcpDiscoveryThread* th = new MobileTcpDiscoveryThread(this, sdRef);
	th->start();
	threads[servant] = th;
	
	return true;
}

void MobileTcpDiscovery::unregister(ObjectServant* servant)
{
	std::map<ObjectServant*, DNSServiceRef>::iterator it = refs.find(servant);
	if (it != refs.end())
	{
		DNSServiceRefDeallocate(it->second);
		refs.erase(it);
		
		std::map<ObjectServant*, MobileTcpDiscoveryThread*>::iterator it2 = threads.find(servant);
		if (it2 != threads.end())
		{
			delete it2->second;
			threads.erase(it2);
		}
	}
}

void MobileTcpDiscovery::resolveUserID(const std::string& name, const std::string& regtype, const std::string& domain)
{
	DNSServiceRef sdRefResolve;
	
	DNSServiceResolve(&sdRefResolve, 0, 0, name.c_str(), regtype.c_str(), domain.c_str(), resolve_reply, (void*)&listeners);
	DNSServiceProcessResult(sdRefResolve);
	DNSServiceRefDeallocate(sdRefResolve);
}

std::string MobileTcpDiscovery::resolveUserID(const std::string& name, int serviceType)
{
	DNSServiceRef sdRef;
	std::string regtype = getRegType(serviceType);
	
	std::vector<std::string> args;
	args.push_back(name);
	args.push_back(regtype);
	
	if (DNSServiceBrowse(&sdRef, 0, kDNSServiceInterfaceIndexAny, regtype.c_str(), NULL, browser_sync_reply, (void*)&args)
		  != kDNSServiceErr_NoError)
	{
		DNSServiceRefDeallocate(sdRef);
		return "";
	}
	
	DNSServiceProcessResult(sdRef);
	
	std::string userID = "";
	
	pthread_mutex_lock(&mutex_sync_resolve);
	while(resolvedName == "")
	{
		pthread_cond_wait(&cond_resolve, &mutex_sync_resolve);
	}
	
	userID = resolvedName;
	resolvedName = "";
	pthread_mutex_unlock(&mutex_sync_resolve);
	
	pthread_cond_broadcast(&cond_resolve);
	
	DNSServiceRefDeallocate(sdRef);
	
	return userID;
}

bool MobileTcpDiscovery::startDetection(int serviceType)
{
	DNSServiceRef sdRef;
	std::string regtype = getRegType(serviceType);
	
	if (DNSServiceBrowse(&sdRef, 0, kDNSServiceInterfaceIndexAny, regtype.c_str(), NULL, browser_reply, (void*)&listeners)
		  != kDNSServiceErr_NoError)
	{
		DNSServiceRefDeallocate(sdRef);
		return false;
	}
	
	detectionThread = new MobileTcpDiscoveryThread(this, sdRef);
	detectionThread->start();
	
	return true;
}

bool MobileTcpDiscovery::startDetection(int serviceType, const std::string& domain)
{
	DNSServiceRef sdRef;
	std::string regtype = getRegType(serviceType);
	
	if (DNSServiceBrowse(&sdRef, 0, kDNSServiceInterfaceIndexAny, regtype.c_str(), domain.c_str(), browser_reply, (void*)&listeners)
		  != kDNSServiceErr_NoError)
	{
		DNSServiceRefDeallocate(sdRef);
		return false;
	}
	
	detectionThread = new MobileTcpDiscoveryThread(this, sdRef);
	detectionThread->start();
	
	return true;
}

void MobileTcpDiscovery::stopDetection()
{
	if (detectionThread != NULL)
	{
		delete detectionThread;
		detectionThread = NULL;
	}
}

MobileTcpDiscoveryThread::MobileTcpDiscoveryThread(MobileTcpDiscovery* tcpd, DNSServiceRef ref) : Thread()
{
	sdRef = ref;
	tcpdiscovery = tcpd;
}

void MobileTcpDiscoveryThread::run()
{
	int dns_sd_fd, result, nfds;
	fd_set readfds;
	
	dns_sd_fd = DNSServiceRefSockFD(sdRef);
	nfds = dns_sd_fd + 1;
	
	int stop = 0;
	while(!stop)
	{
		FD_ZERO(&readfds);
		FD_SET(dns_sd_fd, &readfds);
		
		result = select(nfds, &readfds, 0, 0, 0);
		if (result > 0)
		{
			if (FD_ISSET(dns_sd_fd, &readfds))
			{
				result = DNSServiceProcessResult(sdRef);
				if (result != kDNSServiceErr_NoError)
					stop = 1;
			}
		}
	}
}
	
};

