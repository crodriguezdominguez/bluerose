#include "PairEvent.h"

namespace BlueRose {

PairEvent::PairEvent() : BlueRose::Event(PAIR_EVENT_TOPIC)
{
	BlueRose::Value zero;
	zero.setFloat(0);
	
	setMemberValue("x", zero);
	setMemberValue("y", zero);
}

PairEvent::PairEvent(float x, float y) : BlueRose::Event(PAIR_EVENT_TOPIC)
{
	BlueRose::Value xx;
	xx.setFloat(x);
	
	BlueRose::Value yy;
	yy.setFloat(y);
	setMemberValue("x", xx);
	setMemberValue("y", yy);
}

float PairEvent::getX()
{
	BlueRose::Value val = getMemberValue("x");
	
	return val.getFloat();
}

float PairEvent::getY()
{
	BlueRose::Value val = getMemberValue("y");
	
	return val.getFloat();
}

void PairEvent::setX(float x)
{
	Value value;
	value.setFloat(x);
	
	setMemberValue("x", value);
}

void PairEvent::setY(float y)
{
	Value value;
	value.setFloat(y);
	
	setMemberValue("y", value);
}

};

