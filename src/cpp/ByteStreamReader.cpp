#include <stdlib.h>
#include "BRUtil.h"
#include "ByteStreamReader.h"

namespace BlueRose {

ByteStreamReader::ByteStreamReader()
{
	
}

void ByteStreamReader::open(const std::vector<byte>& str)
{
	stream = str;
	iterator = stream.begin();
}

void ByteStreamReader::close()
{
	iterator = stream.end();
}

unsigned int ByteStreamReader::readSize()
{
	unsigned int res = 0;
	
	if (*iterator != 0xff)
	{
		res = (unsigned int)*iterator;
	}
	else
	{
		byte* p = (byte*)&res;
		
		++iterator;
		
		#ifdef BR_LITTLE_ENDIAN
			p[0] = *iterator;
			++iterator;
			p[1] = *iterator;
			++iterator;
			p[2] = *iterator;
			++iterator;
			p[3] = *iterator;
		#else
			p[3] = *iterator;
			++iterator;
			p[2] = *iterator;
			++iterator;
			p[1] = *iterator;
			++iterator;
			p[0] = *iterator;
		#endif
	}
	
	++iterator;
	
	return res;
}

void ByteStreamReader::skip(int num)
{
	iterator = iterator+num;
}

void ByteStreamReader::readToEnd(std::vector<byte>& bytes)
{
	bytes.insert(bytes.begin(), iterator, stream.end());
}

byte ByteStreamReader::readByte()
{
	byte res = *iterator;
	++iterator;
	
	return res;
}

bool ByteStreamReader::readBoolean()
{
	byte res = *iterator;
	++iterator;
	
	return ( (res == 0x00) ? false : true );
}

short ByteStreamReader::readShort()
{
	short i = 0;
	byte* p = (byte*)&i;
	
	#ifdef BR_LITTLE_ENDIAN
		p[0] = *iterator;
		++iterator;
		p[1] = *iterator;
	#else
		p[1] = *iterator;
		++iterator;
		p[0] = *iterator;
	#endif
	
	++iterator;
	
	return i;
}

int ByteStreamReader::readInteger()
{
	int i = 0;
	byte* p = (byte*)&i;
	
	#ifdef BR_LITTLE_ENDIAN
		p[0] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[3] = *iterator;
	#else
		p[3] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[0] = *iterator;
	#endif
	
	++iterator;
	
	return i;
}

long ByteStreamReader::readLong()
{
	long i = 0;
	byte* p = (byte*)&i;
	
	#ifdef BR_LITTLE_ENDIAN
		p[0] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[3] = *iterator;
		++iterator;
		p[4] = *iterator;
		++iterator;
		p[5] = *iterator;
		++iterator;
		p[6] = *iterator;
		++iterator;
		p[7] = *iterator;
	#else
		p[7] = *iterator;
		++iterator;
		p[6] = *iterator;
		++iterator;
		p[5] = *iterator;
		++iterator;
		p[4] = *iterator;
		++iterator;
		p[3] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[0] = *iterator;
	#endif
	
	++iterator;
	
	return i;
}

float ByteStreamReader::readFloat()
{
	float i;
	byte* p = (byte*)&i;
	
	#ifdef BR_LITTLE_ENDIAN
		p[0] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[3] = *iterator;
	#else
		p[3] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[0] = *iterator;
	#endif
	
	++iterator;
	
	return i;
}

double ByteStreamReader::readDouble()
{
	double i = 0;
	byte* p = (byte*)&i;
	
	#ifdef BR_LITTLE_ENDIAN
		p[0] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[3] = *iterator;
		++iterator;
		p[4] = *iterator;
		++iterator;
		p[5] = *iterator;
		++iterator;
		p[6] = *iterator;
		++iterator;
		p[7] = *iterator;
	#else
		p[7] = *iterator;
		++iterator;
		p[6] = *iterator;
		++iterator;
		p[5] = *iterator;
		++iterator;
		p[4] = *iterator;
		++iterator;
		p[3] = *iterator;
		++iterator;
		p[2] = *iterator;
		++iterator;
		p[1] = *iterator;
		++iterator;
		p[0] = *iterator;
	#endif
	
	++iterator;
	
	return i;
}

void ByteStreamReader::readString(std::string& res)
{
	unsigned int size = readSize();
	res = "";
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(*iterator);
		++iterator;
	}
}

void ByteStreamReader::readUTF8String(std::wstring& res)
{
	unsigned int insize = readSize();
	char* inbuf = (char*)malloc((insize+1)*sizeof(char));
	
	for (unsigned int k=0; k<insize; k++)
	{
		inbuf[k] = *iterator;
		++iterator;
	}
	
	inbuf[insize] = '\0';
	
	size_t outsize = utf8_to_wchar(inbuf, insize, NULL, 0, UTF8_IGNORE_ERROR);
	wchar_t* outbuf = (wchar_t*)malloc((outsize+1)*sizeof(wchar_t));
	
	outsize = utf8_to_wchar(inbuf, insize, outbuf, outsize, UTF8_IGNORE_ERROR);
	
	outbuf[outsize] = '\0';
	
	res = outbuf;
	
	free(inbuf);
	free(outbuf);
}

void ByteStreamReader::readByteSeq(std::vector<byte>& res)
{
	unsigned int size = readSize();
	res.clear();
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(*iterator);
		++iterator;
	}
}

void ByteStreamReader::readBooleanSeq(std::vector<bool>& res)
{
	unsigned int size = readSize();
	res.clear();
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(readBoolean());
	}
}

void ByteStreamReader::readShortSeq(std::vector<short>& res)
{
	unsigned int size = readSize();
	res.clear();
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(readShort());
	}
}

void ByteStreamReader::readIntegerSeq(std::vector<int>& res)
{
	unsigned int size = readSize();
	res.clear();
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(readInteger());
	}
}

void ByteStreamReader::readLongSeq(std::vector<long>& res)
{
	unsigned int size = readSize();
	res.clear();
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(readLong());
	}
}

void ByteStreamReader::readFloatSeq(std::vector<float>& res)
{
	unsigned int size = readSize();
	res.clear();
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(readFloat());
	}
}

void ByteStreamReader::readDoubleSeq(std::vector<double>& res)
{
	unsigned int size = readSize();
	res.clear();
	
	for (unsigned int k=0; k<size; k++)
	{
		res.push_back(readDouble());
	}
}

void ByteStreamReader::readUTF8StringSeq(std::vector<std::wstring>& res)
{
	unsigned int size = readSize();
	res.clear();
	std::wstring str;
	
	for (unsigned int k=0; k<size; k++)
	{
		readUTF8String(str);
		res.push_back(str);
	}
}

void ByteStreamReader::readStringSeq(std::vector<std::string>& res)
{
	unsigned int size = readSize();
	res.clear();
	std::string str;
	
	for (unsigned int k=0; k<size; k++)
	{
		readString(str);
		res.push_back(str);
	}
}

void ByteStreamReader::readDictionary(DictionaryDataType& res)
{
	unsigned int size = readSize();
	res.clear();
	
	std::string key;
	std::vector<byte> value;
	for (unsigned int i=0; i<size; i++)
	{
		readString(key);
		readByteSeq(value);
		
		res[key] = value;
	}
}

};

