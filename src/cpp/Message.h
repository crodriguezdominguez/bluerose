#ifndef _BLUEROSE_MESSAGE_H_
#define _BLUEROSE_MESSAGE_H_

#include <string>
#include <vector>
#include <map>
#include <pthread.h>
#include "BRUtil.h"
#include "MessageHeader.h"
#include "TypeDefinitions.h"
#include "ByteStreamReader.h"
#include "ByteStreamWriter.h"

namespace BlueRose {

	/**
	* Class for modelling an empty message.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class Message
	{
		protected:
			static ByteStreamWriter writer;
			static ByteStreamReader reader;
			static pthread_mutex_t mutex;
			static pthread_mutex_t mutex_reader;
		
		public:
			MessageHeader* header; /**< Header of the message */
			Encapsulation* body;  /**< Body of the message. It may be empty */
			
			/**
			* Default constructor
			*/
			Message();
			
			/**
			* Destructor
			*/
			virtual ~Message();
			
			/**
			* Adds bytes to the body
			*
			* @param vct Bytes to add
			*/
			void addToEncapsulation(const std::vector<byte>& bytes);
			
			/**
			* Returns the full message (header+body) as a byte sequence
			*
			* @param bytes Returned bytes
			*/
			virtual void bytes(std::vector<byte>& bytes) const;
	};
	
	/**
	* Batch request message
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class BatchRequestMessage : public Message {
		public:
			/**
			* Default constructor
			*/
			BatchRequestMessage();
			
			/**
			* Constructor with parameters
			*
			* @param msg Bytes of the message body
			*/
			BatchRequestMessage(const std::vector<byte>& msg);
	};
	
	/**
	* Request message
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class RequestMessage : public Message {
		public:
			/**
			* Default constructor
			*/
			RequestMessage();
			
			/**
			* Constructor with parameters
			*
			* @param msg Bytes of the message body
			*/
			RequestMessage(const std::vector<byte>& msg);
	};
	
	/**
	* Reply message
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class ReplyMessage : public Message {
		public:
			/**
			* Default constructor
			*/
			ReplyMessage();
			
			/**
			* Constructor with parameters
			*
			* @param msg Bytes of the message body
			*/
			ReplyMessage(const std::vector<byte>& msg);
	};
	
	/**
	* Validate connection message
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class ValidateConnectionMessage : public Message {
		public:
			/**
			* Default constructor
			*/
			ValidateConnectionMessage();
	};
	
	/**
	* Close connection message
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class CloseConnectionMessage : public Message {
		public:
			/**
			* Default constructor
			*/
			CloseConnectionMessage();
	};
	
	/**
	* Event message
	*
	* @author Carlos Rodriguez Dominguez
	* @date 12-10-2009
	*/
	class EventMessage : public Message {
		private:
			std::vector<byte> evt;
		
		public:
			/**
			* Default constructor
			*/
			EventMessage(const std::vector<byte>& _evt);
			
			/**
			* Set the event to send inside the message
			*
			* @param msg Event to send
			*/
			void setEvent(const std::vector<byte>& msg);
			
			void bytes(std::vector<byte>& bytes) const;
	};

};

#endif

