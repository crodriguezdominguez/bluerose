#include <cstdlib>
#include <cstdio>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <fcntl.h>
#include <unistd.h>
#include <ifaddrs.h>

#include "RequestReadThread.h"
#include "TcpCompatibleDevice.h"
#include "BRUtil.h"

namespace BlueRose {


TcpCompatibleDevice::TcpCompatibleDevice()
{
	priority = 0;
	isServant = false;
	servantAddress = "";
	pthread_mutex_init(&mutex, NULL);
}

TcpCompatibleDevice::TcpCompatibleDevice(const std::string& lowleveldevice)
{
	priority = 0;
	isServant = false;
	servantAddress = "";
	
	//Get localhost IP
	struct ifaddrs* ifAddrStruct=NULL;
	struct in_addr* tmpAddrPtr=NULL;
	char substr[5];
	char* address = NULL;

	getifaddrs(&ifAddrStruct);
	
	while (ifAddrStruct!=NULL) 
	{
		// check it is IP4 and not lo0
		if (ifAddrStruct->ifa_addr->sa_family==AF_INET &&
			strcmp(ifAddrStruct->ifa_name, lowleveldevice.c_str())==0) 
		{
	    	// is a valid IP4 Address
	    	tmpAddrPtr=&((struct sockaddr_in *)ifAddrStruct->ifa_addr)->sin_addr;
			address = inet_ntoa(*tmpAddrPtr);
			servantAddress = address;
			
			//it is not a 127.x.x.x address...
			substr[0] = address[0]; substr[1] = address[1]; substr[2] = address[2]; substr[3] = '\0';
			if (strcmp(substr, "127") != 0)
			{
				if (strcmp(address, "0.0.0.0") != 0) break;
			}
	  	}
	
		ifAddrStruct=ifAddrStruct->ifa_next;
	}
	
	if (ifAddrStruct == NULL)
	{
		printf("Warning: Only local connections will be allowed. Please, check your network configuration\n");
		fflush(stdout);
	}
	
	pthread_mutex_init(&mutex, NULL);
}

TcpCompatibleDevice::~TcpCompatibleDevice()
{
	std::map<std::string, int>::iterator iter;
	std::map<std::string, RequestReadThread*>::iterator iter2;
	std::map<std::string, ReplyReadThread*>::iterator iter3;
	
	for (iter2 = servantThreads.begin(); iter2 != servantThreads.end(); ++iter2)
	{
		delete iter2->second;
	}
	
	for (iter3 = clientThreads.begin(); iter3 != clientThreads.end(); ++iter3)
	{
		delete iter3->second;
	}
	
	sockets.clear();
	servantThreads.clear();
	clientThreads.clear();
	
	if (isServant) close(servantSocket);
	
	pthread_mutex_destroy(&mutex);
}

bool TcpCompatibleDevice::isConnectionOpenned(std::string userID)
{
	pthread_mutex_unlock(&mutex); //TODO: check this deadlock bug...
	
	bool openned = false;
	
	sockets.lockCollection();
	if (sockets.getSocket(userID) != -1)
	{
		openned = true;
	}
	sockets.unlockCollection();
	
	return openned;
}

void TcpCompatibleDevice::openConnection(std::string userID, DictionaryDataType parameters)
{
	//TODO: parameters
	
	//avoid reopen already opened connection
	bool connectionOpenned = isConnectionOpenned(userID);
	if (connectionOpenned)
	{
		pthread_mutex_lock(&mutex);
		openConnRefCounter[userID]++;
		pthread_mutex_unlock(&mutex);
	}
	else
	{
		pthread_mutex_lock(&mutex);
		openConnRefCounter[userID] = 1;
		pthread_mutex_unlock(&mutex);
	
		//establish connection
		struct sockaddr_in serv_addr;
		struct hostent *server;
	
		std::vector<std::string> results;
		splitString(userID, ":", results);
		int port = atoi(results[1].c_str());
		std::string host = results[0];
		int yes = 1;
		int delay = 1;
	
		int sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0) 
		{
			throw "ERROR: socket creation failed";
		}
	
		//no delay
		if (setsockopt(sockfd, IPPROTO_TCP, 
						TCP_NODELAY, 
						(char*)&delay, sizeof(delay)) < 0)
		{
			throw "ERROR: setting socket options";
		}
	
		//keep connection alive
		if (setsockopt(sockfd, SOL_SOCKET, 
						SO_KEEPALIVE, 
						(char*)&yes, sizeof(yes)) < 0)
		{
			throw "ERROR: setting socket options";
		}
	
		//initialize connection
	    server = gethostbyname(host.c_str());
	    if (server == NULL) 
		{
			throw "ERROR: host not found";
	    }

	    bzero((char *) &serv_addr, sizeof(serv_addr));

	    serv_addr.sin_family = AF_INET;

	    bcopy((char *)server->h_addr, 
			  (char *)&serv_addr.sin_addr.s_addr,
			  server->h_length);

	    serv_addr.sin_port = htons(port);

	    if (connect(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr)) < 0)
		{
			throw "ERROR: connection failed";
		}
	
		sockets.addSocket(userID, sockfd);
	
		//read validate connection msg
		std::vector<byte> msg;
		read(userID, msg);
	}
}

void TcpCompatibleDevice::closeConnection(std::string userID)
{
	//send CloseConnectionMessage
	if (isConnectionOpenned(userID))
	{
		pthread_mutex_lock(&mutex);
		openConnRefCounter[userID]--;
		if (openConnRefCounter[userID] > 0)
		{
			pthread_mutex_unlock(&mutex);
			return;
		}
		pthread_mutex_unlock(&mutex);
	
		if (isServant)
		{
			CloseConnectionMessage msg;
			std::vector<byte> msg_bytes;
			msg.bytes(msg_bytes);
	
			write(userID, msg_bytes);
		}
	
		sockets.removeSocket(userID);
	
		pthread_mutex_lock(&mutex);
	
		if (isServant)
		{
			std::map<std::string, RequestReadThread*>::iterator it = servantThreads.find(userID);
			if (it != servantThreads.end())
			{
				//TODO Mark for deallocate instead of deallocating directly
				delete it->second;
				servantThreads.erase(it);
			}
		}
		else
		{
			std::map<std::string, ReplyReadThread*>::iterator it = clientThreads.find(userID);
		
			if (it != clientThreads.end())
			{
				//TODO Mark for deallocate instead of deallocating directly
				delete it->second;
				clientThreads.erase(it);
			}
		}
	
		pthread_mutex_unlock(&mutex);
	
		sockets.removeSocket(userID);
	
		pthread_mutex_lock(&mutex);
		//if (sockets.find(userID) != sockets.end()) 
		//	sockets.erase(sockets.find(userID));
		
		if (openConnRefCounter.find(userID) != openConnRefCounter.end())
			openConnRefCounter.erase(openConnRefCounter.find(userID));
		//ips.erase(ips.find(userID));
		pthread_mutex_unlock(&mutex);
	}
}

void TcpCompatibleDevice::closeAllConnections()
{
	std::map<std::string, int>::iterator iter;
	std::map<std::string, RequestReadThread*>::iterator iter2;
	std::map<std::string, ReplyReadThread*>::iterator iter3;
	
	pthread_mutex_lock(&mutex);
	for (iter2 = servantThreads.begin(); iter2 != servantThreads.end(); ++iter2)
	{
		delete iter2->second;
	}
	pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	for (iter3 = clientThreads.begin(); iter3 != clientThreads.end(); ++iter3)
	{
		delete iter3->second;
	}
	pthread_mutex_unlock(&mutex);
	
	//pthread_mutex_lock(&mutex);
	sockets.clear();
	//pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	servantThreads.clear();
	pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	clientThreads.clear();
	pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	openConnRefCounter.clear();
	pthread_mutex_unlock(&mutex);
}

bool TcpCompatibleDevice::write(std::string readerID, std::vector<byte> data)
{
	bool found = true;
	
	int sock = sockets.getSocket(readerID);
	if (sock == -1) found = false;
	
	if (!found)
	{
		return false;
	}
	
	pthread_mutex_lock(&mutex);
	if (!isServant && data[8] == BR_REQUEST_MSG)
	{	
		RequestMessage msg(data);
		
		if (((RequestMessageHeader*)msg.header)->mode == BR_TWOWAY_MODE)
		{
			if (clientThreads.find(readerID) == clientThreads.end())
			{
				clientThreads[readerID] = new ReplyReadThread(this, readerID);
				clientThreads[readerID]->start();
			}
		}
	}
	pthread_mutex_unlock(&mutex);
	
	int number_of_bytes = (int)data.size();
	int r;
	char* data_to_write = (char*) &data[0];
	
	//force flush
	while (number_of_bytes) 
	{
	#ifdef SO_NOSIGPIPE
		r = ::write(sock,data_to_write,number_of_bytes);
	#else
		r = ::send(sock, data_to_write,number_of_bytes,MSG_NOSIGNAL);
	#endif
	  	if (r <= 0)
		{
			return false;
		}
		data_to_write += r;
		number_of_bytes -= r;
	}
	
	sockets.unlockCollection();
	return true;
}

bool TcpCompatibleDevice::read(std::string senderID, std::vector<byte>& msg)
{
	bool found = true;
	
	int sockfd = sockets.getSocket(senderID);
	if (sockfd == -1) found = false;
	
	if (!found)
	{
		//sockets.unlockCollection();
		return false;
	}
	
	int n;
	int size = -1;
	int buffer_size = 14;
	
	byte buffer_init[15];
	
	n = ::read(sockfd, buffer_init, 14);
	if (n < 14)
	{
		return false;
	}
	else
	{
		for (int i=0; i<n; i++)
		{
			msg.push_back(buffer_init[i]);
		}
		
		size = 0;
		byte* p = (byte*)&size;

		#ifdef BR_LITTLE_ENDIAN
			p[0] = buffer_init[10];
			p[1] = buffer_init[11];
			p[2] = buffer_init[12];
			p[3] = buffer_init[13];
		#else
			p[3] = buffer_init[10];
			p[2] = buffer_init[11];
			p[1] = buffer_init[12];
			p[0] = buffer_init[13];
		#endif
		
		buffer_size = size-n;
		
		if (buffer_size == 0)
		{
			//sockets.unlockCollection();
			return true;
		}
		else
		{
			byte* buffer = new byte[buffer_size+1];
		
			bool fin = false;
			while(!fin)
			{
				n = ::read(sockfd, buffer, buffer_size);
				if (n <= 0)
				{
					delete buffer;
					//sockets.unlockCollection();
					return false;
				}
			
				buffer_size -= n;

				if (buffer_size == 0)
				{
					fin = true;
				}

				for (int i=0; i<n; i++)
				{
					msg.push_back(buffer[i]);
				}
			}
		
			delete buffer;
		
			//sockets.unlockCollection();
			return true;
		}
	}
}

void TcpCompatibleDevice::setPriority(int __priority)
{
	priority = __priority;
}

int TcpCompatibleDevice::getPriority()
{
	return priority;
}

void TcpCompatibleDevice::broadcast(std::vector<byte> data)
{
	std::vector<std::string> neighbours = sockets.getUsers();
	
	for (unsigned int i=0; i<neighbours.size(); i++)
	{
		write(neighbours[i], data);
	}
}

void TcpCompatibleDevice::setServantIdentifier(std::string servantID)
{
	servantAddress = servantID;
}

void TcpCompatibleDevice::setServant(std::string multiplexingId)
{
	isServant = true;
	servantPort = atoi(multiplexingId.c_str());
	
	//Get localhost IP
	struct ifaddrs* ifAddrStruct=NULL;
	struct in_addr* tmpAddrPtr=NULL;
	char substr[5];
	char* address = NULL;
	char buffer[256];

	if (servantAddress == "" || servantAddress == "localhost" || servantAddress == "127.0.0.1")
	{
		getifaddrs(&ifAddrStruct);
	
		while (ifAddrStruct!=NULL) 
		{
			// check it is IP4 and not lo0
			if (ifAddrStruct->ifa_addr->sa_family==AF_INET && strcmp(ifAddrStruct->ifa_name, "lo0")!=0 &&
			    strlen(ifAddrStruct->ifa_name) <= 3) //avoid virtual connections 
			{
		    	// is a valid IP4 Address
		    	tmpAddrPtr=&((struct sockaddr_in *)ifAddrStruct->ifa_addr)->sin_addr;
				address = inet_ntoa(*tmpAddrPtr);
				sprintf(buffer, "%s:%d", address, servantPort);
				servantAddress = buffer;
			
				//it is not a 127.x.x.x address...
				substr[0] = address[0]; substr[1] = address[1]; substr[2] = address[2]; substr[3] = '\0';
				if (strcmp(substr, "127") != 0)
				{
					if (strcmp(address, "0.0.0.0") != 0) break;
				}
		  	}
	
			ifAddrStruct=ifAddrStruct->ifa_next;
		}
	
		if (ifAddrStruct == NULL)
		{
			printf("Warning: Only local connections will be allowed. Please, check your network configuration\n");
			fflush(stdout);
		}
	}
	else
	{
		//check if the port is not established...
		std::vector<std::string> results;
		splitString(servantAddress, ":", results);
		if (results.size() < 2)
		{
			sprintf(buffer, "%s:%d", servantAddress.c_str(), servantPort);
			servantAddress = buffer;
		}
	}
	
	this->start();
}

void TcpCompatibleDevice::run()
{
	int newsockfd;
	socklen_t clilen;

	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int yes = 1, delay=1;

	servantSocket = ::socket(AF_INET, SOCK_STREAM, 0);
	if (servantSocket < 0)
	{
		throw "ERROR: creating socket";
	}
	
	//avoid port used error
	if (setsockopt(servantSocket, SOL_SOCKET, 
					SO_REUSEADDR, 
					(char*)&yes, sizeof(yes)) < 0)
	{
		throw "ERROR: setting socket options";
	}
	
	//no-delay
	if (setsockopt(servantSocket, IPPROTO_TCP, 
					TCP_NODELAY, 
					(char*)&delay, sizeof(delay)) < 0)
	{
		throw "ERROR: setting socket options";
	}
	
	//keep alive connections
	if (setsockopt(servantSocket, SOL_SOCKET, 
					SO_KEEPALIVE, 
					(char*)&yes, sizeof(yes)) < 0)
	{
		throw "ERROR: setting socket options";
	}
	
#ifdef SO_NOSIGPIPE
	if (setsockopt(servantSocket, SOL_SOCKET, 
					SO_NOSIGPIPE, 
					(char*)&yes, sizeof(yes)) < 0)
	{
		throw "ERROR: setting socket options";					
	}
#endif

	bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(servantPort);
	if (bind(servantSocket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		throw "ERROR: binding servant port";
	}

	//listen for incoming connections
	listen(servantSocket, SOMAXCONN);

	while(1)
	{
		newsockfd = accept(servantSocket, 
					   (struct sockaddr*)&cli_addr, 
					   &clilen);

		if (newsockfd >= 0)
		{
			sprintf(buffer, "%s:%d", inet_ntoa(cli_addr.sin_addr), cli_addr.sin_port);
			
			printf("Connection opened: %s\n", buffer);
			
			pthread_mutex_lock(&mutex);

			//sockets[buffer] = newsockfd;
			sockets.addSocket(buffer, newsockfd);
			//ips[buffer] = inet_ntoa(cli_addr.sin_addr);
			servantThreads[buffer] = new RequestReadThread(this, buffer);
			servantThreads[buffer]->start();
			
			pthread_mutex_unlock(&mutex);
		}
	}
}

bool TcpCompatibleDevice::isBlockantConnection()
{
	return true;
}

bool TcpCompatibleDevice::isAvailable()
{
	sockets.lockCollection();
	
	SocketIterator iter;
	SocketIterator begin;
	SocketIterator end;

	ValidateConnectionMessage msg;
	std::vector<byte> bytes;
	msg.bytes(bytes);

	begin = sockets.begin();
	end = sockets.end();

	for (iter = begin; iter != end; ++iter)
	{
		if (write(iter->first, bytes))
		{
			sockets.unlockCollection();
			return true;
		}
	}

	sockets.unlockCollection();
	return false;
}

bool TcpCompatibleDevice::isAvailable(std::string userID)
{
	sockets.lockCollection();
	
	bool found = true;

	int sock = sockets.getSocket(userID);
	
	if (sock == -1) found = false;
	if (!found)
	{
		sockets.unlockCollection();
		return false;
	}
	
	ValidateConnectionMessage msg;
	std::vector<byte> bytes;
	msg.bytes(bytes);

	if (!write(userID, bytes))
	{
		sockets.unlockCollection();
		return false;
	}
	
	sockets.unlockCollection();
	return true;
}

bool TcpCompatibleDevice::isConnectionOriented()
{
	return true;
}

void TcpCompatibleDevice::waitForConnections()
{
	join();
}

std::string TcpCompatibleDevice::getName()
{
	return "TcpCompatibleDevice";
}

std::string TcpCompatibleDevice::servantIdentifier()
{
	return servantAddress;
}

};

