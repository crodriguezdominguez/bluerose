#include "Event.h"
#include "ByteStreamWriter.h"
#include "ByteStreamReader.h"

namespace BlueRose {
	
/****************************
*  			EVENT			*
****************************/
bool Event::existsMember(const std::string& member) const
{
	if (nodes.find(member) != nodes.end())
	{
		return true;
	}
	else return false;
}

Event::Event()
{
	topic = DEFAULT_TOPIC;
}

Event::Event(int _topic)
{
	topic = _topic;
}

unsigned int Event::size() const
{
	return nodes.size();
}
		
EventNode Event::getMember(const std::string& member)
{
	if (existsMember(member))
	{
		return nodes[member];
	}
	else return EventNode();
}

Value Event::getMemberValue(const std::string& member)
{
	if (existsMember(member))
	{
		return nodes[member].getValue();
	}
	else
	{
		EventNode nullEvent;
		return nullEvent.getValue();
	}
}

void Event::setMember(const std::string& member, EventNode& node)
{
	nodes[member] = node;
}

void Event::setMemberValue(const std::string& member, Value& value)
{
	if (existsMember(member))
	{
		nodes[member].setValue(value);
	}
	else
	{
		EventNode evnode(member, value);
		nodes[member] = evnode;
	}
}

int Event::getTopic() const
{
	return topic;
}

void Event::setTopic(int _topic)
{
	topic = _topic;
}

bool Event::isNull() const
{
	if (topic == DEFAULT_TOPIC && nodes.size() == 0)
		return true;
		
	else return false;
}

EventIterator Event::begin() const
{
	return nodes.begin();
}

EventIterator Event::end() const
{
	return nodes.end();
}

void Event::marshall(ByteStreamWriter& writer) const
{
	//topic
	writer.writeInteger(this->getTopic());
	
	//num. event nodes
	writer.writeSize(this->size());
	
	//event nodes
	for (EventIterator evt = this->begin(); evt != this->end(); ++evt)
	{
		evt->second.marshall(writer);
	}
}

void Event::unmarshall(ByteStreamReader& reader)
{
	//topic
	this->topic = reader.readInteger();
	
	//num. event nodes
	unsigned int tam = reader.readSize();
	
	if (tam == 0) return;
		
	//event nodes
	this->nodes.clear();
	EventNode aux;
	for (unsigned int i=0; i<tam; i++)
	{
		aux.unmarshall(reader);
		nodes[aux.getIdentifier()] = aux;
	}
}

/****************************
*  		EVENT_NODE			*
****************************/

EventNode::EventNode() : Event()
{
	identifier = "";
}

EventNode::EventNode(const std::string& _identifier, Value& _value)
{
	identifier = _identifier;
	value = _value;
}

std::string EventNode::getIdentifier() const
{
	return identifier;
}

Value EventNode::getValue() const
{
	return value;
}

void EventNode::setIdentifier(const std::string& _identifier)
{
	identifier = _identifier;
}

void EventNode::setValue(const Value& _value)
{
	value = _value;
}

void EventNode::marshall(ByteStreamWriter& writer) const
{
	//identifier
	writer.writeString(this->identifier);
	
	//type
	value.marshall(writer);
	
	//topic
	writer.writeInteger(this->topic);
	
	//num. event nodes
	writer.writeSize(this->nodes.size());
	
	//event nodes
	for (EventIterator evt = this->begin(); evt != this->end(); ++evt)
	{
		evt->second.marshall(writer);
	}
}

void EventNode::unmarshall(ByteStreamReader& reader)
{
	//identifier
	reader.readString(this->identifier);
	
	//value
	this->value.unmarshall(reader);
	
	//topic
	this->topic = reader.readInteger();
	
	//num. event nodes
	unsigned int size = reader.readSize();
	
	if (size == 0) return;
	
	//event nodes
	this->nodes.clear();
	EventNode aux;
	for (unsigned int i=0; i<size; i++)
	{
		aux.unmarshall(reader);
		nodes[aux.identifier] = aux;
	}
}
	
};


