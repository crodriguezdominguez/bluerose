#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <sys/time.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <cctype>

#include "BRUtil.h"

namespace BlueRose {

void makeEncapsulation(Encapsulation* enc)
{	
	enc->size = 6 + enc->byteCollection.size();
	enc->major = 0x01;
	enc->minor = 0x00;
}

void splitString(const std::string& _str, const std::string& delim, std::vector<std::string>& results)
{
	int cutAt;
	std::string str = _str;
	
	while( (size_t)(cutAt = str.find(delim)) != str.npos )
	{
		if(cutAt > 0) results.push_back(str.substr(0,cutAt));
		str = str.substr(cutAt+1);
	}
		
	if(str.length() > 0) results.push_back(str);
}

int sendPing(const std::string& target)
{
	int s, i, cc, packlen, datalen = 64-8;
	struct hostent *hp;
	struct sockaddr_in to, from;
	struct protoent	*proto;
	struct ip *ip;
	u_char *packet, outpack[65536-60-8];
	char hnamebuf[MAXHOSTNAMELEN];
	std::string hostname;
	struct icmp *icp;
	int ret, fromlen, hlen;
	fd_set rfds;
	struct timeval tv;
	int retval;
	struct timeval start, end;
	int end_t;
	bool cont = true;

	to.sin_family = AF_INET;

	// try to convert as dotted decimal address, else if that fails assume it's a hostname
	to.sin_addr.s_addr = inet_addr(target.c_str());
	if (to.sin_addr.s_addr != (u_int)-1)
		hostname = target;
	else 
	{
		hp = gethostbyname(target.c_str());
		if (!hp)
		{
			return -1;
		}
		to.sin_family = hp->h_addrtype;
		bcopy(hp->h_addr, (caddr_t)&to.sin_addr, hp->h_length);
		strncpy(hnamebuf, hp->h_name, sizeof(hnamebuf) - 1);
		hostname = hnamebuf;
	}
	packlen = datalen + 60 + 76;
	if ( (packet = new u_char[packlen]) == NULL)
	{
		return -1;
	}

	if ( (proto = getprotobyname("icmp")) == NULL)
	{
		return -1;
	}

	if ( (s = socket(AF_INET, SOCK_RAW, proto->p_proto)) < 0)
	{
		return -1;
	}

	icp = (struct icmp *)outpack;
	icp->icmp_type = ICMP_ECHO;
	icp->icmp_code = 0;
	icp->icmp_seq = 12345;	/* seq and id must be reflected */
	icp->icmp_id = getpid();

	cc = datalen + 8;

	gettimeofday(&start, NULL);

	i = sendto(s, (char *)outpack, cc, 0, (struct sockaddr*)&to, (socklen_t)sizeof(struct sockaddr_in));

	// Watch stdin (fd 0) to see when it has input.
	FD_ZERO(&rfds);
	FD_SET(s, &rfds);
	// Wait up to one seconds.
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	while(cont)
	{
		retval = select(s+1, &rfds, NULL, NULL, &tv);
		if (retval == -1)
		{
			return -1;
		}
		else if (retval)
		{
			fromlen = sizeof(sockaddr_in);
			if ( (ret = recvfrom(s, (char *)packet, packlen, 0,(struct sockaddr *)&from, (socklen_t*)&fromlen)) < 0)
			{
				return -1;
			}

			// Check the IP header
			ip = (struct ip *)((char*)packet); 
			hlen = sizeof( struct ip ); 
			if (ret < (hlen + ICMP_MINLEN)) 
			{
				return -1; 
			} 

			// Now the ICMP part 
			icp = (struct icmp *)(packet + hlen); 
			if (icp->icmp_type == ICMP_ECHOREPLY)
			{
				if (icp->icmp_seq != 12345)
				{
					continue;
				}
				if (icp->icmp_id != getpid())
				{
					continue;
				}
				cont = false;
			}
			else
			{
				continue;
			}

			gettimeofday(&end, NULL);
			end_t = 1000000*(end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec);

			if(end_t < 1)
				end_t = 1;

			return end_t;
		}
		else
		{
			return -1;
		}
	}
	
	return -1;
}

void printVector(const std::vector<byte>& v)
{
	for (unsigned int i=0; i<v.size(); i++)
	{
		printf("%d ", (int)v[i]);
	}

	printf("\n");
}

std::string generateUUID()
{
	int buf_int[4];
	//sranddev();
	srand(time(0));
	
	buf_int[0] = rand();
	buf_int[1] = rand();
	buf_int[2] = rand();
	buf_int[3] = rand();
	
	byte* bytes = (byte*)&buf_int;
	bytes[6] = (bytes[6] & 0x0f) | 0x40;
	bytes[8] = (bytes[8] & 0x3f) | 0x80;
	
	char buffer[50];
	std::string res;
	sprintf(buffer, "%02x%02x%02x%02x", (int)bytes[0], (int)bytes[1], (int)bytes[2], (int)bytes[3]);
	res = buffer;
	res += "-";
	
	sprintf(buffer, "%02x%02x", (int)bytes[4], (int)bytes[5]);
	res += buffer;
	res += "-";
	
	sprintf(buffer, "%02x%02x", (int)bytes[6], (int)bytes[7]);
	res += buffer;
	res += "-";
	
	sprintf(buffer, "%02x%02x", (int)bytes[8], (int)bytes[9]);
	res += buffer;
	res += "-";
	
	sprintf(buffer, "%02x%02x%02x%02x%02x%02x", (int)bytes[10], (int)bytes[11], 
									(int)bytes[12], (int)bytes[13], 
									(int)bytes[14], (int)bytes[15]);
	res += buffer;
	
	return res;
}

/*
 * Copyright (c) 2007 Alexey Vatchenko <av@bsdua.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#define _NXT	0x80
#define _SEQ2	0xc0
#define _SEQ3	0xe0
#define _SEQ4	0xf0
#define _SEQ5	0xf8
#define _SEQ6	0xfc

#define _BOM	0xfeff

static int __wchar_forbitten(wchar_t sym);
static int __utf8_forbitten(u_char octet);

static int __wchar_forbitten(wchar_t sym)
{
	/* Surrogate pairs */
	if (sym >= 0xd800 && sym <= 0xdfff)
		return (-1);

	return (0);
}

static int __utf8_forbitten(u_char octet)
{
	switch (octet) {
	case 0xc0:
	case 0xc1:
	case 0xf5:
	case 0xff:
		return (-1);
	}

	return (0);
}

size_t utf8_to_wchar(const char *in, size_t insize, wchar_t *out, size_t outsize, int flags)
{
	u_char *p, *lim;
	wchar_t *wlim, high;
	size_t n, total, i, n_bits;

	if (in == NULL || insize == 0 || (outsize == 0 && out != NULL))
		return (0);

	total = 0;
	p = (u_char *)in;
	lim = p + insize;
	wlim = out + outsize;

	for (; p < lim; p += n) {
		if (__utf8_forbitten(*p) != 0 &&
		    (flags & UTF8_IGNORE_ERROR) == 0)
			return (0);

		/*
		 * Get number of bytes for one wide character.
		 */
		n = 1;	/* default: 1 byte. Used when skipping bytes. */
		if ((*p & 0x80) == 0)
			high = (wchar_t)*p;
		else if ((*p & 0xe0) == _SEQ2) {
			n = 2;
			high = (wchar_t)(*p & 0x1f);
		} else if ((*p & 0xf0) == _SEQ3) {
			n = 3;
			high = (wchar_t)(*p & 0x0f);
		} else if ((*p & 0xf8) == _SEQ4) {
			n = 4;
			high = (wchar_t)(*p & 0x07);
		} else if ((*p & 0xfc) == _SEQ5) {
			n = 5;
			high = (wchar_t)(*p & 0x03);
		} else if ((*p & 0xfe) == _SEQ6) {
			n = 6;
			high = (wchar_t)(*p & 0x01);
		} else {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			continue;
		}

		/* does the sequence header tell us truth about length? */
		if ((int)(lim - p) <= (int)(n - 1)) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			n = 1;
			continue;	/* skip */
		}

		/*
		 * Validate sequence.
		 * All symbols must have higher bits set to 10xxxxxx
		 */
		if (n > 1) {
			for (i = 1; i < n; i++) {
				if ((p[i] & 0xc0) != _NXT)
					break;
			}
			if (i != n) {
				if ((flags & UTF8_IGNORE_ERROR) == 0)
					return (0);
				n = 1;
				continue;	/* skip */
			}
		}

		total++;

		if (out == NULL)
			continue;

		if (out >= wlim)
			return (0);		/* no space left */

		*out = 0;
		n_bits = 0;
		for (i = 1; i < n; i++) {
			*out |= (wchar_t)(p[n - i] & 0x3f) << n_bits;
			n_bits += 6;		/* 6 low bits in every byte */
		}
		*out |= high << n_bits;

		if (__wchar_forbitten(*out) != 0) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);	/* forbitten character */
			else {
				total--;
				out--;
			}
		} else if (*out == _BOM && (flags & UTF8_SKIP_BOM) != 0) {
			total--;
			out--;
		}

		out++;
	}

	return (total);
}

size_t wchar_to_utf8(const wchar_t *in, size_t insize, char *out, size_t outsize, int flags)
{
	wchar_t *w, *wlim, ch;
	u_char *p, *lim, *oc;
	size_t total, n;

	if (in == NULL || insize == 0 || (outsize == 0 && out != NULL))
		return (0);

	w = (wchar_t *)in;
	wlim = w + insize;
	p = (u_char *)out;
	lim = p + outsize;
	total = 0;
	for (; w < wlim; w++) {
		if (__wchar_forbitten(*w) != 0) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			else
				continue;
		}

		if (*w == _BOM && (flags & UTF8_SKIP_BOM) != 0)
			continue;

		if (*w < 0) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			continue;
		} else if (*w <= 0x0000007f)
			n = 1;
		else if (*w <= 0x000007ff)
			n = 2;
		else if (*w <= 0x0000ffff)
			n = 3;
		else if (*w <= 0x001fffff)
			n = 4;
		else if (*w <= 0x03ffffff)
			n = 5;
		else /* if (*w <= 0x7fffffff) */
			n = 6;

		total += n;

		if (out == NULL)
			continue;

		if ((int)(lim - p) <= (int)(n - 1))
			return (0);		/* no space left */

		/* make it work under different endians */
		ch = htonl(*w);
		oc = (u_char *)&ch;
		switch (n) {
		case 1:
			*p = oc[3];
			break;

		case 2:
			p[1] = _NXT | oc[3] & 0x3f;
			p[0] = _SEQ2 | (oc[3] >> 6) | ((oc[2] & 0x07) << 2);
			break;

		case 3:
			p[2] = _NXT | oc[3] & 0x3f;
			p[1] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[0] = _SEQ3 | ((oc[2] & 0xf0) >> 4);
			break;

		case 4:
			p[3] = _NXT | oc[3] & 0x3f;
			p[2] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[1] = _NXT | ((oc[2] & 0xf0) >> 4) |
			    ((oc[1] & 0x03) << 4);
			p[0] = _SEQ4 | ((oc[1] & 0x1f) >> 2);
			break;

		case 5:
			p[4] = _NXT | oc[3] & 0x3f;
			p[3] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[2] = _NXT | ((oc[2] & 0xf0) >> 4) |
			    ((oc[1] & 0x03) << 4);
			p[1] = _NXT | (oc[1] >> 2);
			p[0] = _SEQ5 | oc[0] & 0x03;
			break;

		case 6:
			p[5] = _NXT | oc[3] & 0x3f;
			p[4] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[3] = _NXT | (oc[2] >> 4) | ((oc[1] & 0x03) << 4);
			p[2] = _NXT | (oc[1] >> 2);
			p[1] = _NXT | oc[0] & 0x3f;
			p[0] = _SEQ6 | ((oc[0] & 0x40) >> 6);
			break;
		}

		/*
		 * NOTE: do not check here for forbitten UTF-8 characters.
		 * They cannot appear here because we do proper convertion.
		 */

		p += n;
	}

	return (total);
}

};

