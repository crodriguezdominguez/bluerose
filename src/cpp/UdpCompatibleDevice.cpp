#include <cstdlib>
#include <cstdio>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <fcntl.h>
#include <unistd.h>
#include <ifaddrs.h>

#include "RequestReadThread.h"
#include "UdpCompatibleDevice.h"
#include "BRUtil.h"

namespace BlueRose {


UdpCompatibleDevice::UdpCompatibleDevice()
{
	priority = 0;
	isServant = false;
	pthread_mutex_init(&mutex, NULL);
}

UdpCompatibleDevice::~UdpCompatibleDevice()
{
	std::map<std::string, int>::iterator iter;
	std::map<std::string, RequestReadThread*>::iterator iter2;
	std::map<std::string, ReplyReadThread*>::iterator iter3;
	
	for (iter2 = servantThreads.begin(); iter2 != servantThreads.end(); ++iter2)
	{
		delete iter2->second;
	}
	
	for (iter3 = clientThreads.begin(); iter3 != clientThreads.end(); ++iter3)
	{
		delete iter3->second;
	}
	
	sockets.clear();
	servantThreads.clear();
	clientThreads.clear();
	
	if (isServant) close(servantSocket);
	
	pthread_mutex_destroy(&mutex);
}

bool UdpCompatibleDevice::isConnectionOpenned(std::string userID)
{
	pthread_mutex_unlock(&mutex); //TODO: check this deadlock bug...
	
	bool openned = false;
	
	sockets.lockCollection();
	if (sockets.getSocket(userID) != -1)
	{
		openned = true;
	}
	sockets.unlockCollection();
	
	return openned;
}

void UdpCompatibleDevice::openConnection(std::string userID, DictionaryDataType parameters)
{
	//TODO: parameters
	
	//avoid reopen already opened connection
	bool connectionOpenned = isConnectionOpenned(userID);
	if (connectionOpenned)
	{
		pthread_mutex_lock(&mutex);
		openConnRefCounter[userID]++;
		pthread_mutex_unlock(&mutex);
	}
	else
	{
		pthread_mutex_lock(&mutex);
		openConnRefCounter[userID] = 1;
		pthread_mutex_unlock(&mutex);
	
		//establish connection
		struct sockaddr_in serv_addr;
		struct hostent *server;
	
		std::vector<std::string> results;
		splitString(userID, ":", results);
		int port = atoi(results[1].c_str());
		std::string host = results[0];
		int yes = 1;
	
		int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
		if (sockfd < 0) 
		{
			throw "ERROR: socket creation failed";
		}
	
		//keep connection alive
		if (setsockopt(sockfd, SOL_SOCKET, 
						SO_KEEPALIVE, 
						(char*)&yes, sizeof(yes)) < 0)
		{
			throw "ERROR: setting socket options";
		}
	
		//initialize connection
	    server = gethostbyname(host.c_str());
	    if (server == NULL) 
		{
			throw "ERROR: host not found";
	    }

	    bzero((char *) &serv_addr, sizeof(serv_addr));

	    serv_addr.sin_family = AF_INET;

	    bcopy((char *)server->h_addr, 
			  (char *)&serv_addr.sin_addr.s_addr,
			  server->h_length);

	    serv_addr.sin_port = htons(port);

	    /*if (connect(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr)) < 0)
		{
			throw "ERROR: connection failed";
		}*/
	
		sockets.addSocket(userID, sockfd);
	
		//write and read validate connection msg
		addrs[userID] = serv_addr;
		
		ValidateConnectionMessage msg;
		std::vector<byte> bytes;
		msg.bytes(bytes);
		write(userID, bytes);
		
		//bytes.clear();
		//read(userID, bytes);
	}
}

void UdpCompatibleDevice::closeConnection(std::string userID)
{
	//send CloseConnectionMessage
	if (isConnectionOpenned(userID))
	{
		pthread_mutex_lock(&mutex);
		openConnRefCounter[userID]--;
		if (openConnRefCounter[userID] > 0)
		{
			pthread_mutex_unlock(&mutex);
			return;
		}
		pthread_mutex_unlock(&mutex);
	
		if (isServant)
		{
			CloseConnectionMessage msg;
			std::vector<byte> msg_bytes;
			msg.bytes(msg_bytes);
	
			write(userID, msg_bytes);
		}
	
		sockets.removeSocket(userID);
	
		pthread_mutex_lock(&mutex);
	
		if (isServant)
		{
			std::map<std::string, RequestReadThread*>::iterator it = servantThreads.find(userID);
			if (it != servantThreads.end())
			{
				//TODO Mark for deallocate instead of deallocating directly
				delete it->second;
				servantThreads.erase(it);
			}
		}
		else
		{
			std::map<std::string, ReplyReadThread*>::iterator it = clientThreads.find(userID);
		
			if (it != clientThreads.end())
			{
				//TODO Mark for deallocate instead of deallocating directly
				delete it->second;
				clientThreads.erase(it);
			}
		}
	
		pthread_mutex_unlock(&mutex);
	
		sockets.removeSocket(userID);
	
		pthread_mutex_lock(&mutex);
		//if (sockets.find(userID) != sockets.end()) 
		//	sockets.erase(sockets.find(userID));
		
		if (openConnRefCounter.find(userID) != openConnRefCounter.end())
			openConnRefCounter.erase(openConnRefCounter.find(userID));
		//ips.erase(ips.find(userID));
		pthread_mutex_unlock(&mutex);
	}
}

void UdpCompatibleDevice::closeAllConnections()
{
	std::map<std::string, int>::iterator iter;
	std::map<std::string, RequestReadThread*>::iterator iter2;
	std::map<std::string, ReplyReadThread*>::iterator iter3;
	
	pthread_mutex_lock(&mutex);
	for (iter2 = servantThreads.begin(); iter2 != servantThreads.end(); ++iter2)
	{
		delete iter2->second;
	}
	pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	for (iter3 = clientThreads.begin(); iter3 != clientThreads.end(); ++iter3)
	{
		delete iter3->second;
	}
	pthread_mutex_unlock(&mutex);
	
	//pthread_mutex_lock(&mutex);
	sockets.clear();
	//pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	servantThreads.clear();
	pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	clientThreads.clear();
	pthread_mutex_unlock(&mutex);
	
	pthread_mutex_lock(&mutex);
	openConnRefCounter.clear();
	pthread_mutex_unlock(&mutex);
}

bool UdpCompatibleDevice::write(std::string readerID, std::vector<byte> data)
{
	int sock;

	bool found = true;
	
	sock = sockets.getSocket(readerID);
	if (sock == -1) found = false;

	if (!found && !isServant)
	{
		return false;
	}
	else if (isServant)
	{
		sock = servantSocket;
	}
	
	pthread_mutex_lock(&mutex);
	if (!isServant && data[8] == BR_REQUEST_MSG)
	{	
		RequestMessage msg(data);
		
		if (((RequestMessageHeader*)msg.header)->mode == BR_TWOWAY_MODE)
		{
			if (clientThreads.find(readerID) == clientThreads.end())
			{
				clientThreads[readerID] = new ReplyReadThread(this, readerID);
				clientThreads[readerID]->start();
			}
		}
	}
	pthread_mutex_unlock(&mutex);
	
	int number_of_bytes = (int)data.size();
	int r;
	char* data_to_write = (char*) &data[0];
	
	//force flush
	struct sockaddr_in sockaddr = addrs[readerID];
	
	while (number_of_bytes) 
	{
		r = ::sendto(sock, data_to_write,number_of_bytes, 0, (struct sockaddr*)&sockaddr, sizeof(sockaddr));
	  	if (r <= 0)
		{
			return false;
		}
		data_to_write += r;
		number_of_bytes -= r;
	}
	
	sockets.unlockCollection();
	return true;
}

bool UdpCompatibleDevice::read(std::string senderID, std::vector<byte>& msg)
{
	int sockfd;
	
	bool found = true;
	
	sockfd = sockets.getSocket(senderID);
	if (sockfd == -1) found = false;

	if (!found && !isServant)
	{
		return false;
	}
	else if (isServant)
	{
		sockfd = servantSocket;
	}
	
	int n;
	int size = -1;
	int buffer_size = 14;
	struct sockaddr_in sockaddr = addrs[senderID];
	socklen_t addr_len = sizeof(sockaddr);
	
	byte buffer_init[15];
	
	n = ::recvfrom(sockfd, buffer_init, 14, 0, (struct sockaddr*)&sockaddr, &addr_len);
	if (n < 14)
	{
		return false;
	}
	else
	{	
		for (int i=0; i<n; i++)
		{
			msg.push_back(buffer_init[i]);
		}
		
		size = 0;
		byte* p = (byte*)&size;

		#ifdef BR_LITTLE_ENDIAN
			p[0] = buffer_init[10];
			p[1] = buffer_init[11];
			p[2] = buffer_init[12];
			p[3] = buffer_init[13];
		#else
			p[3] = buffer_init[10];
			p[2] = buffer_init[11];
			p[1] = buffer_init[12];
			p[0] = buffer_init[13];
		#endif
		
		buffer_size = size-n;
		
		if (buffer_size == 0)
		{
			//sockets.unlockCollection();
			return true;
		}
		else
		{
			byte* buffer = new byte[buffer_size+1];
		
			bool fin = false;
			while(!fin)
			{
				n = ::recvfrom(sockfd, buffer, buffer_size, 0, (struct sockaddr*)&sockaddr, &addr_len);
				if (n <= 0)
				{
					delete buffer;
					//sockets.unlockCollection();
					return false;
				}
			
				buffer_size -= n;

				if (buffer_size == 0)
				{
					fin = true;
				}

				for (int i=0; i<n; i++)
				{
					msg.push_back(buffer[i]);
				}
			}
		
			delete buffer;
		
			//sockets.unlockCollection();
			return true;
		}
	}
}

void UdpCompatibleDevice::setPriority(int __priority)
{
	priority = __priority;
}

int UdpCompatibleDevice::getPriority()
{
	return priority;
}

void UdpCompatibleDevice::broadcast(std::vector<byte> data)
{
	std::vector<std::string> neighbours = sockets.getUsers();
	
	for (unsigned int i=0; i<neighbours.size(); i++)
	{
		write(neighbours[i], data);
	}
}

void UdpCompatibleDevice::setServantIdentifier(std::string servantID)
{
	servantAddress = servantID;
}

void UdpCompatibleDevice::setServant(std::string multiplexingId)
{
	isServant = true;
	servantPort = atoi(multiplexingId.c_str());
	
	//Get localhost IP
	struct ifaddrs* ifAddrStruct=NULL;
	struct in_addr* tmpAddrPtr=NULL;
	char substr[5];
	char* address = NULL;
	char buffer[256];

	getifaddrs(&ifAddrStruct);
	
	while (ifAddrStruct!=NULL) 
	{
		// check it is IP4 and not lo0
		if (ifAddrStruct->ifa_addr->sa_family==AF_INET && strcmp(ifAddrStruct->ifa_name, "lo0")!=0) 
		{
	    	// is a valid IP4 Address
	    	tmpAddrPtr=&((struct sockaddr_in *)ifAddrStruct->ifa_addr)->sin_addr;
			address = inet_ntoa(*tmpAddrPtr);
			sprintf(buffer, "%s:%d", address, servantPort);
			servantAddress = buffer;
			
			//it is not a 127.x.x.x address...
			substr[0] = address[0]; substr[1] = address[1]; substr[2] = address[2]; substr[3] = '\0';
			if (strcmp(substr, "127") != 0)
			{
				if (strcmp(address, "0.0.0.0") != 0) break;
			}
	  	}
	
		ifAddrStruct=ifAddrStruct->ifa_next;
	}
	
	if (ifAddrStruct == NULL)
	{
		printf("Warning: Only local connections will be allowed. Please, check your network configuration\n");
		fflush(stdout);
	}
	
	this->start();
}

void UdpCompatibleDevice::run()
{
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	socklen_t clilen = sizeof(cli_addr);
	int yes = 1;
	std::vector<byte> data;

	servantSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (servantSocket < 0)
	{
		throw "ERROR: creating socket";
	}
	
	//avoid port used error
	if (setsockopt(servantSocket, SOL_SOCKET, 
					SO_REUSEADDR, 
					(char*)&yes, sizeof(yes)) < 0)
	{
		throw "ERROR: setting socket options";
	}
		
	//keep alive connections
	if (setsockopt(servantSocket, SOL_SOCKET, 
					SO_KEEPALIVE, 
					(char*)&yes, sizeof(yes)) < 0)
	{
		throw "ERROR: setting socket options";
	}
	
#ifdef SO_NOSIGPIPE
	if (setsockopt(servantSocket, SOL_SOCKET, 
					SO_NOSIGPIPE, 
					(char*)&yes, sizeof(yes)) < 0)
	{
		throw "ERROR: setting socket options";					
	}
#endif

	bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(servantPort);
	if (bind(servantSocket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		throw "ERROR: binding servant port";
	}

	bool wasthere;
	while(1)
	{
		ssize_t n = ::recvfrom(servantSocket, buffer, 14, MSG_PEEK, (struct sockaddr*)&cli_addr, &clilen);
		
		if (n >= 0)
		{	
			data.clear();
			sprintf(buffer, "%s:%d", inet_ntoa(cli_addr.sin_addr), cli_addr.sin_port);
			std::string userID = buffer;
			if (addrs.find(userID) == addrs.end())
			{
				wasthere = false;
				addrs[userID] = cli_addr;
			}
			else wasthere = true;
			
			if (read(userID, data))
			{	
				if (data[8] == BR_VALIDATE_CONNECTION_MSG)
				{
					if (!wasthere)
					{
						printf("Connection opened: %s\n", buffer);
						write(userID, data);
					}
				}
				
				if (data[8] == BR_REQUEST_MSG)
				{
					DevicePool::dispatchMessage(userID, data);
				}
			}
		}
	}
}

bool UdpCompatibleDevice::isBlockantConnection()
{
	return true;
}

bool UdpCompatibleDevice::isAvailable()
{
	sockets.lockCollection();
	
	SocketIterator iter;
	SocketIterator begin;
	SocketIterator end;

	ValidateConnectionMessage msg;
	std::vector<byte> bytes;
	msg.bytes(bytes);

	begin = sockets.begin();
	end = sockets.end();

	for (iter = begin; iter != end; ++iter)
	{
		if (write(iter->first, bytes))
		{
			sockets.unlockCollection();
			return true;
		}
	}

	sockets.unlockCollection();
	return false;
}

bool UdpCompatibleDevice::isAvailable(std::string userID)
{
	sockets.lockCollection();
	
	bool found = true;

	int sock = sockets.getSocket(userID);
	
	if (sock == -1) found = false;
	if (!found)
	{
		sockets.unlockCollection();
		return false;
	}
	
	ValidateConnectionMessage msg;
	std::vector<byte> bytes;
	msg.bytes(bytes);

	if (!write(userID, bytes))
	{
		sockets.unlockCollection();
		return false;
	}
	
	sockets.unlockCollection();
	return true;
}

bool UdpCompatibleDevice::isConnectionOriented()
{
	return true;
}

void UdpCompatibleDevice::waitForConnections()
{
	join();
}

std::string UdpCompatibleDevice::getName()
{
	return "UdpCompatibleDevice";
}

std::string UdpCompatibleDevice::servantIdentifier()
{
	return servantAddress;
}

};

