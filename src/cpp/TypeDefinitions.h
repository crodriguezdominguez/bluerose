#ifndef _BLUEROSE_TYPE_DEFINITIONS_H_
#define _BLUEROSE_TYPE_DEFINITIONS_H_

#include <string>
#include <map>
#include <vector>

namespace BlueRose {

	typedef unsigned char byte; /**< Definition of a byte */

	typedef std::map<std::string, std::vector<byte> > DictionaryDataType; /**< Definition of a dictionary */

	typedef std::map<std::string, std::string> Context; /**< Definition of a context */

	/**
	* Encapsulation of the body of a message
	*/
	typedef struct __Encapsulation{
		int size; /**< Size of the encapsulation, including this header */
		byte major;  /**< Major allowed encoding version as defined by ICE */
		byte minor;  /**< Minor allowed encoding version as defined by ICE  */
		std::vector<byte> byteCollection;  /**< Message */
	} Encapsulation;

	/**
	* Identity for a request message
	*/
	class Identity {
		public:
			std::string name; /**< Name */
			std::string category;  /**< Category */
	};
	
	/**
	* Types of comparisons between values
	*/
	typedef enum _Comparison {
		EQUAL, NOT_EQUAL, LESS, LESS_EQUAL, GREATER, GREATER_EQUAL
	} Comparison;
};

#endif

