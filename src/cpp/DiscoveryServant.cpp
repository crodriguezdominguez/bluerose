#include "DiscoveryServant.h"
#include <iostream>

namespace BlueRose {
	
namespace Service {

DiscoveryServant::DiscoveryServant() 
 : ObjectServant()
{
	identity.name = "Discovery";
	identity.category = "BlueRoseService";
	
	BlueRose::DevicePool::registerObjectServant(this);
}

DiscoveryServant::~DiscoveryServant()
{
	BlueRose::DevicePool::unregisterObjectServant(this);
}

byte DiscoveryServant::runMethod(const std::string& userID,
								const std::string& method, 
								const std::vector<byte>& args, 
								std::vector<byte>& result)
{
	pthread_mutex_lock(&mutex_writer);
	pthread_mutex_lock(&mutex_reader);
	reader.open(args);
	writer.open(result);
	
	if (method == "addRegistration")
	{
		std::string name;
		std::string address;
		reader.readString(name);
		reader.readString(address);
		addRegistration(name, address);

		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "removeRegistration")
	{
		std::string name;
		reader.readString(name);
		removeRegistration(name);
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	else if (method == "resolve")
	{
		std::string name;
		reader.readString(name);
		writer.writeString(resolve(name));
		
		pthread_mutex_unlock(&mutex_reader);
		pthread_mutex_unlock(&mutex_writer);
		return BR_SUCCESS_STATUS;
	}
	
	writer.close();
	reader.close();
	
	pthread_mutex_unlock(&mutex_reader);
	pthread_mutex_unlock(&mutex_writer);
	
	return BR_OPERATION_NOT_EXIST_STATUS;							
}

void DiscoveryServant::addRegistration(const std::string& name, const std::string& address)
{
	registrations[name] = address;
	
	printf("%s registered as %s\n", name.c_str(), address.c_str());
	fflush(stdout);
}

void DiscoveryServant::removeRegistration(const std::string& name)
{
	std::map<std::string, std::string>::iterator it = registrations.find(name);
	
	if (it != registrations.end())
	{
		registrations.erase(it);
	}
	
	//printf("%s removed\n", name.c_str());
	//fflush(stdout);
}

std::string DiscoveryServant::resolve(const std::string& name)
{
	std::map<std::string, std::string>::iterator it = registrations.find(name);
	
	if (it != registrations.end())
	{
		return it->second;
	}
	else return "";
}

/*DiscoveryServantEventListener::DiscoveryServantEventListener(IMobileDiscovery* tcpd, DiscoveryServant* serv)
 : MobileDiscoveryListener(tcpd)
{
	servant = serv;
}

void DiscoveryServantEventListener::onElementDiscovered(const std::string& name, 
														const std::string& regtype, 
														const std::string& domain,
														bool moreComing, bool error)
{
	if (!error) getMobileDiscovery()->resolveUserID(name, regtype, domain);
}

void DiscoveryServantEventListener::onElementRegistered(const std::string& fullname, 
														const std::string& userID,
														const TXTRecord& records,
														bool moreComing, bool error)
{
	this->onUserIDResolved(fullname, userID, records, moreComing, error);
}

void DiscoveryServantEventListener::onElementRemoved(const std::string& name, 
													const std::string& regtype, 
													const std::string& domain,
													bool moreComing, bool error)
{
	if (!error)
	{
		std::vector<byte> result;
		std::vector<byte> args;
	
		ByteStreamWriter writer;
		writer.open(args);
		writer.writeString(name);
		writer.close();
	
		servant->runMethod(name, "removeRegistration", args, result);
	}
}

void DiscoveryServantEventListener::onUserIDResolved(const std::string& fullname, 
													const std::string& userID, 
													const TXTRecord& records,
													bool moreComing, bool error)
{
	if (!error)
	{
		std::vector<byte> result;
		std::vector<byte> args;
		
		std::vector<std::string> results;
		splitString(fullname, "._bluerose._tcp.", results);
		std::string name = results[0];
	
		ByteStreamWriter writer;
		writer.open(args);
		writer.writeString(name);
		writer.writeString(userID);
		writer.close();
	
		servant->runMethod(userID, "addRegistration", args, result);
	}
}*/
	
};
	
};

int main(int argc, char** argv)
{
	//initialize transmission interfaces
	try{
	BlueRose::TcpCompatibleDevice device;
	
	//initialize objects
	BlueRose::Service::DiscoveryServant* servant = new BlueRose::Service::DiscoveryServant();
	
	//initialize bluerose
	if (argc > 1) BlueRose::Initializer::initialize(argv[1]);
	else BlueRose::Initializer::initialize("config.xml");
	
	//try to detect services...
	//BlueRose::MobileTcpDiscovery tcpd;
	//BlueRose::Service::DiscoveryServantEventListener el(&tcpd, servant);
	//tcpd.registerForDetection(servant, &device);
	//tcpd.addEventListener(&el);
	//tcpd.startDetection(BlueRose::MobileTcpDiscovery::BLUEROSE);
	
	//initialize the servant
	BlueRose::Initializer::initializeServant(servant, &device, false);
	
	//wait for connections to the device
	device.waitForConnections();
	
	//cleanups
	BlueRose::Initializer::destroy();
	delete servant;
	} catch(char* ex)
	{
		std::cout << ex << std::endl;
	}

	return 0;
}

