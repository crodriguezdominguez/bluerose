#include "ReplyReadThread.h"
#include "ByteStreamReader.h"
#include "ReplyMessageStack.h"
#include "EventHandler.h"

namespace BlueRose {
	

ReplyReadThread::ReplyReadThread(ICommunicationDevice* iface, const std::string _userID)
{	
	interface = iface;
	userID = _userID;
}

void ReplyReadThread::run()
{
	pthread_detach(pthread_self());
	
	std::vector<byte> bytes;
	ByteStreamReader reader;
	Event evt;
	
	while ( interface->read(userID, bytes) ) //read messages
	{			
		if (bytes[8] == BR_REPLY_MSG)
		{
			//add to reply stack
			reader.open(bytes);
		
			reader.skip(14);
			int requestId = reader.readInteger();
			
			ReplyMessageStack::produce(requestId, bytes);
		
			reader.close();
			
			bytes.clear();
		}
		
		else if (bytes[8] == BR_EVENT_MSG)
		{
			reader.open(bytes);
			reader.skip(14);
			
			Event evt;
			evt.unmarshall(reader);
			EventHandler::onConsume(evt);
			
			reader.close();
			
			bytes.clear();
		}
		
		else if (bytes[8] == BR_CLOSE_CONNECTION_MSG)
		{
			bytes.clear();
			break;
		}
	}
	
	interface->closeConnection(userID);
}


};
