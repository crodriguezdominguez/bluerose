#include "Message.h"

namespace BlueRose {

ByteStreamWriter Message::writer;
ByteStreamReader Message::reader;
pthread_mutex_t Message::mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t Message::mutex_reader = PTHREAD_MUTEX_INITIALIZER;

Message::Message()
{
	header = NULL;
	body = NULL;
}

Message::~Message()
{
	if (header != NULL)
		delete header;
		
	if (body != NULL)
		delete body;
}

void Message::addToEncapsulation(const std::vector<byte>& bytes)
{
	if (body != NULL)
	{
		body->size += bytes.size();
		body->byteCollection.insert(body->byteCollection.end(), bytes.begin(), bytes.end());
	}
}

void Message::bytes(std::vector<byte>& bytes) const
{
	pthread_mutex_lock(&mutex);
	writer.open(bytes);
	std::vector<byte> hd;

	if (body != NULL)
	{
		header->bytes(hd);
		header->messageSize = hd.size() + 6 + body->byteCollection.size();

		hd.clear();
		header->bytes(hd);

		writer.writeRawBytes(hd);
		writer.writeInteger(body->size);
		writer.writeByte(body->major);
		writer.writeByte(body->minor);
		writer.writeRawBytes(body->byteCollection);
	}
	else
	{
		header->bytes(hd);
		writer.writeRawBytes(hd);
	}
	
	writer.close();
	pthread_mutex_unlock(&mutex);
}



BatchRequestMessage::BatchRequestMessage() : Message()
{
	header = new BatchRequestMessageHeader();
	body = new Encapsulation;
	makeEncapsulation(body);
}

BatchRequestMessage::BatchRequestMessage(const std::vector<byte>& msg) : Message()
{
	Context context;
	unsigned int size, i;
	std::string key;
	
	header = new BatchRequestMessageHeader();
	body = new Encapsulation;
	makeEncapsulation(body);
	
	BatchRequestMessageHeader* h2 = (BatchRequestMessageHeader*)header;
	
	pthread_mutex_lock(&mutex_reader);
	reader.open(msg);
	
	reader.skip(14);

	h2->numRequests = reader.readInteger();

	reader.readString(h2->id.name);
	reader.readString(h2->id.category);
	
	reader.readStringSeq(h2->facet);
	
	reader.readString(h2->operation);
	
	h2->mode = reader.readByte();

	size = reader.readSize();
	for (i=0; i<size; i++)
	{
		reader.readString(key);
		reader.readString(context[key]);
	}
	
	h2->context = context;

	body->size = reader.readInteger();

	body->major = reader.readByte();
	body->minor = reader.readByte();

	body->byteCollection.clear();
	reader.readToEnd(body->byteCollection);
	
	reader.close();
	pthread_mutex_unlock(&mutex_reader);
}


RequestMessage::RequestMessage() : Message()
{
	header = new RequestMessageHeader();
	body = new Encapsulation;
	makeEncapsulation(body);
}

RequestMessage::RequestMessage(const std::vector<byte>& msg) : Message()
{
	Context context;
	unsigned int size, i;
	std::string key;
	
	header = new RequestMessageHeader();
	body = new Encapsulation;
	makeEncapsulation(body);
	
	RequestMessageHeader* h1 = (RequestMessageHeader*)header;
		
	pthread_mutex_lock(&mutex_reader);
	reader.open(msg);
	
	reader.skip(14);
		
	h1->requestId = reader.readInteger();
		
	reader.readString(h1->id.name);
	reader.readString(h1->id.category);
	
	reader.readStringSeq(h1->facet);
	
	reader.readString(h1->operation);
	h1->mode = reader.readByte();

	size = reader.readSize();
	for (i=0; i<size; i++)
	{
		reader.readString(key);
		reader.readString(context[key]);
	}
	
	h1->context = context;
	
	body->size = reader.readInteger();

	body->major = reader.readByte();
	body->minor = reader.readByte();

	body->byteCollection.clear();
	reader.readToEnd(body->byteCollection);
	
	reader.close();
	pthread_mutex_unlock(&mutex_reader);
}


ReplyMessage::ReplyMessage() : Message()
{
	header = new ReplyMessageHeader();
	body = new Encapsulation;
	makeEncapsulation(body);
}

ReplyMessage::ReplyMessage(const std::vector<byte>& msg) : Message()
{
	header = new ReplyMessageHeader();
	body = new Encapsulation;
	makeEncapsulation(body);
	
	ReplyMessageHeader* h3 = (ReplyMessageHeader*)header;
		
	pthread_mutex_lock(&mutex_reader);
	reader.open(msg);
	
	reader.skip(14);
		
	h3->requestId = reader.readInteger();
	h3->replyStatus = reader.readByte();
	
	body->size = reader.readInteger();

	body->major = reader.readByte();
	body->minor = reader.readByte();

	body->byteCollection.clear();
	reader.readToEnd(body->byteCollection);
	
	reader.close();
	pthread_mutex_unlock(&mutex_reader);
}


ValidateConnectionMessage::ValidateConnectionMessage() : Message()
{
	header = new ValidateConnectionMessageHeader();
}


CloseConnectionMessage::CloseConnectionMessage() : Message()
{
	header = new CloseConnectionMessageHeader();
}


EventMessage::EventMessage(const std::vector<byte>& _evt) : Message()
{
	evt = _evt;
	header = new EventMessageHeader();
}

void EventMessage::setEvent(const std::vector<byte>& msg)
{
	evt = msg;
}

void EventMessage::bytes(std::vector<byte>& _bytes) const
{
	std::vector<byte> header_bytes;
	
	pthread_mutex_lock(&mutex);
	writer.open(_bytes);
	
	header->bytes(header_bytes);
	header->messageSize = (header_bytes.size() + evt.size());
	header_bytes.clear();
	header->bytes(header_bytes);
	
	writer.writeRawBytes(header_bytes);
	writer.writeRawBytes(evt);

	writer.close();
	pthread_mutex_unlock(&mutex);
}


};

