#ifndef _BLUEROSE_TCP_TRANSMISSION_H_
#define _BLUEROSE_TCP_TRANSMISSION_H_

#include <vector>
#include <map>
#include <string>
#include <cstdio>
#include "ICommunicationDevice.h"
#include "Thread.h"
#include "RequestReadThread.h"
#include "ReplyReadThread.h"
#include "ReplyMessageStack.h"
#include "SocketCollection.h"

namespace BlueRose {

	/**
	* Module for transferring messages through the system
	* default TCP transmission interface.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 16-10-2009
	*/
	class TcpCompatibleDevice : public ICommunicationDevice {
		protected:
			int priority; /**< Priority of the module inside the interface pool. @see DevicePool.h */
			int servantSocket; /**< Socket for accepting connections to a servant object */
			int servantPort; /**< Port for accepting connections to a servant object */
			bool isServant; /**< True if the interface is used for a servant */
			std::string servantAddress; /**< If it's a servant, here it resides the address */
			pthread_mutex_t mutex; /**< Mutex */
			//std::map<std::string, int> sockets; /**< Sockets for each accepted connection */
			SocketCollection sockets;
			std::map<std::string, int> openConnRefCounter; /**< Number of connections to a servant */
			std::map<std::string, RequestReadThread*> servantThreads; /**< Threads for servants */
			std::map<std::string, ReplyReadThread*> clientThreads; /**< Threads for proxy objects */
		
		public:
			/**
			* Default constructor
			*/
			TcpCompatibleDevice();
			
			/**
			* Constructor that enables the specification of a low level device
			*
			* @param lowleveldevice Device that will be used for the communications
			*/
			TcpCompatibleDevice(const std::string& lowleveldevice);
			
			/**
			* Destructor
			*/
			~TcpCompatibleDevice();
			
			/**
			* Method for accepting connections to the servant
			*/
			void run();
			
			///////////////////////////////////////////////////////////////////
			//overloading of the ICommunicationDevice methods. @see ICommunicationDevice.h///
			///////////////////////////////////////////////////////////////////
			
			void setServantIdentifier(std::string servantID);
			void setServant(std::string multiplexingId);
			bool isConnectionOpenned(std::string userID);
			void openConnection(std::string userID, DictionaryDataType parameters = DictionaryDataType());
			void closeConnection(std::string userID);
			void closeAllConnections();
			bool write(std::string readerID, std::vector<byte> data);
			bool read(std::string senderID, std::vector<byte>& msg);
			void setPriority(int priority);
			int getPriority();
			void broadcast(std::vector<byte> data);
			bool isBlockantConnection();
			bool isAvailable();
			bool isAvailable(std::string userID);
			bool isConnectionOriented();
			void waitForConnections();
			std::string getName();
			std::string servantIdentifier();
	};
	
};

#endif

