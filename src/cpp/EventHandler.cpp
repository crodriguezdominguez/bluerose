#include "EventHandler.h"

namespace BlueRose {

/************************************
			EVENT HANDLER
************************************/

PubSubProxy* EventHandler::proxy = NULL;
std::map<int, std::vector<IEventListener*> > EventHandler::listeners;

void EventHandler::initEventHandler(ICommunicationDevice* _iface,
									bool waitOnline, 
									const DictionaryDataType& connParameters)
{
	if (proxy == NULL) 
		proxy = new PubSubProxy(_iface, waitOnline, connParameters);
}

void EventHandler::initEventHandler(const std::string& _servantID,
									ICommunicationDevice* _iface,
									bool waitOnline, 
									const DictionaryDataType& connParameters)
{
	if (proxy == NULL) 
		proxy = new PubSubProxy(_servantID, _iface, waitOnline, connParameters);
}

void EventHandler::destroyEventHandler()
{
	//remove the listeners
	std::map<int, std::vector<IEventListener*> >::iterator it;
	for (it = listeners.begin(); it != listeners.end(); ++it)
	{
		std::vector<IEventListener*> v = it->second;
		
		for (unsigned int i=0; i<v.size(); i++)
		{
			delete v[i];
		}
	}
	
	listeners.clear();
	
	if (proxy != NULL)
	{
		proxy->unsubscribe();
		delete proxy;
	}
}

void EventHandler::onConsume(const Event& evt)
{
	int topic = evt.getTopic();
	std::map<int, std::vector<IEventListener*> >::iterator it = listeners.find(topic);
	if (it != listeners.end())
	{
		std::vector<IEventListener*> v = it->second;
		for (unsigned int i=0; i<v.size(); i++)
		{
			if (v[i]->check(topic))
			{
				if (v[i]->check(evt))
				{
					v[i]->action(&evt);
				}
			}
		}
	}
}

void EventHandler::addEventListener(IEventListener* listener)
{
	int topic = listener->getTopic();
	
	std::map<int, std::vector<IEventListener*> >::iterator it = listeners.find(topic);
	if (it == listeners.end())
	{
		std::vector<IEventListener*> v;
		v.push_back(listener);
		
		listeners[topic] = v;
	}
	else
	{
		listeners[topic].push_back(listener);
	}
	
	if (!listener->hasPredicate())
		proxy->subscribe(topic);
		
	else proxy->subscribe(topic, listener->getPredicate());
	
	//send an event
	Event evt;
	proxy->publish(evt);
}

void EventHandler::removeEventListener(IEventListener* listener)
{
	removeEventListener(listener->getTopic(), listener->getName());
}

void EventHandler::removeEventListeners(int topic)
{
	std::map<int, std::vector<IEventListener*> >::iterator it = listeners.find(topic);
	if (it != listeners.end())
	{
		proxy->unsubscribe(topic);
		std::vector<IEventListener*> v = it->second;
		for (unsigned int i=0; i<v.size(); i++)
		{
			delete v[i];
		}
		
		listeners.erase(it);
	}
}

void EventHandler::removeEventListener(int topic, const std::string& name)
{
	std::map<int, std::vector<IEventListener*> >::iterator it = listeners.find(topic);
	if (it != listeners.end())
	{
		std::vector<IEventListener*> v = it->second;
		std::vector<IEventListener*>::iterator it2;
		
		for (it2=v.begin(); it2!=v.end(); ++it2)
		{
			if ((*it2)->getName() == name)
			{
				delete *it2;
				v.erase(it2);
				
				listeners[topic] = v;
				
				if (v.size() == 0)
				{
					removeEventListeners(topic);
				}
				
				return;
			}
		}
	}
}

void EventHandler::publish(const Event& evt, bool comeBack)
{
	proxy->publish(evt, comeBack);
}

PubSubProxy* EventHandler::getPubSubProxy()
{
	return proxy;
}

/************************************
			PUB/SUB PROXY
************************************/

PubSubProxy::PubSubProxy(ICommunicationDevice* _iface, bool waitOnline, const DictionaryDataType& connParameters)
 : ObjectProxy()
{
	identity.name = "PubSub";
	identity.category = "BlueRoseService";
	
	resolveInitialization(_iface, waitOnline, connParameters);
}

PubSubProxy::PubSubProxy(const std::string& _servantID, ICommunicationDevice* _iface, bool waitOnline, const DictionaryDataType& connParameters)
 : ObjectProxy(_servantID, _iface, waitOnline, connParameters)
{
	identity.name = "PubSub";
	identity.category = "BlueRoseService";
}

PubSubProxy::~PubSubProxy()
{
	
}

std::string PubSubProxy::getTypeID() const
{
	return "BlueRoseService::PubSub";
}

void PubSubProxy::subscribe(int _topic)
{
	int reqId;

	currentMode = BR_ONEWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;

	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	writer.writeInteger(_topic);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "subscribe0", enc);
	receiveReply(reqId, result_bytes);
}

void PubSubProxy::subscribe(int _topic, Predicate _pred)
{
	int reqId;

	currentMode = BR_ONEWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;

	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	writer.writeInteger(_topic);
	_pred.marshall(writer);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "subscribe1", enc);
	receiveReply(reqId, result_bytes);
}

void PubSubProxy::unsubscribe()
{
	int reqId;

	currentMode = BR_ONEWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;

	reqId = sendRequest(servantID, "unsubscribe0", enc);
	receiveReply(reqId, result_bytes);
}

void PubSubProxy::unsubscribe(int _topic)
{
	int reqId;

	currentMode = BR_ONEWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;

	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	writer.writeInteger(_topic);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "unsubscribe1", enc);
	receiveReply(reqId, result_bytes);
}

bool PubSubProxy::isSubscribed(const std::string& _userID, const Event& _template)
{
	int reqId;
	currentMode = BR_TWOWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;

	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	writer.writeString(_userID);
	_template.marshall(writer);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "isSubscribed", enc);
	receiveReply(reqId, result_bytes);

	pthread_mutex_lock(&mutex_reader);
	reader.open(result_bytes);
	bool result = reader.readBoolean();
	reader.close();
	pthread_mutex_unlock(&mutex_reader);

	return result;
}

std::vector<std::string> PubSubProxy::getSubscribers()
{
	int reqId;

	currentMode = BR_TWOWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;

	reqId = sendRequest(servantID, "getSubscribers0", enc);
	receiveReply(reqId, result_bytes);
	
	pthread_mutex_lock(&mutex_reader);
	reader.open(result_bytes);
	std::vector<std::string> result;
	reader.readStringSeq(result);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);
	
	return result;
}

std::vector<std::string> PubSubProxy::getSubscribers(const Event& _template)
{
	int reqId;

	currentMode = BR_TWOWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;
	
	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	_template.marshall(writer);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "getSubscribers1", enc);
	receiveReply(reqId, result_bytes);
	
	pthread_mutex_lock(&mutex_reader);
	reader.open(result_bytes);
	std::vector<std::string> result;
	reader.readStringSeq(result);
	reader.close();
	pthread_mutex_unlock(&mutex_reader);
	
	return result;
}

void PubSubProxy::publish(const Event& evt, bool comeBack)
{
	int reqId;

	currentMode = BR_TWOWAY_MODE;

	std::vector<BlueRose::byte> result_bytes;
	std::vector<BlueRose::byte> enc;

	pthread_mutex_lock(&mutex_writer);
	writer.open(enc);
	evt.marshall(writer);
	writer.writeBoolean(comeBack);
	writer.close();
	pthread_mutex_unlock(&mutex_writer);

	reqId = sendRequest(servantID, "publish", enc);
	receiveReply(reqId, result_bytes);
}
	
};

