#ifndef _BLUEROSE_PUB_SUB_SERVICE_H_
#define _BLUEROSE_PUB_SUB_SERVICE_H_

#include <string>
#include <map>
#include "BlueRose.h"

namespace BlueRose {
	
	namespace Service {
	
		/**
		* Implements the pub/sub service. It runs (by default)
		* on port 10001.
		*/
		int main(int argc, char** argv);
	
		/**
		* Servant for the pub/sub service requests
		*
		* @author Carlos Rodriguez Dominguez
		* @date 09-02-2010
		*/
		class PubSubServant : public ObjectServant {
			protected:
				/**
				* Subscriptions of each user
				*/
				std::map<std::string, std::vector<std::pair<int, Predicate> > > subscriptions;
				
			public:
				/**
				* Default constructor
				*/
				PubSubServant();
				
				/**
				* Destructor
				*/
				~PubSubServant();
				
				/**
				* Overload of the correspondent method of the ObjectServant class (@see ObjectServant.h).
				*/
				byte runMethod(const std::string& userID,
										const std::string& method, 
										const std::vector<byte>& args, 
										std::vector<byte>& result);
				
				/**
				* Adds a new subscription to a topic.
				* It needs the senderID for internal tasks.
				*
				* @param _topic Topic to subscribe to
				* @param senderID Identifier of the sender
				*/
				void subscribe(int _topic, const std::string& senderID);
				
				/**
				* Adds a new subscription to a topic, but with a set of constraints.
				* It needs the senderID for internal tasks.
				*
				* @param _topic Topic to subscribe to
				* @param _pred Set of constraints over the properties of the events
				* @param senderID Identifier of the sender
				*/
				void subscribe(int _topic, Predicate _pred, const std::string& senderID);
				
				/**
				* Removes all the subscriptions associated with an user
				*
				* @param senderID User to whom all the subscriptions will be removed
				*/
				void unsubscribe(const std::string& senderID);
				
				/**
				* Removes a subscription associated with an user and a topic
				*
				* @param _topic Topic associated with the subscription to remove
				* @param senderID User associated with the subscription to remove
				*/
				void unsubscribe(int _topic, const std::string& senderID);
				
				/**
				* Checks if any of the user subscriptions allow him/her to receive the event
				* that is specified by the template
				*
				* @param _userID User whose subscription is going to be checked
				* @param _template Event template
				* @return True if the user may receive the event specified by the template.
				*         False otherwise else.
				*/
				bool isSubscribed(const std::string& _userID, const Event& _template);
				
				/**
				* Returns all the subscribers
				*
				* @return Vector of subscribers
				*/
				std::vector<std::string> getSubscribers();
				
				/**
				* Returns all the subscribers that may receive the specified event
				*
				* @param _template Event
				* @return Vector of subscribers
				*/
				std::vector<std::string> getSubscribers(const Event& _template);
				
				/**
				* Publishes an event to all its subscribers. TODO: Develop an
				* unreliable (more efficient) method by returning inmediately
				* from the method and charging the event in a queue that is managed
				* by an external thread.
				*
				* @param evt Event to be published
				* @param comeBack If true, the event will be published back to the publisher
				*                if it is subscribed to it.
				* @param senderID Identifier of the sender of the event
				*/
				void publish(const Event& evt, bool comeBack, const std::string& senderID);
		};
		
	};
	
};

#endif

