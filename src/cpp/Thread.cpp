#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include "Thread.h"

namespace BlueRose {

/*
* Funcion tipo C usada para habilitar la compatibilidad
* con hebras POSIX. (No todas las implementaciones de hebras
* POSIX admite usar punteros a metodos de una clase de C++).
*/
void* __thread_auxiliary_function(void* args)
{
	Thread* th = (Thread*)args;
	th->run();
	
	return NULL;
}

Thread::Thread()
{
	routine = &__thread_auxiliary_function;
	_args = (void*)this;
}

Thread::Thread(void* (*start_routine)(void*), void* args)
{
	routine = start_routine;
	_args = args;
}

void Thread::run()
{
	
}

void Thread::start()
{
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	pthread_create(&_thread, &attr, routine, _args);
	pthread_attr_destroy(&attr);
}

void Thread::cancel()
{
	pthread_cancel(_thread);
}

void Thread::join()
{
	pthread_join(_thread, NULL);
}

void Thread::sleep(int seconds)
{
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&cond, NULL);
	
	struct timeval now;
	struct timespec timeout;
	        
	pthread_mutex_lock(&mutex);
	gettimeofday(&now,NULL);
	timeout.tv_sec = now.tv_sec + seconds;
    timeout.tv_nsec = 0;
    int retcode = 0;
	while (retcode != ETIMEDOUT) 
	{
		retcode = pthread_cond_timedwait(&cond, &mutex, &timeout);
	}
	pthread_mutex_unlock(&mutex);
	
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

};

