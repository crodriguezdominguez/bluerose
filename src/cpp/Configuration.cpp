#include "Configuration.h"

namespace BlueRose {

Configuration::Configuration(const std::string& XMLfile)
{
	LIBXML_TEST_VERSION
	
	doc = xmlParseFile(XMLfile.c_str());
	
	if (doc == NULL)
	{
		throw "Configuration file not found";
	}
}

Configuration::~Configuration()
{
	xmlFreeDoc(doc);
	xmlCleanupParser();
}

std::vector<std::string> Configuration::getDevices()
{
	std::vector<std::string> res;
	
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;

	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"devices") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						if (c != NULL)
						{
							std::string name = c;
							res.push_back(name);
							
							xmlFree(c);
						}
					}
					
					cur_node2 = cur_node2->next;
				}
				
				break;
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return res;
}

std::string Configuration::getModuleForDevice(const std::string& device)
{
	std::vector<std::string> res;
	
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;

	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"devices") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						if (c != NULL)
						{
							std::string name = c;
							xmlFree(c);
						
							if (name == device)
							{
								c = (char*)xmlGetProp(cur_node2, (xmlChar*)"module");
								
								if (c != NULL)
								{
									std::string result = c;
									xmlFree(c);
									return result;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return "";
}

std::vector<std::string> Configuration::getCategories()
{
	std::vector<std::string> res;
	
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;

	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{
							std::string name = c;
							res.push_back(name);
							
							xmlFree(c);	
						}
					}
					
					cur_node2 = cur_node2->next;
				}
				
				break;
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return res;
}

std::vector<std::string> Configuration::getServicesForCategory(const std::string& category)
{
	std::vector<std::string> res;
	
	xmlNodePtr root_element;
	xmlNodePtr cur_node;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node = root_element->xmlChildrenNode;

	while(cur_node != NULL)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{
							std::string name1 = c;
							xmlFree(c);
						
							if (name1 == category)
							{
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
							
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											std::string name2 = c;
											res.push_back(name2);
											
											xmlFree(c);
										}
									}
								
									cur_node3 = cur_node3->next;
								}
							
								return res;
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node = cur_node->next;
	}
	
	return res;
}

std::vector<std::string> Configuration::getDevicesForService(const std::string& category, const std::string& service)
{
	std::vector<std::string> res;
	
	xmlNodePtr root_element;
	xmlNodePtr cur_node1;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node1 = root_element->xmlChildrenNode;

	while(cur_node1 != NULL)
	{
		if (cur_node1->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node1->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node1->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{
							std::string name1 = c;
							xmlFree(c);
						
							if (name1 == category)
							{
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
							
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											std::string name2 = c;
											xmlFree(c);
									
											if (name2 == service)
											{
												xmlNodePtr cur_node4 = cur_node3->xmlChildrenNode;
												while(cur_node4 != NULL)
												{
													if (cur_node4->type == XML_ELEMENT_NODE)
													{
														c = (char*)xmlGetProp(cur_node4, (xmlChar*)"name");
														
														if (c != NULL)
														{
															std::string name3 = c;
															res.push_back(name3);
															
															xmlFree(c);
														}
													}
											
													cur_node4 = cur_node4->next;
												}
										
												return res;
											}
										}
									}
								
									cur_node3 = cur_node3->next;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node1 = cur_node1->next;
	}
	
	return res;
}

std::string Configuration::getPort(const std::string& category, const std::string& service, const std::string& device)
{
	xmlNodePtr root_element;
	xmlNodePtr cur_node1;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node1 = root_element->xmlChildrenNode;

	while(cur_node1 != NULL)
	{
		if (cur_node1->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node1->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node1->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{	
							std::string name1 = c;
							xmlFree(c);
						
							if (name1 == category)
							{
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
							
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											std::string name2 = c;
											xmlFree(c);
									
											if (name2 == service)
											{
												xmlNodePtr cur_node4 = cur_node3->xmlChildrenNode;
												while(cur_node4 != NULL)
												{
													if (cur_node4->type == XML_ELEMENT_NODE)
													{
														c = (char*)xmlGetProp(cur_node4, (xmlChar*)"name");
														
														if (c != NULL)
														{
															std::string name3 = c;
															xmlFree(c);
															if (name3 == device)
															{
																c = (char*)xmlGetProp(cur_node4, (xmlChar*)"port");
																
																if (c != NULL)
																{
																	std::string res = c;
																	xmlFree(c);
																	return res;
																}
															}
														}
													}
											
													cur_node4 = cur_node4->next;
												}
											}
										}
									}
								
									cur_node3 = cur_node3->next;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node1 = cur_node1->next;
	}
	
	return "";
}

std::string Configuration::getAddress(const std::string& category, const std::string& service, const std::string& device)
{
	xmlNodePtr root_element;
	xmlNodePtr cur_node1;
	
	root_element = xmlDocGetRootElement(doc);
	cur_node1 = root_element->xmlChildrenNode;

	while(cur_node1 != NULL)
	{
		if (cur_node1->type == XML_ELEMENT_NODE)
		{
			if (xmlStrcmp(cur_node1->name, (const xmlChar*)"services") == 0)
			{
				//extract devices
				xmlNodePtr cur_node2 = cur_node1->xmlChildrenNode;
				
				while(cur_node2 != NULL)
				{
					if (cur_node2->type == XML_ELEMENT_NODE)
					{
						char* c = (char*)xmlGetProp(cur_node2, (xmlChar*)"name");
						
						if (c != NULL)
						{	
							std::string name1 = c;
							xmlFree(c);
						
							if (name1 == category)
							{
								xmlNodePtr cur_node3 = cur_node2->xmlChildrenNode;
							
								while(cur_node3 != NULL)
								{
									if (cur_node3->type == XML_ELEMENT_NODE)
									{
										c = (char*)xmlGetProp(cur_node3, (xmlChar*)"name");
										
										if (c != NULL)
										{
											std::string name2 = c;
											xmlFree(c);
									
											if (name2 == service)
											{
												xmlNodePtr cur_node4 = cur_node3->xmlChildrenNode;
												while(cur_node4 != NULL)
												{
													if (cur_node4->type == XML_ELEMENT_NODE)
													{
														c = (char*)xmlGetProp(cur_node4, (xmlChar*)"name");
														
														if (c != NULL)
														{
															std::string name3 = c;
															xmlFree(c);
															if (name3 == device)
															{
																c = (char*)xmlGetProp(cur_node4, (xmlChar*)"address");
																
																if (c != NULL)
																{
																	std::string res = c;
																	xmlFree(c);
																	return res;
																}
															}
														}
													}
											
													cur_node4 = cur_node4->next;
												}
											}
										}
									}
								
									cur_node3 = cur_node3->next;
								}
							}
						}
					}
					
					cur_node2 = cur_node2->next;
				}
			}
		}
		
		cur_node1 = cur_node1->next;
	}
	
	return "";
}

std::string Configuration::getUserID(const std::string& category, const std::string& service, const std::string& device)
{
	std::string address = getAddress(category, service, device);
	std::string port = getPort(category, service, device);
	
	if (address != "" && port != "")
	{
		return ( (address+":")+port );
	}
	
	return "";
}
	
};

