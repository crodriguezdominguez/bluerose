#include <signal.h>
#include "DevicePool.h"
#include "ReplyMessageStack.h"
#include "DiscoveryProxy.h"

namespace BlueRose {

struct identityPredicate
{
	bool operator()(const Identity& a, const Identity& b) const
	{
		if (a.name < b.name) return true;
		else return false;
	}
};

std::map<std::string, ICommunicationDevice*> DevicePool::deviceCollection;
ICommunicationDevice* DevicePool::interface;
std::map<Identity, ObjectServant*, identityPredicate> DevicePool::objects;

bool sortInterfacesPredicate(ICommunicationDevice* a, ICommunicationDevice* b)
{
	bool a_available = a->isAvailable();
	bool b_available = b->isAvailable();

	if (a_available && b_available)
		return (a->getPriority() > b->getPriority());

	else if (a_available)
		return true;

	else if (b_available)
		return false;

	return true;
}


void DevicePool::init()
{
	signal(SIGPIPE, SIG_IGN); //evitar los mensajes SIGPIPE broken
	ReplyMessageStack::init();
	interface = nextDevice();
}

void DevicePool::destroy()
{
	ReplyMessageStack::destroy();
	
	//std::map<std::string, ICommunicationDevice*>::iterator it;
	//for (it=transmissionInterfaceCollection.begin(); it!=transmissionInterfaceCollection.end(); ++it)
	//{
	//	it->second->closeAllConnections();
	//}
	
	std::map<Identity, ObjectServant*, identityPredicate>::iterator it;

	for (it = objects.begin(); it != objects.end(); ++it)
	{
		if (DiscoveryProxy::defaultProxy() != NULL)
		{
			DiscoveryProxy::defaultProxy()->removeRegistration( it->second->getIdentifier() );
		}
	}
	
	objects.clear();
	deviceCollection.clear();
}

void DevicePool::addDevice(ICommunicationDevice* _interface)
{
	deviceCollection[_interface->getName()] = _interface;
	
	if (deviceCollection.size() == 1)
	{
		interface = nextDevice();
	}
}

void DevicePool::registerObjectServant(ObjectServant* obj)
{
	objects[obj->getIdentity()] = obj;
}

void DevicePool::unregisterObjectServant(ObjectServant* obj)
{
	Identity id = obj->getIdentity();
	objects.erase(objects.find(id));
	
	//unregister from the discovery proxy
	if (DiscoveryProxy::defaultProxy() != NULL)
	{
		DiscoveryProxy::defaultProxy()->removeRegistration((id.category+"::")+id.name);
	}
}

ICommunicationDevice* DevicePool::nextDevice()
{
	std::list<ICommunicationDevice*> aux_v;
	
	std::map<std::string, ICommunicationDevice*>::iterator it;
	for (it = deviceCollection.begin(); it != deviceCollection.end(); ++it)
	{
		aux_v.push_back(it->second);
	}
	
	aux_v.sort(sortInterfacesPredicate);
	
	return *(aux_v.begin());
}

void DevicePool::dispatchMessage(const std::string& userId, const std::vector<byte>& msg)
{
	ReplyMessage reply;
	RequestMessage orig(msg);
	std::vector<byte> result;
	std::vector<byte> reply_bytes;
	byte replyStatus = BR_OBJECT_NOT_EXIST_STATUS;
	
	int requestId = ((RequestMessageHeader*)orig.header)->requestId;
	
	ReplyMessageHeader* header = (ReplyMessageHeader*)reply.header;
	RequestMessageHeader* header2 = (RequestMessageHeader*)orig.header;
	
	header->requestId = requestId;

	//identify destination object
	std::map<Identity, ObjectServant*, identityPredicate>::iterator it = objects.find(header2->id);
	if (it != objects.end())
	{
		replyStatus = it->second->runMethod(userId, header2->operation, orig.body->byteCollection, result);
	}
	
	header->replyStatus = replyStatus;

	if (replyStatus == BR_SUCCESS_STATUS)
		reply.addToEncapsulation(result);
	
	//write reply msg
	reply.bytes(reply_bytes);
	
	if (header2->mode == BR_TWOWAY_MODE)
	{
		interface->write(userId, reply_bytes);
	}
}

ICommunicationDevice* DevicePool::currentDevice()
{
	return interface;
}

ICommunicationDevice* DevicePool::deviceForName(const std::string& name)
{
	return deviceCollection[name];
}


};

