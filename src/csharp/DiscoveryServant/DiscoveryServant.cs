
using System;
using System.Collections.Generic;

using BlueRose;
using BlueRose.Messages;

namespace DiscoveryServant
{
	public class DiscoveryServant : ObjectServant
	{
		protected Dictionary<string, string> registrations;
			
		public DiscoveryServant()
		{
			registrations = new Dictionary<string, string>();
			identity.Name = "Discovery";
			identity.Category = "BlueRoseService";
		}
		
		public override MethodResult RunMethod(string method_name, string userID, List<byte> args)
		{
			ByteStreamReader reader = new ByteStreamReader(args);
			ByteStreamWriter writer = new ByteStreamWriter();
			
			MethodResult result = new MethodResult();
			
			if (method_name.Equals("addRegistration"))
			{
				string name = reader.ReadString();
				string address = reader.ReadString();
				this.AddRegistration(name, address);
		
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("removeRegistration"))
			{
				string name = reader.ReadString();
				RemoveRegistration(name);
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("resolve"))
			{
				string name = reader.ReadString();
				writer.WriteString(Resolve(name));
				
				result.Status = ReplyStatus.Success;
				result.Result = writer.ToVector();
				
				return result;
			}
			
			result.Status = ReplyStatus.OperationNotExist;
			result.Result = writer.ToVector();
			return result;							
		}
		
		public void AddRegistration(string name, string address)
		{
			registrations[name] = address;
			Console.WriteLine(name+" registered as "+address);
		}
		
		public void RemoveRegistration(string name)
		{
			if (registrations.ContainsKey(name))
			{
				registrations.Remove(name);
			}
		}
		
		public string Resolve(string name)
		{
			if (registrations.ContainsKey(name))
			{
				return registrations[name];
			}
			else return "";
		}
	}
}
