
using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;

using BlueRose.Messages;
using BlueRose.Threads;

namespace BlueRose.Devices
{


	/**
	* Module for transferring messages through the system
	* default TCP transmission interface.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 16-10-2009
	*/
	public class TcpCompatibleDevice : ICommunicationDevice {
	
		protected Dictionary<string, TcpClient> clients;
		protected Dictionary<string, ReplyReadThread> clientThreads;
		protected Thread servantThread;
		protected int priority = 0;
		protected bool isServant = false;
		protected int servantPort = -1;
		protected string servantAddress = "";
		
		public TcpCompatibleDevice()
		{
			clients = new Dictionary<string, TcpClient>();
			clientThreads = new Dictionary<string, ReplyReadThread>();
		}
		
		public void Broadcast(List<byte> message) 
		{
			List<string> neighbours = GetNeighbours();
			
			for (int i=0; i<neighbours.Count; i++)
			{
				Write(neighbours[i], message);
			}
		}
	
		public void CloseAllConnections() 
		{
			clients.Clear();
			clientThreads.Clear();
		}
	
		public void CloseConnection(string userID) 
		{
			//send CloseConnectionMessage
			if (IsConnectionOpenned(userID))
			{
				clients.Remove(userID);
				clientThreads.Remove(userID);
			}
		}
	
		public string GetDeviceName() 
		{
			return "TcpCompatibleDevice";
		}
	
		public List<string> GetNeighbours() 
		{
			List<string> res = new List<string>();
			
			foreach(string key in clients.Keys)
			{
				res.Add(key);
			}
			
			return res;
		}
		
		public void SetServantIdentifier(string servantID)
		{
			servantAddress = servantID;
		}
	
		public string GetServantIdentifier() 
		{
			return servantAddress;
		}
	
		public bool IsAvailable() 
		{
			foreach(string key in clients.Keys)
			{
				if (clients[key].IsOpenned())
				{
					return true;
				}
			}
	
			return false;
		}
	
		public bool IsBlockantConnection()
		{
			return true;
		}
	
		public bool IsConnectionOpenned(string userID) 
		{	
			if (clients.ContainsKey(userID)) return clients[userID].IsOpenned();
			else return false;
		}
	
		public bool IsConnectionOriented()
		{
			return true;
		}
	
		public bool IsUserAvailable(string userID)
		{	
			return IsConnectionOpenned(userID);
		}
	
		public void OpenConnection(string userID, Dictionary<string, List<byte>> pars)
		{
			if (clients.ContainsKey(userID))
				return;
			
			char[] splitter = {':'};
			string[] strs = userID.Split(splitter);
			string host = strs[0];
			int port = int.Parse(strs[1]);
			
			clients[userID] = new TcpClient(host, port);
		}
	
		public void OpenConnection(string userID)
		{
			this.OpenConnection(userID, null);
		}
	
		public List<Byte> Read(string senderID)
		{
			TcpClient cli = clients[senderID];
			
			return cli.Read();
		}
	
		public void SetServant(string multiplexingId)
		{
			isServant = true;
			servantPort = int.Parse(multiplexingId);
			
			//Get localhost IP
			
			if (servantAddress == null || servantAddress == "" || servantAddress == "localhost" || servantAddress == "127.0.0.1")
			{
				try {
					IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
		
					bool fin = false;
					foreach(IPAddress addr in localIPs)
					{
						try{
							if (addr.AddressFamily == AddressFamily.InterNetwork)
							{
								servantAddress = addr.ToString()+":"+servantPort;
							
								if (!servantAddress.StartsWith("127"))
								{
									if (!servantAddress.StartsWith("0.0.0.0") && !addr.ToString().Contains(":"))
									{
										fin = true;
										break;
									}
								}
							}
						}
						catch(Exception ex)
						{
							servantAddress = "127.0.0.1";
							Console.WriteLine(ex.ToString());
						}
					}
					
					if (!fin)
					{
						Console.WriteLine("Warning: Only local connections will be allowed. Please, check your network configuration");
					}
				} 
				catch(Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
			else
			{
				string[] results = servantAddress.Split(':');
				if (results.Length < 2)
				{
					servantAddress = servantAddress+":"+servantPort;
				}
			}
			
			this.servantThread = new Thread(new ThreadStart(this.Run));
			this.servantThread.Start();
		}
	
		public void WaitForConnections()
		{
			servantThread.Join();
		}
	
		public bool Write(string readerID, List<byte> data) 
		{
			if (!clients.ContainsKey(readerID)) return false;
			TcpClient cli = clients[readerID];
			
			if (data[8] == (byte)MessageType.Request)
			{	
				RequestMessage msg = new RequestMessage(data);
				
				if (((RequestMessageHeader)msg.Header).Mode == RequestOperationMode.TwoWay)
				{
					if (!clientThreads.ContainsKey(readerID))
					{
						ReplyReadThread rth = new ReplyReadThread(this, readerID);
						clientThreads[readerID] = rth;
						rth.Start();
					}
				}
			}
			
			return cli.Write(data);
		}
		
		public int GetPriority()
		{
			return priority;
		}
		
		public void SetPriority(int p)
		{
			priority = p;
		}
	
		protected void Run()
		{
			Socket servantSocket = null;
			try{
				servantSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				servantSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
				servantSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
				servantSocket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
			
				servantSocket.Bind(new IPEndPoint(IPAddress.Any,servantPort));
				servantSocket.Listen(int.MaxValue);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		
			while(true)
			{
				Socket newsockfd = null;
				try {
					newsockfd = servantSocket.Accept();
				} catch (Exception) {
				}
	
				if (newsockfd != null)
				{
					IPEndPoint clientep = (IPEndPoint)newsockfd.RemoteEndPoint;
					string userID = clientep.Address+":"+clientep.Port;
					Console.WriteLine("Connection opened: "+userID);
					
					try {
						clients.Add(userID, new TcpClient(newsockfd));
					} catch (Exception e) {
						Console.WriteLine(e.ToString());
					}
					
					RequestReadThread th = new RequestReadThread(this, userID);
					th.Start();
				}
			}
		}
	}
}
