
using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;

namespace BlueRose.Devices
{


	public class TcpClient {
		protected Socket socket;
		protected object mutex = new object();
		
		public TcpClient(string host, int port)
		{
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
			socket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
			
			socket.Connect(host, port);
			
			//read validate connection msg
			this.Read();
		}
		
		public TcpClient(Socket sock)
		{
			socket = sock;
		}
		
		~TcpClient()
		{	
			try {
				socket.Close();
			}catch(Exception)
			{
				
			}
		}
		
		public Socket Socket{
			get{
				return socket;
			}
			set{
				socket = value;
			}
		}
		
		public bool IsOpenned()
		{
			return socket.Connected;
		}
		
		public string GetID()
		{
			IPEndPoint ep = (IPEndPoint)socket.RemoteEndPoint;
			
			return ep.Address.ToString()+":"+ep.Port.ToString();
		}
		
		public bool Write(List<Byte> data)
		{	
			lock(mutex) {
				try {
					socket.Send(data.ToArray());
				} catch (Exception) {
					return false;
				}
				
				return true;
			}
		}
		
		[MethodImpl(MethodImplOptions.Synchronized)]
		public List<byte> Read()
		{
			int size = -1;
			int buffer_size = 14;
			
			byte [] buffer_init = new byte[buffer_size];
			try {
				int n=0;
				while (n != buffer_size)
				{
					int s;
					s = socket.Receive(buffer_init, 0, buffer_size, SocketFlags.None);
					n = n+s;
					
					if (s <= 0)
					{
						return null;
					}
				}
			} catch (Exception) {
				return null;
			}
			
			List<byte> msg = new List<byte>(buffer_init);
			
			size = 0;
			
			if (BitConverter.IsLittleEndian)
			{
				size = BitConverter.ToInt32(buffer_init, 10);
			}
			else
			{
				byte[] buffer_aux = new byte[4];
				buffer_aux[0] = buffer_init[13];
				buffer_aux[1] = buffer_init[12];
				buffer_aux[2] = buffer_init[11];
				buffer_aux[3] = buffer_init[10];
				
				size = BitConverter.ToInt32(buffer_aux, 0);
			}
			
			buffer_size = size-buffer_size;
			
			if (buffer_size == 0)
			{
				return msg;
			}
			else
			{
				byte [] buffer = new byte[buffer_size];
				
				try {
					int k = 0;
					while(k != buffer_size)
					{
						int r;
						r = socket.Receive(buffer, 0, buffer_size, SocketFlags.None);
						k = k+r;
						
						if (r <= 0)
						{
							return null;
						}
					}
				} catch (Exception) {
					return null;
				}
				
				msg.AddRange(buffer);
			
				return msg;
			}
		}
	}

}
