
using System;
using System.Collections.Generic;

using BlueRose;
using BlueRose.Messages;
using BlueRose.Events;

namespace PubSubServant
{


	public class PubSubServant : ObjectServant
	{
		protected Dictionary<string, List<Pair<int, Predicate>>> subscriptions;
		
		public PubSubServant()
		{
			subscriptions = new Dictionary<string, List<Pair<int, Predicate>>>();
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
		}
		
		public override MethodResult RunMethod(string method_name, string userID, List<byte> args)
		{
			ByteStreamReader reader = new ByteStreamReader(args);
			ByteStreamWriter writer = new ByteStreamWriter();
			
			MethodResult result = new MethodResult();
			
			if (method_name.Equals("subscribe0"))
			{
				Subscribe(reader.ReadInteger(), userID);
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("subscribe1"))
			{
				int _topic = reader.ReadInteger();
				Predicate _pred = new Predicate();
				_pred.Unmarshall(reader);
				Subscribe(_topic, _pred, userID);
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("isSubscribed"))
			{
				Event _template = new Event();
				string _userID = reader.ReadString();
				_template.Unmarshall(reader);
				
				writer.WriteBoolean(IsSubscribed(_userID, _template));
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("unsubscribe0"))
			{	
				Unsubscribe(userID);
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("unsubscribe1"))
			{	
				int _topic = reader.ReadInteger();
				Unsubscribe(_topic, userID);
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("getSubscribers0"))
			{	
				writer.WriteStringSeq(GetSubscribers());
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("getSubscribers1"))
			{	
				Event _template = new Event();
				_template.Unmarshall(reader);
				writer.WriteStringSeq(GetSubscribers(_template));
				
				result.Result = writer.ToVector();
				result.Status = ReplyStatus.Success;
				return result;
			}
			
			else if (method_name.Equals("publish"))
			{
				Event evt = new Event();
				evt.Unmarshall(reader);
				bool comeBack = reader.ReadBoolean();
				Publish(evt, comeBack, userID);
				
				result.Status = ReplyStatus.Success;
				result.Result = writer.ToVector();
				return result;
			}
			
			result.Status = ReplyStatus.OperationNotExist;
			result.Result = writer.ToVector();
			return result;							
		}
		
		public void Subscribe(int _topic, string senderID)
		{
			Predicate pred = new Predicate();
			Subscribe(_topic, pred, senderID);
		}
		
		public void Subscribe(int _topic, Predicate _pred, string senderID)
		{
			Pair<int, Predicate> sub_pair = new Pair<int, Predicate>(_topic, _pred);
			
			if (subscriptions.ContainsKey(senderID)) //other subscriptions existed
			{
				subscriptions[senderID].Add(sub_pair);
			}
			else
			{
				List<Pair<int, Predicate>> sub = new List<Pair<int, Predicate>>();
				sub.Add(sub_pair);
				
				subscriptions[senderID] = sub;
			}
		}
		
		public void Unsubscribe(string senderID)
		{
			if (subscriptions.ContainsKey(senderID))
			{
				subscriptions.Remove(senderID);
			}
		}
		
		public void Unsubscribe(int _topic, string senderID)
		{
			if (subscriptions.ContainsKey(senderID))
			{
				List<Pair<int, Predicate>> v = subscriptions[senderID];
				List<Pair<int, Predicate>> res = new List<Pair<int, Predicate>>();
				
				foreach (Pair<int, Predicate> pair in v)
				{
					if (pair.First != _topic)
					{
						res.Add(pair);
					}
				}
				
				subscriptions[senderID] = res;
			}
		}
		
		public bool IsSubscribed(string _userID, Event _template)
		{
			if (!subscriptions.ContainsKey(_userID)) return false;
			else
			{
				int topic = _template.Topic;
				List<Pair<int, Predicate> > v = subscriptions[_userID];
				
				for (int i=0; i<v.Count; i++)
				{
					if (v[i].First == topic)
					{
						//check that all the values of the event are consistent with the predicate
						foreach (string name in _template.Nodes.Keys)
						{
							if (!v[i].Second.IsConsistent(name, BlueRose.Comparison.Equal, _template.Nodes[name].Value))
							{
								return false;
							}
							
							//foreach event node, repeat the process...
							foreach (string key in _template.Nodes[name].Nodes.Keys)
							{
								IsSubscribed(_userID, _template.Nodes[name].Nodes[key]);
							}
						}
					}
				}
				
				return true;
			}
		}
		
		public List<string> GetSubscribers()
		{
			List<string> res = new List<string>();
			foreach (string key in subscriptions.Keys)
			{
				res.Add(key);
			}
			
			return res;
		}
		
		public List<string> GetSubscribers(Event _template)
		{
			List<string> res = new List<string>();
			foreach (string key in subscriptions.Keys)
			{
				if (IsSubscribed(key, _template))
					res.Add(key);
			}
			
			return res;
		}
		
		public void Publish(Event evt, bool comeBack, string senderID)
		{
			List<string> subscribers = GetSubscribers(evt);
			
			//raw write...
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteObject(evt);
			List<byte> bytes = writer.ToVector();
			
			EventMessage msg = new EventMessage(bytes);
			List<byte> send_msg = msg.GetBytes();
			
			for (int i=0; i<subscribers.Count; i++)
			{
				//printf("EVENT PUBLISHED to: %s\n", subscribers[i].c_str());
				//fflush(stdout);
				
				//send an event msg
				if ( !senderID.Equals(subscribers[i]) || (senderID.Equals(subscribers[i]) && comeBack) )
				{
					if (!DevicePool.GetCurrentDevice().Write(subscribers[i], send_msg))
					{
						Unsubscribe(subscribers[i]);
					}
				}
			}
		}
	}
}
