
using System;
using System.Collections.Generic;
using System.Threading;

namespace BlueRose.Threads
{


	public class OpenConnectionThread
	{	
		protected ICommunicationDevice device; /**< Transmission interface for the thread */
		protected string userID; /**< User identificator of the sender */
		protected Dictionary<string, List<byte>> parameters;
		protected int maxRetries;
		protected Thread thread;
		
		public OpenConnectionThread(ICommunicationDevice dev, string identifier, int retries, Dictionary<string, List<byte>> pars)
		{
			device = dev;
			userID = identifier;
			parameters = pars;
			maxRetries = retries;
		}
		
		public void Join()
		{
			thread.Join();
		}
		
		public void Interrupt()
		{
			thread.Interrupt();
		}
		
		public void Start()
		{
			thread = new Thread(new ThreadStart(this.Run));
			thread.Start();
		}
		
		public void Run()
		{
			if (maxRetries == int.MaxValue)
			{
				while(true)
				{
					try{
						device.OpenConnection(userID, parameters);
						return;
					} catch(Exception) {
					}
					
					try {
						Thread.Sleep(1000);
					} catch (Exception) {
						return;
					}
				}
			}
			else
			{
				int i;
				for (i=0; i<maxRetries; i++)
				{
					try{
						device.OpenConnection(userID, parameters);
						return;
					} catch(Exception) {
					}
						
					try {
						Thread.Sleep(1000);
					} catch (Exception) {
						return;
					}
				}
				
				if (i>=maxRetries)
				{
					return;
				}
			}
		}
	}

}
