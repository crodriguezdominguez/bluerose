
using System;
using System.Collections.Generic;
using System.Threading;

using BlueRose.Events;
using BlueRose.Messages;

namespace BlueRose
{


	public class RequestReadThread
	{
		protected ICommunicationDevice device; /**< Transmission interface for the thread */
		protected string userID; /**< User identificator of the sender */
		protected Thread thread;
	
		/**
		* Constructor
		*
		* @param iface Transmission interface
		*/
		public RequestReadThread(ICommunicationDevice iface, String _userID)
		{
			device = iface;
			userID = _userID;
		}
		
		public void Start()
		{
			thread = new Thread(new ThreadStart(Run));
			thread.Start();
		}
	
		/**
		* Overload of the main method of the thread
		*/
		public void Run()
		{
			//send validate connection msg
			ValidateConnectionMessage msg = new ValidateConnectionMessage();
			List<Byte> bytes = msg.GetBytes();
			device.Write(userID, bytes);
			bytes.Clear();
			
			while(true) //read messages
			{
				bytes = device.Read(userID);
				
				if (bytes == null)
					break;
				
				if (bytes[8] == (byte)MessageType.CloseConnection)
				{
					break;
				}
				
				else if (bytes[8] == (byte)MessageType.Request)
				{
					//process message
					DevicePool.DispatchMessage(bytes, userID);
				}
				
				bytes.Clear();
			}
			
			Console.WriteLine("Connection closed: "+userID);
			
			device.CloseConnection(userID);
		}
	}
}
