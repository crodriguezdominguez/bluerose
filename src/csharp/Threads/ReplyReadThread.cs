
using System;
using System.Collections.Generic;
using System.Threading;

using BlueRose.Events;
using BlueRose.Messages;

namespace BlueRose.Threads
{


	/**
	* Auxiliary thread for reading replies from a transmission 
	* channel. It's used for receiving servant replies
	*
	* @author Carlos Rodriguez Dominguez
	* @date 7-10-2009
	*/
	public class ReplyReadThread
	{
		protected ICommunicationDevice device; /**< Transmission interface for the thread */
		protected String userID; /**< User identificator of the sender */
		protected Thread thread;
	
	
		/**
		* Constructor
		*
		* @param iface Transmission interface
		*/
		public ReplyReadThread(ICommunicationDevice iface, String _userID)
		{
			device = iface;
			userID = _userID;
		}
		
		public void Start()
		{
			thread = new Thread(new ThreadStart(Run));
			thread.Start();
		}
	
		/**
		* Overload of the main method of the thread
		*/
		public void Run()
		{	
			List<Byte> bytes;
			
			while ( (bytes = device.Read(userID)) != null ) //read messages
			{
				if (bytes[8] == (byte)MessageType.Reply)
				{
					ByteStreamReader reader = new ByteStreamReader(bytes);
					
					//add to reply stack
					reader.Skip(14);
					int requestId = reader.ReadInteger();
					
					ReplyMessageStack.Produce(requestId, bytes);
					
					bytes.Clear();
				}
				
				else if (bytes[8] == (byte)MessageType.Event)
				{	
					ByteStreamReader reader = new ByteStreamReader(bytes);
					
					reader.Skip(14);
					List<byte> bb = reader.ReadToEnd();
	
					Event evt = new Event();
					evt.Unmarshall(new ByteStreamReader(bb));
					
					BlueRose.Events.EventHandler.OnConsume(evt);
					
					bytes.Clear();
				}
				
				else if (bytes[8] == (byte)MessageType.CloseConnection)
				{
					bytes.Clear();
					break;
				}
			}
			
			device.CloseConnection(userID);
		}
	}

}
