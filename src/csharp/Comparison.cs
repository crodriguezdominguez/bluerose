
using System;

namespace BlueRose
{

	/**
	 * Types of comparisons between values
	 */
	public enum Comparison
	{
		Equal,
		NotEqual,
		Less,
		LessEqual,
		Greater,
		GreaterEqual
	}
}
