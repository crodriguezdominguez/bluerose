
using System;

namespace BlueRose.Messages
{


	/**
	 * Header for a connection closing message. 
	 * Definition taken from ICE (zeroc.com) for compatibility reasons.
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 07-10-2009
	 */
	public class CloseConnectionMessageHeader : MessageHeader
	{
		public CloseConnectionMessageHeader() : base()
		{
			messageType = MessageType.CloseConnection;
		}
	}
}
