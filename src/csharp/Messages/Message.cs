
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Class for modelling an empty message.
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class Message {
		protected MessageHeader header; /**< Header of the message */
		protected Encapsulation body; /**< Body of the message */
		
		/**
		 * Default constructor
		 */
		public Message()
		{
			header = null;
			body = null;
		}
		
		public MessageHeader Header
		{
			get
			{
				return header;
			}
		}
		
		public Encapsulation Body
		{
			get
			{
				return body;
			}
			set
			{
				body = value;
			}
		}
		
		/**
		 * Returns the full message (header+body, if body exists) as a byte sequence
		 *
		 * @return Bytes of the message
		 */
		public virtual List<byte> GetBytes()
		{
			ByteStreamWriter writer = new ByteStreamWriter();
			
			if (body != null)
			{
				List<Byte> hd = header.GetBytes();
				header.MessageSize = hd.Count + 6 + body.ByteCollection.Count;
				
				hd = header.GetBytes();
				
				writer.WriteRawBytes(hd);
				writer.WriteInteger(body.Size);
				writer.WriteByte(body.MajorVersion);
				writer.WriteByte(body.MinorVersion);
				writer.WriteRawBytes(body.ByteCollection);
			}
			else
			{
				writer.WriteRawBytes(header.GetBytes());
			}
			
			return writer.ToVector();
		}
		
		/**
		 * Adds bytes to the body
		 *
		 * @param bytes Bytes to add
		 */
		public void AddToEncapsulation(List<byte> bytes)
		{
			if (body != null)
			{
				body.Size += bytes.Count;
				body.ByteCollection.AddRange(bytes);
			}
		}
	}
}
