
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Batch request message
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class BatchRequestMessage : Message 
	{
		/**
		 * Default constructor
		 */
		public BatchRequestMessage()
		{
			header = new BatchRequestMessageHeader();
			body = new Encapsulation();
		}
		
		/**
		 * Constructor that initializes the message with the appropriate bytes
		 * @param bytes Bytes that represent the message body
		 */
		public BatchRequestMessage(List<byte> bytes)
		{
			Dictionary<string, string> context = new Dictionary<string, string>();
			
			int size, i;
			
			header = new BatchRequestMessageHeader();
			body = new Encapsulation();
			
			BatchRequestMessageHeader h2 = (BatchRequestMessageHeader)header;
			
			ByteStreamReader reader = new ByteStreamReader(bytes);
			
			reader.Skip(14);
			
			h2.NumberOfRequests = reader.ReadInteger();
			
			h2.Identity.Name = reader.ReadString();
			h2.Identity.Category = reader.ReadString();
			
			h2.Facet = reader.ReadStringSeq();
			
			h2.Operation = reader.ReadString();
			
			h2.Mode = (RequestOperationMode)reader.ReadOneByte();
			
			size = reader.ReadSize();
			for (i=0; i<size; i++)
			{
				string key = reader.ReadString();
				string val = reader.ReadString();
				
				context[key] = val;
			}
			
			h2.Context = context;
			
			body.Size = reader.ReadInteger();
			
			body.MajorVersion = reader.ReadOneByte();
			body.MinorVersion = reader.ReadOneByte();
			
			body.ByteCollection = reader.ReadToEnd();
			
			reader.Close();
		}
	}
}
