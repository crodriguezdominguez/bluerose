
using System;

namespace BlueRose.Messages
{


	public enum ReplyStatus
	{
		Success = 0x00,
		UserException = 0x01,
		ObjectNotExist = 0x02,
		FacetNotExist = 0x03,
		OperationNotExist = 0x04,
		UnknownLocalException = 0x05,
		UnknownUserException = 0x06,
		UnknownException = 0x07
	}
}
