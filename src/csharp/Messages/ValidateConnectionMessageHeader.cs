
using System;

namespace BlueRose.Messages
{


	/**
	 * Header for a connection validating message. 
	 * Definition taken from ICE (zeroc.com) for compatibility reasons.
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 07-10-2009
	 */
	public class ValidateConnectionMessageHeader : MessageHeader
	{
		public ValidateConnectionMessageHeader() : base()
		{
			messageType = MessageType.ValidateConnection;
		}
	}
}
