
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Reply message
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class ReplyMessage : Message {
		/**
		 * Default constructor
		 */
		public ReplyMessage()
		{
			header = new ReplyMessageHeader();
			body = new Encapsulation();
		}
		
		/**
		 * Constructor that initializes the message with the appropriate bytes
		 * @param bytes Bytes that represent the message body
		 */
		public ReplyMessage(List<byte> bytes)
		{
			header = new ReplyMessageHeader();
			body = new Encapsulation();
			
			ReplyMessageHeader h3 = (ReplyMessageHeader)header;
			
			ByteStreamReader reader = new ByteStreamReader(bytes);
			
			reader.Skip(14);
			
			h3.RequestID = reader.ReadInteger();
			h3.ReplyStatus = (ReplyStatus)reader.ReadOneByte();
			
			body.Size = reader.ReadInteger();
			
			body.MajorVersion = reader.ReadOneByte();
			body.MinorVersion = reader.ReadOneByte();
			
			body.ByteCollection = reader.ReadToEnd();
			
			reader.Close();
		}
	}
}
