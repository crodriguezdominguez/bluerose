
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Encapsulation of the body of a message
	 */
	public class Encapsulation {
		protected int size; /**< Size of the encapsulation, including this header */
		protected byte major; /**< Major allowed encoding version as defined by ICE */
		protected byte minor; /**< Minor allowed encoding version as defined by ICE  */
		protected List<byte> byteCollection; /**< Message */
		
		public Encapsulation()
		{
			size = 6;
			major = 0x01;
			minor = 0x00;
			byteCollection = new List<byte>();
		}
		
		public int Size
		{
			get{
				return size;
			}
			set{
				size = value;
			}
		}
		
		public byte MajorVersion 
		{
			get{
				return major;
			}
			set{
				major = value;
			}
		}
		
		public byte MinorVersion 
		{
			get{
				return minor;
			}
			set{
				minor = value;
			}
		}
		
		public List<byte> ByteCollection
		{
			get{
				return byteCollection;
			}
			set{
				byteCollection = value;
			}
		}
	}
}
