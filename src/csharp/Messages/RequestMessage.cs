
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Request message
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class RequestMessage : Message 
	{	
		/**
		 * Default constructor
		 */
		public RequestMessage()
		{
			header = new RequestMessageHeader();
			body = new Encapsulation();
		}
		
		/**
		 * Constructor that initializes the message with the appropriate bytes
		 * @param bytes Bytes that represent the message body
		 */
		public RequestMessage(List<byte> bytes)
		{
			Dictionary<string, string> context =
				new Dictionary<string, string>();
			
			int size, i;
			
			header = new RequestMessageHeader();
			body = new Encapsulation();
			
			RequestMessageHeader h1 = (RequestMessageHeader)header;
			
			ByteStreamReader reader = new ByteStreamReader(bytes);
			
			reader.Skip(14);
			
			h1.RequestID = reader.ReadInteger();
			
			h1.Identity.Name = reader.ReadString();
			h1.Identity.Category = reader.ReadString();
			
			h1.Facet = reader.ReadStringSeq();
			
			h1.Operation = reader.ReadString();
			h1.Mode = (RequestOperationMode)reader.ReadOneByte();
			
			size = reader.ReadSize();
			for (i=0; i<size; i++)
			{
				String key = reader.ReadString();
				String val = reader.ReadString();
				
				context[key] = val;
			}
			
			h1.Context = context;
			
			body.Size = reader.ReadInteger();
			
			body.MajorVersion = reader.ReadOneByte();
			body.MinorVersion = reader.ReadOneByte();
			
			body.ByteCollection = reader.ReadToEnd();
			
			reader.Close();
		}	
	}
}
