
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Header for a batch requesting message. 
	 * Definition taken from ICE (zeroc.com) for compatibility reasons.
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 07-10-2009
	 */
	public class BatchRequestMessageHeader : MessageHeader {
		protected int numRequests; /**< Number of requests inside the message */
		protected Identity identity; /**< Identity of the servant */
		protected List<string> facet; /**< Facet of the servant */
		protected String operation; /**< Requested operation */
		protected RequestOperationMode mode; /**< Operation mode */
		protected Dictionary<string, string> context; /**< Context of the message */
		
		public BatchRequestMessageHeader() : base()
		{
			messageType = MessageType.BatchRequest;
			numRequests = 0;
			identity = new Identity();
			facet = new List<string>();
			context = new Dictionary<string, string>();
			mode = RequestOperationMode.TwoWay;
		}
		
		public int NumberOfRequests
		{
			get{
				return numRequests;
			}
			set{
				numRequests = value;
			}
		}
		
		public Identity Identity 
		{
			get{
				return identity;
			}
			set{
				identity = value;
			}
		}
		
		public List<string> Facet
		{
			get{
				return facet;
			}
			set{
				facet = value;
			}
		}
		
		public string Operation
		{
			get{
				return operation;
			}
			set{
				operation = value;
			}
		}
		
		public RequestOperationMode Mode
		{
			get{
				return mode;
			}
			set{
				mode = value;
			}
		}
		
		public Dictionary<string, string> Context
		{
			get{
				return context;
			}
			set{
				context = value;
			}
		}
		
		public override List<byte> GetBytes()
		{
			List<byte> bytes = base.GetBytes();
			ByteStreamWriter writer = new ByteStreamWriter();
			
			writer.WriteRawBytes(bytes);
			
			//numRquests
			writer.WriteInteger(numRequests);
			
			//insertar Identity
			writer.WriteString(identity.Name);
			writer.WriteString(identity.Category);
			
			//insertar facet
			writer.WriteStringSeq(facet);
			
			//insertar operation
			writer.WriteString(operation);
			
			//insertar mode
			writer.WriteByte((byte)mode);
			
			//insertar context
			writer.WriteSize(context.Count);
			
			foreach(string key in context.Keys)
			{
				string val = context[key];
				
				writer.WriteString(key);
				writer.WriteString(val);
			}
			
			return writer.ToVector();
		}
	}

}
