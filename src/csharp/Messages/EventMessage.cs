
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Event message
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class EventMessage : Message {
		public List<byte> evt;
		
		/**
		 * Default constructor
		 */
		public EventMessage()
		{
			header = new EventMessageHeader();
			evt = new List<byte>();
		}
		
		/**
		 * Constructor that initializes the message with the appropriate bytes
		 * @param bytes Bytes that represent the message body
		 */
		public EventMessage(List<byte> bytes)
		{
			header = new EventMessageHeader();
			evt = bytes;
		}
		
		public override List<byte> GetBytes()
		{
			ByteStreamWriter writer = new ByteStreamWriter();
			
			List<byte> header_bytes = header.GetBytes();
			
			header.MessageSize = header_bytes.Count+evt.Count;
	
			header_bytes = header.GetBytes();
			
			writer.WriteRawBytes(header_bytes);
			writer.WriteRawBytes(evt);
			
			return writer.ToVector();
		}
	}

}
