
using System;

namespace BlueRose.Messages
{


	/**
	 * Close connection message
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class CloseConnectionMessage : Message 
	{
		/**
		 * Default constructor
		 */
		public CloseConnectionMessage() : base()
		{
			header =  new CloseConnectionMessageHeader();
		}
	}
}
