
using System;

namespace BlueRose.Messages
{


	public enum MessageType
	{
		Request = 0x00,
		BatchRequest = 0x01,
		Reply = 0x02,
		ValidateConnection = 0x03,
		CloseConnection = 0x04,
		Event = 0x05
	}
}
