
using System;

namespace BlueRose.Messages
{


	/**
	 * Validate connection message
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class ValidateConnectionMessage : Message 
	{	
		/**
		 * Default constructor
		 */
		public ValidateConnectionMessage()
		{
			header =  new ValidateConnectionMessageHeader();
		}
	}
}
