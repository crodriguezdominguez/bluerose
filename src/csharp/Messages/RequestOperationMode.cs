
using System;

namespace BlueRose.Messages
{


	public enum RequestOperationMode
	{
		TwoWay = 0x00,
		OneWay = 0x01,
		BatchOneWay = 0x02,
		Datagram = 0x03,
		BatchDatagram = 0x04
	}
}
