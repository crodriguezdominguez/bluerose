
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	 * Common header for any message. 
	 * Definition taken from ICE (zeroc.com) for compatibility reasons.
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 07-10-2009
	 */
	public class MessageHeader
	{
		/************ Some Definitions *********/
		protected static readonly byte PROTOCOL_MINOR = 0x00;
		protected static readonly byte PROTOCOL_MAJOR = 0x01;
	
		protected static readonly byte ENCODING_MINOR = 0x00;
		protected static readonly byte ENCODING_MAJOR = 0x01;
		/*****************************************/
		
		protected byte [] magic = {0x49, 0x63, 0x65, 0x50}; /**< Magic bytes: "I", "c", "e", "P" */
		protected byte protocolMajor; /**< Major version of the protocol: 0x01 */
		protected byte protocolMinor; /**< Minor version of the protocol: 0x00 */
		protected byte encodingMajor; /**< Major version of the encoding: 0x01 */
		protected byte encodingMinor; /**< Minor version of the encoding: 0x00 */
		protected MessageType messageType; /**< Message type */
		protected byte compressionStatus; /**< Compression of the message. Currently only 0x00 */
		protected int messageSize; /**< Size of the message, including the header */
		
		public MessageHeader()
		{
			protocolMajor = PROTOCOL_MAJOR;
			protocolMinor = PROTOCOL_MINOR;
			encodingMajor = ENCODING_MAJOR;
			encodingMinor = ENCODING_MINOR;
			messageType = MessageType.ValidateConnection;
			compressionStatus = 0x00;
			
			messageSize = 14;
		}
		
		public byte[] Magic
		{
			get{
				return magic;
			}
		}
		
		public byte ProtocolMajor
		{
			get{
				return protocolMajor;
			}
			set{
				protocolMajor = value;
			}
		}
		
		public byte ProtocolMinor
		{
			get{
				return protocolMinor;
			}
			set{
				protocolMinor = value;
			}
		}
		
		public byte EncodingMajor
		{
			get{
				return encodingMajor;
			}
			set{
				encodingMajor = value;
			}
		}
		
		public byte EncodingMinor
		{
			get{
				return encodingMinor;
			}
			set{
				encodingMinor = value;
			}
		}
		
		public MessageType Type
		{
			get{
				return messageType;
			}
			set{
				messageType = value;
			}
		}
		
		public byte CompressionStatus
		{
			get{
				return compressionStatus;
			}
			set{
				compressionStatus = value;
			}
		}
		
		public int MessageSize
		{
			get{
				return messageSize;
			}
			set{
				messageSize = value;
			}
		}
		
		public virtual List<byte> GetBytes()
		{
			ByteStreamWriter writer = new ByteStreamWriter();
			
			writer.WriteByte(magic[0]);
			writer.WriteByte(magic[1]);
			writer.WriteByte(magic[2]);
			writer.WriteByte(magic[3]);
			
			writer.WriteByte(protocolMajor);
			writer.WriteByte(protocolMinor);
			writer.WriteByte(encodingMajor);
			writer.WriteByte(encodingMinor);
			
			writer.WriteByte((byte)messageType);
			writer.WriteByte(compressionStatus);
			
			writer.WriteInteger(messageSize);
			
			return writer.ToVector();
		}
	}
}
