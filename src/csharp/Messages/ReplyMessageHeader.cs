
using System;
using System.Collections.Generic;

namespace BlueRose.Messages
{


	/**
	* Header for a replying message. 
	* Definition taken from ICE (zeroc.com) for compatibility reasons.
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	public class ReplyMessageHeader : MessageHeader
	{
		protected int requestId; /**< Identifier of the request */
		protected ReplyStatus replyStatus; /**< Status of the reply */
		
		public ReplyMessageHeader() : base()
		{	
			messageType = MessageType.Reply;
			requestId = 0;
			replyStatus = Messages.ReplyStatus.Success;
			messageSize = 19;
		}
		
		public int RequestID
		{
			get{
				return requestId;
			}
			set{
				requestId = value;
			}
		}
		
		public ReplyStatus ReplyStatus
		{
			get{
				return replyStatus;
			}
			set{
				replyStatus = value;
			}
		}
		
		public override List<byte> GetBytes()
		{
			List<Byte> bytes = base.GetBytes();
			
			ByteStreamWriter writer = new ByteStreamWriter();
			
			writer.WriteRawBytes(bytes);
			
			writer.WriteInteger(requestId);
			writer.WriteByte((byte)replyStatus);
			
			return writer.ToVector();
		}
	}

}
