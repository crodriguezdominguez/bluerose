
using System;

namespace BlueRose.Messages
{


	/**
	* Header for an event message. 
	* Extension over ICE Protocol (zeroc.com).
	*
	* @author Carlos Rodriguez Dominguez
	* @date 07-10-2009
	*/
	public class EventMessageHeader : MessageHeader
	{
		public EventMessageHeader() : base()
		{
			messageType = MessageType.Event;
		}
	}
}
