
using System;
using System.Collections.Generic;
using System.Threading;

using BlueRose.Messages;

namespace BlueRose
{


	public class DiscoveryProxy : ObjectProxy 
	{
		protected static DiscoveryProxy defaultProxy;
		
		protected DiscoveryProxy() : base() {
			
		}
	
		protected DiscoveryProxy(string servID, ICommunicationDevice dev,
				bool wait, Dictionary<string, List<byte>> pars) : base(servID, dev, wait, pars)
		{
			
		}
	
		protected DiscoveryProxy(string servID, ICommunicationDevice dev, bool wait) : base(servID, dev, wait)
		{
			
		}
	
		protected DiscoveryProxy(string servID, ICommunicationDevice dev) : base(servID, dev) 
		{
			
		}
	
		protected DiscoveryProxy(string servID) : base(servID) 
		{
			
		}
		
		public static DiscoveryProxy DefaultProxy
		{
			get{
				return DiscoveryProxy.defaultProxy;
			}
		}
		
		public static void Initialize(string servID)
		{
			defaultProxy = new DiscoveryProxy(servID);
			defaultProxy.Identity.Name = "Discovery";
			defaultProxy.Identity.Category = "BlueRoseService";
		}
	
		public static void Initialize(string servID, ICommunicationDevice device)
		{
			defaultProxy = new DiscoveryProxy(servID, device);
			defaultProxy.Identity.Name = "Discovery";
			defaultProxy.Identity.Category = "BlueRoseService";
		}
		
		public static void Initialize(string servID, ICommunicationDevice device, bool wait)
		{
			defaultProxy = new DiscoveryProxy(servID, device, wait);
			defaultProxy.Identity.Name = "Discovery";
			defaultProxy.Identity.Category = "BlueRoseService";
		}
		
		public static void Destroy()
		{
			defaultProxy = null;
		}
	
		/**
		 * Registers a servant name and an address
		 *
		 * @param name Name of the servant to register
		 * @param address Address of the servant
		 */
		public void AddRegistration(string name, string address)
		{
			int reqID;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			
			currentMode = RequestOperationMode.TwoWay;
			
			writer.WriteString(name);
			writer.WriteString(address);
			
			reqID = this.SendRequest(servantID, "addRegistration", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		/**
		 * Unregisters the address that is associated with a name
		 *
		 * @param name Name of the servant to unregister
		 */
		public void RemoveRegistration(string name)
		{
			int reqID;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			
			currentMode = RequestOperationMode.TwoWay;
			
			writer.WriteString(name);
			
			reqID = this.SendRequest(servantID, "removeRegistration", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		public string ResolveName(string name)
		{
			return ResolveName(name, false);
		}
	
		public string ResolveName(string name, bool wait)
		{
			int reqID;
			ByteStreamReader reader = null;
			ByteStreamWriter writer = new ByteStreamWriter();
			
			currentMode = RequestOperationMode.TwoWay;
			string result = null;
			
			do {
				writer.WriteString(name);
				
				reqID = this.SendRequest(servantID, "resolve", writer.ToVector());
				List<byte> result_bytes = this.ReceiveReply(reqID);
				
				reader = new ByteStreamReader(result_bytes);
				result = reader.ReadString();
				
				reader.Close();
						
				if (result.Equals("") && wait)
				{
					try {
						Thread.Sleep(1000);
					} catch (Exception) {
					}
				}
			} while(result.Equals("") && wait);
				
			return result;
		}
		
		public override string GetTypeID() {
			return "BlueRoseService::Discovery";
		}
	
	}
}
