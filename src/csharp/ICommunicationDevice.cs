
using System;
using System.Collections.Generic;

namespace BlueRose
{


	/**
	 * Generic representation of transmission channel
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 07-10-2009
	 */
	public interface ICommunicationDevice {
	
		/**
		 * Open a new connection between local user and a remote one
		 *
		 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
		 * @param parameters A dictionary containing some parameters for the connection (i.e.: QoS)
		 * @throws Exception 
		 */
		void OpenConnection(string userID, Dictionary<string, List<byte>> pars);
	
		/**
		 * Open a new connection between local user and a remote one
		 *
		 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
		 */
		void OpenConnection(string userID);
	
	
		/**
		 * Checks whether the connection to an specific user is openned or not
		 *
		 * @param userID User to check whether the connection is openned with or not.
		 * @return True if the connection is openned. False otherwise else.
		 */
		bool IsConnectionOpenned(string userID);
	
		/**
		 * Close a connection between local user and a remote one
		 *
		 * @param userID User identification, as specified by the channel (i.e.: An IP and a Port for a TCP Channel)
		 */
		void CloseConnection(string userID);
	
		/**
		 * Close all connections in this transmission channel
		 */
		void CloseAllConnections();
	
		/**
		 * Send a message to a remote user
		 *
		 * @param userID Receiver identification
		 * @param message Message to send
		 */
		bool Write(string userID, List<byte> message);
	
		/**
		 * Receive a message from a sender
		 *
		 * @param userID Identification of the sender
		 * @return Received message or nil if the userID was not connected
		 */
		List<byte> Read(string userID);
	
		/**
		 * Set priority of the transmission channel in the interface pool
		 *
		 * @param priority Priority of the channel
		 */
		void SetPriority(int priority);
	
		/**
		 * Get priority of the transmission channel in the interface pool
		 *
		 * @return Priority
		 */
		int GetPriority();
	
		/**
		 * Get identificators of neighbour users
		 *
		 * @return Vector containing user identificators
		 */
		List<string> GetNeighbours();
	
		/**
		 * Broadcast a message to all neighbour users
		 *
		 * @param message Message to send
		 */
		void Broadcast(List<byte> message);
	
		/**
		 * Checks if writing or reading is blockant in this channel
		 *
		 * @return True if it's blockant. False otherwise else.
		 */
		bool IsBlockantConnection();
	
		/**
		 * Checks if the transmission of data is possible
		 *
		 * @return True if is available. False otherwise else.
		 */
		bool IsAvailable();
	
		/**
		 * Checks if the transmission of data to an specific user is possible
		 *
		 * @param userID Identifier of the user
		 * @return True if is available. False otherwise else.
		 */
		bool IsUserAvailable(string userID);
	
		/**
		 * Checks if the transmission interface is connection oriented
		 */
		bool IsConnectionOriented();
	
		/**
		 * Sets the current machine as a servant
		 *
		 * @param multiplexingId Id for multiplexing connections (i.e. a port in case of TCP)
		 */
		void SetServant(string multiplexingId);
	
		/**
		 * Waits for connections. Only for servants.
		 */
		void WaitForConnections();
	
		/**
		 * Returns the name of the interface
		 *
		 * @return Name of the interface
		 */
		string GetDeviceName();
	
		/**
		 * If it's a servant, it returns the identifier
		 *
		 * @return Identifier of the current servant or "" if it's not a servant
		 */
		string GetServantIdentifier();
		
		/**
		* Sets the current identifier of the servant (i.e. the ip address in case of TCP)
		*
		* @param servantID Identifier of the servant (i.e. the ip address in case of TCP)
		*/
		void SetServantIdentifier(string servantID);
	}
}
