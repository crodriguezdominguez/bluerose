
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BlueRose
{

	public class ByteStreamReader : MemoryStream {
		/**
		* Constructor
		*/
		public ByteStreamReader(byte[] buffer) : base(buffer)
		{
			
		}
		
		/**
		 * Constructor
		 */
		public ByteStreamReader(List<byte> buffer) : base(buffer.ToArray())
		{
			
		}
		
		public void Skip(long num)
		{
			this.Seek(num, SeekOrigin.Current);
		}
		
		/**
		 * Reads until the end of the stream is reached and
		 * returns the bytes.
		 * @return Bytes that were read
		 */
		public List<byte> ReadToEnd()
		{
			List<byte> bytes = new List<byte>();
			
			bool fin = false;
			while(!fin)
			{
				int toadd = this.ReadByte();
				
				if (toadd != -1)
					bytes.Add((byte)toadd);
				else fin = true;
			}
			
			return bytes;
		}
	
		/**
		* Reads a size from the stream
		*
		* @return Size extracted from the stream
		*/
		public int ReadSize()
		{
			int size = this.ReadByte();
			
			if (size == 255)
			{
				return this.ReadInteger();
			}
			
			else if (size == -1) return 0;
			
			else return size;
		}
	
		/**
		* Reads a byte from the stream
		*
		* @return Byte extracted from the stream
		*/
		public byte ReadOneByte()
		{
			return (byte)this.ReadByte();
		}
		
		/**
		* Reads a boolean from the stream
		*
		* @return Boolean extracted from the stream
		*/
		public bool ReadBoolean()
		{
			int b = this.ReadByte();
			return (b==1)?true:false;
		}
		
		/**
		* Reads a short from the stream
		*
		* @return Short extracted from the stream
		*/
		public short ReadShort()
		{
			byte[] buffer = new byte[2];
			try {
				this.Read(buffer, 0, buffer.Length);
			} catch (IOException) {
			}
			
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
				
			return BitConverter.ToInt16(buffer, 0);
		}
		
		/**
		* Reads an integer from the stream
		*
		* @return Integer extracted from the stream
		*/
		public int ReadInteger()
		{
			byte[] buffer = new byte[4];
			try {
				this.Read(buffer, 0, buffer.Length);
			} catch (IOException) {
			}
			
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
				
			return BitConverter.ToInt32(buffer, 0);
		}
		
		/**
		* Reads a long from the stream
		*
		* @return Long extracted from the stream
		*/
		public long ReadLong()
		{
			byte[] buffer = new byte[8];
			try {
				this.Read(buffer, 0, buffer.Length);
			} catch (IOException) {
			}
			
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
				
			return BitConverter.ToInt64(buffer, 0);
		}
		
		/**
		* Reads a float from the stream
		*
		* @return Float extracted from the stream
		*/
		public float ReadFloat()
		{
			byte[] buffer = new byte[4];
			try {
				this.Read(buffer, 0, buffer.Length);
			} catch (IOException) {
			}
			
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
				
			return BitConverter.ToSingle(buffer, 0);
		}
		
		/**
		* Reads a double from the stream
		*
		* @return Double extracted from the stream
		*/
		public double ReadDouble()
		{
			byte[] buffer = new byte[8];
			try {
				this.Read(buffer, 0, buffer.Length);
			} catch (IOException) {
			}
			
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
				
			return BitConverter.ToDouble(buffer, 0);
		}
		
		/**
		* Reads an string from the stream
		*
		* @param res String extracted from the stream
		*/
		public string ReadString()
		{
			int size = this.ReadSize();
			byte[] buffer = new byte[size];
			try {
				this.Read(buffer, 0, buffer.Length);
			} catch (IOException) {
			}
			
			string str = "";
			for (int i=0; i<buffer.Length; i++)
			{
				str = str+(char)buffer[i];
			}
			
			return str;
		}
		
		/**
		* Reads an utf8 string from the stream
		*
		* @return String extracted from the stream
		*/
		public string ReadUTF8String()
		{
			int size = this.ReadSize();
			byte[] buffer = new byte[size];
			try {
				this.Read(buffer, 0, buffer.Length);
			} catch (IOException) {
			}
			
			return Encoding.UTF8.GetString(buffer);
		}
	
		/**
		* Reads a byte sequence from the stream
		*
		* @return Byte sequence extracted from the stream
		*/
		public List<byte> ReadByteSeq()
		{
			int size = this.ReadSize();
			List<byte> res = new List<byte>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadOneByte());
			}
			
			return res;
		}
		
		/**
		* Reads a boolean sequence from the stream
		*
		* @return Boolean sequence extracted from the stream
		*/
		public List<bool> ReadBooleanSeq()
		{
			int size = this.ReadSize();
			List<bool> res = new List<bool>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadBoolean());
			}
			
			return res;
		}
		
		/**
		* Reads a short sequence from the stream
		*
		* @return Short sequence extracted from the stream
		*/
		public List<short> ReadShortSeq()
		{
			int size = this.ReadSize();
			List<short> res = new List<short>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadShort());
			}
			
			return res;
		}
		
		/**
		* Reads an integer sequence from the stream
		*
		* @return Integer sequence extracted from the stream
		*/
		public List<int> ReadIntegerSeq()
		{
			int size = this.ReadSize();
			List<int> res = new List<int>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadInteger());
			}
			
			return res;
		}
		
		/**
		* Reads a long sequence from the stream
		*
		* @return Long sequence extracted from the stream
		*/
		public List<long> ReadLongSeq()
		{
			int size = this.ReadSize();
			List<long> res = new List<long>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadLong());
			}
			
			return res;
		}
		
		/**
		* Reads a float sequence from the stream
		*
		* @return Float sequence extracted from the stream
		*/
		public List<float> ReadFloatSeq()
		{
			int size = this.ReadSize();
			List<float> res = new List<float>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadFloat());
			}
			
			return res;
		}
		
		/**
		* Reads a double sequence from the stream
		*
		* @return Double sequence extracted from the stream
		*/
		public List<double> ReadDoubleSeq()
		{
			int size = this.ReadSize();
			List<double> res = new List<double>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadDouble());
			}
			
			return res;
		}
		
		/**
		* Reads a string sequence from the stream
		*
		* @return String sequence extracted from the stream
		*/
		public List<string> ReadStringSeq()
		{
			int size = this.ReadSize();
			List<string> res = new List<string>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadString());
			}
			
			return res;
		}
		
		/**
		* Reads a utf8 string sequence from the stream
		*
		* @return String sequence extracted from the stream
		*/
		public List<string> ReadUTF8StringSeq()
		{
			int size = this.ReadSize();
			List<string> res = new List<string>();
			
			for (int i=0; i<size; i++)
			{
				res.Add(this.ReadUTF8String());
			}
			
			return res;
		}
	
		/**
		* Reads a dictionary from the stream
		*
		* @return Dictionary extracted from the stream
		*/
		public Dictionary<string, List<byte>> ReadDictionary()
		{
			Dictionary<String, List<byte>> res = new Dictionary<string, List<byte>>();
			
			int size = this.ReadSize();
			
			for (int i=0; i<size; i++)
			{
				string key = this.ReadString();
				List<byte> val = this.ReadByteSeq();
				
				res.Add(key, val);
			}
			
			return res;
		}
	}

}
