
using System;
using System.Collections.Generic;

namespace BlueRose
{
	public interface Marshallable
	{
		void Marshall(ByteStreamWriter writer);
		void Unmarshall(ByteStreamReader reader);
	}
}
