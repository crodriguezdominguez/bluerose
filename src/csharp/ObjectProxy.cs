
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using BlueRose.Messages;
using BlueRose.Threads;

namespace BlueRose
{


	/**
	 * Proxy for transparently connecting to a servant
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 11-11-2009
	 */
	public abstract class ObjectProxy 
	{
		protected static readonly int MAX_REQUEST_ID = 1000000;
		
		protected static AsyncCallback asyncThread = null; /**< Callback thread for asynchronous methods */
		protected static int numInstances = 0; /**< Number of instances of the object */
		protected static int requestId = 0; /**< Request Id */
		
		protected ICommunicationDevice device; /**< Interface for transferring information */
		protected OpenConnectionThread openThread; /**< Thread for openning connections in case the servant is disconnected */
		protected string servantID; /**< Id of the servant */
		protected Identity identity; /**< Identity of the object */
		protected RequestOperationMode currentMode; /**< Current operational mode. @see MessageHeader.h */
		
		protected static object mutex = new object();
		
		public ObjectProxy()
		{
			device = null;
			openThread = null;
			identity = new Identity();
			
			lock(mutex) {
				numInstances++;
			}
			
			//interface->openConnection(_servantID, connParameters);
			currentMode = RequestOperationMode.TwoWay;
		}
		
		/**
		 * Constructor. Uses the current interface of the DevicePool.
		 * If that interface changes, it also changes for the object.
		 *
		 * @param servID Id of the servant to connect to
		 */
		public ObjectProxy(string servID)
		{
			InitalizeAttributes(servID, null, true, null);
		}
		
		/**
		 * Constructor. Uses the current interface of the DevicePool.
		 * If that interface changes, it also changes for the object.
		 *
		 * @param servID Id of the servant to connect to
		 * @param dev Interface to connect to. It it's NULL, it will connect to the current interface in the
		 *               interface pool.
		 */
		public ObjectProxy(string servID, ICommunicationDevice dev)
		{
			InitalizeAttributes(servID, dev, true, null);
		}
		
		/**
		 * Constructor. Uses the current interface of the DevicePool.
		 * If that interface changes, it also changes for the object.
		 *
		 * @param servID Id of the servant to connect to
		 * @param dev Interface to connect to. It it's NULL, it will connect to the current interface in the
		 *               interface pool.
		 * @param wait If it's true, it will wait for the servant to be alive
		 */
		public ObjectProxy(string servID, ICommunicationDevice dev, bool wait)
		{
			InitalizeAttributes(servID, dev, wait, null);
		}
	
		/**
		 * Constructor. Uses the current interface of the DevicePool.
		 * If that interface changes, it also changes for the object.
		 *
		 * @param servID Id of the servant to connect to
		 * @param dev Interface to connect to. It it's NULL, it will connect to the current interface in the
		 *               interface pool.
		 * @param wait If it's true, it will wait for the servant to be alive
		 * @param pars Connection parameters
		 */
		public ObjectProxy(string servID, ICommunicationDevice dev, bool wait, Dictionary<string, List<byte>> pars)
		{
			InitalizeAttributes(servID, dev, wait, pars);
		}
		
		~ObjectProxy()
		{
			openThread.Interrupt();
			
			lock(mutex) {
				numInstances--;
				if (numInstances <= 0)
				{
					asyncThread.Interrupt();
					asyncThread = null;
					numInstances = 0;
				}
			}
		
			device = null;
		}
		
		public Identity Identity
		{
			get{
				return identity;
			}
			set{
				identity = value;
			}
		}
	
		/**
		 * Auxiliar method for constructors
		 */
		protected void InitalizeAttributes(string servID, ICommunicationDevice dev, bool wait, Dictionary<string, List<byte>> pars)
		{
			identity = new Identity();
			servantID = servID;
			device = dev;
			openThread = null;
			
			lock(mutex) {
				numInstances++;
			}
			
			ICommunicationDevice iface_aux = (device != null) ? device : DevicePool.GetCurrentDevice();
			if (!iface_aux.IsConnectionOpenned(servantID))
			{
				openThread = new OpenConnectionThread(iface_aux, servantID, int.MaxValue, null);
				openThread.Start();
				
				if (wait)
				{ 
					openThread.Join();
					openThread = null;
				}
			}
			else
			{
				try {
					iface_aux.OpenConnection(servantID);
				} catch (Exception) {
				}
			}
			
			//interface->openConnection(_servantID, connParameters);
			currentMode = RequestOperationMode.TwoWay;
		}
	
		/**
		 * Sends a request to a servant
		 * 
		 * @param servantID ID of the servant to send the request to
		 * @param operation Operation of the request
		 * @param request Message of the request
		 * @return Request Identifier for receiving the reply message
		 */
		public int SendRequest(string servID, string oper, List<byte> request)
		{
			bool res;
			
			RequestMessage msg = new RequestMessage();
			RequestMessageHeader hd = (RequestMessageHeader)msg.Header;
			
			int req;
			lock(mutex) {
				requestId = ((requestId+1) % MAX_REQUEST_ID);
				req = requestId;
			}
			
			hd.RequestID = req;
			hd.Operation = oper;
			
			lock(this) {
				hd.Mode = currentMode;
				hd.Identity = identity;
			}
			
			msg.AddToEncapsulation(request);
			
			List<byte> msg_bytes = msg.GetBytes();
			
			if (device == null)
				res = DevicePool.GetCurrentDevice().Write(servantID, msg_bytes);
			
			else res = device.Write(servantID, msg_bytes);
			
			if (!res)
			{
				lock (this) {
				/**
				 * TODO: Crear una cache para tratar de proveer algunos mensajes
				 */
					if (openThread == null)
					{
						if (device != null)
							openThread = new OpenConnectionThread(device, servantID, int.MaxValue, null);	
						else openThread = new OpenConnectionThread(DevicePool.GetCurrentDevice(), servantID, int.MaxValue, null);
						
						openThread.Start();
					}
				}
				
				//throw "Service Not Available";
			}
			else
			{
				lock(this) {
					if (openThread != null)
					{
						openThread.Interrupt();
						openThread = null;
					}
				}
			}
			
			return req;
		}
	
		/**
		 * Receives a reply. Is a blockant method.
		 *
		 * @param reqID Request Identifier of the request message
		 * @result Received Message
		 */
		public List<byte> ReceiveReply(int reqID)
		{
			RequestOperationMode currMode;
			lock(this) {
				currMode = currentMode;
			}
			
			List<byte> res = null;
			
			if (currMode == RequestOperationMode.TwoWay)
			{
				List<byte> recv_bytes = ReplyMessageStack.Consume(reqID);
				ReplyMessage msg = new ReplyMessage(recv_bytes);
				res = msg.Body.ByteCollection;
			}
			
			return res;
		}
	
		/**
		 * Receives a message with the callback thread and treats the received
		 * message with the specified AsyncMethodCallback
		 *
		 * @param reqID Request Identifier of the request message
		 * @param method Async method callback for treating the received message
		 */
		public void ReceiveCallback(int reqID, AsyncMethodCallback method)
		{
			lock(mutex) {
				if (asyncThread == null)
				{
					asyncThread = new AsyncCallback();
					asyncThread.Start();
				}
				
				asyncThread.PushRequestID(reqID, method);
			}
		}
	
		public void ResolveInitialization(ICommunicationDevice dev, bool wait, Dictionary<string, List<byte>> pars)
		{
			if (DiscoveryProxy.DefaultProxy == null)
			{
				throw new Exception("ERROR: DiscoveryProxy not initialized");
			}
			
			string _servantID = DiscoveryProxy.DefaultProxy.ResolveName(this.GetTypeID());
			
			if (_servantID.Equals(""))
			{
				throw new Exception("Servant for the "+this.GetTypeID()+" proxy has not been found");
			}
			
			servantID = _servantID;
			device = dev;
			openThread = null;
			
			ICommunicationDevice iface_aux = (dev != null) ? device : DevicePool.GetCurrentDevice();
			if (!iface_aux.IsConnectionOpenned(servantID))
			{
				openThread = new OpenConnectionThread(iface_aux, servantID, int.MaxValue, pars);
				openThread.Start();
				
				if (wait)
				{
					openThread.Join();
					openThread = null;
				}
			}
			else
			{
				iface_aux.OpenConnection(servantID);
			}
		}
	
		/**
		 * Waits for a Servant to be online
		 */
		public void WaitOnlineMode()
		{
			bool wait;
			lock(this) {
				wait = (openThread != null);
			}
			
			if (!wait) return;
			else
			{
				lock(this) {
					try {
						openThread.Join();
					} catch (Exception) {
					}
					openThread = null;
				}
			}
		}
	
		/**
		 * Gets the type ID of the proxy
		 *
		 * @return String that represents the type ID
		 */
		public abstract string GetTypeID();
	}
	
	
	/**
	 * Callback method for an asynchronous request
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 15-11-2009
	 */
	public class AsyncCallback 
	{
		protected Dictionary<int, AsyncMethodCallback> requestsId_callbacks; /**< stack of requestIds and asynchronous method callbacks */
		protected System.Threading.Thread thread;
		
		public AsyncCallback()
		{
			requestsId_callbacks = new Dictionary<int, AsyncMethodCallback>();
		}
		
		/**
		 * Adds a new request id to the async method callback stack
		 *
		 * @param reqId Request identifier for the reply
		 * @param amc Callback method that is called when the reply is received
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public void PushRequestID(int reqID, AsyncMethodCallback method)
		{
			this.requestsId_callbacks[reqID] = method;
			System.Threading.Monitor.PulseAll(this);
		}
		
		public void Interrupt()
		{
			thread.Interrupt();
		}
		
		public void Start()
		{
			thread = new System.Threading.Thread(new System.Threading.ThreadStart(this.Run));
			thread.Start();
		}
		
		protected void Run()
		{
			while(true)
			{
				int size;
				lock(this) {
					while(this.requestsId_callbacks.Count <= 0)
					{
						try {
							System.Threading.Monitor.Wait(this);
						} catch (Exception) {
						}
					}
					
					size = this.requestsId_callbacks.Count;
				}
				
				if (size > 0)
				{
					int reqID = 0;
					int key = 0;
					AsyncMethodCallback amc = null;
					lock(this) {
						foreach(int k in requestsId_callbacks.Keys)
						{
							key = k;
							reqID = key;
							amc = this.requestsId_callbacks[key];
							break;
						}
					}
					
					List<byte> recv_bytes = ReplyMessageStack.Consume(reqID);
					
					ReplyMessage msg = new ReplyMessage(recv_bytes);
					List<byte> recv_bytes2 = msg.Body.ByteCollection;
					if (recv_bytes2.Count > 0)
					{
						amc.Callback(recv_bytes2);
					}
					
					lock(this) {
						this.requestsId_callbacks.Remove(key);
					}
				}
			}
		}
	}
}
