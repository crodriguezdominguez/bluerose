
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using BlueRose.Messages;

namespace BlueRose
{


	/**
	 * Pool of interfaces. It also includes servant objects, as they may be
	 * considered as an abstraction over an specific interface.
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 07-10-2009
	 */
	public class DevicePool 
	{	
		protected static Dictionary<string, ICommunicationDevice> deviceCollection; /**< Allowed transmission devices */
		protected static ICommunicationDevice device; /**< Current transmission channel */
		protected static Dictionary<string, ObjectServant> objects; /**< Registered objects servants */
		
		/**
		 * Starts the pool, selecting the current interface
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static void Initialize()
		{
			deviceCollection = new Dictionary<string, ICommunicationDevice>();
			objects = new Dictionary<string, ObjectServant>();
			ReplyMessageStack.Initialize();
			device = GetNextDevice();
		}
	
		/**
		 * Shutdown the interface pool, releasing all resources.
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static void Destroy()
		{
			ReplyMessageStack.Destroy();

			if (objects != null)
			{
				foreach(string key in objects.Keys)
				{
					ObjectServant val = objects[key];
					
					if (DiscoveryProxy.DefaultProxy != null)
					{
						DiscoveryProxy.DefaultProxy.RemoveRegistration(val.GetIdentifier());
					}
				}
			}
		}
	
		/**
		 * Adds a new interface to the collection of interfaces
		 *
		 * @param dev Interface to add
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static void AddDevice(ICommunicationDevice dev)
		{
			deviceCollection[dev.GetDeviceName()] = dev;
			if (deviceCollection.Count == 1)
			{
				device = GetNextDevice();
			}
		}
	
		/**
		 * Registers a new object into the pool
		 *
		 * @param object Object to register
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static void RegisterObjectServant(ObjectServant obj)
		{
			objects[obj.Identity.ToString()] = obj;
		}
	
		/**
		 * Unregisters an object from the pool
		 *
		 * @param obj Object to unregister
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static void UnregisterObjectServant(ObjectServant obj)
		{
			Identity id = obj.Identity;
			objects.Remove(id.ToString());
			
			//unregister from the discovery proxy
			if (DiscoveryProxy.DefaultProxy != null)
			{
				DiscoveryProxy.DefaultProxy.RemoveRegistration(obj.GetIdentifier());
			}
		}
	
		/**
		 * Selects a new interface for transmitting data based upon the priority of the channels
		 *
		 * @return New transmission interface
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static ICommunicationDevice GetNextDevice()
		{
			//find the device with the higher priority (the lowest number)
			ICommunicationDevice result = null;
			int minPriority = int.MaxValue;
			foreach(string key in deviceCollection.Keys)
			{
				ICommunicationDevice val = deviceCollection[key];
				if (val.GetPriority() <= minPriority)
				{
					minPriority = val.GetPriority();
					result = val;
				}
			}
			
			return result;
		}
	
		/**
		 * Message Dispatcher for asynchronous calls and/or server requests
		 *
		 * @param message Message to dispatch
		 * @param userID Sender user id
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static void DispatchMessage(List<byte> message, string userID)
		{
			ReplyMessage reply = new ReplyMessage();
			RequestMessage orig = new RequestMessage(message);
	
			MethodResult replyStatus = new MethodResult();
			
			ReplyMessageHeader header = (ReplyMessageHeader)reply.Header;
			RequestMessageHeader header2 = (RequestMessageHeader)orig.Header;
			
			int requestId = header2.RequestID;
			
			header.RequestID = requestId;
			
			//identify destination object
			ObjectServant serv = null;
			
			if (objects.ContainsKey(header2.Identity.ToString()))
			{
				serv = objects[header2.Identity.ToString()];
				replyStatus = serv.RunMethod(header2.Operation, userID, orig.Body.ByteCollection);
			}
			else
			{
				replyStatus.Status = ReplyStatus.ObjectNotExist;
			}
			
			header.ReplyStatus = replyStatus.Status;
			
			if (replyStatus.Status == ReplyStatus.Success)
				reply.AddToEncapsulation(replyStatus.Result);
			
			//write reply msg
			List<byte> reply_bytes = reply.GetBytes();
			
			if (header2.Mode == RequestOperationMode.TwoWay)
			{
				device.Write(userID, reply_bytes);
			}
		}
	
		/**
		 * Retrieves the current interface
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static ICommunicationDevice GetCurrentDevice()
		{
			return device;
		}
	
		/**
		 * Returns the transmission interface for the provided name
		 *
		 * @param name Name of the transmission interface
		 *
		 * @return Transmission interface
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static ICommunicationDevice GetDeviceForName(string name)
		{
			if (deviceCollection.ContainsKey(name)) return deviceCollection[name];
			else return null;
		}
	}
}
