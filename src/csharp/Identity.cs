
using System;

namespace BlueRose
{


	/**
	 * Identity for a request message
	 */
	public class Identity {
		protected string id_name;
		protected string category;
		
		public bool Equals(Identity ident)
		{
			return this.IsEqualToIdentity((Identity)ident);
		}
		
		public string Name
		{
			get{
				return id_name;
			}
			set{
				id_name = value;
			}
		}
		
		public string Category
		{
			get{
				return category;
			}
			set{
				category = value;
			}
		}
		
		public bool IsEqualToIdentity(Identity identity)
		{
			if ( identity.id_name.Equals(id_name) && 
				 identity.category.Equals(category) )
			{
				return true;
			}
				
			else return false;
		}
	}
}
