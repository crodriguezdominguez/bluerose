
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace BlueRose
{


	public class Configuration 
	{
		protected XmlTextReader reader;
		protected XmlDocument doc;
		
		public Configuration(Stream stream)
		{
			reader = new XmlTextReader(stream);
			doc = new XmlDocument();
			doc.Load(reader);
		}
		
		public List<string> GetDevices()
		{
			List<string> res = new List<string>();
			
			XmlElement root_element = doc.DocumentElement;
			XmlNodeList nodes = root_element.ChildNodes;
			
			for (int i = 0; i<nodes.Count; i++)
			{
				XmlNode cur_node = nodes[i];
				if (cur_node.NodeType == XmlNodeType.Element)
				{
					if (cur_node.Name.Equals("devices"))
					{
						//extract devices
						XmlNodeList nodes2 = cur_node.ChildNodes;
						
						for(int j=0; j<nodes2.Count; j++)
						{
							XmlNode cur_node2 = nodes2[j];
							if (cur_node2.NodeType == XmlNodeType.Element)
							{
								string c = cur_node2.Attributes["name"].Value;
								if (c != null)
								{
									res.Add(c);
								}
							}
						}
						
						break;
					}
				}
			}
			
			return res;
		}
		
		public string GetModuleForDevice(string device)
		{	
			XmlElement root_element = doc.DocumentElement;
			XmlNodeList nodes = root_element.ChildNodes;
			
			for(int i=0; i<nodes.Count; i++)
			{
				XmlNode cur_node = nodes[i];
				if (cur_node.NodeType == XmlNodeType.Element)
				{
					if (cur_node.Name.Equals("devices"))
					{
						//extract devices
						XmlNodeList nodes2 = cur_node.ChildNodes;
						
						for(int j=0; j<nodes2.Count; j++)
						{
							XmlNode cur_node2 = nodes2[j];
							if (cur_node2.NodeType == XmlNodeType.Element)
							{
								string name = cur_node2.Attributes["name"].Value;
								if (name != null)
								{
									if (name.Equals(device))
									{
										name = cur_node2.Attributes["module"].Value;
										
										if (name != null)
										{
											return name;
										}
									}
								}
							}
						}
					}
				}
			}
			
			return null;
		}
		
		public List<string> GetCategories()
		{
			List<string> res = new List<string>();
			
			XmlElement root_element = doc.DocumentElement;
			XmlNodeList nodes = root_element.ChildNodes;
			
			for(int i=0; i<nodes.Count; i++)
			{
				XmlNode cur_node = nodes[i];
				if (cur_node.NodeType == XmlNodeType.Element)
				{
					if (cur_node.Name.Equals("services"))
					{
						//extract devices
						XmlNodeList nodes2 = cur_node.ChildNodes;
						
						for(int j=0; j<nodes2.Count; j++)
						{
							XmlNode cur_node2 = nodes2[j];
							if (cur_node2.NodeType == XmlNodeType.Element)
							{
								string name = cur_node2.Attributes["name"].Value;
								
								if (name != null)
								{
									res.Add(name);
								}
							}
						}
						
						break;
					}
				}
			}
			
			return res;
		}
		
		public List<string> GetServicesForCategory(string category)
		{	
			XmlElement root_element = doc.DocumentElement;
			XmlNodeList nodes = root_element.ChildNodes;
			
			for(int i=0; i<nodes.Count; i++)
			{
				XmlNode cur_node = nodes[i];
				if (cur_node.NodeType == XmlNodeType.Element)
				{
					if (cur_node.Name.Equals("services"))
					{
						//extract devices
						XmlNodeList nodes2 = cur_node.ChildNodes;
						
						for(int j=0; j<nodes2.Count; j++)
						{
							XmlNode cur_node2 = nodes2[j];
							if (cur_node2.NodeType == XmlNodeType.Element)
							{
								string name1 = cur_node2.Attributes["name"].Value;
								
								if (name1 != null)
								{
									if (name1.Equals(category))
									{
										XmlNodeList nodes3 = cur_node2.ChildNodes;
										
										List<string> res = new List<string>();
										
										for(int k=0; k<nodes3.Count; k++)
										{
											XmlNode cur_node3 = nodes3[k];
											if (cur_node3.NodeType == XmlNodeType.Element)
											{
												string name2 = cur_node3.Attributes["name"].Value;
												
												if (name2 != null)
												{
													res.Add(name2);
												}
											}
										}
										
										return res;
									}
								}
							}
						}
					}
				}
			}
			
			return null;
		}
		
		public List<string> GetDevicesForService(string category, string service)
		{
			XmlElement root_element = doc.DocumentElement;
			XmlNodeList nodes = root_element.ChildNodes;
			
			for(int i=0; i<nodes.Count; i++)
			{
				XmlNode cur_node1 = nodes[i];
				if (cur_node1.NodeType == XmlNodeType.Element)
				{
					if (cur_node1.Name.Equals("services"))
					{
						//extract devices
						XmlNodeList nodes2 = cur_node1.ChildNodes;
						
						for(int j=0; j<nodes2.Count; j++)
						{
							XmlNode cur_node2 = nodes2[j];
							if (cur_node2.NodeType == XmlNodeType.Element)
							{
								string name1 = cur_node2.Attributes["name"].Value;
								
								if (name1 != null)
								{
									if (name1.Equals(category))
									{
										XmlNodeList nodes3 = cur_node2.ChildNodes;
										
										for(int m=0; m<nodes3.Count; m++)
										{
											XmlNode cur_node3 = nodes3[m];
											if (cur_node3.NodeType == XmlNodeType.Element)
											{
												string name2 = cur_node3.Attributes["name"].Value;
												
												if (name2 != null)
												{
													if (name2.Equals(service))
													{
														List<string> res = new List<string>();
														
														XmlNodeList nodes4 = cur_node3.ChildNodes;
														for(int h=0; h<nodes4.Count; h++)
														{
															XmlNode cur_node4 = nodes4[h];
															if (cur_node4.NodeType == XmlNodeType.Element)
															{
																string name3 = cur_node4.Attributes["name"].Value;
																
																if (name3 != null)
																{
																	res.Add(name3);
																}
															}
														}
														
														return res;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			return null;
		}
		
		public string GetPort(string category, string service, string device)
		{
			XmlElement root_element = doc.DocumentElement;
			XmlNodeList nodes = root_element.ChildNodes;
			
			for(int i=0; i<nodes.Count; i++)
			{
				XmlNode cur_node1 = nodes[i];
				if (cur_node1.NodeType == XmlNodeType.Element)
				{
					if (cur_node1.Name.Equals("services"))
					{
						//extract devices
						XmlNodeList nodes2 = cur_node1.ChildNodes;
						
						for(int j=0; j<nodes2.Count; j++)
						{
							XmlNode cur_node2 = nodes2[j];
							if (cur_node2.NodeType == XmlNodeType.Element)
							{
								string name1 = cur_node2.Attributes["name"].Value;
								
								if (name1 != null)
								{
									if (name1.Equals(category))
									{
										XmlNodeList nodes3 = cur_node2.ChildNodes;
										
										for(int k=0; k<nodes3.Count; k++)
										{
											XmlNode cur_node3 = nodes3[k];
											if (cur_node3.NodeType == XmlNodeType.Element)
											{
												string name2 = cur_node3.Attributes["name"].Value;
												
												if (name2 != null)
												{
													if (name2.Equals(service))
													{
														XmlNodeList nodes4 = cur_node3.ChildNodes;
														for(int m=0; m<nodes4.Count; m++)
														{
															XmlNode cur_node4 = nodes4[m];
															if (cur_node4.NodeType == XmlNodeType.Element)
															{
																string name3 = cur_node4.Attributes["name"].Value;
																
																if (name3 != null)
																{
																	if (name3.Equals(device))
																	{
																		string result = cur_node4.Attributes["port"].Value;
																		
																		if (result != null)
																		{
																			return result;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			return null;
		}
		
		public string GetAddress(string category, string service, string device)
		{
			XmlElement root_element = doc.DocumentElement;
			XmlNodeList nodes = root_element.ChildNodes;
			
			for(int i=0; i<nodes.Count; i++)
			{
				XmlNode cur_node1 = nodes[i];
				if (cur_node1.NodeType == XmlNodeType.Element)
				{
					if (cur_node1.Name.Equals("services"))
					{
						//extract devices
						XmlNodeList nodes2 = cur_node1.ChildNodes;
						
						for(int j=0; j<nodes2.Count; j++)
						{
							XmlNode cur_node2 = nodes2[j];
							if (cur_node2.NodeType == XmlNodeType.Element)
							{
								string name1 = cur_node2.Attributes["name"].Value;
								
								if (name1 != null)
								{
									if (name1.Equals(category))
									{
										XmlNodeList nodes3 = cur_node2.ChildNodes;
										
										for(int k=0; k<nodes3.Count; k++)
										{
											XmlNode cur_node3 = nodes3[k];
											if (cur_node3.NodeType == XmlNodeType.Element)
											{
												string name2 = cur_node3.Attributes["name"].Value;
												
												if (name2 != null)
												{
													if (name2.Equals(service))
													{
														XmlNodeList nodes4 = cur_node3.ChildNodes;
														for(int m=0; m<nodes4.Count; m++)
														{
															XmlNode cur_node4 = nodes4[m];
															if (cur_node4.NodeType == XmlNodeType.Element)
															{
																string name3 = cur_node4.Attributes["name"].Value;
																
																if (name3 != null)
																{
																	if (name3.Equals(device))
																	{
																		string result = cur_node4.Attributes["address"].Value;
																		
																		if (result != null)
																		{
																			return result;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			return null;
		}
		
		public string GetUserID(string category, string service, string device)
		{
			string address = this.GetAddress(category, service, device);
			string port = this.GetPort(category, service, device);
			
			if (address != null && port != null)
			{
				return ( address+":"+port );
			}
			
			return null;
		}
	}
}
