
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace BlueRose
{


	/**
	 * Stack where all received messages are stored
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 12-10-2009
	 */
	public class ReplyMessageStack 
	{	
		protected static Dictionary<int, List<byte>> stack; /**< Stack as pairs of requestIds-Messages */
		protected static bool waitingReply; /**< True if someone is waiting a reply */
		private static object mutex = new object();
		
		/**
		 * Initializes the stack
		 */
		public static void Initialize()
		{
			stack = new Dictionary<int, List<byte>>();
			waitingReply = false;
		}
	
		/**
		 * Destroys the stack
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static void Destroy()
		{
			waitingReply = false;
		}
	
		/**
		 * Adds a new message to the stack
		 *
		 * @param requestId Id of the request
		 * @param bytes Message
		 */
		public static void Produce(int requestId, List<byte> bytes)
		{	
			List<byte> toAdd = new List<byte>(bytes);
			
			lock(mutex) {
				stack[requestId] = toAdd;
				Monitor.PulseAll(mutex);
			}
		}
	
		/**
		 * Removes a message from the stack. If the message does not exist,
		 * it blocks undefinitely.
		 *
		 * @param requestId Id of the request
		 * @return Message removed from the stack
		 */
		public static List<byte> Consume(int requestId)
		{
			List<byte> val = null;
			
			lock (mutex) {
				
				while( !stack.ContainsKey(requestId) )
				{
					Monitor.Wait(mutex);
				}
				
				val = stack[requestId];
				
				stack.Remove(requestId);
				
				Monitor.PulseAll(mutex);
				
				return val;
			}
		}
	
		/**
		 * Returns whether somebody is waiting a reply or not
		 *
		 * @return True if someone is waiting. False otherwise else.
		 */
		[MethodImpl(MethodImplOptions.Synchronized)]
		public static bool IsWaitingReply()
		{
			return waitingReply;
		}
	}
}
