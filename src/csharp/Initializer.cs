
using System;
using System.IO;

namespace BlueRose
{


	public class Initializer {
		protected static Configuration configuration = null;
	
		public static void Initialize(Stream stream)
		{
			configuration = new Configuration(stream);
		}
		
		public static void InitializeNonWaitingClient(ICommunicationDevice device)
		{
			DevicePool.Initialize();
			DevicePool.AddDevice(device);
			
			if (DiscoveryProxy.DefaultProxy == null)
			{
				string discoveryAddress = configuration.GetUserID("BlueRoseService", "Discovery", device.GetDeviceName());
				
				if (discoveryAddress != null)
				{
					DiscoveryProxy.Initialize(discoveryAddress, device, false);
				}
				else
				{
					throw new Exception("ERROR: Discovery servant not configured in config file");
				}
			}
			
			BlueRose.Events.EventHandler.Initialize(device, false);
		}
	
		public static void InitializeClient(ICommunicationDevice device)
		{
			InitializeClient(device, true, true);
		}
	
		public static void InitializeClient(ICommunicationDevice device, bool discoveryServant)
		{
			InitializeClient(device, discoveryServant, true);
		}
	
		public static void InitializeClient(ICommunicationDevice device, bool discoveryServant, bool eventHandler)
		{
			DevicePool.Initialize();
			DevicePool.AddDevice(device);
			
			if (discoveryServant)
			{
				if (DiscoveryProxy.DefaultProxy == null)
				{
					String discoveryAddress = configuration.GetUserID("BlueRoseService", "Discovery", device.GetDeviceName());
					
					if (discoveryAddress != null)
					{
						DiscoveryProxy.Initialize(discoveryAddress, device);
					}
					else
					{
						throw new Exception("ERROR: Discovery servant not configured in config file");
					}
				}
			}
			
			if (eventHandler)
			{
				if (discoveryServant)
				{
					BlueRose.Events.EventHandler.Initialize(device);
				}
				
				else //extract from config file
				{
					string pub_sub = configuration.GetUserID("BlueRoseService", "PubSub", device.GetDeviceName());
					
					if (pub_sub != null)
					{
						BlueRose.Events.EventHandler.Initialize(pub_sub, device);
					}
					else
					{
						throw new Exception("ERROR: PubSub servant not configured in config file");
					}
				}
			}
		}
	
		public static void InitializeServant(ObjectServant servant, ICommunicationDevice device)
		{
			Initializer.InitializeServant(servant, device, true);
		}
	
		public static void InitializeServant(ObjectServant servant, ICommunicationDevice device, bool registerToDiscover)
		{
			DevicePool.Initialize();
			DevicePool.AddDevice(device);
			
			Identity identity = servant.Identity;
			string address = configuration.GetAddress(identity.Category, identity.Name, device.GetDeviceName());
			string port = configuration.GetPort(identity.Category, identity.Name, device.GetDeviceName());
			if (port == null)
			{
				throw new Exception("ERROR: Port not specified in config file");
			}
			
			//set as servant
			device.SetServantIdentifier(address);
			device.SetServant(port);
			
			if (registerToDiscover)
			{
				if (DiscoveryProxy.DefaultProxy == null)
				{
					string discoveryAddress = configuration.GetUserID("BlueRoseService", "Discovery", device.GetDeviceName());
					
					if (discoveryAddress != null)
					{
						DiscoveryProxy.Initialize(discoveryAddress, device);
					}
					else
					{
						throw new Exception("ERROR: Discovery servant not configured in config file");
					}
				}
				
				DiscoveryProxy.DefaultProxy.AddRegistration(servant.GetIdentifier(), device.GetServantIdentifier());
			}
			
			DevicePool.RegisterObjectServant(servant);
		}
	
		public static void Destroy()
		{
			DevicePool.Destroy();
			DiscoveryProxy.Destroy();
			BlueRose.Events.EventHandler.Destroy();
		}
	}
}
