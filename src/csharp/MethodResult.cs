
using System;
using System.Collections.Generic;

using BlueRose.Messages;

namespace BlueRose
{


	/**
	 * Result for the runMethod method of the servant of an object
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 03-03-2010
	 */
	public class MethodResult {
		protected ReplyStatus status;
		protected List<byte> result;
		
		public MethodResult()
		{
			status = ReplyStatus.Success;
			result = new List<byte>();
		}
		
		public ReplyStatus Status
		{
			get{
				return status;
			}
			set{
				status = value;
			}
		}
		
		public List<byte> Result
		{
			get{
				return result;
			}
			set{
				result = value;
			}
		}
	}
}
