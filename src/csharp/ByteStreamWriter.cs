
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BlueRose
{


	public class ByteStreamWriter : MemoryStream
	{

		public ByteStreamWriter ()
		{
			
		}
		
		public List<byte> ToVector()
		{
			return new List<byte>(this.ToArray());
		}
		
		/**
		* Adds an integer to the stream
		*
		* @param b Integer to add
		*/
		public void WriteInteger(int n)
		{
			byte[] buffer = BitConverter.GetBytes(n);
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
			
			this.Write(buffer, 0, buffer.Length);
		}
		
		/**
		* Writes a size to the stream. A size is coded as
		* a single byte if is lower than 255. If it's greater,
		* then it uses 5 bytes: The first one is always 255 and
		* the following are the integer representation of the size.
		*
		* @param size Size to write to the stream
		*/
		public void WriteSize(int size)
		{
			if (size < 255)
			{
				this.WriteByte((byte)size);
			}
			else
			{
				this.WriteByte((byte)255);
				this.WriteInteger(size);
			}
		}
		
		/**
		* Adds a boolean to the stream
		*
		* @param b Boolean to add
		*/
		public void WriteBoolean(bool b)
		{
			this.WriteByte((byte)( (b)?1:0 ));
		}
		
		/**
		* Adds a short to the stream
		*
		* @param b Short to add
		*/
		public void WriteShort(short b)
		{
			byte[] buffer = BitConverter.GetBytes(b);
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
			
			this.Write(buffer, 0, buffer.Length);
		}
		
		/**
		* Adds a long to the stream
		*
		* @param b Long to add
		*/
		public void WriteLong(long b)
		{
			byte[] buffer = BitConverter.GetBytes(b);
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
			
			this.Write(buffer, 0, buffer.Length);
		}
		
		/**
		* Adds a float to the stream
		*
		* @param b Float to add
		*/
		public void WriteFloat(float b)
		{
			byte[] buffer = BitConverter.GetBytes(b);
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
			
			this.Write(buffer, 0, buffer.Length);
		}
		
		/**
		* Adds a double to the stream
		*
		* @param b Double to add
		*/
		public void WriteDouble(double b)
		{
			byte[] buffer = BitConverter.GetBytes(b);
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse(buffer);
			}
			
			this.Write(buffer, 0, buffer.Length);
		}
		
		/**
		* Adds an string to the stream
		*
		* @param b String to add
		*/
		public void WriteString(string b)
		{
			this.WriteSize(b.Length);
			
			for (int i=0; i<b.Length; i++)
			{
				this.WriteByte((byte)b[i]);
			}
		}
		
		/**
		* Adds an utf8 string to the stream
		*
		* @param b String to add
		*/
		public void WriteUTF8String(string b)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(b);
			this.WriteSize(bytes.Length);
			
			for (int i=0; i<bytes.Length; i++)
			{
				this.WriteByte(bytes[i]);
			}
		}
		
		/**
		* Adds raw bytes to the stream
		*
		* @param b Bytes to add
		*/
		public void WriteRawBytes(List<byte> b)
		{	
			byte[] buffer = b.ToArray();
			this.Write(buffer, 0, buffer.Length);
		}
	
		/**
		* Adds a sequence of bytes to the stream
		*
		* @param b Bytes to add
		*/
		public void WriteByteSeq(List<byte> b)
		{
			this.WriteSize(b.Count);
			this.WriteRawBytes(b);
		}
		
		/**
		* Adds a sequence of booleans to the stream
		*
		* @param b Booleans to add
		*/
		public void WriteBooleanSeq(List<bool> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteBoolean(b[i]);
			}
		}
		
		/**
		* Adds a sequence of shorts to the stream
		*
		* @param b Shorts to add
		*/
		public void WriteShortSeq(List<short> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteShort(b[i]);
			}
		}
		
		/**
		* Adds a sequence of integers to the stream
		*
		* @param b Integers to add
		*/
		public void WriteIntegerSeq(List<int> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteInteger(b[i]);
			}
		}
		
		/**
		* Adds a sequence of longs to the stream
		*
		* @param b Longs to add
		*/
		public void WriteLongSeq(List<long> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteLong(b[i]);
			}
		}
		
		/**
		* Adds a sequence of floats to the stream
		*
		* @param b Floats to add
		*/
		public void WriteFloatSeq(List<float> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteFloat(b[i]);
			}
		}
		
		/**
		* Adds a sequence of doubles to the stream
		*
		* @param b Doubles to add
		*/
		public void WriteDoubleSeq(List<double> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteDouble(b[i]);
			}
		}
		
		/**
		* Adds a sequence of strings to the stream
		*
		* @param b Strings to add
		*/
		public void WriteStringSeq(List<string> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteString(b[i]);
			}
		}
		
		/**
		* Adds a sequence of utf8 strings to the stream
		*
		* @param b Strings to add
		*/
		public void WriteUTF8StringSeq(List<string> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			for (int i=0; i<size; i++)
			{
				this.WriteUTF8String(b[i]);
			}
		}
		
		/**
		* Adds a dictionary to the stream
		*
		* @param b Bytes to add
		*/
		public void WriteDictionary(Dictionary<string, List<byte>> b)
		{
			int size = b.Count;
			this.WriteSize(size);
			
			foreach(KeyValuePair<string, List<byte>> pair in b)
			{
				this.WriteString(pair.Key);
				this.WriteRawBytes(pair.Value);
			}
		}
		
		/**
		 * Adds a marshallable object to the stream
		 * @param object Object to add
		 */
		public void WriteObject(Marshallable obj)
		{
			obj.Marshall(this);
		}
	}
}
