
using System;

namespace BlueRose
{


	public class Pair<T, U>
	{
		private T first;
		private U second;
		
		public Pair()
		{
			first = default(T);
			second = default(U);
		}
		
		public Pair(T f, U s)
		{
			first = f;
			second = s;
		}
		
		public T First
		{
			get{
				return first;
			}
			set{
				first = value;
			}
		}
		
		public U Second
		{
			get{
				return second;
			}
			set{
				second = value;
			}
		}
		
		public bool equals(Pair<T, U> pair)
		{
			if (pair == null) return false;
			
			if (first.Equals(pair.first) && second.Equals(pair.second))
			{
				return true;
			}
			else return false;
		}
	}
}
