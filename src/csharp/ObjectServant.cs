
using System;
using System.Collections.Generic;

namespace BlueRose
{


	public abstract class ObjectServant
	{

		protected Identity identity;
	
		public ObjectServant()
		{
			identity = new Identity();
		}
		
		public Identity Identity
		{
			get{
				return identity;
			}
		}
		
		public ObjectServant(Identity ident)
		{
			identity = ident;
		}
		
		public ObjectServant(string id_name, string category)
		{
			identity = new Identity();
			
			identity.Name = id_name;
			identity.Category = category;
		}
		
		public bool Equals(ObjectServant servant)
		{
			Identity aux = servant.Identity;
			if (aux.IsEqualToIdentity(this.identity))
			{
				return true;
			}
			else return false;
		}
		
		public string GetIdentifier()
		{
			return identity.Category+"::"+identity.Name;
		}
		
		public abstract MethodResult RunMethod(string method_name, string userID, List<byte> args);
	}
}
