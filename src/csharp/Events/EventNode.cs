
using System;

namespace BlueRose.Events
{


	public class EventNode : Event 
	{
		protected string identifier;
		protected Value val;
		
		public EventNode()
		{
			identifier = null;
			val = null;
		}
		
		public EventNode(string ident, Value _val)
		{
			identifier = ident;
			val = _val;
		}
		
		public string Identifier
		{
			get{
				return identifier;
			}
			set{
				identifier = value;
			}
		}
		
		public Value Value
		{
			get{
				return val;
			}
			set{
				val = value;
			}
		}
	
		public override void Marshall(ByteStreamWriter writer) 
		{	
			//identifier
			writer.WriteString(this.identifier);
			
			//value
			this.val.Marshall(writer);
			
			//topic
			writer.WriteInteger(this.topic);
			
			//num. event nodes
			writer.WriteSize(nodes.Count);
			
			//event nodes
			foreach(string key in nodes.Keys)
			{
				EventNode node = nodes[key];
				writer.WriteObject(node);
			}
		}
		
		public override void Unmarshall(ByteStreamReader reader) 
		{	
			//identifier
			this.identifier = reader.ReadString();
			
			//value
			this.val = new Value();
			this.val.Unmarshall(reader);
			
			//topic
			this.topic = reader.ReadInteger();
			
			//num. event nodes
			int size = reader.ReadSize();
			
			if (size == 0)
			{
				return;
			}
			
			//event nodes
			for (int i=0; i<size; i++)
			{
				EventNode aux = new EventNode();
				aux.Unmarshall(reader);
				
				this.SetMember(aux.Identifier, aux);
			}
		}
	}
}
