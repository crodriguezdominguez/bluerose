
using System;
using System.Collections.Generic;

namespace BlueRose.Events
{


	public class Event : Marshallable
	{	
		public static readonly int DefaultTopic = 0;
		
		protected int topic; /**< Event topic */
		protected Dictionary<string, EventNode> nodes; /**< Map of identifiers and associated nodes */
		
		public Event()
		{
			topic = DefaultTopic;
			nodes = new Dictionary<string, EventNode>();
		}
		
		public Event(int top)
		{
			topic = top;
			nodes = new Dictionary<string, EventNode>();
		}
		
		public Event(Event evt)
		{
			topic = evt.topic;
			nodes = evt.nodes;
		}
		
		public bool ExistsMember(string member)
		{
			return nodes.ContainsKey(member);
		}
		
		public int Topic
		{
			get{
				return topic;
			}
			set{
				topic = value;
			}
		}
		
		public Dictionary<string, EventNode> Nodes
		{
			get{
				return nodes;
			}
		}
		
		public int Count
		{
			get{
				return nodes.Count;
			}
		}
		
		public EventNode GetMember(string member)
		{
			return nodes[member];
		}
		
		public Value GetMemberValue(string member)
		{
			if (nodes.ContainsKey(member)) return nodes[member].Value;
			else return null;
		}
		
		public void SetMember(string member, EventNode node)
		{
			nodes[member] = node;
		}
		
		public void SetMember(string member, Value val)
		{
			EventNode node = new EventNode(member, val);
			nodes[member] = node;
		}
	
		public virtual void Marshall(ByteStreamWriter writer) 
		{
			//topic
			writer.WriteInteger(this.topic);
			
			//num. event nodes
			writer.WriteSize(nodes.Count);
			
			//event nodes
			foreach(string key in nodes.Keys)
			{
				EventNode node = nodes[key];
				writer.WriteObject(node);
			}
		}
	
		public virtual void Unmarshall(ByteStreamReader reader) {
			int top = reader.ReadInteger();
			
			this.topic = top;
			this.nodes = new Dictionary<string, EventNode>();
			
			//num. event nodes
			int tam = reader.ReadSize();
			
			if (tam == 0)
			{
				return;
			}
			
			//event nodes
			for (int i=0; i<tam; i++)
			{
				EventNode aux = new EventNode();
				aux.Unmarshall(reader);
				
				this.SetMember(aux.Identifier, aux);
			}
		}
		
		public override string ToString()
		{
			string res = "Topic="+topic+", {";
			
			int counter = 0;
			int end = nodes.Count;
			foreach(string key in nodes.Keys)
			{
				EventNode val = nodes[key];
				res += key+":"+val.ToString();
				counter++;
				if (counter != end) res +=", ";
			}
				
			res += "}";
			return res;
		}
	}
}
