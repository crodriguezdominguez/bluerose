
using System;
using System.Collections.Generic;

namespace BlueRose.Events
{


	/**
	 * Class that models a set of constraints over the values of a set of properties
	 *
	 * @author Carlos Rodriguez Dominguez
	 * @date 09-02-2010
	 */
	public class Predicate : Marshallable {
		protected Dictionary<string, List<Pair<Comparison, Value>>> constraints;
	
		public Predicate()
		{
			constraints = new Dictionary<string, List<Pair<Comparison, Value>>>();
		}
		
		public Dictionary<string, List<Pair<Comparison, Value>>> Constraints
		{
			get{
				return constraints;
			}
			
			set{
				constraints = value;
			}
		}
		
		/**
		 * Adds a new constraint to the set of constraints, but only if the set remains
		 * consistent.
		 *
		 * @param property Property associated with the constraint
		 * @param comp Comparison operator between the property and the value
		 * @param value Value associated with the property
		 * @return True if the constraint was added to the set (i.e., the set remains
		 *         consistent after adding the constraint). False otherwise else.
		 */
		public bool AddConstraint(string property, Comparison comp, Value val)
		{
			if (!this.IsConsistent(property, comp, val)) return false;
			else
			{
				Pair<Comparison, Value> pair = new Pair<Comparison, Value>();
				pair.First = comp;
				pair.Second = val;
				
				List<Pair<Comparison, Value>> array = null;
				
				if (!constraints.ContainsKey(property))
				{
					array = new List<Pair<Comparison, Value>>();
	
					array.Add(pair);
					constraints[property] = array;
				}
				else
				{
					array = constraints[property];
					array.Add(pair);
					
					constraints[property] = array;
				}
				
				return true;
			}
		}
		
		/**
		 * Removes a constraint from the set
		 *
		 * @param property Property associated with the constraint to remove
		 */
		public void RemoveConstraints(string property)
		{
			constraints.Remove(property);
		}
		
		/**
		 * Checks if the set of constraints is consistent after adding "property comp value".
		 * For example: "x == 5", "y <= 3", etc.
		 *
		 * @param property Property to check
		 * @param value Value of the property
		 */
		public bool IsConsistent(string property, Comparison comp, Value val)
		{
			if (constraints.Count == 0) return true;
			else
			{
				if (!constraints.ContainsKey(property)) return true;
				List<Pair<Comparison, Value>> v = constraints[property];
				
				foreach (Pair<Comparison, Value> pair in v)
				{
					//check inconsistencies
					Comparison first = pair.First;
					Value second = pair.Second;
					
					if (second.Compare(Comparison.Equal, val)) //value == pair.second
					{
						if (first == Comparison.Equal)
						{
							if ( (comp == Comparison.Less) ||
								(comp == Comparison.Greater) ||
								(comp == Comparison.NotEqual) ) 
								return false;
						}
						
						else if (first == Comparison.NotEqual)
						{
							if (comp == Comparison.Equal) return false;
						}
						
						else if (first == Comparison.Less)
						{
							if ( (comp == Comparison.Equal) ||
								(comp == Comparison.Greater) ||
								(comp == Comparison.GreaterEqual) ) 
								return false;
						}
						
						else if (first == Comparison.LessEqual)
						{
							if (comp == Comparison.Greater) return false;
						}
						
						else if (first == Comparison.Greater)
						{
							if ( (comp == Comparison.Equal) ||
								(comp == Comparison.Less) ||
								(comp == Comparison.LessEqual) ) 
								return false;
						}
						
						else if (first == Comparison.GreaterEqual)
						{
							if (comp == Comparison.Less) return false;
						}
					}
					else //value != pair.second
					{
						if (first == Comparison.Equal) return false;
						
						else if (first == Comparison.NotEqual) return true;
						
						else if ( (first == Comparison.Less) ||
								 (first == Comparison.LessEqual) )
						{
							if (val.Compare(Comparison.Greater, second))
							{
								if ( (comp == Comparison.Equal) ||
									(comp == Comparison.Greater) ||
									(comp == Comparison.GreaterEqual) )
									return false;
							}
						}
						
						else if ( (first == Comparison.Greater) ||
								 (first == Comparison.GreaterEqual) )
						{
							if (val.Compare(Comparison.Less, second))
							{
								if ( (comp == Comparison.Equal) ||
									(comp == Comparison.Less) ||
									(comp == Comparison.LessEqual) )
									return false;
							}
						}
					}
				}
				
				return true;
			}
	
		}
		
		/**
		 * Checks if the set of constraints is consistent after adding "property == value".
		 *
		 * @param property Property to check
		 * @param value Value of the property
		 */
		public bool IsConsistent(string property, Value value)
		{
			return IsConsistent(property, Comparison.Equal, value);
		}
	
		/**
		 * Checks if the set of constraints is empty
		 */
		public bool IsEmpty()
		{
			return (constraints.Count == 0);
		}
		
		/**
		 * Returns the number of constraints
		 *
		 * @return Number of constraints
		 */
		public int Count
		{
			get{
				return constraints.Count;
			}
		}
	
		public void Marshall(ByteStreamWriter writer) 
		{
			writer.WriteSize(this.Count);
			
			foreach(string key in constraints.Keys)
			{
				writer.WriteString(key);
				List<Pair<Comparison, Value>> array = constraints[key];
				writer.WriteSize(array.Count);
				
				foreach (Pair<Comparison, Value> pair in array)
				{
					int number = (int)pair.First;
					Value val = pair.Second;
					
					writer.WriteSize(number);
					
					writer.WriteSize((int)val.Type);
					writer.WriteByteSeq(val.RawValue);
				}
			}
		}
	
		public void Unmarshall(ByteStreamReader reader) 
		{
			constraints = new Dictionary<string, List<Pair<Comparison, Value>>>(); 
			
			int size = reader.ReadSize();
			
			for (int i = 0; i < size; i++)
			{
				string property = reader.ReadString();
				
				int size_v = reader.ReadSize();
				for (int j=0; j<size_v; j++)
				{
					Comparison comp = (BlueRose.Comparison)reader.ReadSize();
					
					Value _val = new Value((ValueType)reader.ReadSize());
					
					//read byte array
					int n = reader.ReadSize();
					
					List<byte> data = new List<byte>();
					for (int k=0; k<n; k++)
					{
						data.Add(reader.ReadOneByte());
					}
					_val.RawValue = data;
					
					this.AddConstraint(property, comp, _val);
				}
			}
		}
	}

}
