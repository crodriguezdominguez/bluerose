
using System;
using System.Collections.Generic;

namespace BlueRose.Events
{

	public class Value : IComparable<Value>, Marshallable
	{
		protected ValueType type; /**< Type of the val */
		protected List<byte> rawValue; /**< Value */
		
		public Value()
		{
			type = ValueType.Null;
			rawValue = new List<byte>();
		}
		
		public Value(ValueType valType)
		{
			type = valType;
			rawValue = new List<byte>();
		}
		
		public List<byte> RawValue
		{
			get{
				return rawValue;
			}
			set{
				rawValue = value;
			}
		}
		
		public ValueType Type
		{
			get{
				return type;
			}
			set{
				type = value;
			}
		}
	
		public void SetByte(byte val)
		{
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteByte(val);
			rawValue = writer.ToVector();
		}
		
		public void SetInteger(int val)
		{
			type = ValueType.Integer;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteInteger(val);
			rawValue = writer.ToVector();
		}
		
		public void SetShort(short val)
		{
			type = ValueType.Short;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteShort(val);
			rawValue = writer.ToVector();
		}
		
		public void SetLong(long val)
		{
			type = ValueType.Long;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteLong(val);
			rawValue = writer.ToVector();
		}
	
		public void SetBoolean(bool val)
		{
			type = ValueType.Boolean;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteBoolean(val);
			rawValue = writer.ToVector();
		}
		
		public void SetFloat(float val)
		{
			type = ValueType.Float;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteFloat(val);
			rawValue = writer.ToVector();
		}
		
		public void SetDouble(double val)
		{
			type = ValueType.Double;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteDouble(val);
			rawValue = writer.ToVector();
		}
	
		public void SetString(string val)
		{
			type = ValueType.String;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteUTF8String(val);
			rawValue = writer.ToVector();
		}
	
		public void SetByteSeq(List<byte> val)
		{
			type = ValueType.ByteSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteByteSeq(val);
			rawValue = writer.ToVector();
		}
		
		public void SetIntegerSeq(List<int> val)
		{
			type = ValueType.IntegerSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteIntegerSeq(val);
			rawValue = writer.ToVector();
		}
		
		public void SetShortSeq(List<short> val)
		{
			type = ValueType.ShortSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteShortSeq(val);
			rawValue = writer.ToVector();
		}
		
		public void SetLongSeq(List<long> val)
		{
			type = ValueType.LongSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteLongSeq(val);
			rawValue = writer.ToVector();
		}
		
		public void SetBooleanSeq(List<bool> val)
		{
			type = ValueType.BooleanSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteBooleanSeq(val);
			rawValue = writer.ToVector();
		}
		
		public void SetFloatSeq(List<float> val)
		{
			type = ValueType.FloatSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteFloatSeq(val);
			rawValue = writer.ToVector();
		}
		
		public void SetDoubleSeq(List<double> val)
		{
			type = ValueType.DoubleSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteDoubleSeq(val);
			rawValue = writer.ToVector();
		}
		
		public void SetStringSeq(List<string> val)
		{
			type = ValueType.StringSeq;
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteUTF8StringSeq(val);
			rawValue = writer.ToVector();
		}
		
		public byte GetByte()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadOneByte();
		}
	
		public int GetInteger()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadInteger();
		}
		
		public short GetShort()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadShort();
		}
	
		public long GetLong()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadLong();
		}
		
		public bool GetBoolean()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadBoolean();
		}
		
		public float GetFloat()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadFloat();
		}
	
		public double GetDouble()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadDouble();
		}
		
		public string GetString()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadUTF8String();
		}
		
		public List<byte> GetByteSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadByteSeq();
		}
	
		public List<int> GetIntegerSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadIntegerSeq();
		}
		
		public List<short> GetShortSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadShortSeq();
		}
		
		public List<long> GetLongSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadLongSeq();
		}
		
		public List<bool> GetBooleanSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadBooleanSeq();
		}
		
		public List<float> GetFloatSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadFloatSeq();
		}
	
		public List<double> GetDoubleSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadDoubleSeq();
		}
		
		public List<string> GetStringSeq()
		{
			ByteStreamReader reader = new ByteStreamReader(rawValue);
			return reader.ReadUTF8StringSeq();
		}
		
		public bool Compare(Comparison comp, Value val)
		{
			if (type != val.type) return false;
			
			if (comp == Comparison.Equal)
			{
				if (rawValue.Equals(val.rawValue)) return true;
				else return false;
			}
			
			if (comp == Comparison.NotEqual)
			{
				if (!rawValue.Equals(val.rawValue)) return true;
				else return false;
			}
			
			if (comp == Comparison.Less) return this.IsLessThan(val);
			else if (comp == Comparison.LessEqual) return this.IsLessEqualThan(val);
			else if (comp == Comparison.Greater) return this.IsGreaterThan(val);
			else if (comp == Comparison.GreaterEqual) return this.IsGreaterEqualThan(val);
			else return false;
		}
	
		public int CompareTo(Value val) 
		{	
			if (this.Equals(val))
			{
				return 0;
			}
			else if (this.IsLessThan(val))
			{
				return -1;
			}
			else return 1;
		}
		
		protected int Compare<T>(List<T> v1, List<T> v2) where T : IComparable
		{
			if (v1.Equals(v2)) return 0;
			else
			{
				int size = 0;
				if (v1.Count < v2.Count)
					size = v1.Count;
				
				else size = v2.Count;
				
				for (int i=0; i<size; i++)
				{
					int comp = v1[i].CompareTo(v2[i]);
					if (comp != 0) return comp;
				}
				
				//everything is equal...
				if (v1.Count < v2.Count)
					return -1;
				else if (v1.Count > v2.Count)
					return 1;
				
				return 0;
			}
		}
		
		protected bool IsLessThan(Value val)
		{
			if (val.type != type) return false;
			
			if (type == ValueType.Byte) return (this.GetByte() < val.GetByte());
			else if (type == ValueType.Short) return (this.GetShort() < val.GetShort());
			else if (type == ValueType.Integer) return (this.GetInteger() < val.GetInteger());
			else if (type == ValueType.Long) return (this.GetLong() < val.GetLong());
			else if (type == ValueType.Float) return (this.GetFloat() < val.GetFloat());
			else if (type == ValueType.Double) return (this.GetDouble() < val.GetDouble());
			else if (type == ValueType.String) return (this.GetString().CompareTo(val.GetString()) < 0);
			else if (type == ValueType.ByteSeq) return (Compare<byte>(this.GetByteSeq(), val.GetByteSeq()) < 0);
			else if (type == ValueType.ShortSeq) return (Compare<short>(this.GetShortSeq(), val.GetShortSeq()) < 0);
			else if (type == ValueType.IntegerSeq) return (Compare<int>(this.GetIntegerSeq(), val.GetIntegerSeq()) < 0);
			else if (type == ValueType.LongSeq) return (Compare<long>(this.GetLongSeq(), val.GetLongSeq()) < 0);
			else if (type == ValueType.FloatSeq) return (Compare<float>(this.GetFloatSeq(), val.GetFloatSeq()) < 0);
			else if (type == ValueType.DoubleSeq) return (Compare<double>(this.GetDoubleSeq(), val.GetDoubleSeq()) < 0);
			else if (type == ValueType.StringSeq) return (Compare<string>(this.GetStringSeq(), val.GetStringSeq()) < 0);
			else return false;
		}
		
		protected bool IsGreaterThan(Value val)
		{
			if (type != val.type) return false;
			
			if (type == ValueType.Byte) return (this.GetByte() > val.GetByte());
			else if (type == ValueType.Short) return (this.GetShort() > val.GetShort());
			else if (type == ValueType.Integer) return (this.GetInteger() > val.GetInteger());
			else if (type == ValueType.Long) return (this.GetLong() > val.GetLong());
			else if (type == ValueType.Float) return (this.GetFloat() > val.GetFloat());
			else if (type == ValueType.Double) return (this.GetDouble() > val.GetDouble());
			else if (type == ValueType.String) return (this.GetString().CompareTo(val.GetString()) > 0);
			else if (type == ValueType.ByteSeq) return (Compare<byte>(this.GetByteSeq(), val.GetByteSeq()) > 0);
			else if (type == ValueType.ShortSeq) return (Compare<short>(this.GetShortSeq(), val.GetShortSeq()) > 0);
			else if (type == ValueType.IntegerSeq) return (Compare<int>(this.GetIntegerSeq(), val.GetIntegerSeq()) > 0);
			else if (type == ValueType.LongSeq) return (Compare<long>(this.GetLongSeq(), val.GetLongSeq()) > 0);
			else if (type == ValueType.FloatSeq) return (Compare<float>(this.GetFloatSeq(), val.GetFloatSeq()) > 0);
			else if (type == ValueType.DoubleSeq) return (Compare<double>(this.GetDoubleSeq(), val.GetDoubleSeq()) > 0);
			else if (type == ValueType.StringSeq) return (Compare<string>(this.GetStringSeq(), val.GetStringSeq()) > 0);
			else return false;
		}
		
		public bool Equals(Value val)
		{
			if (val.type != this.type) return false;
			
			if (type == ValueType.Byte) return (this.GetByte() == val.GetByte());
			else if (type == ValueType.Short) return (this.GetShort() == val.GetShort());
			else if (type == ValueType.Integer) return (this.GetInteger() == val.GetInteger());
			else if (type == ValueType.Long) return (this.GetLong() == val.GetLong());
			else if (type == ValueType.Float) return (this.GetFloat() == val.GetFloat());
			else if (type == ValueType.Double) return (this.GetDouble() == val.GetDouble());
			else if (type == ValueType.String) return (this.GetString().Equals(val.GetString()));
			else if (type == ValueType.ByteSeq) return (this.GetByteSeq().Equals(val.GetByteSeq()));
			else if (type == ValueType.ShortSeq) return (this.GetShortSeq().Equals(val.GetShortSeq()));
			else if (type == ValueType.IntegerSeq) return (this.GetIntegerSeq().Equals(val.GetIntegerSeq()));
			else if (type == ValueType.LongSeq) return (this.GetLongSeq().Equals(val.GetLongSeq()));
			else if (type == ValueType.FloatSeq) return (this.GetFloatSeq().Equals(val.GetFloatSeq()));
			else if (type == ValueType.DoubleSeq) return (this.GetDoubleSeq().Equals(val.GetDoubleSeq()));
			else if (type == ValueType.StringSeq) return (this.GetStringSeq().Equals(val.GetStringSeq()));
			else return false;
		}
		
		protected bool IsLessEqualThan(Value val)
		{
			return !this.IsGreaterThan(val);
		}
		
		protected bool IsGreaterEqualThan(Value val)
		{
			return !this.IsLessThan(val);
		}
		
		public void Marshall(ByteStreamWriter writer)
		{
			writer.WriteSize((int)type);
			writer.WriteByteSeq(rawValue);
		}
		
		public void Unmarshall(ByteStreamReader reader)
		{
			type = (ValueType)reader.ReadSize();
			rawValue = reader.ReadByteSeq();
		}
		
		public override string ToString()
		{
			if (type == ValueType.Byte) return ""+this.GetByte();
			else if (type == ValueType.Short) return ""+this.GetShort();
			else if (type == ValueType.Integer) return ""+this.GetInteger();
			else if (type == ValueType.Long) return ""+this.GetLong();
			else if (type == ValueType.Float) return ""+this.GetFloat();
			else if (type == ValueType.Double) return ""+this.GetDouble();
			else if (type == ValueType.String) return this.GetString();
			else if (type == ValueType.ByteSeq) return this.GetByteSeq().ToString();
			else if (type == ValueType.ShortSeq) return this.GetShortSeq().ToString();
			else if (type == ValueType.IntegerSeq) return this.GetIntegerSeq().ToString();
			else if (type == ValueType.LongSeq) return this.GetLongSeq().ToString();
			else if (type == ValueType.FloatSeq) return this.GetFloatSeq().ToString();
			else if (type == ValueType.DoubleSeq) return this.GetDoubleSeq().ToString();
			else if (type == ValueType.StringSeq) return this.GetStringSeq().ToString();
			else return rawValue.ToString();
		}
	}

}
