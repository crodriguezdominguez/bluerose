
using System;
using System.Collections.Generic;

using BlueRose.Messages;

namespace BlueRose.Events
{


	public class PubSubProxy : ObjectProxy 
	{
		public PubSubProxy() : base()
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
			
			this.ResolveInitialization(null, true, null);
		}
		
		public PubSubProxy(ICommunicationDevice device) : base()
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
			this.ResolveInitialization(device, true, null);
		}
		
		public PubSubProxy(ICommunicationDevice device, bool waitOnline) : base()
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
			this.ResolveInitialization(device, waitOnline, null);
		}
		
		public PubSubProxy(ICommunicationDevice device, bool waitOnline, Dictionary<string, List<byte>> pars) : base()
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
			this.ResolveInitialization(device, waitOnline, pars);
		}
	
		public PubSubProxy(string servantID) : base(servantID)
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
		}
		
		public PubSubProxy(string servantID, ICommunicationDevice device) : base(servantID, device)
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
		}
		
		public PubSubProxy(string servantID, ICommunicationDevice device, bool waitOnline) : base(servantID, device, waitOnline)
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
		}
	
		public PubSubProxy(string servantID, ICommunicationDevice device, bool waitOnline, Dictionary<string, List<byte>> pars) : base(servantID, device, waitOnline, pars)
		{
			identity.Name = "PubSub";
			identity.Category = "BlueRoseService";
		}
	
		
		/**
		 * Adds a new subscription to a topic
		 *
		 * @param _topic Topic to subscribe to
		 */
		public void Subscribe(int topic)
		{
			int reqID;
			currentMode = RequestOperationMode.OneWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteInteger(topic);
			
			reqID = this.SendRequest(servantID, "subscribe0", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		/**
		 * Adds a new subscription to a topic, but with a set of constraints
		 *
		 * @param _topic Topic to subscribe to
		 * @param _pred Set of constraints over the properties of the events
		 */
		public void Subscribe(int topic, Predicate predicate)
		{
			int reqID;
			currentMode = RequestOperationMode.OneWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteInteger(topic);
			writer.WriteObject(predicate);
			
			reqID = this.SendRequest(servantID, "subscribe1", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		/**
		 * Removes all the subscriptions
		 */
		public void Unsubscribe()
		{
			int reqID;
			currentMode = RequestOperationMode.OneWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			
			reqID = this.SendRequest(servantID, "unsubscribe0", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		/**
		 * Remove a subscription to a topic
		 *
		 * @param _topic Topic associated with the subscription to remove
		 */
		public void Unsubscribe(int topic)
		{
			int reqID;
			currentMode = RequestOperationMode.OneWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteInteger(topic);
			
			reqID = this.SendRequest(servantID, "unsubscribe1", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		/**
		 * Checks if any of the user subscriptions allow him/her to receive the event
		 * that is specified by the template
		 *
		 * @param userID User whose subscription is going to be checked
		 * @param templateEvent Event template
		 * @return YES if the user may receive the event specified by the template.
		 *         NO otherwise else.
		 */
		public bool IsSubscribed(string userID, Event templateEvent)
		{
			int reqID;
			
			currentMode = RequestOperationMode.TwoWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteString(userID);
			writer.WriteObject(templateEvent);
			
			reqID = this.SendRequest(servantID, "isSubscribed", writer.ToVector());
			List<byte> result_bytes = this.ReceiveReply(reqID);
			
			ByteStreamReader reader = new ByteStreamReader(result_bytes);
			bool result = reader.ReadBoolean();
			
			return result;
		}
	
		/**
		 * Returns all the subscribers
		 *
		 * @return Vector of subscribers
		 */
		public List<string> GetSubscribers()
		{
			int reqID;
			
			currentMode = RequestOperationMode.TwoWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			
			reqID = this.SendRequest(servantID, "getSubscribers0", writer.ToVector());
			List<byte> result_bytes = this.ReceiveReply(reqID);
			
			ByteStreamReader reader = new ByteStreamReader(result_bytes);
			List<string> result = reader.ReadStringSeq();
			
			return result;
		}
	
		/**
		 * Returns all the subscribers that may receive the specified event
		 *
		 * @param templateEvent Event
		 * @return Vector of subscribers
		 */
		public List<string> GetSubscribers(Event templateEvent)
		{
			int reqID;
			
			currentMode = RequestOperationMode.TwoWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteObject(templateEvent);
			
			reqID = this.SendRequest(servantID, "getSubscribers1", writer.ToVector());
			List<byte> result_bytes = this.ReceiveReply(reqID);
			
			ByteStreamReader reader = new ByteStreamReader(result_bytes);
			List<string> result = reader.ReadStringSeq();
			
			return result;
		}
	
		/**
		 * Publishes an event to all its subscribers (including the publisher, if it's subscribed
		 * to the event)
		 *
		 * @param event Event to be published
		 *
		 */
		public void Publish(Event evt)
		{
			this.Publish(evt, true);
		}
	
		/**
		 * Publishes an event to all its subscribers
		 *
		 * @param event Event to be published
		 * @param comeBack True if the event has to be published to the publisher because
		 *                 it is subscribed. False if the event will not be published to
		 *                 the published even if it is subscribed to it.
		 */
		public void Publish(Event evt, bool comeback)
		{
			int reqID;
			currentMode = RequestOperationMode.TwoWay;
			
			ByteStreamWriter writer = new ByteStreamWriter();
			writer.WriteObject(evt);
			writer.WriteBoolean(comeback);
			
			reqID = this.SendRequest(servantID, "publish", writer.ToVector());
			this.ReceiveReply(reqID);
		}
	
		public override string GetTypeID() 
		{
			return "BlueRoseService::PubSub";
		}
	}
	
	
	public class EventHandler {
		
		protected static PubSubProxy proxy;
		protected static Dictionary<int, List<EventListener>> listeners;
	
		public static void Initialize(ICommunicationDevice device)
		{
			listeners = new Dictionary<int, List<EventListener>>();
			proxy = new PubSubProxy(device);
		}
	
		public static void Initialize(ICommunicationDevice device, bool wait)
		{
			listeners = new Dictionary<int, List<EventListener>>();
			proxy = new PubSubProxy(device, wait);
		}
		
		public static void Initialize(ICommunicationDevice device, bool wait, Dictionary<string, List<byte>> pars)
		{
			listeners = new Dictionary<int, List<EventListener>>();
			proxy = new PubSubProxy(device, wait, pars);
		}
	
		public static void Initialize(string servID)
		{
			listeners = new Dictionary<int, List<EventListener>>();
			proxy = new PubSubProxy(servID);
		}
		
		public static void Initialize(string servID, ICommunicationDevice device)
		{
			listeners = new Dictionary<int, List<EventListener>>();
			proxy = new PubSubProxy(servID, device);
		}
		
		public static void Initialize(string servID, ICommunicationDevice device, bool wait)
		{
			listeners = new Dictionary<int, List<EventListener>>();
			proxy = new PubSubProxy(servID, device, wait);
		}
		
		public static void Initialize(string servID, ICommunicationDevice device, bool wait, Dictionary<string, List<Byte>> pars)
		{
			listeners = new Dictionary<int, List<EventListener>>();
			proxy = new PubSubProxy(servID, device, wait, pars);
		}
	
		/**
		 * Destructor
		 */
		public static void Destroy()
		{
			proxy.Unsubscribe();
		}
		
		PubSubProxy Proxy
		{
			get{
				return proxy;
			}
		}
	
		/**
		 * Action to be performed whenever an event is consumed
		 *
		 * @param event Received event
		 */
		public static void OnConsume(Event evt)
		{
			int topic = evt.Topic;
			if (listeners.ContainsKey(topic))
			{
				List<EventListener> array = listeners[topic];
				
				foreach (EventListener listener in array)
				{
					if (listener.AcceptsTopic(topic))
					{
						if (listener.AcceptsEvent(evt))
						{
							listener.PerformAction(evt);
						}
					}
				}
			}
		}
	
		/**
		 * Adds an event listener to the listeners. The event handler
		 * manages the memory of the listener that is passed as an argument.
		 *
		 * @param listener Event listener to add
		 */
		public static void AddEventListener(EventListener listener)
		{
			int topic = listener.Topic;
			
			if (!listeners.ContainsKey(topic))
			{
				List<EventListener> v = new List<EventListener>();
				v.Add(listener);
				
				listeners[topic] = v;
			}
			else
			{
				List<EventListener> array = listeners[topic];
				array.Add(listener);
				listeners[topic] = array;
			}
			
			if (listener.Predicate == null)
				proxy.Subscribe(topic);
			
			else proxy.Subscribe(topic, listener.Predicate);
			
			Event evt = new Event();
			proxy.Publish(evt);
		}
	
		/**
		 * Removes all the event listener that are related with a topic
		 *
		 * @param topic Topic to remove
		 */
		public static void RemoveEventListeners(int topic)
		{
			if (listeners.ContainsKey(topic))
			{
				proxy.Unsubscribe(topic);
				listeners.Remove(topic);
			}
		}
	
		/**
		 * Removes an event listener from the listeners
		 *
		 * @param listener Listener to remove
		 */
		public static void RemoveEventListener(EventListener listener)
		{
			int topic = listener.Topic;
			
			if (listeners.ContainsKey(topic))
			{
				List<EventListener> array = listeners[topic];
				foreach (EventListener list in array)
				{
					if (list.Equals(listener))
					{
						array.Remove(list);
						
						listeners[topic] = array;
						
						if (array.Count == 0)
						{
							RemoveEventListeners(topic);
						}
						
						return;
					}
				}
			}
		}
	
		/**
		 * Publishes an event to all its subscribers (including the publisher, if it's subscribed
		 * to the event)
		 *
		 * @param event Event to be published
		 */
		public static void Publish(Event evt)
		{
			proxy.Publish(evt);
		}
	
		/**
		 * Publishes an event to all its subscribers
		 *
		 * @param event Event to be published
		 * @param comeBack True if the event has to be published to the publisher because
		 *                 it is subscribed. False if the event will not be published to
		 *                 the published even if it is subscribed to it.
		 */
		public static void Publish(Event evt, bool comeback)
		{
			proxy.Publish(evt, comeback);
		}
	}


}
