
using System;

namespace BlueRose.Events
{


	public enum ValueType
	{
		Byte=0, 
		Short=1, 
		Integer=2, 
		Long=3,
		Boolean=4,
		Float=5, 
		Double=6,
		String=7,
		ByteSeq=8, 
		ShortSeq=9,
		IntegerSeq=10, 
		LongSeq=11,
		BooleanSeq=12,
		FloatSeq=13,
		DoubleSeq=14,
		StringSeq=15,
		Other=16, 
		Null=17
	}
}
