
using System;
using System.Collections.Generic;

namespace BlueRose.Events
{


	public abstract class EventListener 
	{
		protected int topic;
		protected Predicate predicate;
		
		/**
		 * Default constructor
		 */
		public EventListener()
		{
			topic = Event.DefaultTopic;
			predicate = new Predicate();
		}
		
		/**
		 * Default constructor
		 *
		 * @param top Topic to listen
		 */
		public EventListener(int top)
		{
			topic = top;
			predicate = new Predicate();
		}
		
		/**
		 * Default constructor
		 *
		 * @param top Topic to listen
		 * @param pred Predicate to accomplish
		 */
		public EventListener(int top, Predicate pred)
		{
			topic = top;
			predicate = pred;
		}
		
		public int Topic
		{
			get{
				return topic;
			}
			set{
				topic = value;
			}
		}
		
		public Predicate Predicate
		{
			get{
				return predicate;
			}
			set{
				predicate = value;
			}
		}
		
		/**
		 * Checks whether this listener accepts a topic or not
		 *
		 * @param top Topic to check
		 * @return YES if the topic is accepted. NO otherwise else
		 */
		public bool AcceptsTopic(int top)
		{
			return (topic == top);
		}
		
		/**
		 * Checks whether this listener accepts an event or not. It is
		 * a more complex check than the check method that only accepts
		 * a topic. In this method, it is assured that the event is
		 * related with the topic established in the constructor. It uses
		 * the predicate to check if it is accepted or not.
		 *
		 * @param event Event to check
		 * @return YES if the event is accepted. NO otherwise else
		 */
		public bool AcceptsEvent(Event evt)
		{
			if (predicate.IsEmpty()) return true;
			
			Dictionary<string, EventNode> dict = evt.Nodes;
			
			foreach(string key in dict.Keys)
			{
				EventNode node = dict[key];
				if (!predicate.IsConsistent(key, node.Value))
				{
					return false;
				}
			}
			
			return true;
		}
		
		/**
		 * Action that is run whenever an appropriate event is received.
		 *
		 * @param evt Event that is received
		 */
		public abstract void PerformAction(Event evt);
		
		/**
		 * Checks if two listeners are equal (i.e., have the same name)
		 *
		 * @return YES if both are equal. NO otherwise else.
		 */
		public bool Equals(EventListener obj)
		{
			return this.GetType().Equals(obj.GetType());
		}
	}
}
